import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from './core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ManifestationDetailsModule } from './components/manifestation-details/manifestation-details.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptor } from './interceptors/intercept.service';
import { RegistrationComponent } from './components/registration/registration.component';
import { RegistrationConfirmationComponent } from './components/registration-confirmation/registration-confirmation.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LocationReportsComponent } from './components/reports/location-reports/location-reports.component';
import { ManifestationReportsComponent } from './components/reports/manifestation-reports/manifestation-reports.component';
import { ManifestationsModule } from "./components/manifestations/manifestations.module";
import { LocationsModule } from './components/locations/locations.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { PaymentConfirmationComponent } from './components/payment-confirmation/payment-confirmation.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    RegistrationConfirmationComponent,
    EditProfileComponent,
    ReservationsComponent,
    LocationReportsComponent,
    ManifestationReportsComponent,
    PageNotFoundComponent,
    PaymentConfirmationComponent
  ],
  imports: [
    BrowserModule,
    ManifestationsModule,
    LocationsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 400000,
      positionClass: 'toast-top-center',
    }),
    AppRoutingModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ManifestationDetailsModule,
    NgbModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
