import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  private headers = new HttpHeaders({ 'Content-Type': 'application/json'});

  constructor(
    private http: HttpClient
  ) { }

  getDailyReportLocation(id:number, startDate:string, endDate:string): Observable<any> {
		let url =  "http://localhost:8080/api/reportDailyLocation/"+id + "/"+startDate+"/"+endDate;;
		return this.http.get(url, {responseType: 'json'});
  }

  getWeeklyReportLocation(id:number, startDate:string, endDate:string): Observable<any> {
		let url =  "http://localhost:8080/api/reportWeeklyLocation/"+id + "/"+startDate+"/"+endDate;
		return this.http.get(url, {responseType: 'json'});
  }

  getMonthlyReportLocation(id:number, startDate:string, endDate:string): Observable<any> {
		let url =  "http://localhost:8080/api/reportMonthlyLocation/"+id + "/"+startDate+"/"+endDate;
		return this.http.get(url, {responseType: 'json'});
  }

  getDailyReportManifestation(id:number, startDate:string, endDate:string): Observable<any> {
		let url =  "http://localhost:8080/api/reportDailyManifestation/"+id + "/"+startDate+"/"+endDate;
		return this.http.get(url, {responseType: 'json'});
  }

  getWeeklyReportManifestation(id:number, startDate:string, endDate:string): Observable<any> {
		let url =  "http://localhost:8080/api/reportWeeklyManifestation/"+id + "/"+startDate+"/"+endDate;
		return this.http.get(url, {responseType: 'json'});
  }

  getMonthlyReportManifestation(id:number, startDate:string, endDate:string): Observable<any> {
		let url =  "http://localhost:8080/api/reportMonthlyManifestation/"+id + "/"+startDate+"/"+endDate;
		return this.http.get(url, {responseType: 'json'});
  }
  
}
