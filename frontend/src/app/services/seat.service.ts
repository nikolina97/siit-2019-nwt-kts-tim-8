import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';;

@Injectable({
  providedIn: 'root'
})
export class SeatService {

  private seatssUrl = 'http://localhost:8080/api/seat/'; 

  private headers = new HttpHeaders({ 'Content-Type': 'application/json',
										'Access-Control-Allow-Origin': '*',
										'Access-Control-Allow-Credentials': 'true'});

  constructor(private http: HttpClient) { }

  getAllSeatsByManifestation(id: number, date: string) {
    const url = `${this.seatssUrl}getAllSeats/${id}`;
		return this.http.post(url, date, {headers: this.headers, responseType : "text"});
  }

  reserveSeats(seatDate: any) {
    const url = `${this.seatssUrl}reserveSeats`;
    return this.http.post(url, seatDate, {headers: this.headers});
  }
}
