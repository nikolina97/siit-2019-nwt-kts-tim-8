import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private baseUrl = 'http://localhost:8080/api/locations/';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': 'true'
  });

  constructor(private http: HttpClient) { }

  getLocations(): Observable<object[]> {
    return this.http.get<object[]>(this.baseUrl + 'all', { headers: this.headers });
  }

  getLocationsPageable(page: number, size: number): Observable<object[]> {
    return this.http.get<object[]>(this.baseUrl + '?page=' + page + '&size=' + size, { headers: this.headers });
  }

  getLocation(id: number): Observable<object> {
    return this.http.get<object>(this.baseUrl + id, { headers: this.headers });
  }

  getCategories(id: number): Observable<object[]> {
    return this.http.get<object[]>(this.baseUrl + id + '/categories', { headers: this.headers });
  }

  getManifestations(id: number, page: number, size: number): Observable<object[]> {
    return this.http.get<object[]>(this.baseUrl + id + '/manifestations?page=' + page + '&size=' + size, { headers: this.headers });
  }

  getManifCount(id: number): Observable<number> {
    return this.http.get<number>(this.baseUrl + id + '/manifCount', { headers: this.headers });
  }

  postLocation(location: object, layout: object): Observable<object> {
    location['categories'] = [];
    layout['categories'].forEach(category => {
      location['categories'].push({
        name: category['name'], enumerable: category['isEnumerable'], rows: category['rows'], columns: category['columns'],
        xCoordinate: category["x"], yCoordinate: category["y"], width: category["width"], height: category["height"], distance: category["r"],
        stage: category["isStage"]
      });
    });

    return this.http.post<object>(this.baseUrl, location, { headers: this.headers });
  }

  putLocation(id: number, model: object): Observable<object> {
    return this.http.put<object>(this.baseUrl + id, model, { headers: this.headers });
  }

  deleteLocation(id: number): Observable<string> {
    return this.http.delete<string>(this.baseUrl + id, { headers: this.headers, responseType: 'text' as 'json' });
  }

}
