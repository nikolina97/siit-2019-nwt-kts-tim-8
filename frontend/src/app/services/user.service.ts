import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private headers = new HttpHeaders({ 'Content-Type': 'application/json'})

  constructor(
    private http: HttpClient
  ) { }

  getLoggedUser(): Observable<any> {
		let getUserUrl =  "http://localhost:8080/api/getLoggedUser";
		return this.http.get(getUserUrl, {responseType: 'json'});
  }

  changePassword(passwords : any): Observable<any> {
		let changePasswordUrl =  "http://localhost:8080/auth/changePassword";
		return this.http.put(changePasswordUrl, passwords, {headers: this.headers, responseType: 'json'});
  }

  editProfile(editInfo : any): Observable<any> {
		let editProfileUrl =  "http://localhost:8080/api/editProfile";
		return this.http.put(editProfileUrl, editInfo, {headers: this.headers, responseType: 'json'});
  }

  uploadImage(uploadData : FormData): Observable<any> {
		let editProfileUrl =  "http://localhost:8080/api/uploadImage";
		return this.http.post(editProfileUrl, uploadData);
  }

  cancel(ticketId : Number): Observable<any> {
    let cancelUrl =  "http://localhost:8080/api/user/cancel/" + ticketId;
		return this.http.post(cancelUrl, {}, {headers: this.headers, responseType: 'json'});
  }
}
