import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';

fdescribe('UserService', () => {

  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(UserService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return error for nonexistant ticket', () => {
    const ret = {
      status: 404,
      statusText: `This ticket does not exists or ticket is not yours`
    };
    service.cancel(100).subscribe(data => {
      expect(data).toEqual(ret);
    })

    const req = httpMock.expectOne(`http://localhost:8080/api/user/cancel/100`, 'call to api');
    expect(req.request.method).toBe('POST');
    req.flush(ret);
    httpMock.verify();
  });

  it('should return error for already paid ticket', () => {
    const ret = {
      status: 400,
      statusText: `You can't cancel an already paid reservation`
    };
    service.cancel(10).subscribe(data => {
      expect(data).toEqual(ret);
    })

    const req = httpMock.expectOne(`http://localhost:8080/api/user/cancel/10`, 'call to api');
    expect(req.request.method).toBe('POST');
    req.flush(ret);
    httpMock.verify();
  });

  it('should return success for reserved ticket', () => {
    const ret = {
      status: 200,
      statusText: `Ticket reservation successfully canceled`
    };
    service.cancel(1).subscribe(data => {
      expect(data).toEqual(ret);
    })

    const req = httpMock.expectOne(`http://localhost:8080/api/user/cancel/1`, 'call to api');
    expect(req.request.method).toBe('POST');
    req.flush(ret);
    httpMock.verify();
  });
});
