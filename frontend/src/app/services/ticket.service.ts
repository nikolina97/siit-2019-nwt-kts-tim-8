import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  private ticketsUrl = 'http://localhost:8080/api/ticket/';  // URL to web api
  private paymentUrl = 'http://localhost:8080/api/payment/';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': 'true'
  });

  constructor(private http: HttpClient) { }


  getReservations(): Observable<any> {
    const url = `${this.ticketsUrl}getAllReservedTicketsByUser`;
    return this.http.get<any>(url, { headers: this.headers });
  }

  buyTickets(seatDate: any) {
    const url = `${this.ticketsUrl}buyTickets`;
    return this.http.post(url, seatDate, { headers: this.headers });
  }

  payTicket(ticketId: number) {
    return this.http.put(this.ticketsUrl + 'payTicket/' + ticketId, null, { headers: this.headers });
  }

  completePayment(paymentId: string, payerId: string) {
    return this.http.post(this.paymentUrl + 'complete', { paymentId: paymentId, payerId: payerId }, { headers: this.headers });
  }

  markAsPaid(ids: string): Observable<any> {
    return this.http.get(this.ticketsUrl + 'markAsPaid/' + ids, { headers: this.headers });
  }

  deleteTickets(ids: string): Observable<any> {
    return this.http.get(this.ticketsUrl + 'deleteTickets/' + ids, { headers: this.headers });
  }

}
