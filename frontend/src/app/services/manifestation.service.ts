import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ManifestationService {

  private baseUrl = 'http://localhost:8080/api/manifestations/';

  private headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': 'true'
  });

  constructor(private http: HttpClient) { }


  getManifestationInfo(id: number): Observable<any> {
    const url = `${this.baseUrl}getManifestationInfo/${id}`;
    return this.http.get<any>(url, { headers: this.headers });
  }

  getJson(fileName: string): Observable<any> {
    return this.http.get<any>(`/assets/layouts/${fileName}`);
  }

  getManifestations(page: number, size: number): Observable<object[]> {
    return this.http.get<object[]>(this.baseUrl + '?page=' + page + '&size=' + size, { headers: this.headers });
  }

  getManifestation(id: number): Observable<object> {
    return this.http.get<object>(this.baseUrl + id, { headers: this.headers });
  }

  getManifestationForUpdate(id: number): Observable<object> {
    return this.http.get<object>(this.baseUrl + 'infoForUpdate/' + id, { headers: this.headers });
  }

  getCount(): Observable<number> {
    return this.http.get<number>(this.baseUrl + 'count', { headers: this.headers });
  }

  postManifestation(manifestation: object): Observable<object> {
    return this.http.post<object>(this.baseUrl, manifestation, { headers: this.headers });
  }

  searchManifestations(params: object, page: number, size: number): Observable<object[]> {
    return this.http.post<object[]>(this.baseUrl + 'search/?page=' + page + '&size=' + size, params, { headers: this.headers });
  }

  putManifestation(id: number, model: object): Observable<object> {
    return this.http.put<object>(this.baseUrl + id, model, { headers: this.headers });
  }

  cancelManifestation(id: number): Observable<string> {
    return this.http.put<string>(this.baseUrl + 'cancel/' + id, null, { headers: this.headers });
  }

}
