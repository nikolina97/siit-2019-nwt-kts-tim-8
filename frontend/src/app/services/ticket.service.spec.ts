import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { TicketService } from './ticket.service';

fdescribe('TicketService', () => {

  let service: TicketService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TicketService],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(TicketService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return list of reservations for user', () => {
    const reservationList = [
      {
        ticketId: 1,
        row: 3,
        column: 4,
        startDate: "2020-03-03T19:00:00.000+0000",
        endDate: "2020-03-03T19:00:00.000+0000",
        ticketPrice: 7,
        manifestationName: "Koncert godine",
        manifestationId: 1,
        seatCategoryName: "East"
      },
      { 
        ticketId: 2,
        row: 3,
        column: 5,
        startDate: "2020-03-03T19:00:00.000+0000",
        endDate: "2020-03-03T19:00:00.000+0000",
        ticketPrice: 7,
        manifestationName: "Koncert godine",
        manifestationId: 1,
        seatCategoryName: "East"
      }
    ];

    service.getReservations().subscribe(data => {
      for (let index = 0; index < data.length; index++) {
        expect(data[index]).toEqual(reservationList[index]);
      }
    });

    const req = httpMock.expectOne(`http://localhost:8080/api/ticket/getAllReservedTicketsByUser`, 'call to api');
    expect(req.request.method).toBe('GET');
    req.flush(reservationList);
    httpMock.verify();
  });

  it('should return error for nonexistant ticket', () => {
    const ret = {
      status: 404,
      statusText: `This ticket does not exists or ticket is not yours`
    };
    service.payTicket(100).subscribe(data => {
      expect(data).toEqual(ret);
    })

    const req = httpMock.expectOne(`http://localhost:8080/api/ticket/payTicket/100`, 'call to api');
    expect(req.request.method).toBe('PUT');
    req.flush(ret);
    httpMock.verify();
  });

  it('should return redirect for proper ticket', () => {
    const ret = {
      status: 200,
      redirectUrl: `https://www.sandbox.paypal.com`
    };
    service.payTicket(3).subscribe(data => {
      expect(data).toEqual(ret);
    })

    const req = httpMock.expectOne(`http://localhost:8080/api/ticket/payTicket/3`, 'call to api');
    expect(req.request.method).toBe('PUT');
    req.flush(ret);
    httpMock.verify();
  });
});
