import { TestBed } from '@angular/core/testing';

import { LocationService } from './location.service';
import { asyncData, asyncError } from 'testing/async-observable-helpers';
import { HttpErrorResponse } from '@angular/common/http';

fdescribe('LocationService', () => {
  let httpClientSpy: { get: jasmine.Spy, put: jasmine.Spy };
  let locationService: LocationService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put']);
    locationService = new LocationService(<any>httpClientSpy);

    TestBed.configureTestingModule({})
  });

  it('#get should return expected location', () => {
    const expected = {
      id: 1,
      name: 'Tvrdjava',
      address: 'Petrovaradin',
      latitude: 45.252917,
      longitude: 19.863365
    };

    httpClientSpy.get.and.returnValue(asyncData(expected));

    locationService.getLocation(expected.id).subscribe(
      location => {
        expect(location).toEqual(expected);
      }
    );
  });

  it('#get should return an error when the server returns a 404', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Location not found',
      status: 404, statusText: 'Not Found'
    });

    const expected = {
      id: 1,
      name: 'Tvrdjava',
      address: 'Petrovaradin',
      latitude: 45.252917,
      longitude: 19.863365
    };

    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    locationService.getLocation(expected.id).subscribe(
      location => fail('expected an error, not location'),
      error => expect(error.error).toContain('Location not found')
    );
  });

  it('#put should return expected location', () => {
    const expected = {
      id: 1,
      name: 'Tvrdjava',
      address: 'Petrovaradin',
      latitude: 45.252917,
      longitude: 19.863365
    };

    httpClientSpy.put.and.returnValue(asyncData(expected));

    locationService.putLocation(expected.id, expected).subscribe(
      location => {
        expect(location).toEqual(expected);
      }
    );
  });

  it('#put should return an error when the server returns a 404', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Location not found',
      status: 404, statusText: 'Not Found'
    });

    const expected = {
      id: 1,
      name: 'Tvrdjava',
      address: 'Petrovaradin',
      latitude: 45.252917,
      longitude: 19.863365
    };

    httpClientSpy.put.and.returnValue(asyncError(errorResponse));

    locationService.putLocation(expected.id, expected).subscribe(
      location => fail('expected an error, not location'),
      error => expect(error.error).toContain('Location not found')
    );
  });
});
