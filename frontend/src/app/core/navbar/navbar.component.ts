import { Component, OnInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { ROUTES } from '../sidebar/sidebar.component';
import { Router } from '@angular/router';
import { AuthenticationService } from 'app/services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  location: Location;
  private listTitles: any[];

  constructor(location: Location, private router: Router, private authenticationService: AuthenticationService) {
    this.location = location;
  }

  ngOnInit() {
    this.listTitles = ROUTES.filter(listTitle => listTitle);

  }

  getTitle() {
    var titlee = this.location.prepareExternalUrl(this.location.path());
    if (titlee.charAt(0) === '#') {
      titlee = titlee.slice(1);
    }

    for (var item = 0; item < this.listTitles.length; item++) {
      if (titlee.startsWith(this.listTitles[item].path)) {
        return this.listTitles[item].title;
      }
    }
    return 'Manifestations';
  }

  logout() {
    this.authenticationService.logout().subscribe(
      result => {
        localStorage.removeItem('user');
        this.router.navigate(['login']);
      },
      error => {
        alert(error.error);
      }
    );
  }

  loggedIn() {
    return this.authenticationService.isLoggedIn();
  }

  login() {
    this.router.navigate(['login']);
  }

  register() {
    this.router.navigate(['registration']);
  }

  editProfile() {
    this.router.navigate(['profile']);
  }
}
