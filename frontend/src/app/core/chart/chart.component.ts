import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Chart } from 'chart.js';
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnChanges {

  barchart : Chart;
  @Input() labels: Array<string>;
  @Input() data: Array<number>;
  constructor() {
   }

  ngOnChanges(changes: SimpleChanges) {
    this.showChart(changes.labels.currentValue, changes.data.currentValue);
  }

  showChart(_labels, _data){
    if(this.barchart){
      this.barchart.destroy();
  }
    this.barchart = new Chart('canvas', {  
      type: 'bar',  
      data: {  
        labels: _labels,
        datasets: [  
          {   
            data: _data,
            borderColor: '#3cba9f',  
            backgroundColor:'rgba(150, 54, 148,0.5)',  
            fill: true  
          }  
        ]  
      },  
      options: {  
        responsive: true,
        maintainAspectRatio: false,
        legend: {  
          display: false  
        },  
        scales: {  
          xAxes: [{  
            display: true  
          }],  
          yAxes: [{  
            display: true ,
            ticks: {
              beginAtZero:true
          } 
          }],  
        }  
      }  
    });
    this.barchart.update();
  }
}
