import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MapComponent } from './map/map.component';
import { ChartComponent } from './chart/chart.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [
    SidebarComponent,
    NavbarComponent,
    MapComponent,
    ChartComponent
  ],
  exports: [
    SidebarComponent,
    NavbarComponent,
    MapComponent,
    ChartComponent
  ]
})
export class CoreModule { }
