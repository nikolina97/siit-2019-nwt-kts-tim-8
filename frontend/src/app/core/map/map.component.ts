import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import 'leaflet/dist/images/marker-icon-2x.png';
import 'leaflet/dist/images/marker-shadow.png';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {

  @Input() private mapid: string;
  @Input() private lat: number;
  @Input() private lon: number;

  constructor() { }

  ngAfterViewInit() {
    let mymap = L.map(this.mapid).setView([this.lat, this.lon], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      accessToken: 'pk.eyJ1IjoiaXNhbXJzMTQiLCJhIjoiY2s1dmcwNzdiMHByaDNrbzB5eXVoNnMwNCJ9.kycCLycHkwV0nZRw4MqcbQ'
    }).addTo(mymap);
    var marker = L.marker([this.lat, this.lon]).addTo(mymap);
  }

}
