import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainManifestationsComponent } from './main-manifestations.component';

describe('MainManifestationsComponent', () => {
  let component: MainManifestationsComponent;
  let fixture: ComponentFixture<MainManifestationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainManifestationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainManifestationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
