import { FormControl } from '@angular/forms';
import { LocationService } from 'app/services/location.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ManifestationService } from 'app/services/manifestation.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MatSidenav } from '@angular/material';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-main-manifestations',
  templateUrl: './main-manifestations.component.html',
  styleUrls: ['./main-manifestations.component.css']
})
export class MainManifestationsComponent implements OnInit {

  manifestations: object[] = [];
  private count: number;
  private pageSizeOptions: number[] = [8, 16, 32, 64];
  private pageSize: number = this.pageSizeOptions[1];
  pageEvent: PageEvent;
  private searchParams: any = {};
  private categories: String[] = ["Culture", "Sport", "Entertainment"];
  private locations: object[] = [];

  @ViewChild('drawer', { static: false })
  drawer: MatSidenav;

  private locationsControl = new FormControl();
  private categoriesControl = new FormControl();

  constructor(private manifestationService: ManifestationService, private locationService: LocationService, private activatedRoute: ActivatedRoute, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.activatedRoute.queryParams
      .subscribe(params => {
        this.searchParams.name = params.search;
        this.getManifestations();
      });
    this.locationService.getLocations().subscribe(locations => this.locations = locations);
  }

  roleAdmin() {
    const token = localStorage.getItem('user');
    const jwt: JwtHelperService = new JwtHelperService();

    if (token) {
      const info = jwt.decodeToken(token);
      //console.log(info.role);
      if (info.role === "ROLE_ADMIN") {
        return true;
      }
    }
    return false;
  }

  roleRegistered() {
    const token = localStorage.getItem('user');
    const jwt: JwtHelperService = new JwtHelperService();
    if (token) {
      const info = jwt.decodeToken(token);
      //console.log(info.role);
      if (info.role === "ROLE_REGISTEREDUSER") {
        return true;
      }
    }
    return false;
  }
  getManifestations(pageEvent?: PageEvent): void {
    if (!pageEvent) {
      if (this.hasParams()) {
        //console.log("if");
        this.manifestationService.searchManifestations(this.searchParams, 0, this.pageSize).subscribe(
          manifestations => {
            this.manifestations = manifestations;
            this.count = this.manifestations.length;
          }
        );
      }
      else {
        //console.log("else");
        this.manifestationService.getManifestations(0, this.pageSize).subscribe(
          manifestations => {
            this.manifestations = manifestations;
            this.count = this.manifestations.length;
          }
        );
      }
    }
    else {
      if (this.hasParams())
        this.manifestationService.searchManifestations(this.searchParams, pageEvent.pageIndex, pageEvent.pageSize).subscribe(manifestations => this.manifestations = manifestations);
      else
        this.manifestationService.getManifestations(pageEvent.pageIndex, pageEvent.pageSize).subscribe(manifestations => this.manifestations = manifestations);
    }
  }

  getCount(): void {
    this.manifestationService.getCount().subscribe(count => this.count = count);
  }

  hasParams(): boolean {
    if (this.searchParams.name != null || this.searchParams.locations != null || this.searchParams.categories != null)
      return true;
    return false;
  }

  onCancel(id: number) {
    this.manifestationService.cancelManifestation(id).subscribe(
      message => {
        this.manifestations.splice(this.manifestations.indexOf(this.manifestations.find(m => m['id'] === id)), 1);
        this.toastr.success("Manifestation canceled");
      },
      error => console.log(error.error)
    );
  }

  onSubmitFilter() {
    //console.log(this.locationsControl.value);
    //console.log(this.categoriesControl.value);
    this.searchParams.locations = this.locationsControl.value;
    this.searchParams.categories = this.categoriesControl.value;
    this.getManifestations();
  }

}
