import { Component, OnInit } from '@angular/core';
import { ManifestationService } from 'app/services/manifestation.service';
import { LocationService } from 'app/services/location.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-manifestation-form',
  templateUrl: './new-manifestation-form.component.html',
  styleUrls: ['./new-manifestation-form.component.css']
})
export class NewManifestationFormComponent implements OnInit {

  private locations: object[] = [];

  private form = this.fb.group({
    location: [null, Validators.required],
    name: ['', Validators.required],
    description: ['', Validators.required],
    startDate: [null, Validators.required],
    endDate: [null, Validators.required],
    daysGroup: this.fb.group({
      reservationAllowed: [true],
      daysBeforeExpires: [null, [Validators.required, Validators.min(1)]]
    }),
    category: ['', Validators.required],
    categories: this.fb.array([null, Validators.required])
  });

  private error: boolean = false;
  private message: string;
  private selectedLocation: object;
  private submitted: boolean = false;
  private layoutCreated: boolean = false;
  private image: string = '/assets/img/no-image.jpg';
  private imageName: string;
  private imageUploaded: boolean = false;

  constructor(private fb: FormBuilder, private manifestationService: ManifestationService, private locationService: LocationService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.locationService.getLocations().subscribe(locations => this.locations = locations);
  }

  get location() { return this.form.get('location'); }
  get categories() { return this.form.get('categories') as FormArray }

  private onLocationChange(): void {
    let l = this.locations.find(l => l['locationId'] === this.location.value);
    if (this.location.valid)
      if (!l['categories'])
        this.locationService.getCategories(this.location.value).subscribe(
          categories => {
            this.locations.find(l => l['locationId'] === this.location.value)['categories'] = categories;
            this.selectedLocation = l;

            this.createCategoriesControls(categories);
          }
        );
      else {
        this.selectedLocation = l;
        this.createCategoriesControls(l['categories']);
      }
    else
      this.layoutCreated = false;
  }

  private createCategoriesControls(categories: object[]) {
    // remove all controls
    while (this.categories.length !== 0) {
      this.categories.removeAt(0)
    }

    for (let i = 0; i < categories.length; i++) {
      if (!categories[i]['stage']) {
        let categoryGroup = this.fb.group([]);

        categoryGroup.addControl('checked', this.fb.control(true));
        categoryGroup.addControl('ticketPrice', new FormControl({ value: null, disabled: false }, [Validators.min(0)]));
        if (!categories[i]['enumerable'])
          categoryGroup.addControl('numberOfSeats', new FormControl({ value: null, disabled: false }, [Validators.min(1)]));

        this.categories.push(categoryGroup);
      }

    }

    this.layoutCreated = true;
  }

  private toggleInputs(cg: FormGroup): void {
    if (cg.controls['checked'].value) {
      cg.controls['ticketPrice'].enable();
      if (cg.controls['numberOfSeats'])
        cg.controls['numberOfSeats'].enable();
    } else {
      cg.controls['ticketPrice'].disable();
      if (cg.controls['numberOfSeats'])
        cg.controls['numberOfSeats'].disable();
    }

    let check: boolean = false;
    for (let i = 0; i < this.categories.controls.length; i++) {
      let group = this.categories.controls[i] as FormGroup;
      if (group.controls['checked'].value && group.controls['checked'].value == true) {
        check = true;
        break;
      }
    }

    if (!check)
      this.form.controls['categories'].setErrors({ 'incorrect': true });
    else
      this.form.controls['categories'].setErrors(null);
  }

  private toggleDaysInput(): void {
    if (this.form.controls['daysGroup'].get('daysBeforeExpires').enabled)
      this.form.controls['daysGroup'].get('daysBeforeExpires').disable();
    else
      this.form.controls['daysGroup'].get('daysBeforeExpires').enable();
  }

  private onFileLoad(input: any) {
    let reader = new FileReader();
    reader.onload = () => {
      this.image = reader.result as string;
    };

    if (input.target.files && input.target.files.length > 0) {
      reader.readAsDataURL(input.target.files[0]);
      this.imageUploaded = true;
      this.imageName = input.target.files[0].name;
    } else {
      this.image = '/assets/img/no-image.jpg';
      this.imageUploaded = false;
    }
  }

  onSubmit(): void {
    let locationId: number = this.form.get('location').value;
    let name: string = this.form.get('name').value;
    let description: string = this.form.get('description').value;
    let startDate: Date = new Date(this.form.get('startDate').value);
    let endDate: Date = new Date(this.form.get('endDate').value);
    let daysNumber: number = this.form.controls['daysGroup'].get('reservationAllowed').value ? this.form.controls['daysGroup'].get('daysBeforeExpires').value : -1;
    let category: string = this.form.get('category').value;

    let sscDTOs: object[] = [];

    for (let i = 0; i < this.categories.controls.length; i++) {
      let group = this.categories.controls[i] as FormGroup;
      if (group.controls['checked'].value) {
        let id: number = this.selectedLocation['categories'][i].id;
        let numberOfSeats: number = group.controls['numberOfSeats'] ? group.controls['numberOfSeats'].value : 0;
        let ticketPrice: number = group.controls['ticketPrice'].value;
        let ssc: object = { seatCategoryId: id, numberOfSeats: numberOfSeats, ticketPrice: ticketPrice };
        sscDTOs.push(ssc);
      }
    }

    let manifestation = {
      daysBeforeExpires: daysNumber,
      locationId: locationId,
      name: name,
      description: description,
      image: this.image,
      imageName: this.imageName,
      startDate: startDate,
      endDate: endDate,
      state: 'OPEN',
      category: category,
      sscDTOs: sscDTOs
    };

    this.error = false;
    this.submitted = true;

    this.manifestationService.postManifestation(manifestation).subscribe(
      manifestation => {
        this.toastr.success('You have successfully added a new manifestation');
        this.router.navigate(['/manifestations']);
      },
      error => {
        this.message = error.error;
        this.error = true;
        this.submitted = false;
      }
    );
  }

}
