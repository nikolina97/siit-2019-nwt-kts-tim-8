import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewManifestationFormComponent } from './new-manifestation-form.component';

describe('NewManifestationFormComponent', () => {
  let component: NewManifestationFormComponent;
  let fixture: ComponentFixture<NewManifestationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewManifestationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewManifestationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
