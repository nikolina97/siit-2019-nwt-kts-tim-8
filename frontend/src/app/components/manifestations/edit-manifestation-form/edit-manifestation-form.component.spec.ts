import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManifestationFormComponent } from './edit-manifestation-form.component';

describe('EditManifestationFormComponent', () => {
  let component: EditManifestationFormComponent;
  let fixture: ComponentFixture<EditManifestationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditManifestationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManifestationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
