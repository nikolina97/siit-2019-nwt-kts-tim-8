import { Component, OnInit } from '@angular/core';
import { ManifestationService } from 'app/services/manifestation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-manifestation-form',
  templateUrl: './edit-manifestation-form.component.html',
  styleUrls: ['./edit-manifestation-form.component.css']
})
export class EditManifestationFormComponent implements OnInit {

  private form = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required]
  });

  id: number;
  private submitted: boolean = false;

  constructor(private fb: FormBuilder, private manifestationService: ManifestationService, private toastr: ToastrService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getManifestation();
  }

  getManifestation(): void {
    this.route.paramMap.subscribe(
      pmap => {
        this.id = +pmap.get('id');
        this.manifestationService.getManifestationForUpdate(this.id).subscribe(
          manifestation => {
            this.form.controls['name'].setValue(manifestation['name'])
            this.form.controls['description'].setValue(manifestation['description'])
          },
          error => {
            if (error.status == 404)
              this.router.navigate(['/page-not-found']);
          }
        );
      }
    );
  }

  onSubmit(): void {
    let name: string = this.form.get('name').value;
    let description: string = this.form.get('description').value;

    this.submitted = true;

    this.manifestationService.putManifestation(this.id, { name, description }).subscribe(
      () => {
        this.toastr.success('You have successfully modified the manifestation');
        this.router.navigate(['/manifestations']);
      },
      error => {
      }
    );
  }

}
