import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: string, ...args: any[]): any {
    if (value.length <= args[0]) return value;

    let result: string = value.substring(0, value.lastIndexOf(' ', args[0]));
    if (args[0] < value.length)
      result += '...';

    return result;
  }

}
