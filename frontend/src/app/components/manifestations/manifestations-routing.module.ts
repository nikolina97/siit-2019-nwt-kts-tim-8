import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainManifestationsComponent } from './main-manifestations/main-manifestations.component';
import { NewManifestationFormComponent } from './new-manifestation-form/new-manifestation-form.component';
import { EditManifestationFormComponent } from './edit-manifestation-form/edit-manifestation-form.component';

import { MatPaginatorModule } from '@angular/material/paginator';
import { RoleGuard } from 'app/guards/role.service';

const routes: Routes = [
  { path: 'manifestations', component: MainManifestationsComponent },
  { path: 'manifestations/new', 
    component: NewManifestationFormComponent,
    data: { expectedRoles: 'ROLE_ADMIN' },
    canActivate: [RoleGuard] },
  { path: 'manifestations/edit/:id', 
    component: EditManifestationFormComponent,
    data: { expectedRoles: 'ROLE_ADMIN' },
    canActivate: [RoleGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes), MatPaginatorModule],
  exports: [RouterModule]
})
export class ManifestationsRoutingModule { }
