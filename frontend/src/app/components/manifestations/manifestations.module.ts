import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { ManifestationsRoutingModule } from './manifestations-routing.module';
import { MainManifestationsComponent } from './main-manifestations/main-manifestations.component';
import { NewManifestationFormComponent } from './new-manifestation-form/new-manifestation-form.component';
import { EditManifestationFormComponent } from './edit-manifestation-form/edit-manifestation-form.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule, MatInputModule, MatNativeDateModule } from '@angular/material';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ReactiveFormsModule } from "@angular/forms";
import { ShortenPipe } from './pipes/shorten.pipe';
import { MatToolbarModule, MatSidenavModule, MatIconModule } from '@angular/material';


@NgModule({
  declarations: [MainManifestationsComponent, NewManifestationFormComponent, EditManifestationFormComponent, ShortenPipe],
  imports: [
    CommonModule,
    FormsModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    ManifestationsRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule
  ]
})
export class ManifestationsModule { }
