import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationService } from 'app/services/location.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-location-form',
  templateUrl: './edit-location-form.component.html',
  styleUrls: ['./edit-location-form.component.css']
})
export class EditLocationFormComponent implements OnInit {

  form = this.fb.group({
    name: ['', Validators.required],
    address: ['', Validators.required],
    latitude: [null, [Validators.required, Validators.min(-90), Validators.max(90)]],
    longitude: [null, [Validators.required, Validators.min(-180), Validators.max(180)]]
  });

  private error: boolean = false;
  private message: string;
  private id: number;
  private submitted: boolean = false;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, private locationService: LocationService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getLocation();
  }

  getLocation(): void {
    this.route.paramMap.subscribe(
      pmap => {
        this.id = +pmap.get('id');
        this.locationService.getLocation(this.id).subscribe(
          location => {
            this.form.controls['name'].setValue(location['name']);
            this.form.controls['address'].setValue(location['address']);
            this.form.controls['latitude'].setValue(location['latitude']);
            this.form.controls['longitude'].setValue(location['longitude']);
          },
          error => {
            if (error.status == 404)
              this.router.navigate(['/page-not-found']);
          }
        )
      }
    );
  }

  onSubmit(): void {
    let name: string = this.form.get('name').value;
    let address: string = this.form.get('address').value;
    let latitude: number = this.form.get('latitude').value;
    let longitude: number = this.form.get('longitude').value;

    this.error = false;
    this.submitted = true;

    let location: object = { name, address, latitude, longitude };
    this.locationService.putLocation(this.id, location).subscribe(
      location => {
        this.toastr.success('You have successfully modified the location');
        this.router.navigate(['/locations']);
      },
      error => {
        this.message = error.error;
        this.error = true;
        this.submitted = false;
      }
    );
  }

}
