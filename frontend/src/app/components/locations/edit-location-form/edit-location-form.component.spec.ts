import { async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { By } from "@angular/platform-browser";
import { EditLocationFormComponent } from './edit-location-form.component';
import { LocationService } from 'app/services/location.service';
import { Router, ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ActivatedRouteStub } from 'testing/activated-route-stub';
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule, MatInputModule, MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

fdescribe('EditLocationFormComponent', () => {
  let component: EditLocationFormComponent;
  let fixture: ComponentFixture<EditLocationFormComponent>;
  let locationService: LocationService;
  let router: Router;
  let toastr: ToastrService;

  beforeEach(async(() => {
    let locationServiceMocked = {
      getLocation: jasmine.createSpy('getLocation').and.returnValue(of({ name: 'Tvrdjava', address: 'Petrovaradin', latitude: 45.252917, longitude: 19.863365 })), //asyncData umesto of? Ako da, testovi sa pozivom ove funkcije moraju biti fakeAsync
      putLocation: jasmine.createSpy('putLocation').and.returnValue(of({ name: 'Djava', address: 'Petrovaradin', latitude: 45.252917, longitude: 19.863365 }))
    };

    let routerMocked = jasmine.createSpyObj('router', ['navigate']);

    let toastrMocked = jasmine.createSpyObj('toastr', ['success']);

    let activatedRouteStub = new ActivatedRouteStub();
    activatedRouteStub.setParamMap({ id: 1 });

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, CommonModule, MatFormFieldModule, MatInputModule, MatProgressSpinnerModule, BrowserAnimationsModule],
      declarations: [EditLocationFormComponent],
      providers: [
        { provide: LocationService, useValue: locationServiceMocked },
        { provide: Router, useValue: routerMocked },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: ToastrService, useValue: toastrMocked }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLocationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    locationService = TestBed.get(LocationService);
    router = TestBed.get(Router);
    toastr = TestBed.get(ToastrService);
  });

  it('should get location and fill inputs with data', () => {
    component.ngOnInit();

    expect(locationService.getLocation).toHaveBeenCalledWith(1); // id iz activatedRouteStub

    expect(component.form.get('name').value).toEqual('Tvrdjava');
    expect(component.form.get('address').value).toEqual('Petrovaradin');
    expect(component.form.get('latitude').value).toEqual(45.252917);
    expect(component.form.get('longitude').value).toEqual(19.863365);
  });

  it('should bind data from fields to location as form object', () => {
    component.ngOnInit();

    expect(component.form.get('name').value).not.toEqual('Tvrdjavaa');
    expect(component.form.get('address').value).not.toEqual('Petrovaradinn');
    expect(component.form.get('latitude').value).not.toEqual(45.2529177);
    expect(component.form.get('longitude').value).not.toEqual(19.8633655);

    let nameInput = fixture.debugElement.query(By.css('input[formControlName="name"]')).nativeElement;
    let addressInput = fixture.debugElement.query(By.css('input[formControlName="address"]')).nativeElement;
    let latitudeInput = fixture.debugElement.query(By.css('input[formControlName="latitude"]')).nativeElement;
    let longitudeInput = fixture.debugElement.query(By.css('input[formControlName="longitude"]')).nativeElement;

    nameInput.value = 'Tvrdjavaa';
    nameInput.dispatchEvent(newEvent('input'));

    addressInput.value = 'Petrovaradinn';
    addressInput.dispatchEvent(newEvent('input'));

    latitudeInput.value = 45.2529177;
    latitudeInput.dispatchEvent(newEvent('input'));

    longitudeInput.value = 19.8633655;
    longitudeInput.dispatchEvent(newEvent('input'));

    expect(component.form.get('name').value).toEqual('Tvrdjavaa');
    expect(component.form.get('address').value).toEqual('Petrovaradinn');
    expect(component.form.get('latitude').value).toEqual(45.2529177);
    expect(component.form.get('longitude').value).toEqual(19.8633655);
  });

  it('should edit location', () => {
    component.ngOnInit();

    component.onSubmit();

    expect(router.navigate).toHaveBeenCalledWith(['/locations']);
    expect(toastr.success).toHaveBeenCalled();
  });

  it('form should have an invalid state with invalid data', () => {
    let nameInput = fixture.debugElement.query(By.css('input[formControlName="name"]')).nativeElement;
    let addressInput = fixture.debugElement.query(By.css('input[formControlName="address"]')).nativeElement;
    let latitudeInput = fixture.debugElement.query(By.css('input[formControlName="latitude"]')).nativeElement;
    let longitudeInput = fixture.debugElement.query(By.css('input[formControlName="longitude"]')).nativeElement;

    nameInput.value = '';
    nameInput.dispatchEvent(newEvent('input'));

    addressInput.value = 'Petrovaradin';
    addressInput.dispatchEvent(newEvent('input'));

    latitudeInput.value = 45.252917;
    latitudeInput.dispatchEvent(newEvent('input'));

    longitudeInput.value = 19.863365;
    longitudeInput.dispatchEvent(newEvent('input'));

    expect(component.form.valid).toBeFalsy();

    nameInput.value = 'abc';
    nameInput.dispatchEvent(newEvent('input'));

    addressInput.value = '';
    addressInput.dispatchEvent(newEvent('input'));

    expect(component.form.valid).toBeFalsy();

    addressInput.value = 'abc';
    addressInput.dispatchEvent(newEvent('input'));

    latitudeInput.value = null;
    latitudeInput.dispatchEvent(newEvent('input'));

    expect(component.form.valid).toBeFalsy();

    latitudeInput.value = 91.0;
    latitudeInput.dispatchEvent(newEvent('input'));

    expect(component.form.valid).toBeFalsy();

    latitudeInput.value = 45.252917;
    latitudeInput.dispatchEvent(newEvent('input'));

    longitudeInput.value = null;
    longitudeInput.dispatchEvent(newEvent('input'));

    expect(component.form.valid).toBeFalsy();

    longitudeInput.value = 181;
    longitudeInput.dispatchEvent(newEvent('input'));

    expect(component.form.valid).toBeFalsy();

    longitudeInput.value = 19.863365;
    longitudeInput.dispatchEvent(newEvent('input'));

    expect(component.form.valid).toBeTruthy();
  });

  function newEvent(eventName: string, bubbles = false, cancelable = false) {
    let evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(eventName, bubbles, cancelable, null);
    return evt;
  }
});
