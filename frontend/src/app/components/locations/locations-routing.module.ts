import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLocationsComponent } from './main-locations/main-locations.component';
import { NewLocationFormComponent } from './new-location-form/new-location-form.component';
import { EditLocationFormComponent } from './edit-location-form/edit-location-form.component';
import { LocationInfoComponent } from './location-info/location-info.component';
import { RoleGuard } from 'app/guards/role.service';


const routes: Routes = [
  { path: 'locations', component: MainLocationsComponent },
  { path: 'location/:id', component: LocationInfoComponent },
  {
    path: 'locations/new',
    component: NewLocationFormComponent,
    data: { expectedRoles: 'ROLE_ADMIN' },
    canActivate: [RoleGuard]
  },
  {
    path: 'locations/edit/:id',
    component: EditLocationFormComponent,
    data: { expectedRoles: 'ROLE_ADMIN' },
    canActivate: [RoleGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationsRoutingModule { }
