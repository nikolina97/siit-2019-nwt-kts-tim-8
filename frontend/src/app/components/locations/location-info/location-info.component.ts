import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { LocationService } from 'app/services/location.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ManifestationService } from 'app/services/manifestation.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-location-info',
  templateUrl: './location-info.component.html',
  styleUrls: ['./location-info.component.css']
})
export class LocationInfoComponent implements OnInit {

  private id: number;
  private location: object;
  private manifestations: object[] = [];
  private count: number;
  private pageSizeOptions: number[] = [8, 16, 32, 64];
  private pageSize: number = this.pageSizeOptions[1];
  private pageEvent: PageEvent;

  constructor(private locationService: LocationService, private manifestationService: ManifestationService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      pmap => {
        this.id = +pmap.get('id');

        this.getLocation();
        this.getManifestations();
        this.getCount();
      });
  }

  private roleAdmin() {
    const token = localStorage.getItem('user');
    const jwt: JwtHelperService = new JwtHelperService();

    if (token) {
      const info = jwt.decodeToken(token);
      console.log(info.role);
      if (info.role === "ROLE_ADMIN") {
        return true;
      }
    }
    return false;
  }

  private getLocation(): void {
    this.locationService.getLocation(this.id).subscribe(
      location => {
        this.location = location;
      },
      error => {
        if (error.status == 404)
          this.router.navigate(['/page-not-found']);
      }
    );
  }

  private getManifestations(pageEvent?: PageEvent): void {
    if (!pageEvent)
      this.locationService.getManifestations(this.id, 0, this.pageSize).subscribe(
        manifestations => {
          this.manifestations = manifestations;
        }
      );
    else {
      this.locationService.getManifestations(this.id, pageEvent.pageIndex, pageEvent.pageSize).subscribe(manifestations => this.manifestations = manifestations);
    }
  }

  private getCount(): void {
    this.locationService.getManifCount(this.id).subscribe(count => this.count = count);
  }

  private onCancel(id: number) {
    this.manifestationService.cancelManifestation(id).subscribe(
      message => this.manifestations.splice(this.manifestations.indexOf(this.manifestations.find(m => m['id'] === id)), 1),
      error => console.log(error.error)
    );
  }

}
