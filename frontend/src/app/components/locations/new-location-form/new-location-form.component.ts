import { Component, OnInit } from '@angular/core';
import { LocationService } from 'app/services/location.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-new-location-form',
  templateUrl: './new-location-form.component.html',
  styleUrls: ['./new-location-form.component.css']
})
export class NewLocationFormComponent implements OnInit {

  private form = this.fb.group({
    name: ['', Validators.required],
    address: ['', Validators.required],
    latitude: [null, [Validators.required, Validators.min(-90), Validators.max(90)]],
    longitude: [null, [Validators.required, Validators.min(-180), Validators.max(180)]]
  });

  private error: boolean = false;
  private message: string;
  private layout: object = null;
  private layoutName: string = null;
  private submitted: boolean = false;

  constructor(private fb: FormBuilder, private locationService: LocationService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
  }

  onFileLoad(input: any) {
    let reader = new FileReader();
    reader.onload = () => {
      this.layout = JSON.parse(reader.result as string);
    };

    if (input.target.files && input.target.files.length > 0) {
      reader.readAsText(input.target.files[0]);
      this.layoutName = (input.target.files[0].name as string).substring(0, (input.target.files[0].name as string).indexOf('.'));
    } else {
      this.layout = null;
      this.layoutName = null;
    }
  }

  onSubmit(): void {
    let name: string = this.form.get('name').value;
    let address: string = this.form.get('address').value;
    let latitude: number = this.form.get('latitude').value;
    let longitude: number = this.form.get('longitude').value;

    this.error = false;
    this.submitted = true;

    let location: object = { name, address, latitude, longitude };
    this.locationService.postLocation(location, this.layout).subscribe(
      location => {
        this.toastr.success('You have successfully added a new location');
        this.router.navigate(['/locations']);
      },
      error => {
        this.message = error.error;
        this.error = true;
        this.submitted = false;
      }
    );
  }

}
