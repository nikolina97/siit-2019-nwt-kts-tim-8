import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainLocationsComponent } from './main-locations.component';

describe('MainLocationsComponent', () => {
  let component: MainLocationsComponent;
  let fixture: ComponentFixture<MainLocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainLocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainLocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
