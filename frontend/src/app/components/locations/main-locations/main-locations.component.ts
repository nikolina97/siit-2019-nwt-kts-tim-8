import { Component, OnInit } from '@angular/core';
import { LocationService } from 'app/services/location.service';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-main-locations',
  templateUrl: './main-locations.component.html',
  styleUrls: ['./main-locations.component.css']
})
export class MainLocationsComponent implements OnInit {

  locations: object[] = [];
  private count: number;
  private pageSizeOptions: number[] = [8, 16, 32, 64];
  private pageSize: number = this.pageSizeOptions[1];
  pageEvent: PageEvent;

  constructor(private locationService: LocationService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    //this.getCount();
    this.getLocations();
  }

  roleAdmin() {
    const token = localStorage.getItem('user');
    const jwt: JwtHelperService = new JwtHelperService();

    if (token) {
      const info = jwt.decodeToken(token);
      console.log(info.role);
      if (info.role === "ROLE_ADMIN") {
        return true;
      }
    }
    return false;
  }

  getLocations(pageEvent?: PageEvent): void {
    if (!pageEvent)
      this.locationService.getLocationsPageable(0, this.pageSize).subscribe(
        locations => {
          this.locations = locations;
          this.count = this.locations.length;
        }
      );
    else
      this.locationService.getLocationsPageable(pageEvent.pageIndex, pageEvent.pageSize).subscribe(locations => this.locations = locations);
  }

  // getCount(): void {
  //   this.locationService.getCount().subscribe(count => this.count = count, error => console.log(error.error));
  // }

  onDelete(id: number) {
    this.locationService.deleteLocation(id).subscribe(
      message => {
        this.toastr.success("Location deleted");
      },
      error => {
        console.log(error.error);
      }, () => {
        this.removeLocation(this.locations.find(location => location['locationId'] === id));
      });
  }

  private removeLocation(location: object) {
    this.locations.splice(this.locations.indexOf(location), 1);
  }

}
