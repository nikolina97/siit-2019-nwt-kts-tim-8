import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ReactiveFormsModule } from "@angular/forms";

import { LocationsRoutingModule } from './locations-routing.module';
import { MainLocationsComponent } from './main-locations/main-locations.component';
import { NewLocationFormComponent } from './new-location-form/new-location-form.component';
import { EditLocationFormComponent } from './edit-location-form/edit-location-form.component';
import { LocationInfoComponent } from './location-info/location-info.component';
import { CoreModule } from 'app/core/core.module';


@NgModule({
  declarations: [MainLocationsComponent, NewLocationFormComponent, EditLocationFormComponent, LocationInfoComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    CoreModule,
    LocationsRoutingModule
  ]
})
export class LocationsModule { }
