import { Component, Inject, Optional } from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-reservation',
    templateUrl: './dialog-reservation.component.html',
    styleUrls: ['./dialog-reservation.component.css']
  })
  export class DialogReservationComponent {

    color: string = 'default';

    constructor(public dialogRef: MatDialogRef<DialogReservationComponent>,
      @Optional() @Inject(MAT_DIALOG_DATA) public data: number) { }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
    
    incrementValue(step: number = 1):void {
        let inputValue = this.data + step;
        this.data = inputValue;
    }
    shouldDisableDecrement(inputValue: number): boolean {
        return inputValue <= 0;
    }
    shouldDisableIncrement(inputValue: number): boolean {
        return inputValue >= 7;
    }
    setColor(color: string): void {
        this.color = color;
    }

    getColor(): string {
        return this.color
    }
  }