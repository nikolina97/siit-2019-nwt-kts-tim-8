import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { ManifestationDetailsComponent } from './manifestation-details.component';
import { ManifestationInfoComponent } from './manifestation-info/manifestation-info.component';
import { TicketSalesComponent } from './ticket-sales/ticket-sales.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; 
import { DialogReservationComponent } from './dialog-reservation/dialog-reservation.component';
import { DatePipe } from '@angular/common';


@NgModule({
  declarations: [
    ManifestationDetailsComponent,
    ManifestationInfoComponent,
    TicketSalesComponent,
    DialogReservationComponent
    
  ],
  imports: [
    CommonModule,
    NgbModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatIconModule
  ],
  exports: [
    ManifestationDetailsComponent
  ],
  entryComponents: [DialogReservationComponent],
  providers: [DatePipe]
})
export class ManifestationDetailsModule { }
