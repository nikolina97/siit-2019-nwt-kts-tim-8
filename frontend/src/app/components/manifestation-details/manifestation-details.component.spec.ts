import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestationDetailsComponent } from './manifestation-details.component';

describe('ManifestationDetailsComponent', () => {
  let component: ManifestationDetailsComponent;
  let fixture: ComponentFixture<ManifestationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
