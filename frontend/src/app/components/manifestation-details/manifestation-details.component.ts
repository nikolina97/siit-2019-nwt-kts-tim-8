import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location, DatePipe } from '@angular/common';
import { ManifestationService } from '../../services/manifestation.service';
import { Router } from '@angular/router';
import { SeatService } from 'app/services/seat.service';
import { ToastrService } from 'ngx-toastr';

export interface ITab {
  id: number;
  title: string;
}

@Component({
  selector: 'app-manifestation-details',
  templateUrl: './manifestation-details.component.html',
  styleUrls: ['./manifestation-details.component.css']
})
export class ManifestationDetailsComponent implements OnInit {
  
  manifestation: any;

  datesToShow: Date[] = [];

  activeTab = 1;

  permission = true;

  dates: Date[] = [];

  categories: any[] = [];

  public tabs: ITab[] = [
    { id: 1, title: "Info" },
    { id: 2, title: "Tickets" }
  ];
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private manifestationService: ManifestationService,
    private seatService: SeatService,
    private router: Router,
    private datePipe: DatePipe,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.dates = [];
    this.getManifestation();
  }

  getManifestation(): void {
    const manifId = +this.route.snapshot.paramMap.get('id');
    this.manifestationService.getManifestationInfo(manifId)
    .subscribe(
      
        (manifestation) => {
          this.manifestation =  manifestation;
          if (manifestation) {
            if (manifestation.dates.length == 1) {
              this.datesToShow.push(new Date(manifestation.dates[0]));
            }
            else{
              this.datesToShow.push(new Date(manifestation.dates[0]));
              this.datesToShow.push(new Date(manifestation.dates[manifestation.dates.length-1]));
            }
            for (let i = 0; i<manifestation.dates.length; i++) {
              this.dates.push(new Date(manifestation.dates[i]));
            }
          }
        this.getTickets(manifId, this.datePipe.transform(new Date(manifestation.dates[0]), "yyyy-MM-dd HH:mm:ss"));
        },
        (error) => {
          this.toastr.error(error.error);
          this.router.navigate(['manifestations']);
        }
      
    );
  }

  getTickets(manifId: number, dateS: string){
    this.seatService.getAllSeatsByManifestation(manifId, dateS)
      .subscribe(
        (data)=> {
          this.categories = [JSON.parse(data)][0];
          for (let cat of this.categories) {
            if (!cat.stage) {
              cat.availableSeats = cat.seats.filter(item => item.reserved === false);
            }
            for (let seat of cat.seats) {
              var x = 0;
              var y = 0;
              if (seat.row == 1){
                y = cat.yCoordinate + 2.5*cat.distance;
              }
              else{
                y = (cat.yCoordinate + 2.5*cat.distance) + 3*cat.distance*(seat.row-1);
              }
              if (seat.column == 1) {
                x = cat.xCoordinate + 2.5*cat.distance;
              }
              else{
                x = (cat.xCoordinate + 2.5*cat.distance) + 3*cat.distance*(seat.column-1);
              }
              seat.cx = x;
              seat.cy = y;
              seat.categoryName = cat.name;
              if (dateS == "Festival ticket") {
                seat.ticketPrice = cat.ticketPrice*this.dates.length;  
              }
              else{
                seat.ticketPrice = cat.ticketPrice;
              }
            }
          }
        },
        (error) => {
          this.toastr.error(error.error);
        }
      )  
  }


  checkTickets(){
    if (this.activeTab == 1) {
      if (this.categories == []) {
        this.toastr.error("Layout is not available");
        this.permission = false;
      }
      else {
        this.permission = true;
      }
    }
  }

  chooseDate(dateS: string) {
    this.categories = [];
    this.getTickets(this.manifestation.manifestationId, dateS);
  }

}
