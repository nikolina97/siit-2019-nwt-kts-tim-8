import {Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-manifestation-info',
  templateUrl: './manifestation-info.component.html',
  styleUrls: ['./manifestation-info.component.css']
})
export class ManifestationInfoComponent implements OnInit {

  @Input()
  manifestation: any;

  @Input()
  datesToShow: Date[];

  constructor() {}

  ngOnInit() {
  }
  
}
