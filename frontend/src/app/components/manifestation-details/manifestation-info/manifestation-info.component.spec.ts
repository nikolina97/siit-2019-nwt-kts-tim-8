import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestationInfoComponent } from './manifestation-info.component';

describe('ManifestationInfoComponent', () => {
  let component: ManifestationInfoComponent;
  let fixture: ComponentFixture<ManifestationInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestationInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestationInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
