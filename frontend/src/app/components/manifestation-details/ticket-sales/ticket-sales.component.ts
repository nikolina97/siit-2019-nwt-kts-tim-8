import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
import { DialogReservationComponent } from '../dialog-reservation/dialog-reservation.component';
import { Router } from '@angular/router';
import { SeatService } from 'app/services/seat.service';
import { TicketService } from 'app/services/ticket.service';
import { ToastrService } from 'ngx-toastr';
import { JwtHelperService } from '@auth0/angular-jwt';


@Component({
  selector: 'app-ticket-sales',
  templateUrl: './ticket-sales.component.html',
  styleUrls: ['./ticket-sales.component.css']
})
export class TicketSalesComponent implements OnInit {

  @Input()
  categories;

  @Input()
  dates: Date[] = [];

  @Output()
  optionSelected = new EventEmitter<string>();

  selectedDate: string = '';

  seatsForReservation: any[] = [];

  numberOfParterreSeats: number = 0;

  totalPrice: number = 0.00;

  tableToShow = false;

  constructor(private dialog: MatDialog, private datePipe: DatePipe,
    private seatService: SeatService, private ticketService: TicketService, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
  }

  clickEvent(obj: any) {
    let circle = document.getElementById(String(obj.seatId));

    if (obj.reserved == false) {
      if (circle.classList.contains(String(obj.reserved))) {

        if (this.seatsForReservation.length > 5) {
          this.toastr.warning("You can not reserve more then 6 seats at once.")
        }
        else {
          circle.classList.remove(String(obj.reserved));
          circle.classList.add("success");
          this.seatsForReservation.push(obj);
          this.tableToShow = true;
        }

      }
      else {
        circle.classList.remove("success");
        circle.classList.add(String(obj.reserved));
        this.seatsForReservation = this.seatsForReservation.filter(item => item.seatId !== obj.seatId);
        if (this.seatsForReservation.length == 0) {
          this.tableToShow = false;
        }
      }

    }
    var numbers = this.seatsForReservation.map(i => i.ticketPrice);
    this.totalPrice = numbers.reduce((a, b) => a + b, 0);
  }

  clickCategory(category: any) {

    if (category.rows == -1) {
      const dialogRef = this.dialog.open(DialogReservationComponent, {
        width: '250px',
        data: this.numberOfParterreSeats
      });

      dialogRef.afterClosed().subscribe(result => {
        this.numberOfParterreSeats = result;
        var parterreSeats = category.seats.filter(item => item.row == -1 && item.reserved == false);
        if (result > parterreSeats.length) {
          this.toastr.error("Not enough free tickets for " + category.name);
        }
        else {
          this.tableToShow = true;
          this.seatsForReservation = this.seatsForReservation.filter(item => item.row != -1);
          for (let i = 0; i < result; i++) {
            this.seatsForReservation.push(parterreSeats[i]);
          }
          var numbers = this.seatsForReservation.map(i => i.ticketPrice);
          this.totalPrice = numbers.reduce((a, b) => a + b, 0);
        }

      });
      if (this.seatsForReservation.length == 0) {
        this.tableToShow = false;
      }
    }
    else {
      return;
    }


  }

  clickDate(date: Date) {
    this.seatsForReservation = [];
    this.setDefaultClasses();
    this.selectedDate = this.datePipe.transform(date, "yyyy-MM-dd HH:mm:ss");
    this.optionSelected.emit(this.selectedDate);
    this.tableToShow = false;
  }

  clickFestivalTicket() {
    this.seatsForReservation = [];
    this.setDefaultClasses();
    this.selectedDate = "Festival ticket";
    this.optionSelected.emit(this.selectedDate); 3
    this.tableToShow = false;
  }

  setDefaultClasses() {
    var list = document.getElementsByTagName("circle");
    for (let i = 0; i < list.length; i++) {
      for (let seat of this.seatsForReservation) {
        if (list[i].id == seat.saetId) {
          list[i].classList[0] = String(seat.reserved);
        }
      }
    }
  }

  reserveSeats() {
    if (this.seatsForReservation.length == 0) {
      this.toastr.warning("You have not selected any seat");
      return;
    }
    const item = localStorage.getItem('user');
    if (!item) {
      this.toastr.warning("You are not logged in");
      this.router.navigate(['/login']);
      return;
    }
    else {
      const jwt: JwtHelperService = new JwtHelperService();
      const info = jwt.decodeToken(item);
      if (info.role !== "ROLE_REGISTEREDUSER") {
        this.toastr.warning("You are not logged in as registered user");
        return;
      }
    }
    if (this.selectedDate == '') {
      this.selectedDate = this.datePipe.transform(this.dates[0], "yyyy-MM-dd HH:mm:ss");
    }
    var seatIds = [];
    for (let seat of this.seatsForReservation) {

      seatIds.push(seat.seatId);
    }
    var seatsDate = {
      seatIds: seatIds,
      date: this.selectedDate
    };
    this.seatService.reserveSeats(seatsDate).subscribe(
      (date) => {
        if (date) {
          for (let seat of this.seatsForReservation) {
            for (let cat of this.categories) {
              for (let seat_ of cat.seats) {
                if (seat.seatId == seat_.seatId) {
                  seat.reserved = true;
                  cat.availableSeats = cat.seats.filter(item => item.reserved === false);

                }
              }

            }
          }
          this.seatsForReservation = [];
          this.toastr.success("Successfull reservation");
          if (this.seatsForReservation.length == 0) {
            this.tableToShow = false;
          }

        }
      },
      (error) => {
        this.toastr.warning(error.error);
      }
    )
  }

  buyTickets() {
    if (this.seatsForReservation.length == 0) {
      this.toastr.warning("You have not selected any seat");
      return;
    }
    const item = localStorage.getItem('user');
    if (!item) {
      this.toastr.warning("You are not logged in");
      this.router.navigate(['/login']);
      return;
    }
    else {
      const jwt: JwtHelperService = new JwtHelperService();
      const info = jwt.decodeToken(item);
      if (info.role !== "ROLE_REGISTEREDUSER") {
        this.toastr.warning("You are not logged in as registered user");
        return;
      }
    }
    if (this.selectedDate == '') {
      this.selectedDate = this.datePipe.transform(this.dates[0], "yyyy-MM-dd HH:mm:ss");
    }
    var seatIds = [];
    for (let seat of this.seatsForReservation) {

      seatIds.push(seat.seatId);
    }
    var seatsDate = {
      seatIds: seatIds,
      date: this.selectedDate
    };
    this.ticketService.buyTickets(seatsDate).subscribe(
      (result) => {
        for (let seat of this.seatsForReservation) {
          for (let cat of this.categories) {
            for (let seat_ of cat.seats) {
              if (seat.seatId == seat_.seatId) {
                seat.reserved = true;
                cat.availableSeats = cat.seats.filter(item => item.reserved === false);

              }
            }

          }
        }
        this.seatsForReservation = [];
        if (this.seatsForReservation.length == 0) {
          this.tableToShow = false;
        }

        window.location.href = result['redirectUrl'];
      },
      (error) => {
        this.toastr.warning(error.error);
      }
    )
  }

}
