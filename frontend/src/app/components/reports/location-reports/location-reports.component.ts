import { Component, OnInit } from '@angular/core'; 
import { NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReportsService } from 'app/services/reports.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LocationService } from 'app/services/location.service';

@Component({
  selector: 'app-location-reports',
  templateUrl: './location-reports.component.html',
  styleUrls: ['./location-reports.component.css']
})
export class LocationReportsComponent implements OnInit {

  id: number;
  startDate: any;
  endDate: any;
  location: any;
  datesToShow: Date[] = [];
  private dailySelected = true;
  private weeklySelected = false;
  private monthlySelected = false;
  labels = [];
  data = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private reportService: ReportsService,
    private locationService: LocationService,
  ) { 
  }

  ngOnInit(){ 
    this.getLocation()
  }

  getLocation(){
    this.id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.locationService.getLocation(this.id)
    .subscribe(
      
        (result) => {
          this.location =  result;
        },
        (error) => {
          alert(error.error);
          this.router.navigate(['manifestations']);
        }   
    );
  }
  dailyClick(){
    this.dailySelected = true;
    this.weeklySelected = false;
    this.monthlySelected = false;
  }

  weeklyClick(){
    this.dailySelected = false;
    this.weeklySelected = true;
    this.monthlySelected = false;
  }

  monthlyClick(){
    this.dailySelected = false;
    this.weeklySelected = false;
    this.monthlySelected = true;
  }

  getReport(){
    const start = `${this.startDate.day}-${this.startDate.month}-${this.startDate.year}` ;
    const end = `${this.endDate.day}-${this.endDate.month}-${this.endDate.year}`;
    if(this.dailySelected)
      this.getReportDaily(start, end);
    else if(this.weeklySelected)
      this.getReportWeekly(start, end);
    else if(this.monthlySelected)
      this.getReportMonthly(start, end);
  }

  getReportDaily(start:string, end:string){
    this.reportService.getDailyReportLocation(this.id, start, end).subscribe(
      result => {
        let _labels = [];
        let _data = [];
        Object.keys(result).forEach(key => {
          _labels.push(key);
          var obj1 = result[key];
          _data.push(obj1);
        })
        this.labels = _labels;
        this.data = _data;
      },
			error => {
				alert(error.error);
			}
    );
  }

  getReportWeekly(start:string, end:string){
    this.reportService.getWeeklyReportLocation(this.id, start, end).subscribe(
      result => {
        let _labels = [];
        let _data = [];
        Object.keys(result).forEach(key => {
          _labels.push(key);
          var obj1 = result[key];
          _data.push(obj1);
        })
        this.labels = _labels;
        this.data = _data;
      },
			error => {
				alert(error.error);
			}
    );
  }

  getReportMonthly(start:string, end:string){
    this.reportService.getMonthlyReportLocation(this.id, start, end).subscribe(
      result => {
        console.log(result);
        let _labels = [];
        let _data = [];
        Object.keys(result).forEach(key => {
          _labels.push(key);
          var obj1 = result[key];
          _data.push(obj1);
        })
        this.labels = _labels;
        this.data = _data;
      },
			error => {
				alert(error.error);
			}
    );
  }
}
