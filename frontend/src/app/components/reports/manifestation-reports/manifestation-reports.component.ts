import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js'; 
import { NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReportsService } from 'app/services/reports.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ManifestationService } from 'app/services/manifestation.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manifestation-reports',
  templateUrl: './manifestation-reports.component.html',
  styleUrls: ['./manifestation-reports.component.css']
})
export class ManifestationReportsComponent implements OnInit {

  id: number;
  startDate: any;
  endDate: any;
  manifestation: any;
  datesToShow: Date[] = [];
  private dailySelected = true;
  private weeklySelected = false;
  private monthlySelected = false;
  labels = [];
  data = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private reportService: ReportsService,
    private manifestationService: ManifestationService,
    private toastr: ToastrService
  ) { 
  }

  ngOnInit(){ 
    this.getManifestation()
  }

  getManifestation(){
    this.id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.manifestationService.getManifestationInfo(this.id)
    .subscribe(
      
        (result) => {
          this.manifestation =  result;
          console.log(result);
          if (result) {
            if (result.dates.length == 1) {
              this.datesToShow.push(new Date(result.dates[0]));
            }
            else{
              this.datesToShow.push(new Date(result.dates[0]));
              this.datesToShow.push(new Date(result.dates[result.dates.length-1]));
            }
          }
        },
        (error) => {
          this.toastr.error(error.error);
          this.router.navigate(['manifestations']);
        }   
    );
  }
  dailyClick(){
    this.dailySelected = true;
    this.weeklySelected = false;
    this.monthlySelected = false;
  }

  weeklyClick(){
    this.dailySelected = false;
    this.weeklySelected = true;
    this.monthlySelected = false;
  }

  monthlyClick(){
    this.dailySelected = false;
    this.weeklySelected = false;
    this.monthlySelected = true;
  }

  getReport(){
    const start = `${this.startDate.day}-${this.startDate.month}-${this.startDate.year}` ;
    const end = `${this.endDate.day}-${this.endDate.month}-${this.endDate.year}`;
    if(this.dailySelected)
      this.getReportDaily(start, end);
    else if(this.weeklySelected)
      this.getReportWeekly(start, end);
    else if(this.monthlySelected)
      this.getReportMonthly(start, end);
  }

  getReportDaily(start:string, end:string){
    this.reportService.getDailyReportManifestation(this.id, start, end).subscribe(
      result => {
        let labels = [];
        let data = [];
        Object.keys(result).forEach(key => {
          labels.push(key);
          var obj1 = result[key];
          data.push(obj1);
        })
        this.labels = labels;
        this.data = data;
      },
			error => {
				this.toastr.error(error.error);
			}
    );
  }

  getReportWeekly(start:string, end:string){
    this.reportService.getWeeklyReportManifestation(this.id, start, end).subscribe(
      result => {
        let labels = [];
        let data = [];
        Object.keys(result).forEach(key => {
          labels.push(key);
          var obj1 = result[key];
          data.push(obj1);
        })
        this.labels = labels;
        this.data = data;
      },
			error => {
				this.toastr.error(error.error);
			}
    );
  }

  getReportMonthly(start:string, end:string){
    this.reportService.getMonthlyReportManifestation(this.id, start, end).subscribe(
      result => {
        console.log(result);
        let labels = [];
        let data = [];
        Object.keys(result).forEach(key => {
          labels.push(key);
          var obj1 = result[key];
          data.push(obj1);
        })
        this.labels = labels;
        this.data = data;
      },
			error => {
				this.toastr.error(error.error);
			}
    );
    }
}
