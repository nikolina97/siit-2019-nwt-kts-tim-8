import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestationReportsComponent } from './manifestation-reports.component';

describe('ManifestationReportsComponent', () => {
  let component: ManifestationReportsComponent;
  let fixture: ComponentFixture<ManifestationReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManifestationReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManifestationReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
