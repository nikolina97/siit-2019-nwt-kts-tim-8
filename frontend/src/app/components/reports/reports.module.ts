import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'app/core/core.module';
import { NgbModule, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { LocationReportsComponent } from './location-reports/location-reports.component';
import { ManifestationReportsComponent } from './manifestation-reports/manifestation-reports.component';



@NgModule({
  declarations: [LocationReportsComponent, ManifestationReportsComponent],
  imports: [
    CommonModule,
    CoreModule,
    NgbModule,
    NgbDate
  ]
})
export class ReportsModule { }
