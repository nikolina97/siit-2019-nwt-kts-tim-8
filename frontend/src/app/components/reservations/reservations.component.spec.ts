import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { TicketService } from 'app/services/ticket.service';
import { of } from 'rxjs';

import { ReservationsComponent } from './reservations.component';
import { DebugElement } from '@angular/core';

fdescribe('ReservationsComponent', () => {
  let component: ReservationsComponent;
  let fixture: ComponentFixture<ReservationsComponent>;
  let router: Router;
  let userService: UserService;
  let ticketService: TicketService;

  beforeEach(async(() => {
    let ticketServiceMocked = {
      getReservations: jasmine.createSpy('getReservations')
        .and.returnValue(of([{}, {}, {}])), 
      payTicket: jasmine.createSpy('payTicket')
        .and.returnValue(of({}))
    };

    let userServiceMocked = {
      cancel: jasmine.createSpy('cancel')
        .and.returnValue(of({})), 
    };

    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [ ReservationsComponent ],
      providers: [
        { provide: TicketService, useValue: ticketServiceMocked },
        { provide: UserService, useValue: userServiceMocked }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    ticketService = TestBed.get(TicketService);
    router = TestBed.get(Router);
    userService = TestBed.get(UserService);
    spyOn(router, 'navigate');
    spyOn(component, 'doRedirect');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display values when initialized', async(() => {
    component.ngOnInit();
    expect(ticketService.getReservations).toHaveBeenCalled();

    fixture.whenStable()
      .then(() => {
        expect(component.reservations.length).toBe(3);
        fixture.detectChanges();     
        let elements: DebugElement[] = 
          fixture.debugElement.queryAll(By.css('div.card'));
        expect(elements.length).toBe(3);
      });
  }));

  it('should redirect when reservation is paid', async(() => {
    component.completePayment(1);
    expect(ticketService.payTicket).toHaveBeenCalledWith(1);
    expect(component.doRedirect).toHaveBeenCalled();
  }));

  it('should not cancel reservation when confirmation dialog is dismissed', () => {
    spyOn(window, 'confirm').and.callFake(function () {
      return false;
    });

    component.cancelButton(1);
    expect(userService.cancel).not.toHaveBeenCalled();
  });

  it('should cancel reservation when confirmation dialog is confirmed', () => {
    spyOn(window, 'confirm').and.callFake(function () {
      return true;
    });

    component.cancelButton(1);
    expect(userService.cancel).toHaveBeenCalledWith(1);
  });

});
