import { UserService } from 'app/services/user.service';
import { Router } from '@angular/router';
import { TicketService } from 'app/services/ticket.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {
  reservations: any[];

  constructor(
    private ticketService: TicketService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getReservations();
  }

  private getReservations() {
    this.ticketService.getReservations().subscribe(
      (tickets) => {
        this.reservations = tickets;
      },
      (error) => {
        alert(error.error);
        this.router.navigate(['manifestations']);
      }
    );
  }

  completePayment(ticketId: number) {
    this.ticketService.payTicket(ticketId).subscribe(
      result => {
        this.doRedirect(result['redirectUrl']);
      }
    );
  }

  doRedirect(redirectUrl: string) {
    window.location.href = redirectUrl;
  } 

  cancelButton(ticketId: Number) {
    if (confirm("Cancel reservation?")) {
      this.userService.cancel(ticketId).subscribe(
        () => {
          this.getReservations();
        },
        () => {
          this.getReservations();
        }
      );
    }
  }

}
