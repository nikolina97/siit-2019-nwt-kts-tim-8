import { Component, OnInit } from '@angular/core';
import { TicketService } from 'app/services/ticket.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment-confirmation',
  templateUrl: './payment-confirmation.component.html',
  styleUrls: ['./payment-confirmation.component.css']
})
export class PaymentConfirmationComponent implements OnInit {

  private paymentId: string;
  private payerId: string;
  private ids: string;
  private cancel: boolean = false;

  constructor(private ticketService: TicketService, private route: ActivatedRoute, private router: Router, private toastr: ToastrService) { }

  ngOnInit() {
    this.ids = this.route.snapshot.queryParams['ids'];
    this.cancel = this.route.snapshot.queryParams['cancel'];
    this.paymentId = this.route.snapshot.queryParams['paymentId'];
    this.payerId = this.route.snapshot.queryParams['PayerID'];

    if (!this.cancel)
      this.completePayment();
    else
      this.deleteTickets();
  }

  private completePayment() {
    this.ticketService.completePayment(this.paymentId, this.payerId).subscribe(
      result => {
        this.ticketService.markAsPaid(this.ids).subscribe(
          _ => {
            this.toastr.success("Successful buying. Your ticket has been sent to your email.");
            this.router.navigate(['manifestations']);
          }
        );
      },
      error => {
        this.ticketService.markAsPaid(this.ids).subscribe(
          _ => {
            this.toastr.success("Successful buying. Your ticket has been sent to your email.");
            this.router.navigate(['manifestations']);
          }
        );
      }
    );
  }

  private deleteTickets() {
    this.ticketService.deleteTickets(this.ids).subscribe(
      _ => this.router.navigate(['manifestations'])
    )
  }

}
