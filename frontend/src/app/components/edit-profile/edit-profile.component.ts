import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  private user : any;
  private showChangePassword = false;
  private showEditProfile = false;
  private url = "/assets/img/default-image.jpg";

  private editForm: FormGroup;
  private changePasswordForm: FormGroup;
  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService) 
    { 
    this.user = this.userService.getLoggedUser().subscribe(
			result => {
        this.user = result;
        
				console.log(this.user);
			},
			error => {
				alert(error.error);
    });
    
    this.editForm = this.fb.group({
			firstName : [null, Validators.required],
			lastName: [null, Validators.required]
    });
    
    this.changePasswordForm = this.fb.group({
			oldPassword : [null, Validators.required],
      newPassword: [null, Validators.required],
      repeatedPassword: [null, Validators.required]
		});
  }
  ngOnInit() {
  }

 
  onSelectFile(event) { // called each time file input changes
    const file = event.target.files[0];
    const uploadData = new FormData();
    uploadData.append('file', file, file.name);

   this.userService.uploadImage(uploadData).subscribe(
      result => {
        this.user = result;
        setTimeout(()=>{
          (this.url = result.image).subscribe(
            result => {},
            error =>{this.url = result.image}
          )
        }, 3000);
        this.toastr.success("Profile picture successfully updated!");
    },
      error => {
        console.log(error);
        this.toastr.error(error.error)
    });
  }

  showEdit(){
    this.showChangePassword = false;
    this.showEditProfile = true;
    this.editForm.patchValue({
      firstName: this.user.firstName,
      lastName: this.user.lastName
    });
  }

  showPasswordChange(){
    this.showChangePassword = true;
    this.showEditProfile = false;
  }

  saveEdited(){
    const editInfo: any = {};
    editInfo.firstName = this.editForm.value.firstName;
    editInfo.lastName = this.editForm.value.lastName;
    
    console.log(editInfo);
    this.userService.editProfile(editInfo).subscribe(
			result => {
        this.toastr.success("Changes successfully saved!");
        this.user = result;
        this.showEditProfile = false;
			},
			error => {
        console.log(editInfo);
				this.toastr.error(error.error);
			}
		);

  }

  saveChangedPassword(){
    const passwords: any = {};
    passwords.oldPassword = this.changePasswordForm.value.oldPassword;
    passwords.newPassword = this.changePasswordForm.value.newPassword;
    passwords.repeatedPassword = this.changePasswordForm.value.repeatedPassword;
    console.log(passwords);
    if(passwords.newPassword !== passwords.repeatedPassword){
      this.toastr.error("Password doesn't match confirmation!");
      return;
    }
		this.userService.changePassword(passwords).subscribe(
			result => {
        this.toastr.success("Password successfully changed! Log in again!");
        this.showChangePassword = false;
        localStorage.removeItem('user');
        this.router.navigate(['login']);
			},
			error => {
        if(error.status === 401){
          this.toastr.error("Old password is incorrect!");
        }
        else{
				this.toastr.error(error.error);
        }
      }
		);
  }
}
