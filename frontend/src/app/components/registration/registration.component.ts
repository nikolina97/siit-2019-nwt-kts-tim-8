import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService} from '../../services/authentication.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
	formReg: FormGroup;
	waitngForConfirmation;

	constructor(
	  private fb: FormBuilder,
	  private router: Router,
	  private authenticationService: AuthenticationService,
	  private toastr: ToastrService
  ) {
	  this.formReg = this.fb.group({
		  username : [null, Validators.required],
		  password: [null, Validators.required],
		  repeatedPassword : [null, Validators.required],
		  firstname : [null, Validators.required],
		  lastname : [null, Validators.required],
		  email : [null, Validators.compose([
			Validators.required,
			Validators.email])]
	  });
  }

	ngOnInit() {
	}

	submit() {
	  const user: any = {};
	  user.username = this.formReg.value.username;
	  user.password = this.formReg.value.password;
	  const repeatedPassword = this.formReg.value.repeatedPassword;
	  if(user.password !== repeatedPassword){
		this.toastr.error("Passwords are not the same");
		return;
	  }
	  user.firstName = this.formReg.value.firstname;
	  user.lastName = this.formReg.value.lastname;
	  user.email = this.formReg.value.email;
	  this.authenticationService.register(user).subscribe(
		  result => {
			  this.waitngForConfirmation = true;
			  console.log(result)
		  },
		  error => {
			console.log(error);
			this.toastr.error(error.error);
		  }
	  );
  }
}
