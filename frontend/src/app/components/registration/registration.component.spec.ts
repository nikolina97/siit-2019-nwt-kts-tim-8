import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { By } from "@angular/platform-browser";
import { MatFormFieldModule, MatInputModule, MatProgressSpinnerModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RegistrationComponent } from './registration.component';
import { Router } from '@angular/router';
import { AuthenticationService} from '../../services/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { error } from '@angular/compiler/src/util';
import { Observable } from 'rxjs';

fdescribe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;
  let authenticationService: AuthenticationService;
  let router: Router;
  let toastr: ToastrService;

  beforeEach(async(() => {

    let authenticationServiceMocked = {
      register: jasmine.createSpy('register').and.returnValue(of({id:5, username:
      "user123", password:"$1234abc", firstName:"Marko", lastName:
      "Markovic", email:"user123marko@gmail.com", image:null, tickets:[],
      verified:false, enabled:true}))
    };

    let routerMocked = jasmine.createSpyObj('router', ['navigate']);
    let toastrMocked = jasmine.createSpyObj('toastr', ['error']);

    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, CommonModule, MatFormFieldModule, MatInputModule, MatProgressSpinnerModule, BrowserAnimationsModule],
      declarations: [ RegistrationComponent ],
      providers: [
        { provide: AuthenticationService, useValue: authenticationServiceMocked },
        { provide: Router, useValue: routerMocked },
        { provide: ToastrService, useValue: toastrMocked }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    authenticationService = TestBed.get(AuthenticationService);
    router = TestBed.get(Router);
    toastr = TestBed.get(ToastrService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be shown', () => {
    expect(component.formReg).toBeDefined();
    expect(component.formReg.invalid).toBeTruthy();
  });

  it('should fill inputs with valid data', () => {
    component.ngOnInit();
    let username = fixture.debugElement.query(By.css('input#username')).nativeElement;
    let password = fixture.debugElement.query(By.css('input#password')).nativeElement;
    let repeatedPassword = fixture.debugElement.query(By.css('input#repeatedPassword')).nativeElement;
    let firstName = fixture.debugElement.query(By.css('input#firstname')).nativeElement;
    let lastName = fixture.debugElement.query(By.css('input#lastname')).nativeElement;
    let email = fixture.debugElement.query(By.css('input#email')).nativeElement;

    username.value = 'user123';
    username.dispatchEvent(newEvent('input'));

    password.value = '123';
    password.dispatchEvent(newEvent('input'));

    repeatedPassword.value = '123';
    repeatedPassword.dispatchEvent(newEvent('input'));

    firstName.value = 'Marko';
    firstName.dispatchEvent(newEvent('input'));

    lastName.value = 'Markovic';
    lastName.dispatchEvent(newEvent('input'));

    email.value = 'user123marko@gmail.com';
    email.dispatchEvent(newEvent('input'));

    lastName.value = 'Markovic';
    lastName.dispatchEvent(newEvent('input'));

    expect(component.formReg.valid).toBeTruthy();

  });
  
  it('should register', () => {
    component.ngOnInit();
    component.submit();

    expect(component.waitngForConfirmation).toBeTruthy();
  });

  it('should have invalid state with incomplete data', () => {
    component.ngOnInit();
    let username = fixture.debugElement.query(By.css('input#username')).nativeElement;
    let password = fixture.debugElement.query(By.css('input#password')).nativeElement;
    let repeatedPassword = fixture.debugElement.query(By.css('input#repeatedPassword')).nativeElement;
    let firstName = fixture.debugElement.query(By.css('input#firstname')).nativeElement;
    let lastName = fixture.debugElement.query(By.css('input#lastname')).nativeElement;
    let email = fixture.debugElement.query(By.css('input#email')).nativeElement;

    username.value = '';
    username.dispatchEvent(newEvent('input'));

    password.value = '123';
    password.dispatchEvent(newEvent('input'));

    repeatedPassword.value = '123';
    repeatedPassword.dispatchEvent(newEvent('input'));

    firstName.value = 'Marko';
    firstName.dispatchEvent(newEvent('input'));

    lastName.value = '';
    lastName.dispatchEvent(newEvent('input'));

    email.value = 'user123marko@gmail.com';
    email.dispatchEvent(newEvent('input'));

    lastName.value = 'Markovic';
    lastName.dispatchEvent(newEvent('input'));

    expect(component.formReg.valid).toBeFalsy();

  });

  it('should not register, wrong password', () => {
    component.ngOnInit();
    let username = fixture.debugElement.query(By.css('input#username')).nativeElement;
    let password = fixture.debugElement.query(By.css('input#password')).nativeElement;
    let repeatedPassword = fixture.debugElement.query(By.css('input#repeatedPassword')).nativeElement;
    let firstName = fixture.debugElement.query(By.css('input#firstname')).nativeElement;
    let lastName = fixture.debugElement.query(By.css('input#lastname')).nativeElement;
    let email = fixture.debugElement.query(By.css('input#email')).nativeElement;

    username.value = 'user123';
    username.dispatchEvent(newEvent('input'));

    password.value = '123';
    password.dispatchEvent(newEvent('input'));

    repeatedPassword.value = '12333';
    repeatedPassword.dispatchEvent(newEvent('input'));

    firstName.value = 'Marko';
    firstName.dispatchEvent(newEvent('input'));

    lastName.value = 'Markovic';
    lastName.dispatchEvent(newEvent('input'));

    email.value = 'user123marko@gmail.com';
    email.dispatchEvent(newEvent('input'));

    lastName.value = 'Markovic';
    lastName.dispatchEvent(newEvent('input'));

    component.submit();
    expect(toastr.error).toHaveBeenCalled();
  });

  it('should have an invalid state, wrong email', () => {
    component.ngOnInit();
    let username = fixture.debugElement.query(By.css('input#username')).nativeElement;
    let password = fixture.debugElement.query(By.css('input#password')).nativeElement;
    let repeatedPassword = fixture.debugElement.query(By.css('input#repeatedPassword')).nativeElement;
    let firstName = fixture.debugElement.query(By.css('input#firstname')).nativeElement;
    let lastName = fixture.debugElement.query(By.css('input#lastname')).nativeElement;
    let email = fixture.debugElement.query(By.css('input#email')).nativeElement;

    username.value = 'user123';
    username.dispatchEvent(newEvent('input'));

    password.value = '123';
    password.dispatchEvent(newEvent('input'));

    repeatedPassword.value = '123';
    repeatedPassword.dispatchEvent(newEvent('input'));

    firstName.value = 'Marko';
    firstName.dispatchEvent(newEvent('input'));

    lastName.value = 'Markovic';
    lastName.dispatchEvent(newEvent('input'));

    email.value = 'user123.com';
    email.dispatchEvent(newEvent('input'));

    lastName.value = 'Markovic';
    lastName.dispatchEvent(newEvent('input'));

    expect(component.formReg.valid).toBeFalsy();
  });


  function newEvent(eventName: string, bubbles = false, cancelable = false) {
    let evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(eventName, bubbles, cancelable, null);
    return evt;
  }
});
