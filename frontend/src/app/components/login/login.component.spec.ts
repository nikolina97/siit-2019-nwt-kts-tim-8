import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'app/services/authentication.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authenticationService : AuthenticationService;
  let router: Router;
  let toastr: ToastrService;

  beforeEach(async(() => {
    let authenticationServiceMocked = {
        login: jasmine.createSpy('login').and.returnValue(of({
        username: 'user1',
        password: '123'
      }))
    }
    let routerMocked = jasmine.createSpyObj('router', ['navigate']);

    let toastrMocked = jasmine.createSpyObj('toastr', ['success']);

    TestBed.configureTestingModule({
      imports: [FormsModule,ReactiveFormsModule],
      declarations: [ LoginComponent ],
      providers: [
        { provide: AuthenticationService, useValue: authenticationServiceMocked },
        { provide: Router, useValue: routerMocked },
        { provide: ToastrService, useValue: toastrMocked }
      ]
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    authenticationService = TestBed.get(AuthenticationService);
    router = TestBed.get(Router);
    toastr = TestBed.get(ToastrService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be initialized', () => {
    component.ngOnInit();
    expect(component.form).toBeDefined();
    expect(component.form.invalid).toBeTruthy();
  });

  it('should be invalid form when username and password are empty', () => {
    component.form.controls.username.setValue('');
    component.form.controls.password.setValue('');
    expect(component.form.invalid).toBeTruthy();
  });

  it('should be invalid form when username is empty', () => {
    component.form.controls.username.setValue('');
    component.form.controls.password.setValue('123');
    expect(component.form.invalid).toBeTruthy();
  });

  it('should be invalid form when password is empty', () => {
    component.form.controls.username.setValue('user1');
    component.form.controls.password.setValue('');
    expect(component.form.invalid).toBeTruthy();
  });

  it('should login on submit', () => {
    component.form.controls.username.setValue('user1');
    component.form.controls.password.setValue('123');

    expect(component.form.invalid).toBeFalsy();

    component.submit();
    expect(router.navigate).toHaveBeenCalledWith(['manifestations']);
    expect(toastr.success).toHaveBeenCalled();
  });
});
