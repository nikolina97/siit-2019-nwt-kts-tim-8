import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ManifestationDetailsComponent } from './components/manifestation-details/manifestation-details.component';
import { LoginComponent } from './components/login/login.component';
import { LoginGuard } from './guards/login.service';
import { RegistrationComponent } from './components/registration/registration.component';
import { RegistrationConfirmationComponent } from './components/registration-confirmation/registration-confirmation.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { ManifestationReportsComponent } from './components/reports/manifestation-reports/manifestation-reports.component';
import { LocationReportsComponent } from './components/reports/location-reports/location-reports.component';
import { RoleGuard } from './guards/role.service';
import { PaymentConfirmationComponent } from './components/payment-confirmation/payment-confirmation.component';


const routes: Routes = [
  { path: '', redirectTo: '/manifestations', pathMatch: 'full' },
  { path: 'manifestations/:id', component: ManifestationDetailsComponent },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'payment-confirmation',
    component: PaymentConfirmationComponent,
    data: { expectedRoles: 'ROLE_REGISTEREDUSER' },
    canActivate: [RoleGuard]
  },
  {
    path: 'registration', pathMatch: 'full',
    component: RegistrationComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'registration/confirmation/:token',
    component: RegistrationConfirmationComponent
  },
  {
    path: 'profile',
    data: { expectedRoles: 'ROLE_ADMIN|ROLE_REGISTEREDUSER' },
    canActivate: [RoleGuard],
    component: EditProfileComponent
  },
  {
    path: 'reports/manifestation/:id',
    component: ManifestationReportsComponent,
    data: { expectedRoles: 'ROLE_ADMIN' },
    canActivate: [RoleGuard]
  },
  {
    path: 'reports/location/:id',
    component: LocationReportsComponent,
    data: { expectedRoles: 'ROLE_ADMIN' },
    canActivate: [RoleGuard]
  },
  {
    path: 'reservations',
    component: ReservationsComponent,
    data: { expectedRoles: 'ROLE_REGISTEREDUSER' },
    canActivate: [RoleGuard]
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
