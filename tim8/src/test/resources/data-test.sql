INSERT INTO authorities (user_type)
VALUES  ("ROLE_REGISTEREDUSER"),
        ("ROLE_ADMIN");

INSERT INTO user (DTYPE, EMAIL, FIRST_NAME, LAST_NAME, PASSWORD, USERNAME, VERIFIED)
VALUES ("RegisteredUser", "user1@gmail.com", "Pera", "Peric", "$2a$04$CKjDkEtBltT5FYXDl2vM.uUxuTDE7oT2eqlHnzqE7SWQ6IVJC2Tri", "user1", true ),
	   ("RegisteredUser", "user2@gmail.com", "Laza", "Lazic", "$2y$12$gjMRabHVWWVl22wt4kwhQOFLx8wYsyPozcf2bHKmuNAaleJiUUsZe", "user2", true ),
	   ("RegisteredUser", "user3@gmail.com", "Jovan", "Jovic", "$2a$10$Gu3y/2UFHzLTNxlF628A9.6TYAVRKJpwu2A4t9wVQ4.thh.kJAfI.", "user3", false ),
	   ("Admin", "admin1@gmail.com", "Mika", "Mikic", "$2a$04$m9Zge.y/mkJJZvy0AAWtQuLUiVoNHEFAYwvaYJ4PCF/OrltTJG4Me", "admin1", null);

INSERT INTO user_authority (user_id, authority_id) VALUES
        (1, 1),
        (2, 1),
        (3, 1),
        (4, 2);

insert into location(name, address, latitude, longitude, is_active) values ('Djava', 'Petrovaradin', 12, 12, b'1'),
																		   ('Marakana', 'Beograd', 25, 25, b'1'),
																		   ('Cair', 'Nis', 25.5, 25.5, b'1'),
																		   ('JNA', 'Beograd', 41.1, 20.0, b'1');

insert into manifestation(name, description, start_date, end_date, days_before_reservation_expires, location_id, state, category, is_active, image) values
						 ('Exit', 'Exit is awesome', '2020-07-02', '2020-07-05', 7, 1, 'OPEN', 'ENTERTAINMENT', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('Cola', 'Cola is awesome', '2019-12-02', '2019-12-03', 7, 1, 'PAST', 'ENTERTAINMENT', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('Zvezda-Bajern', 'Fudbal is awesome', '2019-12-26', '2019-12-26', 7, 2, 'PAST', 'SPORT', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('RHCP', 'RHCP is awesome', '2020-12-26', '2020-12-26', 7, 2, 'CANCELED', 'CULTURE', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('ACDC', 'ACDC is awesome', '2021-09-06', '2020-09-06', -1, 2, 'SOLD_OUT', 'CULTURE', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('Exit 2.0', 'Exit is awesome', '2020-09-06', '2020-12-06', 2, 1, 'OPEN', 'ENTERTAINMENT', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('Superbovl', 'Superbovl is awesome', '2020-09-15', '2020-09-15', 3, 3, 'OPEN', 'SPORT', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('Logan vs KSI', 'Boks is awesome', '2020-10-15', '2020-10-15', 3, 2, 'OPEN', 'SPORT', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('LCS', 'Esports is awesome', '2020-11-15', '2020-11-17', 7, 2, 'OPEN', 'SPORT', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('VidCon', 'Youtube is awesome', '2020-11-17', '2020-11-19', 7, 3, 'OPEN', 'ENTERTAINMENT', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('Sajam Tambure', 'Tambura is awesome', '2020-06-09', '2020-06-13', 7, 1, 'OPEN', 'CULTURE', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg'),
						 ('Megdan', 'Megdan is awesome', '2020-08-08', '2020-08-10', 7, 3, 'OPEN', 'CULTURE', b'1', 'http://localhost:8080/mob_EXIT2-6.jpg');

insert into seat_category(name, enumerable, stage, rows_number, columns_number, location_id) values ('Tribina 1', b'1', b'0', 3, 3, 1), ('Parter', b'0', b'0', -1, -1, 1), ('Zapad', b'1', b'0', 3, 3, 1);

insert into supported_seat_categories(seat_category_id, manifestation_id, ticket_price, number_of_seats) values
									 (1, 1, 150, 9), (1, 2, 200, 9), (1, 3, 300, 9), (1, 4, 150, 9), (1, 6, 50, 9);

insert into seat(seat_row, seat_column, supported_seat_category_id) values (1, 1, 1), (1, 2, 1), (1, 3, 1), (2, 1, 1),
																(2, 2, 1), (2, 3, 1), (3, 1, 1), (3, 2, 1), (3, 3, 1),
																(1, 1, 2), (1, 2, 2), (1, 3, 2), (2, 1, 2),	(2, 2, 2),
																(2, 3, 2), (3, 1, 2), (3, 2, 2), (3, 3, 2), (1, 1, 3),
																(1, 2, 3), (1, 3, 3), (2, 1, 3), (2, 2, 3), (2, 3, 3),
																(3, 1, 3), (3, 2, 3), (3, 3, 3), (1, 1, 5), (1, 2, 5),
																(1, 3, 5), (2, 1, 5), (2, 2, 5), (2, 3, 5), (3, 1, 5),
																(3, 2, 5), (3, 3, 5), (1, 1, 4), (1, 2, 4), (1, 3, 4),
																(2, 1, 4), (2, 2, 4), (2, 3, 4), (3, 1, 4), (3, 2, 4),
																(3, 3, 4);

insert into ticket(seat_id, date, buying_date, user_id, manifestation_id, paid, is_active) values (1, '2020-07-03', '2019-11-23', 1, 1, b'0', b'1'),
																		  			 (2, null, '2019-12-13', 1, 1, b'1', b'1'),
																		  			 (3, '2020-07-04', '2019-11-10', 2, 1, b'0', b'1'),
																		  			 (4, '2020-07-02', '2019-12-11', 2, 1, b'1', b'1'),
																		  			 (5, '2020-07-04', '2019-12-10', 1, 1, b'1', b'1'),
																		  			 (10, '2019-12-02', '2019-10-17', 2, 2, b'1', b'1'),
																		  			 (11, '2019-12-02', '2019-11-02', 1, 2, b'1', b'1'),
																		  			 (12, '2019-12-02', '2019-10-17', 3, 2, b'1', b'1'),
																		  			 (1, '2020-07-02', '2019-11-23', 1, 1, b'0', b'1'),
																		  			 (1, '2020-07-04', '2019-11-23', 1, 1, b'0', b'1'),
																		  			 (1, '2020-07-05', '2019-11-23', 1, 1, b'0', b'1');
																		  			 
insert into verification_tokens(id, token, user) values (1, 'AbCdEf123456', 3);
