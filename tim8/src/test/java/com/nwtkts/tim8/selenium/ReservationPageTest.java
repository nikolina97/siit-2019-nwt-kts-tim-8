package com.nwtkts.tim8.selenium;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.testng.AssertJUnit.assertEquals;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import org.openqa.selenium.support.PageFactory;

public class ReservationPageTest {
	private WebDriver browser;	

	private ManifestationsPage manifestationsPage;
	private LoginPage loginPage;
	private ReservationPage reservationPage;
		
	@Before
	public void setupSelenium() {
		//instantiate browser
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		browser = new ChromeDriver();
		//maximize window
		browser.manage().window().maximize();
		//navigate
		browser.navigate().to("http://localhost:4200/login");
		
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		manifestationsPage = PageFactory.initElements(browser, ManifestationsPage.class);
		reservationPage = PageFactory.initElements(browser, ReservationPage.class);
	}
	
	@Test
	public void reserveTicketTest() {
		
		//login as registered user
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		reservationPage.ensureToastIsDisplayed();
		reservationPage.getTaoast().click();
		
		//ensure manifestaions are displayed
		manifestationsPage.ensureBookingButtonIsDisplayed();
		manifestationsPage.getBookingButton().click();
		
		//manifestation page
		assertEquals("http://localhost:4200/manifestations/1", browser.getCurrentUrl());
		reservationPage.ensureTicketsButtonIsDisplayed();
		
		//reservation page
		reservationPage.getTicketsButton().click();		
		reservationPage.ensureTableIsDisplayed();
		
		//check number of selected seats
		reservationPage.getFirstFreeSeat().click();
		int noOfSelectedSeats = 1;
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();
		reservationPage.ensureReservationButtonIsDisplayed();
		assertEquals(noOfSelectedSeats, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		
		//reserve 1 seat - ok
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getReserveButton().click();	
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Successfull reservation", reservationPage.getTaoast().getText());
		assertEquals(noOfReservedSeats+1, browser.findElements(By.cssSelector("circle.true")).size());
		reservationPage.getTaoast().click();
		
		//reserve 2 seats
		reservationPage.getFirstFreeSeat().click();
		reservationPage.getFirstFreeSeat().click();
		reservationPage.ensureReservationButtonIsDisplayed();
		assertEquals(noOfSelectedSeats+1, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getReserveButton().click();
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Successfull reservation", reservationPage.getTaoast().getText());
		assertEquals(noOfReservedSeats+3, browser.findElements(By.cssSelector("circle.true")).size());
		
		
	}
	
	@Test
	public void reserveTicketTest_festivalTicket() {
		
		//login as registered user
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		reservationPage.ensureToastIsDisplayed();
		reservationPage.getTaoast().click();
		
		//ensure manifestaions are displayed
		manifestationsPage.ensureBookingButtonIsDisplayed();
		manifestationsPage.getBookingButton().click();
		
		//manifestation page
		assertEquals("http://localhost:4200/manifestations/1", browser.getCurrentUrl());
		reservationPage.ensureTicketsButtonIsDisplayed();
		
		//reservation page
		reservationPage.getTicketsButton().click();
		reservationPage.ensureTableIsDisplayed();

		//change date - festival tickets
		reservationPage.ensureFestivalTicketRadioIsDisplayed();
		reservationPage.getFestivalTicketRadio().click();
		reservationPage.ensureTableIsDisplayed();
		
		//check number of selected seats
		reservationPage.getFirstFreeSeat().click();
		int noOfSelectedSeats = 1;
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();
		reservationPage.ensureReservationButtonIsDisplayed();
		assertEquals(noOfSelectedSeats, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		
		//reserve 1 seat - ok
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getReserveButton().click();	
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Successfull reservation", reservationPage.getTaoast().getText());
		assertEquals(noOfReservedSeats+1, browser.findElements(By.cssSelector("circle.true")).size());
		reservationPage.getTaoast().click();

		//select 2 seats
		reservationPage.getFirstFreeSeat().click();
		reservationPage.getFirstFreeSeat().click();
		reservationPage.ensureParterreIsDisplayed();
		reservationPage.getParterre().click();
		reservationPage.ensureMatInputIsDisplayed();
		
		//try to select more tickets then is enabled
		reservationPage.setMatInput("50");	
		reservationPage.setMatInput("50");	  
		reservationPage.ensureTextInsideMatInput("50"); 
		reservationPage.getOkButton().click();
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Not enough free tickets for Parterre", reservationPage.getTaoast().getText());
		reservationPage.getTaoast().click();
		reservationPage.ensureParterreIsDisplayed();
		reservationPage.getParterre().click();
		reservationPage.ensureMatInputIsDisplayed();
		
		//select 2 parterre places
		reservationPage.setMatInput("2");
		reservationPage.getOkButton().click();
		reservationPage.ensureReservationButtonIsDisplayed();
		reservationPage.ensureSeatsAreDisplayed();
		assertEquals(noOfSelectedSeats+3, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		
		//reserve -ok
		reservationPage.getReserveButton().click();		
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Successfull reservation", reservationPage.getTaoast().getText());
		assertEquals(noOfReservedSeats+3, browser.findElements(By.cssSelector("circle.true")).size());

	}
	
	@Test
	public void reservationTest_unregisteredUser() {
		browser.navigate().to("http://localhost:4200/manifestations/1");

		reservationPage.getTicketsButton().click();
		
		reservationPage.ensureTableIsDisplayed();
		
		int noOfSelectedSeats = 1;
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();

		reservationPage.getFirstFreeSeat().click();
		
		reservationPage.ensureReservationButtonIsDisplayed();
		assertEquals(noOfSelectedSeats, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		
		//reservation not allowed to unregistered user
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getReserveButton().click();
		reservationPage.ensureToastIsDisplayed();
		assertEquals("You are not logged in", reservationPage.getTaoast().getText());
		
		browser.navigate().to("http://localhost:4200/manifestations/1");
		reservationPage.ensureTicketsButtonIsDisplayed();
		reservationPage.getTicketsButton().click();
		reservationPage.ensureTableIsDisplayed();
		assertEquals(noOfReservedSeats, browser.findElements(By.cssSelector("circle.true")).size());

	}
	
	@Test
	public void reservationTest_admin() {
		
		//login as admin
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		reservationPage.ensureToastIsDisplayed();
		reservationPage.getTaoast().click();
		
		manifestationsPage.ensureManifestationImageIsDisplayed();
		manifestationsPage.getFirstManifestationImage().click();
		
		//reservation page
		assertEquals("http://localhost:4200/manifestations/1", browser.getCurrentUrl());		
		reservationPage.ensureTicketsButtonIsDisplayed();
		reservationPage.getTicketsButton().click();
		
		reservationPage.ensureTableIsDisplayed();

		//select seats
		reservationPage.getFirstFreeSeat().click();
		int noOfSelectedSeats = 1;
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();
		reservationPage.ensureReservationButtonIsDisplayed();
		assertEquals(noOfSelectedSeats, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		reservationPage.ensureSeatsAreDisplayed();
		
		//reservation not allowed to admin
		reservationPage.getReserveButton().click();
		reservationPage.ensureToastIsDisplayed();
		assertEquals("You are not logged in as registered user", reservationPage.getTaoast().getText());
		
		browser.navigate().to("http://localhost:4200/manifestations/1");
		reservationPage.ensureTicketsButtonIsDisplayed();
		reservationPage.getTicketsButton().click();
		reservationPage.ensureTableIsDisplayed();
		assertEquals(noOfReservedSeats, browser.findElements(By.cssSelector("circle.true")).size());

	}
	
	@Test
	public void reserveTest_reservationExpired() {
		
		//login as registered user
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		reservationPage.ensureToastIsDisplayed();
		reservationPage.getTaoast().click();
		
		//ensure manifestaions are displayed
		manifestationsPage.ensureBookingButtonResExpManifestationisDisplayed();
		manifestationsPage.getBookingButtonResExpManifestation().click();
		
		//manifestation page
		assertEquals("http://localhost:4200/manifestations/4", browser.getCurrentUrl());
		reservationPage.ensureTicketsButtonIsDisplayed();
		
		//reservation page
		reservationPage.getTicketsButton().click();		
		reservationPage.ensureTableIsDisplayed();
		
		//check number of selected seats
		reservationPage.getFirstFreeSeat().click();
		int noOfSelectedSeats = 1;
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();
		reservationPage.ensureReservationButtonIsDisplayed();
		assertEquals(noOfSelectedSeats, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		
		//reserve 1 seat - reservation expired
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getReserveButton().click();	
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Reservation expired", reservationPage.getTaoast().getText());
		assertEquals(noOfReservedSeats, browser.findElements(By.cssSelector("circle.true")).size());
	}
	
	@Test
	public void reserveTest_reservationForbidden() {
		
		//login as registered user
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		reservationPage.ensureToastIsDisplayed();
		reservationPage.getTaoast().click();
		
		//ensure manifestaions are displayed
		manifestationsPage.ensureBookingButtonResForbManifestationisDisplayed();
		manifestationsPage.getBookingButtonResForbManifestation().click();
		
		//manifestation page
		assertEquals("http://localhost:4200/manifestations/3", browser.getCurrentUrl());
		reservationPage.ensureTicketsButtonIsDisplayed();
		
		//reservation page
		reservationPage.getTicketsButton().click();		
		reservationPage.ensureTableIsDisplayed();
		
		//check number of selected seats
		reservationPage.getFirstFreeSeat().click();
		int noOfSelectedSeats = 1;
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();
		reservationPage.ensureReservationButtonIsDisplayed();
		assertEquals(noOfSelectedSeats, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		
		//reserve 1 seat - reservation expired
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getReserveButton().click();	
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Reservation is forbidden", reservationPage.getTaoast().getText());
		assertEquals(noOfReservedSeats, browser.findElements(By.cssSelector("circle.true")).size());
	}
	
	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}	

}
