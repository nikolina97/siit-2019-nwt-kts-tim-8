package com.nwtkts.tim8.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class NewLocationPageTest {

	private WebDriver browser;
	private LocationsPage locationsPage;
	private NewLocationPage newLocationPage;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

		browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.navigate().to("http://localhost:4200/login");

		LoginPage loginPage = PageFactory.initElements(browser, LoginPage.class);

		loginPage.setUsername("admin1");
		loginPage.setPassword("456");

		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));
		browser.findElement(By.cssSelector("div[toast-component]")).click();

		locationsPage = PageFactory.initElements(browser, LocationsPage.class);
		newLocationPage = PageFactory.initElements(browser, NewLocationPage.class);
	}

	@Test
	public void addLocationTest() {
		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")));
		browser.findElement(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")).click();

		locationsPage.ensureAddButtonIsClickable();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("card")));
		int locationCountBefore = browser.findElements(By.className("card")).size();

		locationsPage.getAddButton().click();

		assertEquals("http://localhost:4200/locations/new", browser.getCurrentUrl());

		newLocationPage.ensureEntireFormIsDisplayed();

		assertFalse(newLocationPage.getAddButton().isEnabled());

		newLocationPage.setNameInput("Stark Arena");
		newLocationPage.setAddressInput("Sutjeska 2, Novi Sad");
		newLocationPage.setLatitudeInput("45.247024");
		newLocationPage.setLongitudeInput("19.845306");
		newLocationPage.setLayoutInput(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\indoor_arena_concerts.json");
		newLocationPage.getAddButton().click();

		newLocationPage.ensureErrorMessageIsDisplayer();
		assertEquals("Location on the same address already exists", newLocationPage.getErrorMessage().getText());

		newLocationPage.setAddressInput("Bulevar Arsenija Carnojevica 58, Beograd");
		newLocationPage.setLatitudeInput("44.814157");
		newLocationPage.setLongitudeInput("20.421288");
		newLocationPage.getAddButton().click();

		locationsPage.ensureAddButtonIsClickable();

		List<WebElement> locations = browser.findElements(By.className("card"));

		assertEquals(locationCountBefore + 1, locations.size());
		assertEquals("Stark Arena", locations.get(locations.size() - 1).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("Bulevar Arsenija Carnojevica 58, Beograd", locations.get(locations.size() - 1).findElement(By.className("card-text")).getText());
	}

	@After
	public void cleanUp() {
		browser.quit();
	}

}
