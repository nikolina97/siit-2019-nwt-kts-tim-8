package com.nwtkts.tim8.selenium;

import static org.testng.AssertJUnit.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class BuyingPageTest {
	private WebDriver browser;	

	private ManifestationsPage manifestationsPage;
	private LoginPage loginPage;
	private ReservationPage reservationPage;
	private PayPalPage paypalPage;
	
	@Before
	public void setupSelenium() {
		//instantiate browser
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
		browser = new ChromeDriver();
		//maximize window
		browser.manage().window().maximize();
		//navigate
		browser.navigate().to("http://localhost:4200/login");
		
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		manifestationsPage = PageFactory.initElements(browser, ManifestationsPage.class);
		reservationPage = PageFactory.initElements(browser, ReservationPage.class);
		paypalPage = PageFactory.initElements(browser, PayPalPage.class);
	}

	@Test
	public void buyTicketTest() {
		
		//login as registered user
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		reservationPage.ensureToastIsDisplayed();
		reservationPage.getTaoast().click();
		
		//ensure manifestaions are displayed
		manifestationsPage.ensureBookingButtonResExpManifestationisDisplayed();
		manifestationsPage.getBookingButtonResExpManifestation().click();
		
		//manifestation page
		assertEquals("http://localhost:4200/manifestations/4", browser.getCurrentUrl());
		reservationPage.ensureTicketsButtonIsDisplayed();
		
		//reservation page
		reservationPage.getTicketsButton().click();		
		reservationPage.ensureTableIsDisplayed();
		
		//buy 2 tickets
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();
		reservationPage.getFirstFreeSeat().click();
		reservationPage.getFirstFreeSeat().click();
		reservationPage.ensureBuyingButtonIsDisplayed();
		assertEquals(2, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getBuyingButton().click();
		
		//paypal login page
		paypalPage.ensureEmailInputIsDisplayed();
		paypalPage.setEmailInput("sb-tqehp964308@personal.example.com");
		paypalPage.ensureNextButtonIsDisplayed();
		paypalPage.getNextButton().click();
		paypalPage.ensurePasswordInputIsDisplayed();
		paypalPage.setPasswordInput("PQ>*4pEL");
		paypalPage.ensureLoginButtonIsDisplayed();
		paypalPage.getLogInButton().click();
		
		//payment
		paypalPage.ensureContitueButtonIsDisplayed();
		paypalPage.getContinueButton().click();
		
		//check if ticket is bought
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Successful buying. Your ticket has been sent to your email.", reservationPage.getTaoast().getText());
		manifestationsPage.ensureBookingButtonResExpManifestationisDisplayed();
		manifestationsPage.getBookingButtonResExpManifestation().click();
		assertEquals("http://localhost:4200/manifestations/4", browser.getCurrentUrl());
		reservationPage.ensureTicketsButtonIsDisplayed();
		reservationPage.getTicketsButton().click();		
		reservationPage.ensureTableIsDisplayed();
		assertEquals(noOfReservedSeats+2, browser.findElements(By.cssSelector("circle.true")).size());

	}
	
	@Test
	public void buyTicketTest_cancelBuying() {
		
		//login as registered user
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		reservationPage.ensureToastIsDisplayed();
		reservationPage.getTaoast().click();
		
		//ensure manifestaions are displayed
		manifestationsPage.ensureBookingButtonResExpManifestationisDisplayed();
		manifestationsPage.getBookingButtonResExpManifestation().click();
		
		//manifestation page
		assertEquals("http://localhost:4200/manifestations/4", browser.getCurrentUrl());
		reservationPage.ensureTicketsButtonIsDisplayed();
		
		//reservation page
		reservationPage.getTicketsButton().click();		
		reservationPage.ensureTableIsDisplayed();
		
		//buy 2 tickets
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();
		reservationPage.getFirstFreeSeat().click();
		reservationPage.getFirstFreeSeat().click();
		reservationPage.ensureBuyingButtonIsDisplayed();
		assertEquals(2, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getBuyingButton().click();
		
		//paypal login page
		paypalPage.ensureEmailInputIsDisplayed();
		paypalPage.setEmailInput("sb-tqehp964308@personal.example.com");
		paypalPage.ensureNextButtonIsDisplayed();
		paypalPage.getNextButton().click();
		paypalPage.ensurePasswordInputIsDisplayed();
		paypalPage.setPasswordInput("PQ>*4pEL");
		paypalPage.ensureLoginButtonIsDisplayed();
		paypalPage.getLogInButton().click();
		
		//cancel payment
		paypalPage.ensureCancelLinkIsDiplayed();
		paypalPage.getCancelBuying().click();
		
		//check if ticket is bought
		manifestationsPage.ensureBookingButtonResExpManifestationisDisplayed();
		manifestationsPage.getBookingButtonResExpManifestation().click();
		assertEquals("http://localhost:4200/manifestations/4", browser.getCurrentUrl());
		reservationPage.ensureTicketsButtonIsDisplayed();
		reservationPage.getTicketsButton().click();		
		reservationPage.ensureTableIsDisplayed();
		assertEquals(noOfReservedSeats, browser.findElements(By.cssSelector("circle.true")).size());

	}
	
	@Test
	public void buyingTest_unregisteredUser() {
		browser.navigate().to("http://localhost:4200/manifestations/1");

		reservationPage.getTicketsButton().click();
		
		reservationPage.ensureTableIsDisplayed();
		int noOfCategories = browser.findElements(By.cssSelector("tbody > tr")).size();
		assertEquals(4, noOfCategories);
		
		int noOfSelectedSeats = 1;
		int noOfReservedSeats = browser.findElements(By.cssSelector("circle.true")).size();

		reservationPage.getFirstFreeSeat().click();
		
		reservationPage.ensureBuyingButtonIsDisplayed();
		assertEquals(noOfSelectedSeats, browser.findElements(By.cssSelector("tbody > tr")).size()-1);
		
		//reservation not allowed to unregistered user
		reservationPage.ensureSeatsAreDisplayed();
		reservationPage.getBuyingButton().click();
		reservationPage.ensureToastIsDisplayed();
		assertEquals("You are not logged in", reservationPage.getTaoast().getText());
		
		browser.navigate().to("http://localhost:4200/manifestations/1");
		reservationPage.ensureTicketsButtonIsDisplayed();
		reservationPage.getTicketsButton().click();
		reservationPage.ensureTableIsDisplayed();
		assertEquals(noOfReservedSeats, browser.findElements(By.cssSelector("circle.true")).size());

	}

	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}
	
	
}
