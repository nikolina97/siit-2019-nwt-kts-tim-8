package com.nwtkts.tim8.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;


public class LoginPageTest {
	
	private WebDriver browser;

	LoginPage loginPage;
	HomePage homePage;

	@Before
	public void setupSelenium() {
		// instantiate browser
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		browser = new ChromeDriver();
		// maximize window
		browser.manage().window().maximize();
		// navigate
		browser.navigate().to("http://localhost:4200/manifestations");
		
		homePage = PageFactory.initElements(browser, HomePage.class);
		loginPage = PageFactory.initElements(browser, LoginPage.class);
	}
	
	@Test
	public void logInTest() throws InterruptedException {
		homePage.ensureLoginButtonIsDisplayed();
		homePage.getLogInButton().click();

		assertEquals("http://localhost:4200/login",	browser.getCurrentUrl());
		
		//both fields empty
		loginPage.setUsername("");
		loginPage.setPassword("");
		loginPage.getUsername().click();
		loginPage.getLogInButton().click();
		assertFalse(loginPage.getLogInButton().isEnabled());
		assertTrue(loginPage.getUsernameError().isDisplayed());
		assertEquals("This field can't be empty!", loginPage.getUsernameError().getText());
		assertTrue(loginPage.getPasswordError().isDisplayed());
		assertEquals("This field can't be empty!", loginPage.getPasswordError().getText());
		
		
		//only empty password
		loginPage.setUsername("username");
		assertTrue(loginPage.getPasswordError().isDisplayed());
		assertEquals("This field can't be empty!", loginPage.getPasswordError().getText());
		assertFalse(loginPage.getLogInButton().isEnabled());
		
		//only empty username
		loginPage.getUsername().sendKeys(Keys.CONTROL + "a");
		loginPage.getUsername().sendKeys(Keys.DELETE);

		loginPage.setPassword("password");
		assertTrue(loginPage.getUsernameError().isDisplayed());
		assertEquals("This field can't be empty!", loginPage.getUsernameError().getText());
		assertFalse(loginPage.getLogInButton().isEnabled());
		
		//wrong username
		loginPage.setUsername("WrongUser");
		assertTrue(loginPage.getLogInButton().isDisplayed());
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		assertEquals("User with username 'WrongUser' not found",loginPage.getToastr().getText());
		loginPage.getToastr().click();
		
		//wrong password
		loginPage.setUsername("user1");
		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		assertEquals("Wrong password",loginPage.getToastr().getText());
		loginPage.getToastr().click();
		
		//not verified
		loginPage.setUsername("user3");
		loginPage.setPassword("123");
		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		assertEquals("Not verified! See your email for verification.",loginPage.getToastr().getText());
		loginPage.getToastr().click();
		
		//Ok
		loginPage.setUsername("user1");
		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		assertEquals("Login successfull!",loginPage.getToastr().getText());
		assertEquals("http://localhost:4200/manifestations", browser.getCurrentUrl());
		
		browser.navigate().to("http://localhost:4200/login");
		assertEquals("http://localhost:4200/manifestations", browser.getCurrentUrl());
		
	}
	
	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}

}
