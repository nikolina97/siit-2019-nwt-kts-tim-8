package com.nwtkts.tim8.selenium;

import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class ReportsPageTest {
private WebDriver browser;
	
	HomePage homePage;
	LoginPage loginPage;
	ReportsPage reportsPage;
	LocationsPage locationsPage;
	
	
	@Before
	public void setupSelenium() {
		// instantiate browser
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		browser = new ChromeDriver();
		// maximize window
		browser.manage().window().maximize();
		// navigate
		browser.navigate().to("http://localhost:4200/login");
		
		homePage = PageFactory.initElements(browser, HomePage.class);
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		reportsPage = PageFactory.initElements(browser, ReportsPage.class);
		locationsPage = PageFactory.initElements(browser, LocationsPage.class);
	}
	
	@Test
	public void testReportsUserNotLoggedIn() {
		browser.navigate().to("http://localhost:4200/reports/location/1");
		assertEquals("http://localhost:4200/login",browser.getCurrentUrl());
	}
	
	@Test
	public void testReportsUserNotAdmin() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		browser.navigate().to("http://localhost:4200/reports/manifestation/1");
		assertEquals("http://localhost:4200/manifestations",browser.getCurrentUrl());
	}
	
	@Test
	public void testReportsEmptyFields() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureReportButtonsAreDisplayed();
		homePage.getmanifReportsButton().get(2).click();

		assertEquals("http://localhost:4200/reports/manifestation/3",browser.getCurrentUrl());
		reportsPage.ensureStartDateButtonDisplayed();
		reportsPage.getStartDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		Select year = new Select(reportsPage.getYearSelect());
		year.selectByValue("2019");
		reportsPage.getEndDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		year.selectByValue("2014");
		reportsPage.ensureMonthIsDisplayed();
		Select month = new Select(reportsPage.getMonthSelect());
		month.selectByValue("12");
		assertFalse(reportsPage.getReportButton().isEnabled());
	}
	
	@Test
	public void testReportsEndDateBeforeStartDate() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureReportButtonsAreDisplayed();
		homePage.getmanifReportsButton().get(2).click();

		assertEquals("http://localhost:4200/reports/manifestation/3",browser.getCurrentUrl());

		reportsPage.ensureStartDateButtonDisplayed();
		reportsPage.getStartDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		Select year = new Select(reportsPage.getYearSelect());
		year.selectByValue("2020");
		reportsPage.ensureMonthIsDisplayed();
		Select month = new Select(reportsPage.getMonthSelect());
		month.selectByValue("2");
		reportsPage.getDayButtons().get(6).click();
		reportsPage.getEndDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		year.selectByValue("2020");
		reportsPage.ensureMonthIsDisplayed();
		month.selectByValue("2");
		reportsPage.getDayButtons().get(5).click();
		
		reportsPage.ensureStartDateInputNotEmpty();
		assertEquals("2020-02-02", reportsPage.getStartDateInput().getAttribute("value"));
		
		reportsPage.ensureEndDateInputNotEmpty();
		assertEquals("2020-02-01", reportsPage.getEndDateInput().getAttribute("value"));
		
		assertTrue(reportsPage.getReportButton().isDisplayed());
		reportsPage.getReportButton().click();
		loginPage.ensureToastrIsDisplayed();
		assertEquals("Invalid date!", loginPage.getToastr().getText());
		
	}
	
	@Test
	public void testManifestationDailyReports() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureReportButtonsAreDisplayed();
		homePage.getmanifReportsButton().get(2).click();

		assertEquals("http://localhost:4200/reports/manifestation/3",browser.getCurrentUrl());
		
		reportsPage.ensureStartDateButtonDisplayed();
		reportsPage.getStartDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		Select year = new Select(reportsPage.getYearSelect());
		year.selectByValue("2019");
		reportsPage.ensureMonthIsDisplayed();
		Select month = new Select(reportsPage.getMonthSelect());
		month.selectByValue("12");
		reportsPage.getDayButtons().get(30).click();
		reportsPage.getEndDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		year.selectByValue("2020");
		reportsPage.ensureMonthIsDisplayed();
		month.selectByValue("2");
		reportsPage.getDayButtons().get(5).click();
		
		reportsPage.ensureStartDateInputNotEmpty();
		assertEquals("2019-12-25", reportsPage.getStartDateInput().getAttribute("value"));
		
		reportsPage.ensureEndDateInputNotEmpty();
		assertEquals("2020-02-01", reportsPage.getEndDateInput().getAttribute("value"));
		assertTrue(reportsPage.getReportButton().isDisplayed());
		reportsPage.getReportButton().click();
		
		reportsPage.ensureChartIsDisplayed();
		reportsPage.ensureChartLabelsNotEmpty();
		assertEquals("26-12-2019,26-01-2020,29-12-20", reportsPage.getChart().getAttribute("ng-reflect-labels"));
		reportsPage.ensureChartValuesNotEmpty();
		assertEquals("1,2,1,1", reportsPage.getChart().getAttribute("ng-reflect-data"));
	}
	
	@Test
	public void testManifestationWeeklyReports() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureReportButtonsAreDisplayed();
		homePage.getmanifReportsButton().get(2).click();
		
		assertEquals("http://localhost:4200/reports/manifestation/3",browser.getCurrentUrl());
		
		reportsPage.ensureDailyMonthlyWeekliAreDisplayed();
		reportsPage.getDailyMonthlyWeeklyButtons().get(1).click();
		
		reportsPage.ensureStartDateButtonDisplayed();
		reportsPage.getStartDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		Select year = new Select(reportsPage.getYearSelect());
		year.selectByValue("2019");
		reportsPage.ensureMonthIsDisplayed();
		Select month = new Select(reportsPage.getMonthSelect());
		month.selectByValue("12");
		reportsPage.getDayButtons().get(15).click();
		reportsPage.getEndDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		year.selectByValue("2020");
		reportsPage.ensureMonthIsDisplayed();
		month.selectByValue("2");
		reportsPage.getDayButtons().get(11).click();
		
		reportsPage.ensureStartDateInputNotEmpty();
		assertEquals("2019-12-10", reportsPage.getStartDateInput().getAttribute("value"));
		
		reportsPage.ensureEndDateInputNotEmpty();
		assertEquals("2020-02-07", reportsPage.getEndDateInput().getAttribute("value"));
		assertTrue(reportsPage.getReportButton().isDisplayed());
		reportsPage.getReportButton().click();
		reportsPage.ensureChartIsDisplayed();
		reportsPage.ensureChartLabelsNotEmpty();
		assertEquals("Jan 2020 week: 5,Feb 2020 week", reportsPage.getChart().getAttribute("ng-reflect-labels"));
		
		reportsPage.ensureChartValuesNotEmpty();
		assertEquals("2,5,1,2", reportsPage.getChart().getAttribute("ng-reflect-data"));
	}
	
	
	@Test
	public void testManifestationMonthlyReports() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureReportButtonsAreDisplayed();
		homePage.getmanifReportsButton().get(2).click();

		assertEquals("http://localhost:4200/reports/manifestation/3",browser.getCurrentUrl());
		
		reportsPage.ensureDailyMonthlyWeekliAreDisplayed();
		reportsPage.getDailyMonthlyWeeklyButtons().get(2).click();
		
		reportsPage.ensureStartDateButtonDisplayed();
		reportsPage.getStartDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		Select year = new Select(reportsPage.getYearSelect());
		year.selectByValue("2019");
		reportsPage.ensureMonthIsDisplayed();
		Select month = new Select(reportsPage.getMonthSelect());
		month.selectByValue("12");
		reportsPage.getDayButtons().get(15).click();
		reportsPage.getEndDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		year.selectByValue("2020");
		reportsPage.ensureMonthIsDisplayed();
		month.selectByValue("2");
		reportsPage.getDayButtons().get(11).click();
		
		reportsPage.ensureStartDateInputNotEmpty();
		assertEquals("2019-12-10", reportsPage.getStartDateInput().getAttribute("value"));
		
		reportsPage.ensureEndDateInputNotEmpty();
		assertEquals("2020-02-07", reportsPage.getEndDateInput().getAttribute("value"));
		assertTrue(reportsPage.getReportButton().isDisplayed());
		reportsPage.getReportButton().click();
		reportsPage.ensureChartIsDisplayed();
		reportsPage.ensureChartLabelsNotEmpty();
		assertEquals("Dec 2019,Feb 2020,Jan 2020", reportsPage.getChart().getAttribute("ng-reflect-labels"));
		reportsPage.ensureChartValuesNotEmpty();
		assertEquals("3,5,2", reportsPage.getChart().getAttribute("ng-reflect-data"));
	}
	
	@Test
	public void testLocationsDailyReports() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureLocationsIsDisplayed();
		homePage.getLocations().click();
		
		locationsPage.ensureReportButtonIsClickable();
		locationsPage.getReportsButtons().get(0).click();
		
		assertEquals("http://localhost:4200/reports/location/1",browser.getCurrentUrl());
		
		reportsPage.ensureDailyMonthlyWeekliAreDisplayed();
		
		reportsPage.ensureStartDateButtonDisplayed();
		reportsPage.getStartDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		Select year = new Select(reportsPage.getYearSelect());
		year.selectByValue("2019");
		Select month = new Select(reportsPage.getMonthSelect());
		month.selectByValue("12");
		reportsPage.getDayButtons().get(15).click();
		reportsPage.getEndDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		reportsPage.getDayButtons().get(11).click();
		
		reportsPage.ensureStartDateInputNotEmpty();
		assertEquals("2019-12-10", reportsPage.getStartDateInput().getAttribute("value"));
		reportsPage.ensureEndDateInputNotEmpty();
		assertEquals("2020-02-07", reportsPage.getEndDateInput().getAttribute("value"));
		assertTrue(reportsPage.getReportButton().isDisplayed());
		reportsPage.getReportButton().click();
		reportsPage.ensureChartIsDisplayed();
		reportsPage.ensureChartLabelsNotEmpty();
		assertEquals("04-02-2020,29-12-2019,30-12-20", reportsPage.getChart().getAttribute("ng-reflect-labels"));
		reportsPage.ensureChartValuesNotEmpty();
		assertEquals("1,1,1,1,3,2,1", reportsPage.getChart().getAttribute("ng-reflect-data"));
	}
	
	@Test
	public void testLocationsWeeklyReports() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureLocationsIsDisplayed();
		homePage.getLocations().click();
		
		locationsPage.ensureReportButtonIsClickable();
		locationsPage.getReportsButtons().get(0).click();
		
		assertEquals("http://localhost:4200/reports/location/1",browser.getCurrentUrl());
		
		reportsPage.ensureDailyMonthlyWeekliAreDisplayed();
		reportsPage.getDailyMonthlyWeeklyButtons().get(1).click();
		reportsPage.ensureStartDateButtonDisplayed();
		reportsPage.getStartDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		Select year = new Select(reportsPage.getYearSelect());
		year.selectByValue("2020");
		Select month = new Select(reportsPage.getMonthSelect());
		month.selectByValue("1");
		reportsPage.getDayButtons().get(10).click();
		reportsPage.getEndDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		reportsPage.getDayButtons().get(11).click();
		
		reportsPage.ensureStartDateInputNotEmpty();
		assertEquals("2020-01-09", reportsPage.getStartDateInput().getAttribute("value"));
		
		reportsPage.ensureEndDateInputNotEmpty();
		assertEquals("2020-02-07", reportsPage.getEndDateInput().getAttribute("value"));
		assertTrue(reportsPage.getReportButton().isDisplayed());
		reportsPage.getReportButton().click();
		reportsPage.ensureChartIsDisplayed();
		reportsPage.ensureChartLabelsNotEmpty();
		assertEquals("Jan 2020 week: 5,Feb 2020 week", reportsPage.getChart().getAttribute("ng-reflect-labels"));
		
		reportsPage.ensureChartValuesNotEmpty();
		assertEquals("2,5", reportsPage.getChart().getAttribute("ng-reflect-data"));
	}
	
	@Test
	public void testLocationMonthlyReports() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("admin1");
		loginPage.setPassword("456");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureLocationsIsDisplayed();
		homePage.getLocations().click();
		
		locationsPage.ensureReportButtonIsClickable();
		locationsPage.getReportsButtons().get(0).click();
		
		assertEquals("http://localhost:4200/reports/location/1",browser.getCurrentUrl());

		reportsPage.ensureDailyMonthlyWeekliAreDisplayed();
		reportsPage.getDailyMonthlyWeeklyButtons().get(2).click();
		
		reportsPage.ensureStartDateButtonDisplayed();
		reportsPage.getStartDateButton().click();
		reportsPage.ensureYearIsDisplayed();
		Select year = new Select(reportsPage.getYearSelect());
		year.selectByValue("2019");
		Select month = new Select(reportsPage.getMonthSelect());
		month.selectByValue("12");
		reportsPage.getDayButtons().get(15).click();
		reportsPage.getEndDateButton().click();
		year.selectByValue("2020");
		month.selectByValue("2");
		reportsPage.ensureYearIsDisplayed();
		reportsPage.getDayButtons().get(11).click();
		
		reportsPage.ensureStartDateInputNotEmpty();
		assertEquals("2019-12-10", reportsPage.getStartDateInput().getAttribute("value"));
		reportsPage.ensureEndDateInputNotEmpty();
		assertEquals("2020-02-07", reportsPage.getEndDateInput().getAttribute("value"));
		assertTrue(reportsPage.getReportButton().isDisplayed());
		reportsPage.getReportButton().click();
		reportsPage.ensureChartIsDisplayed();
		reportsPage.ensureChartLabelsNotEmpty();
		assertEquals("Dec 2019,Feb 2020,Jan 2020", reportsPage.getChart().getAttribute("ng-reflect-labels"));
		
		reportsPage.ensureChartValuesNotEmpty();
		assertEquals("3,5,2", reportsPage.getChart().getAttribute("ng-reflect-data"));
	}

	
	  @After public void closeSelenium() {
		  //Shutdown the browser 
		  browser.quit();
	  }
	 
}
