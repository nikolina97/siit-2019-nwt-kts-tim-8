package com.nwtkts.tim8.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class NewManifestationPageTest {

	private WebDriver browser;
	private ManifestationsPage eventsPage;
	private NewManifestationPage newEventPage;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

		browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.navigate().to("http://localhost:4200/login");

		LoginPage loginPage = PageFactory.initElements(browser, LoginPage.class);

		loginPage.setUsername("admin1");
		loginPage.setPassword("456");

		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));
		browser.findElement(By.cssSelector("div[toast-component]")).click();

		eventsPage = PageFactory.initElements(browser, ManifestationsPage.class);
		newEventPage = PageFactory.initElements(browser, NewManifestationPage.class);
	}

	@Test
	public void addManifestationTest() throws InterruptedException {
		eventsPage.ensureAddButtonIsClickable(10);

		eventsPage.ensureEventsAreVisible();
		int eventCountBefore = eventsPage.getEditButtons().size();

		eventsPage.getAddButton().click();

		assertEquals("http://localhost:4200/manifestations/new", browser.getCurrentUrl());

		newEventPage.ensureEntireFormIsDisplayed();

		assertFalse(newEventPage.getAddButton().isEnabled());

		newEventPage.setImageInput(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\mob_EXIT2-6.jpg");
		newEventPage.setNameInput("Exit");
		newEventPage.setDescriptionInput("Exit is awesome");
		newEventPage.setLocationSelect("1");
		newEventPage.setStartDateInput("03/03/2020", "0700PM");
		newEventPage.setEndDateInput("03/04/2020", "0900PM");
		newEventPage.setDaysBeforeExpiresInput("2");
		newEventPage.setCategorySelect("ENTERTAINMENT");
		newEventPage.setTicketPriceInputs("20");
		newEventPage.setNumberOfSeatsInputs("20");
		newEventPage.getAddButton().click();
		newEventPage.ensureErrorMessageIsDisplayed();
		assertEquals("Some other event will be held at this time", newEventPage.getErrorMessage().getText());

		newEventPage.setStartDateInput("07/09/2019", "0200PM");
		newEventPage.getAddButton().click();
		newEventPage.ensureErrorMessageIsDisplayed();
		assertEquals("The date when the reservations are no longer allowed cannot be earlier than today", newEventPage.getErrorMessage().getText());

		newEventPage.setStartDateInput("07/13/2020", "0200PM");
		newEventPage.setEndDateInput("07/08/2020", "0200PM");
		newEventPage.getAddButton().click();
		newEventPage.ensureErrorMessageIsDisplayed();
		assertEquals("End date can't be before start date", newEventPage.getErrorMessage().getText());

		newEventPage.setEndDateInput("07/14/2020", "0200PM");
		newEventPage.getAddButton().click();

		eventsPage.ensureAddButtonIsClickable(60);

		assertEquals(eventCountBefore + 1, eventsPage.getEvents().size());
		assertEquals("Exit", eventsPage.getEvents().get(eventsPage.getEvents().size() - 1).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("Exit is awesome", eventsPage.getEvents().get(eventsPage.getEvents().size() - 1).findElement(By.cssSelector("p:nth-child(3)")).getText());
	}

	@After
	public void cleanUp() {
		browser.quit();
	}

}
