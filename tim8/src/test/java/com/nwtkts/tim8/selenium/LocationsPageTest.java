package com.nwtkts.tim8.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class LocationsPageTest {

	private WebDriver browser;
	private LocationsPage locationsPage;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

		browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.navigate().to("http://localhost:4200/login");

		LoginPage loginPage = PageFactory.initElements(browser, LoginPage.class);

		loginPage.setUsername("admin1");
		loginPage.setPassword("456");

		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));
		browser.findElement(By.cssSelector("div[toast-component]")).click();

		locationsPage = PageFactory.initElements(browser, LocationsPage.class);
	}

	@Test
	public void deleteLocationTest() {
		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")));
		browser.findElement(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")).click();

		locationsPage.ensureDeleteButtonIsClickable();

		int locationCountBefore = locationsPage.getLocations().size();

		locationsPage.getDeleteButtons().get(2).click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));

		assertEquals(locationCountBefore - 1, locationsPage.getLocations().size());
	}

	@Test
	public void locationsTest() {
		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")));
		browser.findElement(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")).click();

		locationsPage.ensureLocationsAreVisible();

		assertEquals(3, locationsPage.getLocations().size());
		assertEquals("Spens", locationsPage.getLocations().get(0).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("Sutjeska 2, Novi Sad", locationsPage.getLocations().get(0).findElement(By.className("card-text")).getText());
		assertFalse(locationsPage.getDeleteButtons().get(0).isEnabled());
		assertEquals("Stadion Karadjordje", locationsPage.getLocations().get(1).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("Dimitrija Tucovica 1, Novi Sad", locationsPage.getLocations().get(1).findElement(By.className("card-text")).getText());
		assertFalse(locationsPage.getDeleteButtons().get(1).isEnabled());
		assertEquals("Kulturni centar", locationsPage.getLocations().get(2).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("Katolicka porta 5, Novi Sad", locationsPage.getLocations().get(2).findElement(By.className("card-text")).getText());
		assertTrue(locationsPage.getDeleteButtons().get(2).isEnabled());
	}

	@After
	public void cleanUp() {
		browser.quit();
	}

}
