package com.nwtkts.tim8.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class EditManifestationPageTest {

	private WebDriver browser;
	private ManifestationsPage eventsPage;
	private EditManifestationPage editEventPage;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

		browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.navigate().to("http://localhost:4200/login");

		LoginPage loginPage = PageFactory.initElements(browser, LoginPage.class);

		loginPage.setUsername("admin1");
		loginPage.setPassword("456");

		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));
		browser.findElement(By.cssSelector("div[toast-component]")).click();

		eventsPage = PageFactory.initElements(browser, ManifestationsPage.class);
		editEventPage = PageFactory.initElements(browser, EditManifestationPage.class);
	}

	@Test
	public void editManifestationTest() {
		eventsPage.ensureEditButtonIsClickable();
		eventsPage.getEditButtons().get(0).click();

		assertEquals("http://localhost:4200/manifestations/edit/1", browser.getCurrentUrl());

		editEventPage.ensureEntireFormIsDisplayed();

		editEventPage.setNameInput("Exitt");
		editEventPage.setDescriptionInput("Exitt is awesome");
		editEventPage.getSaveButton().click();

		eventsPage.ensureAddButtonIsClickable(10);

		assertEquals("Exitt", eventsPage.getEvents().get(0).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("Exitt is awesome", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(3)")).getText());
	}

	@After
	public void cleanUp() {
		browser.quit();
	}

}
