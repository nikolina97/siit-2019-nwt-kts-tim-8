package com.nwtkts.tim8.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.testng.AssertJUnit.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReservationsPageTest {
	
	private WebDriver browser;

	ReservationsPage reservationsPage;
	HomePage homePage;
	LoginPage loginPage;
	ReservationPage reservationPage;
	PayPalPage paypalPage;
	
	@Before
	public void setupSelenium() {
		// instantiate browser
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		browser = new ChromeDriver();
		// maximize window
		browser.manage().window().maximize();
		// navigate
		browser.navigate().to("http://localhost:4200/login");
		
		homePage = PageFactory.initElements(browser, HomePage.class);
		reservationsPage = PageFactory.initElements(browser, ReservationsPage.class);
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		reservationPage = PageFactory.initElements(browser, ReservationPage.class);
		paypalPage = PageFactory.initElements(browser, PayPalPage.class);
	}
	
	@Test
	public void cancelReservationTest() {
		
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		
		homePage.getReservationsLink().click();
		
		assertEquals("http://localhost:4200/reservations", browser.getCurrentUrl());
		
		reservationsPage.ensureReservationsAreLoaded();
		
		//click to cancel first reservation, but change your mind
		reservationsPage.getFirstReservationCancelLink().click();
		reservationsPage.ensureAlertIsPresent();
		Alert alert = browser.switchTo().alert();
        alert.dismiss();
        
        List<WebElement> reservations = browser.findElements(By.cssSelector("div.row > div.col-lg-3.col-md-6"));
        
        //check if all reservations are still there
        assertEquals(8, reservations.size());
        assertDoesNotThrow(() -> browser.findElement(By.xpath("//span[text()=', 3-4']")));
        
        //click to cancel second reservation
        reservationsPage.getSecondReservationCancelLink().click();
		reservationsPage.ensureAlertIsPresent();
		alert = browser.switchTo().alert();
        alert.accept();
        
        //wait for reservations to refresh
        (new WebDriverWait(browser, 2))
		.until(ExpectedConditions.textToBePresentInElementLocated(
				By.cssSelector("div.row > div.col-lg-3.col-md-6:nth-child(2) > div.card > div.card-body > p"),
				"Parterre"));
        
        reservations = browser.findElements(By.cssSelector("div.row > div.col-lg-3.col-md-6"));
        
        //check if second reservation is gone
        assertEquals(7, reservations.size());
        assertThrows(NoSuchElementException.class, () -> browser.findElement(By.xpath("//span[text()=', 3-5']")));
	}
	
	@Test
	public void completePaymentTest() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		
		homePage.getReservationsLink().click();
		
		assertEquals("http://localhost:4200/reservations", browser.getCurrentUrl());
		
		reservationsPage.ensureReservationsAreLoaded();
		
		assertEquals(3, reservationsPage.getConfirmPaymentTekmaButtons().size());
		
		reservationsPage.getConfirmPaymentTekmaButtons().get(0).click();
		
		//paypal login page
		paypalPage.ensureEmailInputIsDisplayed();
		paypalPage.setEmailInput("sb-tqehp964308@personal.example.com");
		paypalPage.ensureNextButtonIsDisplayed();
		paypalPage.getNextButton().click();
		paypalPage.ensurePasswordInputIsDisplayed();
		paypalPage.setPasswordInput("PQ>*4pEL");
		paypalPage.ensureLoginButtonIsDisplayed();
		paypalPage.getLogInButton().click();
		
		//payment
		paypalPage.ensureContitueButtonIsDisplayed();
		paypalPage.getContinueButton().click();
		
		reservationPage.ensureToastIsDisplayed();
		assertEquals("Successful buying. Your ticket has been sent to your email.", reservationPage.getTaoast().getText());
		reservationPage.getTaoast().click();
		
		homePage.getReservationsLink().click();
		
		assertEquals("http://localhost:4200/reservations", browser.getCurrentUrl());
		
		reservationsPage.ensureReservationsAreLoaded();
		
		assertEquals(2, reservationsPage.getConfirmPaymentTekmaButtons().size());
	}
	
	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}
	
}
