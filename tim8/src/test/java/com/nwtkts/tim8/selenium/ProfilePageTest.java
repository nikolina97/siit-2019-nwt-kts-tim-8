package com.nwtkts.tim8.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class ProfilePageTest {
	private WebDriver browser;
	
	HomePage homePage;
	LoginPage loginPage;
	ProfilePage profilePage;
	
	
	@Before
	public void setupSelenium() {
		// instantiate browser
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		browser = new ChromeDriver();
		// maximize window
		browser.manage().window().maximize();
		// navigate
		browser.navigate().to("http://localhost:4200/login");
		
		homePage = PageFactory.initElements(browser, HomePage.class);
		loginPage = PageFactory.initElements(browser, LoginPage.class);
		profilePage = PageFactory.initElements(browser, ProfilePage.class);
	}
	
	@Test
	public void testReportsUserNotLoggedIn() {
		browser.navigate().to("http://localhost:4200/profile");
		assertEquals("http://localhost:4200/login",browser.getCurrentUrl());
	}
	
	@Test
	public void testEditProfileEmptyFields() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		homePage.ensureProfileIconIsDisplayed();
		homePage.getPrifileIcon().click();
		assertEquals("http://localhost:4200/profile",browser.getCurrentUrl());
		
		profilePage.getEditButton().click();
		profilePage.ensureFirstNameIsDisplayed();
		profilePage.getFirstName().sendKeys(Keys.CONTROL + "a");
		profilePage.getFirstName().sendKeys(Keys.DELETE);
		profilePage.getLastName().sendKeys(Keys.CONTROL + "a");
		profilePage.getLastName().sendKeys(Keys.DELETE);
		profilePage.getFirstName().click();
		
		assertFalse(profilePage.getSaveButton().isEnabled());
		assertEquals("This field can't be empty!", profilePage.getFirstNameError().getText());
		assertEquals("This field can't be empty!", profilePage.getLastNameError().getText());
	}
	
	@Test
	public void testEditProfileOK() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureProfileIconIsDisplayed();
		homePage.getPrifileIcon().click();
		assertEquals("http://localhost:4200/profile",browser.getCurrentUrl());
		
		profilePage.getEditButton().click();
		profilePage.ensureFirstNameIsDisplayed();
		profilePage.setFirstName("Joca");
		
		assertTrue(profilePage.getSaveButton().isEnabled());
		profilePage.getSaveButton().click();
		profilePage.ensureToastrIsDisplayed();
		assertEquals("Changes successfully saved!", profilePage.getToastr().getText());
		profilePage.getEditButton().click();
		profilePage.ensureFirstNameIsDisplayed();
		assertEquals("Joca", profilePage.getFirstName().getAttribute("value"));

	}
	
	@Test
	public void testChangePasswordEmptyFields() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureProfileIconIsDisplayed();
		homePage.getPrifileIcon().click();
		assertEquals("http://localhost:4200/profile",browser.getCurrentUrl());
		
		profilePage.getChangePasswordButton().click();
		profilePage.ensurePasswordIsDisplayed();
		profilePage.setPassword("");
		profilePage.setNewPassword("");
		profilePage.setRepeatedPassword("");
		profilePage.getPassword().click();
		
		assertFalse(profilePage.getSaveButton().isEnabled());
		assertEquals("This field can't be empty!", profilePage.getPasswordError().getText());
		assertEquals("This field can't be empty!", profilePage.getNewPasswordError().getText());
		assertEquals("This field can't be empty!", profilePage.getRepeatedPasswordError().getText());
	}
	
	@Test
	public void testChangePasswordRepeatedPasswordIncorrect() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureProfileIconIsDisplayed();
		homePage.getPrifileIcon().click();
		assertEquals("http://localhost:4200/profile",browser.getCurrentUrl());
		
		profilePage.getChangePasswordButton().click();
		profilePage.ensurePasswordIsDisplayed();
		profilePage.setPassword("123");
		profilePage.setNewPassword("124");
		profilePage.setRepeatedPassword("333333");
		profilePage.getPassword().click();
		
		assertTrue(profilePage.getSaveButton().isEnabled());
		profilePage.getSaveButton().click();
		profilePage.ensureToastrIsDisplayed();
		assertEquals("Password doesn't match confirmation!", profilePage.getToastr().getText());
		
	}
	
	@Test
	public void testChangeOldPasswordIncorrect() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureProfileIconIsDisplayed();
		homePage.getPrifileIcon().click();
		assertEquals("http://localhost:4200/profile",browser.getCurrentUrl());
		
		profilePage.getChangePasswordButton().click();
		profilePage.ensurePasswordIsDisplayed();
		profilePage.setPassword("129");
		profilePage.setNewPassword("124");
		profilePage.setRepeatedPassword("124");
		profilePage.getPassword().click();
		
		assertTrue(profilePage.getSaveButton().isEnabled());
		profilePage.getSaveButton().click();
		profilePage.ensureToastrIsDisplayed();
		assertEquals("Old password is incorrect!", profilePage.getToastr().getText());
	}
	
	@Test
	public void testChangePasswordOK() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user4");
		loginPage.setPassword("124");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureProfileIconIsDisplayed();
		homePage.getPrifileIcon().click();
		assertEquals("http://localhost:4200/profile",browser.getCurrentUrl());
		
		profilePage.getChangePasswordButton().click();
		profilePage.ensurePasswordIsDisplayed();
		profilePage.setPassword("124");
		profilePage.setNewPassword("444");
		profilePage.setRepeatedPassword("444");
		profilePage.getPassword().click();
		
		assertTrue(profilePage.getSaveButton().isEnabled());
		profilePage.getSaveButton().click();
		profilePage.ensureToastrIsDisplayed();
		assertEquals("Password successfully changed! Log in again!", profilePage.getToastr().getText());
		assertEquals("http://localhost:4200/login",browser.getCurrentUrl());
	}
	
	@Test
	public void testChangeProfilePicture() {
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		homePage.ensureProfileIconIsDisplayed();
		homePage.getPrifileIcon().click();
		assertEquals("http://localhost:4200/profile",browser.getCurrentUrl());
		
		assertEquals("http://localhost:4200/assets/img/default-image.jpg", profilePage.getImage().getAttribute("src"));
		profilePage.setImageUpload("C:\\marc.jpg");
		profilePage.ensureToastrIsDisplayed();
		assertEquals("Profile picture successfully updated!", profilePage.getToastr().getText());
		profilePage.ensureImageChanged();
		assertEquals("http://localhost:8080/marc.jpg", profilePage.getImage().getAttribute("src"));
	}
	

	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}
}