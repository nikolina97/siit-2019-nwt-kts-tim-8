package com.nwtkts.tim8.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class EditLocationPageTest {

	private WebDriver browser;
	private LocationsPage locationsPage;
	private EditLocationPage editLocationPage;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

		browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.navigate().to("http://localhost:4200/login");

		LoginPage loginPage = PageFactory.initElements(browser, LoginPage.class);

		loginPage.setUsername("admin1");
		loginPage.setPassword("456");

		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));
		browser.findElement(By.cssSelector("div[toast-component]")).click();

		locationsPage = PageFactory.initElements(browser, LocationsPage.class);
		editLocationPage = PageFactory.initElements(browser, EditLocationPage.class);
	}

	@Test
	public void editLocationTest() {
		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")));
		browser.findElement(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")).click();

		locationsPage.ensureEditButtonIsClickable();
		locationsPage.getEditButtons().get(0).click();

		assertEquals("http://localhost:4200/locations/edit/1", browser.getCurrentUrl());

		editLocationPage.ensureEntireFormIsDisplayed();

		editLocationPage.setNameInput("Spens");
		editLocationPage.setAddressInput("Dimitrija Tucovica 1, Novi Sad");
		editLocationPage.setLatitudeInput("45.246912");
		editLocationPage.setLongitudeInput("19.842178");
		editLocationPage.getSaveButton().click();

		editLocationPage.ensureErrorMessageIsDisplayer();
		assertEquals("Location on the same address already exists", editLocationPage.getErrorMessage().getText());

		editLocationPage.setAddressInput("Sutjeska 3, Novi Sad");
		editLocationPage.setLatitudeInput("45.247025");
		editLocationPage.setLongitudeInput("19.845307");
		editLocationPage.getSaveButton().click();

		locationsPage.ensureAddButtonIsClickable();
		assertEquals("Spens", locationsPage.getLocations().get(0).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("Sutjeska 3, Novi Sad", locationsPage.getLocations().get(0).findElement(By.className("card-text")).getText());
	}

	@After
	public void cleanUp() {
		browser.quit();
	}

}
