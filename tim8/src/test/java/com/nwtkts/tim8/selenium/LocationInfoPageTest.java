package com.nwtkts.tim8.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class LocationInfoPageTest {

	private WebDriver browser;
	private LocationsPage locationsPage;
	private LocationInfoPage locationInfoPage;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

		browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.navigate().to("http://localhost:4200/login");

		LoginPage loginPage = PageFactory.initElements(browser, LoginPage.class);

		loginPage.setUsername("admin1");
		loginPage.setPassword("456");

		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));
		browser.findElement(By.cssSelector("div[toast-component]")).click();

		locationsPage = PageFactory.initElements(browser, LocationsPage.class);
		locationInfoPage = PageFactory.initElements(browser, LocationInfoPage.class);
	}

	@Test
	public void viewLocationInfoTest() {
		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")));
		browser.findElement(By.cssSelector("div.sidebar-wrapper > ul > li:nth-child(2)")).click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.col-sm-4.col-lg-3:nth-child(2) a")));
		browser.findElement(By.cssSelector("div.col-sm-4.col-lg-3:nth-child(2) a")).click();

		locationInfoPage.ensureLocationIsLoaded();

		assertEquals("Stadion Karadjordje", locationInfoPage.getNameLabel().getText());
		assertEquals("Address: Dimitrija Tucovica 1, Novi Sad", locationInfoPage.getAddressLabel().getText());
		assertEquals("Vojvodina-Zvezda", locationInfoPage.getEvents().get(0).findElement(By.cssSelector("div.card div.card h4.card-title")).getText());
		assertEquals("20.02.2020", locationInfoPage.getEvents().get(0).findElement(By.cssSelector("div.card div.card p")).getText());
	}

	@After
	public void cleanUp() {
		browser.quit();
	}

}
