package com.nwtkts.tim8.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class RegistrationPageTest {
	private WebDriver browser;

	RegistrationPage registrationPage;
	HomePage homePage;
	LoginPage loginPage;

	@Before
	public void setupSelenium() {
		// instantiate browser
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		browser = new ChromeDriver();
		// maximize window
		browser.manage().window().maximize();
		// navigate
		browser.navigate().to("http://localhost:4200/manifestations");
		
		homePage = PageFactory.initElements(browser, HomePage.class);
		registrationPage = PageFactory.initElements(browser, RegistrationPage.class);
		loginPage = PageFactory.initElements(browser, LoginPage.class);
	}
	
	@Test
	public void testRegistrationUserLoggedIn() {
		homePage.ensureLoginButtonIsDisplayed();
		homePage.getLogInButton().click();
		loginPage.ensureIsDisplayed();
		loginPage.setUsername("user1");
		loginPage.setPassword("123");
		loginPage.getLogInButton().click();
		loginPage.ensureToastrIsDisplayed();
		loginPage.getToastr().click();
		browser.navigate().to("http://localhost:4200/registration");
		assertEquals("http://localhost:4200/manifestations",browser.getCurrentUrl());
	}
	
	@Test
	public void testSignUpEmptyFields() {
		homePage.ensureSignUpButtonIsDisplayed();
		homePage.getSignUpButton().click();

		assertEquals("http://localhost:4200/registration",	browser.getCurrentUrl());
		
		registrationPage.setUsername("");
		registrationPage.setPassword("");
		registrationPage.setRepeatedPassword("");
		registrationPage.setFirstName("");
		registrationPage.setLastName("");
		registrationPage.setEmail("");
		registrationPage.getFirstName().click();
		assertFalse(registrationPage.getRegistrationButton().isEnabled());
		assertTrue(registrationPage.getUsernameError().isDisplayed());
		assertEquals("This field can't be empty!", registrationPage.getUsernameError().getText());
		assertTrue(registrationPage.getPasswordError().isDisplayed());
		assertEquals("This field can't be empty!", registrationPage.getPasswordError().getText());
		assertTrue(registrationPage.getRepeatedPasswordError().isDisplayed());
		assertEquals("This field can't be empty!", registrationPage.getRepeatedPasswordError().getText());
		assertTrue(registrationPage.getFirstNameError().isDisplayed());
		assertEquals("This field can't be empty!", registrationPage.getFirstNameError().getText());
		assertTrue(registrationPage.getLastName().isDisplayed());
		assertEquals("This field can't be empty!", registrationPage.getLastNameError().getText());
		assertTrue(registrationPage.getEmailError().isDisplayed());
		assertEquals("This field can't be empty!", registrationPage.getEmailError().getText());
		
	}
	
	@Test
	public void testSignUpInvalidEmailFormat() {
		homePage.ensureSignUpButtonIsDisplayed();
		homePage.getSignUpButton().click();

		assertEquals("http://localhost:4200/registration",	browser.getCurrentUrl());
		
		registrationPage.setUsername("usermane");
		registrationPage.setPassword("111");
		registrationPage.setRepeatedPassword("145");
		registrationPage.setFirstName("first");
		registrationPage.setEmail("invalidemail");
		registrationPage.setLastName("last");
		assertFalse(registrationPage.getRegistrationButton().isEnabled());
		assertTrue(registrationPage.getEmailError().isDisplayed());
		assertEquals("Invalid email format!", registrationPage.getEmailError().getText());
	}
	
	@Test
	public void testSignUpDifferentPasswords() {
		homePage.ensureSignUpButtonIsDisplayed();
		homePage.getSignUpButton().click();

		assertEquals("http://localhost:4200/registration",	browser.getCurrentUrl());
		
		registrationPage.setUsername("usermane");
		registrationPage.setPassword("111");
		registrationPage.setRepeatedPassword("145");
		registrationPage.setFirstName("first");
		registrationPage.setEmail("newuser@gmail.com");
		registrationPage.setLastName("last");
		
		assertTrue(registrationPage.getRegistrationButton().isEnabled());
		registrationPage.getRegistrationButton().click();
		registrationPage.ensureToastrIsDisplayed();
		assertEquals("Passwords are not the same", registrationPage.getToastr().getText());
	}
	
	@Test
	public void testSignUpUsernameTaken() {
		homePage.ensureSignUpButtonIsDisplayed();
		homePage.getSignUpButton().click();

		assertEquals("http://localhost:4200/registration",	browser.getCurrentUrl());
		
		registrationPage.setUsername("user3");
		registrationPage.setPassword("111");
		registrationPage.setRepeatedPassword("111");
		registrationPage.setFirstName("first");
		registrationPage.setEmail("newuser@gmail.com");
		registrationPage.setLastName("last");
		
		assertTrue(registrationPage.getRegistrationButton().isEnabled());
		registrationPage.getRegistrationButton().click();
		registrationPage.ensureToastrIsDisplayed();
		assertEquals("Username already taken!", registrationPage.getToastr().getText());
	}
	
	@Test
	public void testSignUpEmailTaken() {
		homePage.ensureSignUpButtonIsDisplayed();
		homePage.getSignUpButton().click();

		assertEquals("http://localhost:4200/registration",	browser.getCurrentUrl());
		
		registrationPage.setUsername("newUser");
		registrationPage.setPassword("111");
		registrationPage.setRepeatedPassword("111");
		registrationPage.setFirstName("first");
		registrationPage.setEmail("admin1@gmail.com");
		registrationPage.setLastName("last");
		
		assertTrue(registrationPage.getRegistrationButton().isEnabled());
		registrationPage.getRegistrationButton().click();
		registrationPage.ensureToastrIsDisplayed();
		assertEquals("Email already taken!", registrationPage.getToastr().getText());
	}
	
	@Test
	public void testSignUpOK() {
		homePage.ensureSignUpButtonIsDisplayed();
		homePage.getSignUpButton().click();

		assertEquals("http://localhost:4200/registration",	browser.getCurrentUrl());
		
		registrationPage.setUsername("new_user");
		registrationPage.setPassword("111");
		registrationPage.setRepeatedPassword("111");
		registrationPage.setFirstName("first");
		registrationPage.setEmail("new_user@gmail.com");
		registrationPage.setLastName("last");
		
		assertTrue(registrationPage.getRegistrationButton().isEnabled());
		registrationPage.getRegistrationButton().click();
		registrationPage.ensureVerificationMessageIsDisplayed();
		assertEquals("Please, check your email to verify your account and complete the registration!",
				registrationPage.getEmailVerification().getText());
	}
	
	
	
	@After
	public void closeSelenium() {
		// Shutdown the browser
		browser.quit();
	}
}
