package com.nwtkts.tim8.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;


public class ManifestationsPageTest {

	private WebDriver browser;
	private ManifestationsPage eventsPage;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");

		browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.navigate().to("http://localhost:4200/login");

		LoginPage loginPage = PageFactory.initElements(browser, LoginPage.class);

		loginPage.setUsername("admin1");
		loginPage.setPassword("456");

		loginPage.ensureIsDisplayed();
		loginPage.getLogInButton().click();

		(new WebDriverWait(browser, 10)).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));
		browser.findElement(By.cssSelector("div[toast-component]")).click();

		eventsPage = PageFactory.initElements(browser, ManifestationsPage.class);
	}

	@Test
	public void cancelEventTest() {
		eventsPage.ensureCancelButtonIsClickable();

		assertEquals(4, eventsPage.getEvents().size());
		assertEquals("Koncert godine", eventsPage.getEvents().get(0).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("03.03.2020 - 04.03.2020", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("Concert is awesome", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(3)")).getText());
		assertEquals("Vojvodina-Zvezda", eventsPage.getEvents().get(1).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("20.02.2020", eventsPage.getEvents().get(1).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("Football is awesome", eventsPage.getEvents().get(1).findElement(By.cssSelector("p:nth-child(3)")).getText());
		assertEquals("Festival of culture", eventsPage.getEvents().get(2).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("01.06.2020", eventsPage.getEvents().get(2).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("culture is awesome", eventsPage.getEvents().get(2).findElement(By.cssSelector("p:nth-child(3)")).getText());
		assertEquals("Zabranjeno pusenje - koncert", eventsPage.getEvents().get(3).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("14.02.2020", eventsPage.getEvents().get(3).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("concert is awesome", eventsPage.getEvents().get(3).findElement(By.cssSelector("p:nth-child(3)")).getText());

		int eventCountBefore = eventsPage.getEvents().size();

		eventsPage.getCancelButtons().get(0).click();

		(new WebDriverWait(browser, 10))
		.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div[toast-component]")));
		(new WebDriverWait(browser, 10))
		.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("div.card"), eventCountBefore - 1));
		
		assertEquals(eventCountBefore - 1, eventsPage.getEvents().size());
		assertEquals("Vojvodina-Zvezda", eventsPage.getEvents().get(0).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("20.02.2020", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("Football is awesome", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(3)")).getText());
		assertEquals("Festival of culture", eventsPage.getEvents().get(1).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("01.06.2020", eventsPage.getEvents().get(1).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("culture is awesome", eventsPage.getEvents().get(1).findElement(By.cssSelector("p:nth-child(3)")).getText());
		assertEquals("Zabranjeno pusenje - koncert", eventsPage.getEvents().get(2).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("14.02.2020", eventsPage.getEvents().get(2).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("concert is awesome", eventsPage.getEvents().get(2).findElement(By.cssSelector("p:nth-child(3)")).getText());
	}
	
	@Test
	public void searchNonExistantTest() {
		eventsPage.ensureSearchButtonIsDisplayed();
		
		eventsPage.getSearchInput().sendKeys("jo bre");
		eventsPage.getSearchButton().click();
		
		eventsPage.ensureNoManifTextIsDisplayed();
		
		//test if no events come up
		assertTrue(eventsPage.getEvents().isEmpty());
		assertDoesNotThrow(() -> browser.findElement(By.xpath("//h4[text()='There are no matching events.']")));
	}
	
	@Test
	public void searchAndFilterTest() {
		eventsPage.ensureSearchButtonIsDisplayed();
		
		eventsPage.getSearchInput().sendKeys("u");
		eventsPage.getSearchButton().click();
		
		eventsPage.ensureEventsAreVisible();
		
		//test if 2 events come up
		assertEquals(2, eventsPage.getEvents().size());
		assertEquals("Festival of culture", eventsPage.getEvents().get(0).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("01.06.2020", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("culture is awesome", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(3)")).getText());
		assertEquals("Zabranjeno pusenje - koncert", eventsPage.getEvents().get(1).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("14.02.2020", eventsPage.getEvents().get(1).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("concert is awesome", eventsPage.getEvents().get(1).findElement(By.cssSelector("p:nth-child(3)")).getText());
		
		eventsPage.getShowFiltersButton().click();
		
		eventsPage.ensureCategoriesFilterIsDisplayed();
		eventsPage.selectCategories(new String[]{"category0"});
		
		eventsPage.applyFilters();
		
		//wait for filter to apply
		(new WebDriverWait(browser, 2))
		.until(ExpectedConditions.numberOfElementsToBe(By.cssSelector("div.card"), 1));
		
		//test if 1 event comes up
		assertEquals(1, eventsPage.getEvents().size());
		assertEquals("Festival of culture", eventsPage.getEvents().get(0).findElement(By.cssSelector("h4.card-title")).getText());
		assertEquals("01.06.2020", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(2)")).getText());
		assertEquals("culture is awesome", eventsPage.getEvents().get(0).findElement(By.cssSelector("p:nth-child(3)")).getText());
	}
	
	@Test
	public void filterNonExistantTest() {
		eventsPage.ensureShowFiltersButtonIsDisplayed();
		
		eventsPage.getShowFiltersButton().click();
		
		eventsPage.ensureCategoriesFilterIsDisplayed();
		eventsPage.selectCategories(new String[]{"category0"});
		eventsPage.ensureLocationsFilterIsDisplayed();
		eventsPage.selectLocations(new String[]{"location1", "location2"});
		
		eventsPage.applyFilters();
		
		eventsPage.ensureNoManifTextIsDisplayed();
		
		//test if no events come up
		assertTrue(eventsPage.getEvents().isEmpty());
		assertDoesNotThrow(() -> browser.findElement(By.xpath("//h4[text()='There are no matching events.']")));
	}

	@After
	public void cleanUp() {
		browser.quit();
	}

}
