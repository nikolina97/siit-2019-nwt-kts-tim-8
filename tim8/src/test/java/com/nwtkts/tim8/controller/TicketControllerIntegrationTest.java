package com.nwtkts.tim8.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nwtkts.tim8.dto.SeatDateDTO;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TicketControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private ObjectMapper mapper;

	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	@Test
	@Transactional
	public void buyTicketsTest_nonExistingSeat() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(150);

		mockMvc.perform(post("/api/ticket/buyTickets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(new SeatDateDTO(ids, "2020-12-26 00:00:00")))).andExpect(status().isInternalServerError()).andExpect(content().string("Invalid seats"));
	}

	@Test
	@Transactional
	public void buyTicketsTest_manifestationStateIsNotOpen() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(23);

		mockMvc.perform(post("/api/ticket/buyTickets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(new SeatDateDTO(ids, "2019-12-26 00:00:00")))).andExpect(status().isInternalServerError()).andExpect(content().string("Manifestation is not available for buying tickets."));
	}

	@Test
	@Transactional
	@Ignore
	public void buyTicketsTest_invalidDateFormat() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(20);

		mockMvc.perform(post("/api/ticket/buyTickets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(new SeatDateDTO(ids, "2020-12-45")))).andExpect(status().isOk()).andExpect(content().string("true"));
	}

	@Test
	@Transactional
	public void buyTicketsTest_someoneElsesReservedSeat() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(3);

		mockMvc.perform(post("/api/ticket/buyTickets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(new SeatDateDTO(ids, "2020-07-04 00:00:00")))).andExpect(status().isInternalServerError()).andExpect(content().string("Seat is already reserved or bought."));
	}

	@Test
	@Transactional
	@Ignore
	public void buyTicketsTest_ok() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(28);
		ids.add(29);

		mockMvc.perform(post("/api/ticket/buyTickets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(new SeatDateDTO(ids, "2020-07-03 00:00:00")))).andExpect(status().isOk());
	}

	@Test
	@Transactional
	@Ignore
	public void buyTicketsTest_festivalTickets() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(28);
		ids.add(29);

		mockMvc.perform(post("/api/ticket/buyTickets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(new SeatDateDTO(ids, null)))).andExpect(status().isOk());
	}

	@Test
	@Transactional
	public void buyTicketsTest_noLongerPossibleToReserve() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(10);

		mockMvc.perform(post("/api/ticket/buyTickets").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(new SeatDateDTO(ids, "2019-12-02 00:00:00")))).andExpect(status().isInternalServerError()).andExpect(content().string("Manifestation is not available for buying tickets."));
	}

	@Test
	@Transactional
	public void getAllBoughtTicketsByUserTest() throws Exception {
		mockMvc.perform(get("/api/ticket/getAllBoughtTicketsByUser")).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()").value(3))
				.andExpect(jsonPath("$[0].row").value(1))
				.andExpect(jsonPath("$[0].column").value(2))
				.andExpect(jsonPath("$[0].manifestationId").value(1))
				.andExpect(jsonPath("$[0].ticketPrice").value(150))
				.andExpect(jsonPath("$[0].manifestationName").value("Exit"))
				.andExpect(jsonPath("$[0].seatCategoryName").value("Tribina 1"))
				.andExpect(jsonPath("$[0].startDate").value("2020-07-01T22:00:00.000+0000"))
				.andExpect(jsonPath("$[0].endDate").value("2020-07-04T22:00:00.000+0000"))
				.andExpect(jsonPath("$[1].row").value(2))
				.andExpect(jsonPath("$[1].column").value(2))
				.andExpect(jsonPath("$[1].manifestationId").value(1))
				.andExpect(jsonPath("$[1].ticketPrice").value(150))
				.andExpect(jsonPath("$[1].manifestationName").value("Exit"))
				.andExpect(jsonPath("$[1].seatCategoryName").value("Tribina 1"))
				.andExpect(jsonPath("$[1].startDate").value("2020-07-03T22:00:00.000+0000"))
				.andExpect(jsonPath("$[1].endDate").value("2020-07-03T22:00:00.000+0000"))
				.andExpect(jsonPath("$[2].row").value(1))
				.andExpect(jsonPath("$[2].column").value(2))
				.andExpect(jsonPath("$[2].manifestationId").value(2))
				.andExpect(jsonPath("$[2].ticketPrice").value(200))
				.andExpect(jsonPath("$[2].manifestationName").value("Cola"))
				.andExpect(jsonPath("$[2].seatCategoryName").value("Tribina 1"))
				.andExpect(jsonPath("$[2].startDate").value("2019-12-01T23:00:00.000+0000"))
				.andExpect(jsonPath("$[2].endDate").value("2019-12-01T23:00:00.000+0000"));
	}

	@Test
	@Transactional
	public void getAllReservedTicketsByUserTest() throws Exception {
		mockMvc.perform(get("/api/ticket/getAllReservedTicketsByUser")).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()").value(4))
				.andExpect(jsonPath("$[0].row").value(1))
				.andExpect(jsonPath("$[0].column").value(1))
				.andExpect(jsonPath("$[0].manifestationId").value(1))
				.andExpect(jsonPath("$[0].ticketPrice").value(150))
				.andExpect(jsonPath("$[0].manifestationName").value("Exit"))
				.andExpect(jsonPath("$[0].seatCategoryName").value("Tribina 1"))
				.andExpect(jsonPath("$[0].startDate").value("2020-07-02T22:00:00.000+0000"))
				.andExpect(jsonPath("$[0].endDate").value("2020-07-02T22:00:00.000+0000"))
				.andExpect(jsonPath("$[1].row").value(1))
				.andExpect(jsonPath("$[1].column").value(1))
				.andExpect(jsonPath("$[1].manifestationId").value(1))
				.andExpect(jsonPath("$[1].ticketPrice").value(150))
				.andExpect(jsonPath("$[1].manifestationName").value("Exit"))
				.andExpect(jsonPath("$[1].seatCategoryName").value("Tribina 1"))
				.andExpect(jsonPath("$[1].startDate").value("2020-07-01T22:00:00.000+0000"))
				.andExpect(jsonPath("$[1].endDate").value("2020-07-01T22:00:00.000+0000"))
				.andExpect(jsonPath("$[2].row").value(1))
				.andExpect(jsonPath("$[2].column").value(1))
				.andExpect(jsonPath("$[2].manifestationId").value(1))
				.andExpect(jsonPath("$[2].ticketPrice").value(150))
				.andExpect(jsonPath("$[2].manifestationName").value("Exit"))
				.andExpect(jsonPath("$[2].seatCategoryName").value("Tribina 1"))
				.andExpect(jsonPath("$[2].startDate").value("2020-07-03T22:00:00.000+0000"))
				.andExpect(jsonPath("$[2].endDate").value("2020-07-03T22:00:00.000+0000"))
				.andExpect(jsonPath("$[3].row").value(1))
				.andExpect(jsonPath("$[3].column").value(1))
				.andExpect(jsonPath("$[3].manifestationId").value(1))
				.andExpect(jsonPath("$[3].ticketPrice").value(150))
				.andExpect(jsonPath("$[3].manifestationName").value("Exit"))
				.andExpect(jsonPath("$[3].seatCategoryName").value("Tribina 1"))
				.andExpect(jsonPath("$[3].startDate").value("2020-07-04T22:00:00.000+0000"))
				.andExpect(jsonPath("$[3].endDate").value("2020-07-04T22:00:00.000+0000"));
	}
	
	@Test
	@Transactional
	public void payTicketTest_ok() throws Exception {
		mockMvc.perform(put("/api/ticket/payTicket/1"))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.status").value("success"));
	}
	
	@Test
	public void payTicketTest_wrongTicket() throws Exception {
		mockMvc.perform(put("/api/ticket/payTicket/13"))
		.andExpect(status().isNotFound());
	}
	
	@Test
	@Transactional
	public void markAsPaidTest_ok() throws Exception {
		mockMvc.perform(get("/api/ticket/markAsPaid/1"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void markAsPaidTest_wrongTickets() throws Exception {
		mockMvc.perform(get("/api/ticket/markAsPaid/13"))
		.andExpect(status().isNotFound());
	}
	
	@Test
	@Transactional
	public void deleteTickets_ok() throws Exception {
		mockMvc.perform(get("/api/ticket/deleteTickets/1-5"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void deleteTickets_wrongTickets() throws Exception {
		mockMvc.perform(get("/api/ticket/deleteTickets/13-14"))
		.andExpect(status().isBadRequest());
	}

}
