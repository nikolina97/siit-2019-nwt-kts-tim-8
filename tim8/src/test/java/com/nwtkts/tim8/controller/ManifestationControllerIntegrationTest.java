package com.nwtkts.tim8.controller;

import com.nwtkts.tim8.dto.LocationDTO;
import com.nwtkts.tim8.dto.MainLocationInfoDTO;
import com.nwtkts.tim8.dto.MainManifestationInfoDTO;
import com.nwtkts.tim8.dto.ManifestationDTO;
import com.nwtkts.tim8.dto.ManifestationInfoDTO;
import com.nwtkts.tim8.dto.SearchManifestationDTO;
import com.nwtkts.tim8.dto.SupportedSeatCategoryDTO;
import com.nwtkts.tim8.dto.UpdateManifestationDTO;
import com.nwtkts.tim8.model.ManifestationCategory;
import com.nwtkts.tim8.model.ManifestationState;
import com.nwtkts.tim8.repository.LocationRepository;
import com.nwtkts.tim8.repository.SupportedSeatCategoryRepository;
import com.nwtkts.tim8.security.auth.JwtAuthenticationRequest;
import com.nwtkts.tim8.service.InvalidDataException;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ManifestationControllerIntegrationTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype());

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
    private TestRestTemplate testRestTemplate;
	
	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private SupportedSeatCategoryRepository sscRepository;
	
	private String[] manifestationNames;
	private String[] manifestationDescriptions;
	private Timestamp[] manifestationStartDates;
	private Timestamp[] manifestationEndDates;
	private String[] manifestationImages;
	
	public void logInAsAdmin() {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken("admin1", "456"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	public void logInAsRegUser() {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	String token;
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken("admin1", "456"));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		JwtAuthenticationRequest loginDto = new JwtAuthenticationRequest("admin1", "456");
        ResponseEntity<String> response = testRestTemplate.postForEntity("/auth/login", loginDto, String.class);
        token = response.getBody();
		
		
		manifestationNames = new String[] {
				"Exit", "Cola", "Zvezda-Bajern", 
				"RHCP", "ACDC", "Exit 2.0", 
				"Superbovl", "Logan vs KSI", "LCS", 
				"VidCon", "Sajam Tambure", "Megdan"
		};
		
		manifestationDescriptions = new String[] {
				"Exit is awesome", "Cola is awesome", "Fudbal is awesome", 
				"RHCP is awesome", "ACDC is awesome", "Exit is awesome", 
				"Superbovl is awesome", "Boks is awesome", "Esports is awesome", 
				"Youtube is awesome", "Tambura is awesome", "Megdan is awesome"
		};
		
		manifestationStartDates = new Timestamp[] {
				Timestamp.valueOf("2020-07-02 00:00:00"), Timestamp.valueOf("2019-12-02 00:00:00"), Timestamp.valueOf("2019-12-26 00:00:00"), 
				Timestamp.valueOf("2020-12-26 00:00:00"), Timestamp.valueOf("2021-09-06 00:00:00"), Timestamp.valueOf("2020-09-06 00:00:00"), 
				Timestamp.valueOf("2020-09-15 00:00:00"), Timestamp.valueOf("2020-10-15 00:00:00"), Timestamp.valueOf("2020-11-15 00:00:00"), 
				Timestamp.valueOf("2020-11-17 00:00:00"), Timestamp.valueOf("2020-06-09 00:00:00"), Timestamp.valueOf("2020-08-08 00:00:00")
		};
		
		manifestationEndDates = new Timestamp[] {
				Timestamp.valueOf("2020-07-05 00:00:00"), Timestamp.valueOf("2019-12-03 00:00:00"), Timestamp.valueOf("2019-12-26 00:00:00"), 
				Timestamp.valueOf("2020-12-26 00:00:00"), Timestamp.valueOf("2020-09-06 00:00:00"), Timestamp.valueOf("2020-12-06 00:00:00"), 
				Timestamp.valueOf("2020-09-15 00:00:00"), Timestamp.valueOf("2020-10-15 00:00:00"), Timestamp.valueOf("2020-11-17 00:00:00"), 
				Timestamp.valueOf("2020-11-19 00:00:00"), Timestamp.valueOf("2020-06-13 00:00:00"), Timestamp.valueOf("2020-08-10 00:00:00")
		};
		
		manifestationImages = new String[] {
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg"
		};
	}

	@Test
	public void findById_ok() {
		String url = "/api/manifestations/1";
		ResponseEntity<ManifestationInfoDTO> responseEntity = testRestTemplate.exchange(url, HttpMethod.GET, null, ManifestationInfoDTO.class);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		ManifestationInfoDTO manifestation = responseEntity.getBody();
		int myDay = 02;
		int myMonth = 07;
		int myYear = 2020;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
                myYear, myMonth, myDay));
		myDay = 05;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
                myYear, myMonth, myDay));
		assertEquals(4,manifestation.getDates().size());
		assertEquals(start, manifestation.getDates().get(0));
		assertEquals(end, manifestation.getDates().get(manifestation.getDates().size()-1));
		assertEquals(Integer.valueOf(7), manifestation.getDaysBeforeExpires());
		assertEquals("Exit is awesome", manifestation.getDescription());
		assertEquals("http://localhost:8080/mob_EXIT2-6.jpg", manifestation.getImage());
		assertEquals(Integer.valueOf(1), manifestation.getLocation().getLocationId());
		assertEquals(ManifestationCategory.ENTERTAINMENT, manifestation.getManifestationCategory());
		assertEquals(Integer.valueOf(1), manifestation.getManifestationId());
		assertEquals(ManifestationState.OPEN, manifestation.getManifestationState());
		assertEquals("Exit", manifestation.getName());
	}
	
	@Test
	public void findById_notFound() {
		String url = "/api/manifestations/64";
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.GET, null, String.class);
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("Manifestation not found", responseEntity.getBody());
	}
	
	@Test
	@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
	public void saveManifestation_ok() throws Exception {
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(1, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,1,"Name", "Zanimljiva", null, null, new Timestamp(2020, 10, 12, 0, 0, 0, 0),new Timestamp(2020, 10, 13, 0, 0, 0, 0),ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<ManifestationInfoDTO> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, ManifestationInfoDTO.class);
		Integer id = 13;
		ManifestationInfoDTO manifInfo = responseEntity.getBody();
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(manifDTO.getDescription(), manifInfo.getDescription());
		assertEquals(manifDTO.getLocationId(), manifInfo.getLocation().getLocationId());
		assertEquals(manifDTO.getCategory(), manifInfo.getManifestationCategory());
		assertEquals(manifDTO.getState(), manifInfo.getManifestationState());
		assertEquals(manifDTO.getName(), manifInfo.getName());
		assertEquals(manifDTO.getStartDate(), manifInfo.getDates().get(0));
		assertEquals(manifDTO.getEndDate(), manifInfo.getDates().get(manifInfo.getDates().size()-1));
		assertEquals(id, manifInfo.getManifestationId());
	}
	
	@Test
	public void saveManifestation_badRequestWrongDate() throws Exception {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(1, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,1,"Name", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Some other event will be held at this time", responseEntity.getBody());
	}
	
	@Test
	public void saveManifestation_badRequestMissingData() throws Exception {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(1, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,1, null, "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Some data is missing", responseEntity.getBody());
	}
	
	@Test
	public void saveManifestation_badRequestFound_emptyName() throws Exception {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(2, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(2,1, "", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Name can't be empty", responseEntity.getBody());
	}
	
	@Test
	public void saveManifestation_badRequestFound_emptyDescription() throws Exception {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(2, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(2,1, "Ime", "", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Description can't be empty", responseEntity.getBody());
	}
	
	@Test
	public void saveManifestation_badRequestFound_allowedReservationsWrongDate() throws Exception {
		int myYear = 2020;
		int myMonth = 02;
		int myDay = 13;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(2, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(10,1, "Ime", "Opis", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("The date when the reservations are no longer allowed cannot be earlier than today", responseEntity.getBody());
	}
	
	@Test
	public void saveManifestation_badRequestFound_negativeExpirationDays() throws Exception {
		int myYear = 2020;
		int myMonth = 02;
		int myDay = 13;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(2, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(-7,1, "Ime", "Opis", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Number of days after which the possibility of reservation expires can't be less than -1", responseEntity.getBody());
	}
	
	@Test
	public void saveManifestation_badRequestFound_endDateBeforeStart() throws Exception {
		int myYear = 2020;
		int myMonth = 02;
		int myDay = 17;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		myDay = 15;
		Timestamp tsEnd = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(3, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(-1,1, "Ime", "Opis", null, null, ts, tsEnd,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("End date can't be before start date", responseEntity.getBody());
	}
	
	@Test
	public void saveManifestation_notFound_noSeatCategory() throws Exception {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(200, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,1, "Ime", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("Seat category with id 200 not found", responseEntity.getBody());
	}
	
	@Test
	public void saveManifestation_notFound_noLocation() throws Exception {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 10;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		String url = "/api/manifestations";
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(1, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,99,"Name", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<ManifestationDTO> httpEntity = new HttpEntity<ManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("Location not found", responseEntity.getBody());
	}
	
	@Test
	@Transactional
	public void updateManifestation_ok() throws Exception {
		String url = "/api/manifestations/1";
		UpdateManifestationDTO manifDTO =
				new UpdateManifestationDTO("Ime", "Opis");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<UpdateManifestationDTO> httpEntity = new HttpEntity<UpdateManifestationDTO>(manifDTO, headers);
		ResponseEntity<ManifestationInfoDTO> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, ManifestationInfoDTO.class);
		ManifestationInfoDTO manifInfo = responseEntity.getBody();
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(manifDTO.getDescription(), manifInfo.getDescription());
		assertEquals(Integer.valueOf(1), manifInfo.getManifestationId());
	}
	
	@Test
	public void updateManifestation_badRequest_canceledManifestation() throws Exception {
		String url = "/api/manifestations/4";
		UpdateManifestationDTO manifDTO =
				new UpdateManifestationDTO("Ime", "Opis");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<UpdateManifestationDTO> httpEntity = new HttpEntity<UpdateManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("You can't edit canceled manifestation", responseEntity.getBody());
	}
	
	@Test
	public void updateManifestation_badRequest_pastManifestation() throws Exception {
		String url = "/api/manifestations/3";
		UpdateManifestationDTO manifDTO =
				new UpdateManifestationDTO("Ime", "Opis");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<UpdateManifestationDTO> httpEntity = new HttpEntity<UpdateManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("You can't edit manifestation that has passed", responseEntity.getBody());
	}
	
	@Test
	public void updateManifestation_badRequest_missingData() throws Exception {
		String url = "/api/manifestations/1";
		UpdateManifestationDTO manifDTO =
				new UpdateManifestationDTO("", null);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<UpdateManifestationDTO> httpEntity = new HttpEntity<UpdateManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Some data is missing", responseEntity.getBody());
	}
	
	@Test
	public void updateManifestation_badRequest_emptyName() throws Exception {
		String url = "/api/manifestations/1";
		UpdateManifestationDTO manifDTO =
				new UpdateManifestationDTO("", "Opis");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<UpdateManifestationDTO> httpEntity = new HttpEntity<UpdateManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Name can't be empty", responseEntity.getBody());
	}
	
	@Test
	public void updateManifestation_badRequest_emptyDescription() throws Exception {
		String url = "/api/manifestations/1";
		UpdateManifestationDTO manifDTO =
				new UpdateManifestationDTO("Ime", "");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<UpdateManifestationDTO> httpEntity = new HttpEntity<UpdateManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Description can't be empty", responseEntity.getBody());
	}
	
	@Test
	public void updateManifestationNotFound() throws Exception {
		String url = "/api/manifestations/125";
		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("", "");
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<UpdateManifestationDTO> httpEntity = new HttpEntity<UpdateManifestationDTO>(manifDTO, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("Manifestation not found", responseEntity.getBody());
	}
	
	@Test
	public void findAllTest_successful() throws Exception {
		
		String url = String.format("/api/manifestations?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.GET, null, MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findAllTest_simplePageable() throws Exception {
		
		String url = String.format("/api/manifestations?page=%d&size=%d", 1, 4);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.GET, null, MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8, 9, 10, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findAllTest_tooFarPageable() throws Exception {
		
		String url = String.format("/api/manifestations?page=%d&size=%d", 3, 4);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.GET, null, MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 0);
	}
	
	@Test
	public void findTest_emptyParams() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_nullParams() {
		String name = null;
		List<String> categories = null;
		List<String> locations = null;
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_simpleName() {
		String name = "i";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 6, 8, 10
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_nonexistantName() {
		String name = "prodigy";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);

		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(manifestations.size(), 0);
	}
	
	@Test
	public void findTest_singleLocation() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Petrovaradin"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);

		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 3);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 6, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_multiLocation() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Petrovaradin", "Beograd"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 6);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 8, 9, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_singleCategory() {
		String name = "";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport"}));
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);

		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 3);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				7, 8, 9
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_multiCategory() {
		String name = "";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport", "Culture"}));
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);

		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 6);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				5, 7, 8, 9, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_allParams() {
		String name = "vs";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport"}));
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Beograd"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);

		String url = String.format("/api/manifestations/search?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 1);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_simplePageable() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);

		String url = String.format("/api/manifestations/search?page=%d&size=%d", 1, 4);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8, 9, 10, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_tooFarPageable() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		String url = String.format("/api/manifestations/search?page=%d&size=%d", 3, 4);
		
		ResponseEntity<MainManifestationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.POST, new HttpEntity<SearchManifestationDTO>(dto), MainManifestationInfoDTO[].class);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(manifestations.size(), 0);
	}

	@Test
	public void getManifestationInfoTest_nonExisting() throws Exception {
		logInAsRegUser();
		mockMvc.perform(get("/api/manifestations/getManifestationInfo/150")).andExpect(status().isBadRequest())
				.andExpect(content().string("Invalid manifestation Id"));
	}

	@Test
	public void getManifestationInfoTest_ok() throws Exception {
		logInAsRegUser();
		mockMvc.perform(get("/api/manifestations/getManifestationInfo/1")).andExpect(status().isOk())
				.andExpect(jsonPath("$.manifestationId").value(1))
				.andExpect(jsonPath("$.name").value("Exit"))
				.andExpect(jsonPath("$.location.latitude").value(12))
				.andExpect(jsonPath("$.location.longitude").value(12))
				.andExpect(jsonPath("$.manifestationState").value("OPEN"))
				.andExpect(jsonPath("$.manifestationCategory").value("ENTERTAINMENT"));
	}
	
	@Test
	@Transactional
	@Rollback
	@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
	public void cancelTest_successful() throws InvalidDataException {
		Integer manifestationId = 1;
		String url = "/api/manifestations/cancel/" + manifestationId;
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<Boolean> httpEntity = new HttpEntity<Boolean>(null, headers);
		
		ResponseEntity<Boolean> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, Boolean.class);
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(true, responseEntity.getBody());
	}
	
	@Test
	@Transactional
	@Rollback
	//@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
	public void cancelTest_manifestationNotFound() throws InvalidDataException {
		Integer manifestationId = 100;
		String url = "/api/manifestations/cancel/" + manifestationId;
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<Boolean> httpEntity = new HttpEntity<Boolean>(null, headers);
		
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("Manifestation not found", responseEntity.getBody());
	}
	
	@Test
	@Transactional
	@Rollback
	//@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
	public void cancelTest_manifestationPassed() throws InvalidDataException {
		Integer manifestationId = 2;
		String url = "/api/manifestations/cancel/" + manifestationId;
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<Boolean> httpEntity = new HttpEntity<Boolean>(null, headers);
		
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Manifestation has passed", responseEntity.getBody());
	}
	
	@Test
	@Transactional
	@Rollback
	//@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
	public void cancelTest_manifestationAlreadyCanceled() throws InvalidDataException {
		Integer manifestationId = 4;
		String url = "/api/manifestations/cancel/" + manifestationId;
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<Boolean> httpEntity = new HttpEntity<Boolean>(null, headers);
		
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Manifestation has already been canceled", responseEntity.getBody());
	}
	
	@Test
	public void dailyReportTest_ok() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportDailyManifestation/1/01-07-2019/01-01-2020"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(3))
					.andExpect(jsonPath("$", Matchers.hasEntry("13-12-2019", 1)))
					.andExpect(jsonPath("$", Matchers.hasEntry("11-12-2019", 1)))
					.andExpect(jsonPath("$", Matchers.hasEntry("10-12-2019", 1)));
										
	}
	
	@Test
	public void dailyReportTest_empty() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportDailyManifestation/2/02-01-2018/08-01-2019"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(0));
										
	}
	
	@Test
	public void dailyReportTest_invalidManifestation() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportDailyManifestation/95/02-01-2018/08-01-2019"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Manifestation not found."));
										
	}
	
	@Test
	public void dailyReportTest_invalidDate() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportDailyManifestation/2/02-12-2020/01-01-2019"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Invalid date!"));
		
										
	}
	
	@Test
	public void weeklyReportTest_ok() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportWeeklyManifestation/2/02-06-2019/08-01-2020"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(2))
					.andExpect(jsonPath("$", Matchers.hasEntry("Oct 2019 week: 3", 2)))
					.andExpect(jsonPath("$", Matchers.hasEntry("Nov 2019 week: 1", 1)));
										
	}
	

	@Test
	public void weeklyReportTest_empty() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportWeeklyManifestation/2/02-01-2018/08-01-2019"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(0));
										
	}
	
	@Test
	public void weeklyReportTest_invalidManifestation() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportWeeklyManifestation/65/02-01-2018/08-01-2019"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Manifestation not found."));
		
										
	}
	
	@Test
	public void weeklyReportTest_invalidDate() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportWeeklyManifestation/2/02-12-2020/01-01-2020"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Invalid date!"));
											
	}
	
	@Test
	public void monthlyReportTest_ok() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportMonthlyManifestation/2/02-06-2019/08-01-2020"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(2))
					.andExpect(jsonPath("$", Matchers.hasEntry("Oct 2019", 2)))
					.andExpect(jsonPath("$", Matchers.hasEntry("Nov 2019", 1)));
										
	}
	

	@Test
	public void monthlyReportTest_empty() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportMonthlyManifestation/2/02-01-2018/08-01-2019"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(0));
										
	}
	
	@Test
	public void monthlyReportTest_invalidManifestation() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportMonthlyManifestation/75/02-01-2018/08-01-2019"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Manifestation not found."));
		
										
	}
	
	@Test
	public void monthlyReportTest_invalidDate() throws Exception {
		logInAsAdmin();
		this.mockMvc.perform(get("/api/reportMonthlyManifestation/2/02-12-2020/01-01-2020"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Invalid date!"));
											
	}
	
}
