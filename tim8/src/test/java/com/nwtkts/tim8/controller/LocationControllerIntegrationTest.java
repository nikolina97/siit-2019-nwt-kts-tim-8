package com.nwtkts.tim8.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.nwtkts.tim8.TestUtil;
import com.nwtkts.tim8.dto.LocationDTO;
import com.nwtkts.tim8.dto.MainLocationInfoDTO;
import com.nwtkts.tim8.dto.MainManifestationInfoDTO;
import com.nwtkts.tim8.dto.UserChangePasswordDTO;
import com.nwtkts.tim8.model.Location;
import com.nwtkts.tim8.model.SeatCategory;
import com.nwtkts.tim8.model.User;
import com.nwtkts.tim8.security.auth.JwtAuthenticationRequest;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class LocationControllerIntegrationTest {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype());

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
    private TestRestTemplate testRestTemplate;
	
	private String[] locationNames;
	private String[] locationAddresses;
	private Double[] locationLatitudes;
	private Double[] locationLongitudes;
	private Integer[] locationManifestationCounts;
	
	String token;
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken("admin1", "456"));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		JwtAuthenticationRequest loginDto = new JwtAuthenticationRequest("admin1", "456");
        ResponseEntity<String> response = testRestTemplate.postForEntity("/auth/login", loginDto, String.class);
        token = response.getBody();
		
		locationNames = new String[] {
				"Djava", "Marakana", "Cair", "JNA"
		};

		locationAddresses = new String[] {
				"Petrovaradin", "Beograd", "Nis", "Beograd"
		};
		
		locationLatitudes = new Double[] {
				12.0, 25.0, 25.5, 41.1
		};
		
		locationLongitudes = new Double[] {
				12.0, 25.0, 25.5, 20.0
		};
		
		locationManifestationCounts = new Integer[] {
				3, 3, 3, 0
		};
	}

	@Test
	public void getLocationsTest() throws Exception {
		mockMvc.perform(get("/api/locations")).andExpect(status().isOk());
	}

	@Test
	public void findById_ok() {
		String url = "/api/locations/1";
		ResponseEntity<MainLocationInfoDTO> responseEntity = testRestTemplate.exchange(url, HttpMethod.GET, null, MainLocationInfoDTO.class);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		MainLocationInfoDTO location = responseEntity.getBody();
		assertEquals("Petrovaradin",location.getAddress());
		assertEquals(Double.valueOf(12),location.getLatitude());
		assertEquals(Double.valueOf(12),location.getLongitude());
		assertEquals(Integer.valueOf(1),location.getLocationId());
		assertEquals("Djava",location.getName());
		assertEquals(Integer.valueOf(3),location.getManifestationCount());
	}
	
	@Test
	public void findById_notFound() {
		String url = "/api/locations/104";
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.GET, null, String.class);
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("Location not found", responseEntity.getBody());
	}
	
	@Test
	@Transactional
	public void saveLocation_ok() throws Exception {
		String url = "/api/locations";
		LocationDTO location = new LocationDTO("Spens", "Liman", 41.22, 19.55, new HashSet<SeatCategory>());
		Integer id = 5;
		Integer manifCount = 0;
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<LocationDTO> httpEntity = new HttpEntity<LocationDTO>(location, headers);
		ResponseEntity<MainLocationInfoDTO> responseEntity = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, MainLocationInfoDTO.class);
		MainLocationInfoDTO responseLocation = responseEntity.getBody();
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(id, responseLocation.getLocationId());
		assertEquals(location.getName(), responseLocation.getName());
		assertEquals(location.getAddress(), responseLocation.getAddress());
		assertEquals(location.getLatitude(), responseLocation.getLatitude());
		assertEquals(location.getLongitude(), responseLocation.getLongitude());
		assertEquals(manifCount, responseLocation.getManifestationCount());
	}

	@Test
	public void saveLocation_badRequest_wrongDate() throws Exception {
		String url = "/api/locations";
		LocationDTO location = new LocationDTO("Cair2", "Nis", 25.5, 25.5, new HashSet<SeatCategory>());

		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<LocationDTO> httpEntity = new HttpEntity<LocationDTO>(location, headers);
		ResponseEntity<String> response = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		String message = response.getBody();
		assertEquals("Location on the same address already exists", message);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
	public void saveLocation_badRequest_missingData() throws Exception {
		String url = "/api/locations";
		LocationDTO location = new LocationDTO();
		location.setName("Ime1");
		location.setLatitude(20.3);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<LocationDTO> httpEntity = new HttpEntity<LocationDTO>(location, headers);
		ResponseEntity<String> response = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		String message = response.getBody();
		assertEquals("Some data is missing", message);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
	public void saveLocation_badRequest_emptyName() throws Exception {
		String url = "/api/locations";
		LocationDTO location = new LocationDTO("", "Liman", 41.22, 19.55, new HashSet<SeatCategory>());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<LocationDTO> httpEntity = new HttpEntity<LocationDTO>(location, headers);
		ResponseEntity<String> response = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		String message = response.getBody();
		assertEquals("Name can't be empty", message);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}
	
	@Test
	public void saveLocation_badRequest_emptyAddress() throws Exception {
		String url = "/api/locations";
		LocationDTO location = new LocationDTO("Spens", "", 41.22, 19.55, new HashSet<SeatCategory>());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<LocationDTO> httpEntity = new HttpEntity<LocationDTO>(location, headers);
		ResponseEntity<String> response = testRestTemplate.exchange(url, HttpMethod.POST, httpEntity, String.class);
		String message = response.getBody();
		assertEquals("Address can't be empty", message);
		assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
	}

	@Test
	@Transactional
	public void updateLocation_ok() throws Exception {
		Integer id = 2;
		MainLocationInfoDTO location = new MainLocationInfoDTO(id, "Spens", "Petrovaradin", 42.15, 20.33, null);
		String url = "/api/locations/"+ id;
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<MainLocationInfoDTO> httpEntity = new HttpEntity<MainLocationInfoDTO>(location, headers);
		ResponseEntity<MainLocationInfoDTO> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, MainLocationInfoDTO.class);
		MainLocationInfoDTO responseLocation = responseEntity.getBody();
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(location.getLocationId(), responseLocation.getLocationId());
		assertEquals(location.getName(), responseLocation.getName());
		assertEquals(location.getAddress(), responseLocation.getAddress());
		assertEquals(location.getLatitude(), responseLocation.getLatitude());
		assertEquals(location.getLongitude(), responseLocation.getLongitude());
	}
	
	@Test
	public void updateLocation_notFound() throws Exception {
		MainLocationInfoDTO location = new MainLocationInfoDTO(55, "Spens", "Petrovaradin", (double)12, (double)12, null);
		String url = "/api/locations/"+ location.getLocationId().intValue();
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<MainLocationInfoDTO> httpEntity = new HttpEntity<MainLocationInfoDTO>(location, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("Location not found", responseEntity.getBody());
	}
	
	@Test
	public void updateLocation_badRequest_addressTaken() throws Exception {
		MainLocationInfoDTO location = new MainLocationInfoDTO(2, "Spens", "Petrovaradin", (double)12, (double)12, null);		
		String url = "/api/locations/"+ location.getLocationId().intValue();
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<MainLocationInfoDTO> httpEntity = new HttpEntity<MainLocationInfoDTO>(location, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Location on the same address already exists", responseEntity.getBody());
	}
	
	@Test
	public void updateLocation_badRequest_missingFields() throws Exception {
		MainLocationInfoDTO location = new MainLocationInfoDTO();
		location.setLatitude(55.0);
		location.setName("Name");
		location.setLocationId(3);
		String url = "/api/locations/"+ location.getLocationId().intValue();
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<MainLocationInfoDTO> httpEntity = new HttpEntity<MainLocationInfoDTO>(location, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Some data is missing", responseEntity.getBody());
	}
	
	@Test
	public void updateLocation_badRequest_emptyName() throws Exception {
		MainLocationInfoDTO location = new MainLocationInfoDTO(1, "", "Sombor", 15.6, 42.3, 5);
		String url = "/api/locations/"+ location.getLocationId().intValue();
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<MainLocationInfoDTO> httpEntity = new HttpEntity<MainLocationInfoDTO>(location, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Name can't be empty", responseEntity.getBody());
	}
	
	@Test
	public void updateLocation_badRequest_emptyAddress() throws Exception {
		MainLocationInfoDTO location = new MainLocationInfoDTO(1, "Sportska hala", "", 15.6, 42.3, 5);
		String url = "/api/locations/"+ location.getLocationId().intValue();
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<MainLocationInfoDTO> httpEntity = new HttpEntity<MainLocationInfoDTO>(location, headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Address can't be empty", responseEntity.getBody());
	}
	
	@Test
	@Transactional
	public void deleteLocation_ok() throws Exception {
		String url = "/api/locations/4";
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<?> httpEntity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.DELETE, httpEntity, String.class);
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals("Location deleted", responseEntity.getBody());
	}
	
	@Test
	public void deleteLocation_badRequest() throws Exception {
		String url = "/api/locations/1";
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<?> httpEntity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.DELETE, httpEntity, String.class);
		assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
		assertEquals("Some manifestations will be held on this location", responseEntity.getBody());
	}
	
	@Test
	public void deleteLocation_notFound() throws Exception {
		String url = "/api/locations/100";
		HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + this.token);
		HttpEntity<?> httpEntity = new HttpEntity<>(headers);
		ResponseEntity<String> responseEntity = testRestTemplate.exchange(url, HttpMethod.DELETE, httpEntity, String.class);
		assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
		assertEquals("Location not found", responseEntity.getBody());
	}
	
	@Test
	public void findAll_successful() {
		String url = String.format("/api/locations?page=%d&size=%d", 0, 10);
		
		ResponseEntity<MainLocationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.GET, null, MainLocationInfoDTO[].class);
		List<MainLocationInfoDTO> locations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				locations.stream().map(m -> m.getLocationId()).collect(Collectors.toList()));
		
		assertEquals(locations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 2, 3, 4
		}))));
		
		for (MainLocationInfoDTO location : locations) {
			assertEquals(location.getName(), locationNames[location.getLocationId()-1]);
			assertEquals(location.getAddress(), locationAddresses[location.getLocationId()-1]);
			assertEquals(location.getLatitude(), locationLatitudes[location.getLocationId()-1]);
			assertEquals(location.getLongitude(), locationLongitudes[location.getLocationId()-1]);
			assertEquals(location.getManifestationCount(), locationManifestationCounts[location.getLocationId()-1]);
		}
	}
	
	@Test
	public void findAll_simplePageable() {
		String url = String.format("/api/locations?page=%d&size=%d", 1, 2);
		
		ResponseEntity<MainLocationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.GET, null, MainLocationInfoDTO[].class);
		List<MainLocationInfoDTO> locations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				locations.stream().map(m -> m.getLocationId()).collect(Collectors.toList()));
		
		assertEquals(locations.size(), 2);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				3, 4
		}))));
		
		for (MainLocationInfoDTO location : locations) {
			assertEquals(location.getName(), locationNames[location.getLocationId()-1]);
			assertEquals(location.getAddress(), locationAddresses[location.getLocationId()-1]);
			assertEquals(location.getLatitude(), locationLatitudes[location.getLocationId()-1]);
			assertEquals(location.getLongitude(), locationLongitudes[location.getLocationId()-1]);
			assertEquals(location.getManifestationCount(), locationManifestationCounts[location.getLocationId()-1]);
		}
	}
	
	@Test
	public void findAll_tooFarPageable() {
		String url = String.format("/api/locations?page=%d&size=%d", 3, 2);
		
		ResponseEntity<MainLocationInfoDTO[]> responseEntity = testRestTemplate
				.exchange(url, HttpMethod.GET, null, MainLocationInfoDTO[].class);
		List<MainLocationInfoDTO> locations = new ArrayList<>(Arrays.asList(responseEntity.getBody()));
		
		assertEquals(locations.size(), 0);
	}
	
	@Test
	public void dailyReportTest() throws Exception {
		
		this.mockMvc.perform(get("/api/reportDailyLocation/1/02-01-2019/08-08-2020"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(5))
					.andExpect(jsonPath("$", Matchers.hasEntry("13-12-2019", 1)))
					.andExpect(jsonPath("$", Matchers.hasEntry("11-12-2019", 1)))
					.andExpect(jsonPath("$", Matchers.hasEntry("10-12-2019", 1)))
					.andExpect(jsonPath("$", Matchers.hasEntry("17-10-2019", 2)))
					.andExpect(jsonPath("$", Matchers.hasEntry("02-11-2019", 1)));
										
	}
	
	@Test
	@Transactional
	public void dailyReportTest_empty() throws Exception {
		
		this.mockMvc.perform(get("/api/reportDailyLocation/2/02-01-2018/08-01-2019"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(0));
										
	}
	
	@Test
	@Transactional
	public void dailyReportTest_invalidLocation() throws Exception {
		
		this.mockMvc.perform(get("/api/reportDailyLocation/5/02-01-2018/08-01-2019"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Location not found"));
		
										
	}
	
	@Test
	@Transactional
	public void dailyReportTest_invalidDate() throws Exception {
		
		this.mockMvc.perform(get("/api/reportDailyLocation/2/02-13-2020/00-01-2020"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Invalid date!"));
		
										
	}
	
	@Test
	public void weeklyReportTest() throws Exception {
		
		this.mockMvc.perform(get("/api/reportWeeklyLocation/1/02-01-2019/08-08-2020"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(3))
					.andExpect(jsonPath("$", Matchers.hasEntry("Nov 2019 week: 1", 1)))
					.andExpect(jsonPath("$", Matchers.hasEntry("Oct 2019 week: 3", 2)))
					.andExpect(jsonPath("$", Matchers.hasEntry("Dec 2019 week: 2", 3)));
										
	}
	
	@Test
	public void weeklyReportTest_empty() throws Exception {
		
		this.mockMvc.perform(get("/api/reportWeeklyLocation/2/02-01-2018/08-01-2019"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(0));
										
	}
	
	@Test
	@Transactional
	public void weeklyReportTest_invalidLocation() throws Exception {
		
		this.mockMvc.perform(get("/api/reportWeeklyLocation/5/02-01-2018/08-01-2019"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Location not found"));
		
										
	}
	
	@Test
	public void weeklyReportTest_invalidDate() throws Exception {
		
		this.mockMvc.perform(get("/api/reportWeeklyLocation/2/02-13-2020/00-01-2020"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Invalid date!"));
		
										
	}
	
	@Test
	public void monthlyReportTest() throws Exception {
		
		this.mockMvc.perform(get("/api/reportMonthlyLocation/1/02-01-2019/08-08-2020"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(3))
					.andExpect(jsonPath("$", Matchers.hasEntry("Dec 2019", 3)))
					.andExpect(jsonPath("$", Matchers.hasEntry("Nov 2019", 1)))
					.andExpect(jsonPath("$", Matchers.hasEntry("Oct 2019", 2)));
										
	}
	
	@Test
	public void monthlyReportTest_empty() throws Exception {
		
		this.mockMvc.perform(get("/api/reportMonthlyLocation/2/02-02-2018/08-03-2019"))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.size()").value(0));
										
	}
	
	@Test
	@Transactional
	public void monthlyReportTest_invalidLocation() throws Exception {
		
		this.mockMvc.perform(get("/api/reportMonthlyLocation/5/02-01-2018/08-01-2019"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Location not found"));
		
										
	}
	
	@Test
	@Transactional
	public void monthlyReportTest_invalidDate() throws Exception {
		
		this.mockMvc.perform(get("/api/reportMonthlyLocation/2/02-13-2020/00-01-2020"))
					.andExpect(status().isNotAcceptable())
					.andExpect(content().string("Invalid date!"));
		
										
	}
}
