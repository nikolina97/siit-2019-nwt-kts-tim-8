package com.nwtkts.tim8.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nwtkts.tim8.dto.SeatDateDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class SeatControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private ObjectMapper mapper;

	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	@Test
	@Transactional
	public void reserveSeatsTest_ok() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(7);
		ids.add(8);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 6, 4, 0, 0, 0);

		mockMvc.perform(post("/api/seat/reserveSeats").contentType(MediaType.APPLICATION_JSON)
		.content(mapper.writeValueAsString(new SeatDateDTO(ids, "2020-07-04 00:00:00"))))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.size()").value(2))
		.andExpect(jsonPath("$[0].paid").value(false))
		.andExpect(jsonPath("$[0].seat.id").value(7))
		.andExpect(jsonPath("$[0].seat.row").value(3))
		.andExpect(jsonPath("$[0].seat.column").value(1))
		.andExpect(jsonPath("$[0].user.username").value("user1"))
		.andExpect(jsonPath("$[0].manifestation.id").value(1))
		.andExpect(jsonPath("$[1].paid").value(false))
		.andExpect(jsonPath("$[1].seat.id").value(8))
		.andExpect(jsonPath("$[1].seat.row").value(3))
		.andExpect(jsonPath("$[1].seat.column").value(2))
		.andExpect(jsonPath("$[1].user.username").value("user1"))
		.andExpect(jsonPath("$[1].manifestation.id").value(1));
	}

	@Test
	@Transactional
	public void reserveSeatsTest_passed() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(13);

		mockMvc.perform(post("/api/seat/reserveSeats").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new SeatDateDTO(ids, "2019-12-02 00:00:00"))))
				.andExpect(status().isInternalServerError()).andExpect(content().string("Manifestation has passed"));
	}

	@Test
	@Transactional
	public void reserveSeatsTest_canceled() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(27);

		mockMvc.perform(post("/api/seat/reserveSeats").contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(new SeatDateDTO(ids, "2020-12-26 00:00:00"))))
				.andExpect(status().isInternalServerError()).andExpect(content().string("Reservation is disabled"));
	}

	@Test
	@Transactional
	public void reserveSeatsTest_takenSeat() throws Exception {
		List<Integer> ids = new ArrayList<>();
		ids.add(3);

		mockMvc.perform(post("/api/seat/reserveSeats").contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(new SeatDateDTO(ids, "2020-07-04 00:00:00")))).andExpect(status().isInternalServerError()).andExpect(content().string("Seat is already reserved or bought."));
	}

	@Test
	@Transactional
	public void getAllSeatsByManifestationTest_nonExistingManif() throws Exception {
		mockMvc.perform(post("/api/seat/getAllSeats/150").content("2019-12-02 00:00:00")).andExpect(status().isBadRequest())
				.andExpect(content().string("Invalid manifestation id"));
	}

	@Test
	@Transactional
	public void getAllSeatsByManifestationTest_notAllowedManifState() throws Exception {
		mockMvc.perform(post("/api/seat/getAllSeats/3").content("2019-12-26 00:00:00")).andExpect(status().isBadRequest())
				.andExpect(content().string("Manifestation has passed"));
	}

	@Test
	@Transactional
	public void getAllSeatsByManifestationTest_ok() throws Exception {
		mockMvc.perform(post("/api/seat/getAllSeats/1").content("2020-07-03 00:00:00")).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()").value(1)).andExpect(jsonPath("$[0].seats.size()").value(9))
				.andExpect(jsonPath("$[0].seats[0].reserved").value(true))
				.andExpect(jsonPath("$[0].seats[1].reserved").value(true))
				.andExpect(jsonPath("$[0].seats[2].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[3].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[4].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[5].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[6].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[7].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[8].reserved").value(false));
	}

	@Test
	@Transactional
	public void getAllSeatsByManifestationTest_festivalTicket() throws Exception {
		mockMvc.perform(post("/api/seat/getAllSeats/1").content("Festival ticket")).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()").value(1)).andExpect(jsonPath("$[0].seats.size()").value(9))
				.andExpect(jsonPath("$[0].seats[0].reserved").value(true))
				.andExpect(jsonPath("$[0].seats[1].reserved").value(true))
				.andExpect(jsonPath("$[0].seats[2].reserved").value(true))
				.andExpect(jsonPath("$[0].seats[3].reserved").value(true))
				.andExpect(jsonPath("$[0].seats[4].reserved").value(true))
				.andExpect(jsonPath("$[0].seats[5].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[6].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[7].reserved").value(false))
				.andExpect(jsonPath("$[0].seats[8].reserved").value(false));
	}

}
