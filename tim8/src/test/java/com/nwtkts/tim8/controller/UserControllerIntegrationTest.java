package com.nwtkts.tim8.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.Assert.assertFalse;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.nio.charset.Charset;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.nwtkts.tim8.TestUtil;
import com.nwtkts.tim8.dto.UserEditProfileDTO;
import com.nwtkts.tim8.model.RegisteredUser;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIntegrationTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	private MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	@Test
	@Transactional
	public void editProfileTest_changeFirstName() throws Exception {
		UserEditProfileDTO userDTO = new UserEditProfileDTO("Marko", "Peric");
		String json = TestUtil.json(userDTO);
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String firstName = user.getFirstName();
		
		this.mockMvc.perform(put("/api/editProfile")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.username").value("user1"))
				.andExpect(jsonPath("$.firstName").value("Marko"))
				.andExpect(jsonPath("$.lastName").value("Peric"))
				.andExpect(jsonPath("$.email").value("user1@gmail.com"));
		
		assertFalse(firstName.equals(user.getFirstName()));
	}
	
	@Test
	@Transactional
	public void editProfileTest_changeLastName() throws Exception {
		UserEditProfileDTO userDTO = new UserEditProfileDTO("Pera", "Markovic");
		String json = TestUtil.json(userDTO);
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String lastName = user.getLastName();
		
		this.mockMvc.perform(put("/api/editProfile")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.username").value("user1"))
				.andExpect(jsonPath("$.firstName").value("Pera"))
				.andExpect(jsonPath("$.lastName").value("Markovic"))
				.andExpect(jsonPath("$.email").value("user1@gmail.com"));
		
		assertFalse(lastName.equals(user.getLastName()));
	}
	
	@Test
	@Transactional
	public void editProfileTest_changeFirstAndLastName() throws Exception {
		UserEditProfileDTO userDTO = new UserEditProfileDTO("Marko", "Markovic");
		String json = TestUtil.json(userDTO);
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String lastName = user.getLastName();
		String firstName = user.getFirstName();

		this.mockMvc.perform(put("/api/editProfile")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.username").value("user1"))
				.andExpect(jsonPath("$.firstName").value("Marko"))
				.andExpect(jsonPath("$.lastName").value("Markovic"))
				.andExpect(jsonPath("$.email").value("user1@gmail.com"));
		
		assertFalse(firstName.equals(user.getFirstName()));
		assertFalse(lastName.equals(user.getLastName()));
	}
	
	@Test
	@Transactional
	public void editProfileTest_emptyFields() throws Exception {
		UserEditProfileDTO userDTO = new UserEditProfileDTO("", "Peric");
		String json = TestUtil.json(userDTO);

		this.mockMvc.perform(put("/api/editProfile")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isNotAcceptable())
				.andExpect(content().string("You can't leave empty fields"));
		
	}

}
