package com.nwtkts.tim8.controller;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.nwtkts.tim8.TestUtil;
import com.nwtkts.tim8.dto.UserChangePasswordDTO;
import com.nwtkts.tim8.model.Admin;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.User;
import com.nwtkts.tim8.security.auth.JwtAuthenticationRequest;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationControllerIntegrationTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	private MediaType contentType = new MediaType(
			MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
	
	@Test
	@Transactional
	public void loginTest_successfully() throws Exception {
		JwtAuthenticationRequest req = new JwtAuthenticationRequest("user1", "123");
		String json = TestUtil.json(req);
		
        this.mockMvc.perform(post("/auth/login")
                .content(json)
       			.contentType(contentType))
                .andExpect(status().isOk());
				
	}
	
	@Test
	@Transactional
	public void loginTest_wrongUsername() throws Exception {
		JwtAuthenticationRequest req = new JwtAuthenticationRequest("user123", "123");
		String json = TestUtil.json(req);
		
        this.mockMvc.perform(post("/auth/login")
                .content(json)
       			.contentType(contentType))
                .andExpect(status().isNotAcceptable());
	}
	
	@Test
	@Transactional
	public void loginTest_wrongPassword() throws Exception {
		JwtAuthenticationRequest req = new JwtAuthenticationRequest("user1", "0000");
		String json = TestUtil.json(req);
		
        this.mockMvc.perform(post("/auth/login")
                .content(json)
       			.contentType(contentType))
                .andExpect(status().isNotAcceptable());
	}
	
	@Test
	@Transactional
	public void loginTest_incompleteData() throws Exception {
		JwtAuthenticationRequest req = new JwtAuthenticationRequest("user123", "");
		String json = TestUtil.json(req);
		
        this.mockMvc.perform(post("/auth/login")
                .content(json)
       			.contentType(contentType))
                .andExpect(status().isNotAcceptable());
	}
	
	@Test
	@Transactional
	public void loginTest_unverifiedUser() throws Exception {
		JwtAuthenticationRequest req = new JwtAuthenticationRequest("user3", "abcd");
		String json = TestUtil.json(req);
		
        this.mockMvc.perform(post("/auth/login")
                .content(json)
       			.contentType(contentType))
                .andExpect(status().isNotAcceptable())
                .andExpect(content().string("Not verified! See your email for verification."));
	}
	
	@Test
	@Transactional
	public void userRegistration_successfully() throws Exception {
		RegisteredUser user = new RegisteredUser("marko12", "1234", "Marko", "Markovic", "markommm@gmail.com", null);
		String json = TestUtil.json(user);
		
		this.mockMvc.perform(post("/auth/registration")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.username").value("marko12"))
				.andExpect(jsonPath("$.email").value("markommm@gmail.com"))
				.andExpect(jsonPath("$.firstName").value("Marko"))
				.andExpect(jsonPath("$.lastName").value("Markovic"));
	}
	
	@Test
	@Transactional
	public void userRegistration_usernameTaken() throws Exception {
		RegisteredUser user = new RegisteredUser("user1", "1234", "Marko", "Markovic", "marko@gmail.com", null);
		String json = TestUtil.json(user);
		
		this.mockMvc.perform(post("/auth/registration")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isNotAcceptable())
				.andExpect(content().string("Username already taken!"));
	}
	
	@Test
	@Transactional
	public void userRegistration_emailTaken() throws Exception {
		RegisteredUser user = new RegisteredUser("marko1", "1234", "Marko", "Markovic", "user3@gmail.com", null);
		String json = TestUtil.json(user);
		
		this.mockMvc.perform(post("/auth/registration")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isNotAcceptable())
				.andExpect(content().string("Email already taken!"));
	}
	
	@Test
	@Transactional
	public void userRegistration_incompleteData() throws Exception {
		RegisteredUser user = new RegisteredUser("marko1", "", "Marko", "Markovic", "marko@gmail.com", null);
		String json = TestUtil.json(user);
		
		this.mockMvc.perform(post("/auth/registration")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isNotAcceptable())
				.andExpect(content().string("User information is incomplete!"));
	}
	
	@Test
	@Transactional
	public void adminRegistration_successfully() throws Exception {
		User user = new User("marko1", "1234", "Marko", "Markovic", "marko@gmail.com", null);
		Admin a = new Admin(user);
		String json = TestUtil.json(a);
		
		this.mockMvc.perform(post("/auth/registration")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.username").value("marko1"))
				.andExpect(jsonPath("$.email").value("marko@gmail.com"))
				.andExpect(jsonPath("$.firstName").value("Marko"))
				.andExpect(jsonPath("$.lastName").value("Markovic"));
	}
	
	@Test
	@Transactional
	public void adminRegistration_usernameTaken() throws Exception {
		User user = new User("user3", "1234", "Marko", "Markovic", "marko@gmail.com", null);
		Admin a = new Admin(user);
		String json = TestUtil.json(a);
		
		this.mockMvc.perform(post("/auth/registration")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isNotAcceptable())
				.andExpect(content().string("Username already taken!"));
	}
	
	@Test
	@Transactional
	public void confirmRegistration_validToken() throws Exception {
		this.mockMvc.perform(post("/auth/confirm/AbCdEf123456"))
				.andExpect(status().isCreated());
	}
	
	@Test
	@Transactional
	public void confirmRegistration_invalidToken() throws Exception {
		this.mockMvc.perform(post("/auth/confirm/AbCddasdEf123456"))
				.andExpect(status().isNotAcceptable())
				.andExpect(content().string("Invalid token!"));
	}

	@Test
	@Transactional
	public void changePassword_ok() throws Exception {
		
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		UserChangePasswordDTO userDTO = new UserChangePasswordDTO("123", "new_pass", "new_pass");
		String json = TestUtil.json(userDTO);
		
		this.mockMvc.perform(put("/auth/changePassword")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isOk());
		assertTrue(passwordEncoder.matches(userDTO.getNewPassword(), user.getPassword()));  //provjeravamo da li je lozinka encodovana
		
	}
	
	@Test
	@Transactional
	public void changePassword_invalidNewPassword() throws Exception {
		
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		UserChangePasswordDTO userDTO = new UserChangePasswordDTO("123", "new_pass", "ad");
		String json = TestUtil.json(userDTO);
		
		this.mockMvc.perform(put("/auth/changePassword")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isNotAcceptable())
				.andExpect(content().string("You heaven't repeted new password correctly!"));
		
	}
	
	@Test
	@Transactional
	public void changePassword_invalidOldPassword() throws Exception {
		
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		UserChangePasswordDTO userDTO = new UserChangePasswordDTO("1233", "new_pass", "new_pass");
		String json = TestUtil.json(userDTO);
		
		this.mockMvc.perform(put("/auth/changePassword")
				.content(json)
				.contentType(contentType))
				.andExpect(status().isUnauthorized());		
	}
}
