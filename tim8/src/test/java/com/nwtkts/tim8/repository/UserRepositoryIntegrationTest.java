package com.nwtkts.tim8.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nwtkts.tim8.model.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryIntegrationTest {
	
	@Autowired
	private IUserRepository userRepository;
	
	@Test
	@Transactional
	public void findByTokenTest() {
		User user = userRepository.findByToken("AbCdEf123456");
		
		assertEquals("user3", user.getUsername());
		assertEquals("user3@gmail.com", user.getEmail());
	}
}
