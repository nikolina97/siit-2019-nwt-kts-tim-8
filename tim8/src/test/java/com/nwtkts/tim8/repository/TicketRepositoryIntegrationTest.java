package com.nwtkts.tim8.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.Seat;
import com.nwtkts.tim8.model.Ticket;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketRepositoryIntegrationTest {

	@Autowired
	private TicketRepository ticketRepository;

	@Autowired
	private SeatRepository seatRepository;

	@Test
	@Transactional
	public void findBetweenTwoDatesTest() {
		Calendar startDate = Calendar.getInstance();
		startDate.clear();
		startDate.set(2018, 2, 2);

		Calendar endDate = Calendar.getInstance();
		endDate.clear();
		endDate.set(2021, 2, 2);

		Timestamp startTimestamp = new Timestamp(startDate.getTimeInMillis());
		Timestamp endTimestamp = new Timestamp(endDate.getTimeInMillis());

		Collection<Ticket> found = ticketRepository.findBetweenTwoDates(seatRepository.findById(1).get(), startTimestamp, endTimestamp);

		assertEquals(4, found.size());
		assertTrue(((Ticket)found.toArray()[0]).getDate().after(startTimestamp));
		assertTrue(((Ticket)found.toArray()[0]).getDate().before(endTimestamp));
	}

	@Test
	@Transactional
	public void findFestivalTicketTestWithInvalidData() {
		Seat seat = seatRepository.findById(1).get();

		Optional<Ticket> found = ticketRepository.findFestivalTicket(seat);

		assertFalse(found.isPresent());
	}

	@Test
	@Transactional
	public void findFestivalTicketTestWithValidData() {
		Seat seat = seatRepository.findById(2).get();

		Optional<Ticket> found = ticketRepository.findFestivalTicket(seat);

		assertTrue(found.isPresent());
		assertNull(found.get().getDate());
	}
	
	@Test
	public void findByLocationsAndDatesTest() throws ParseException {
		
		Integer locationId = 1;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		Date parseDate1 = sdf.parse("20-09-2019 00:00");
		Date parseDate2 = sdf.parse("08-08-2020 00:00");
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		Collection<Ticket> tickets = ticketRepository.findByLocation(locationId, timestamp1, timestamp2);
		ArrayList<Ticket> listTickets = new ArrayList<Ticket>(tickets);
		
		assertEquals(6, tickets.size());
		assertEquals(Integer.valueOf(2) , listTickets.get(0).getId());
		assertEquals("Pera" , listTickets.get(0).getUser().getFirstName());
		assertEquals(Integer.valueOf(4) , listTickets.get(1).getId());
		assertEquals("Laza" , listTickets.get(1).getUser().getFirstName());
		assertEquals(Integer.valueOf(5) , listTickets.get(2).getId());
		assertEquals("Pera" , listTickets.get(2).getUser().getFirstName());
		assertEquals(Integer.valueOf(6) , listTickets.get(3).getId());
		assertEquals("Laza" , listTickets.get(3).getUser().getFirstName());
		assertEquals(Integer.valueOf(7) , listTickets.get(4).getId());
		assertEquals("Pera" , listTickets.get(4).getUser().getFirstName());
		assertEquals(Integer.valueOf(8) , listTickets.get(5).getId());
		assertEquals("Jovan" , listTickets.get(5).getUser().getFirstName());

				
	}
	
	@Test
	public void findByLocationsAndDatesTest_emptyList() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		Date parseDate1 = sdf.parse("02-01-2018 00:00");
		Date parseDate2 = sdf.parse("08-01-2018 00:00");
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		Collection<Ticket> tickets = ticketRepository.findByLocation(2,timestamp1,timestamp2);
		assertEquals(0, tickets.size());

	}
	
	@Test
	public void findByLocationsAndDatesTest_invalidDates() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		Date parseDate1 = sdf.parse("08-01-2020 00:00");
		Date parseDate2 = sdf.parse("02-01-2020 00:00");
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		Collection<Ticket> tickets = ticketRepository.findByLocation(2,timestamp1,timestamp2);
		assertEquals(0, tickets.size());

	}
	
	@Test
	public void findAllByManifestationBetweenTwoDatesTest() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		Date parseDate1 = sdf.parse("20-09-2019 00:00");
		Date parseDate2 = sdf.parse("08-08-2020 00:00");
		
		Date startDate = sdf.parse("02-07-2020 00:00");
		Date endDate = sdf.parse("05-07-2020 00:00");

		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		Manifestation manifestation = new Manifestation();
		manifestation.setId(1);
		manifestation.setStartDate(new Timestamp(startDate.getTime()));
		manifestation.setEndDate(new Timestamp(endDate.getTime()));
		
		Collection<Ticket> tickets = ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, timestamp1, timestamp2);
		ArrayList<Ticket> listTickets = new ArrayList<Ticket>(tickets);
		
		assertEquals(3, tickets.size());
		assertEquals(Integer.valueOf(2) , listTickets.get(0).getId());
		assertEquals("Pera" , listTickets.get(0).getUser().getFirstName());
		assertEquals(Integer.valueOf(4) , listTickets.get(1).getId());
		assertEquals("Laza" , listTickets.get(1).getUser().getFirstName());
		assertEquals(Integer.valueOf(5) , listTickets.get(2).getId());
		assertEquals("Pera" , listTickets.get(2).getUser().getFirstName());
	}
	
	@Test
	public void findAllByManifestationBetweenTwoDatesTest_emptyList() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		Date parseDate1 = sdf.parse("02-01-2018 00:00");
		Date parseDate2 = sdf.parse("08-01-2018 00:00");
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		Date startDate = sdf.parse("02-07-2020 00:00");
		Date endDate = sdf.parse("05-07-2020 00:00");
		Manifestation manifestation = new Manifestation();
		manifestation.setId(1);
		manifestation.setStartDate(new Timestamp(startDate.getTime()));
		manifestation.setEndDate(new Timestamp(endDate.getTime()));
		
		Collection<Ticket> tickets = ticketRepository.findAllByManifestationBetweenTwoDates(manifestation,timestamp1,timestamp2);
		assertEquals(0, tickets.size());

	}
	
	@Test
	public void findAllByManifestationBetweenTwoDatesTest_invalidDates() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		Date parseDate1 = sdf.parse("08-01-2020 00:00");
		Date parseDate2 = sdf.parse("02-01-2020 00:00");
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		Date startDate = sdf.parse("02-07-2020 00:00");
		Date endDate = sdf.parse("05-07-2020 00:00");
		Manifestation manifestation = new Manifestation();
		manifestation.setId(1);
		manifestation.setStartDate(new Timestamp(startDate.getTime()));
		manifestation.setEndDate(new Timestamp(endDate.getTime()));

		Collection<Ticket> tickets = ticketRepository.findAllByManifestationBetweenTwoDates(manifestation,timestamp1,timestamp2);
		assertEquals(0, tickets.size());

	}

}
