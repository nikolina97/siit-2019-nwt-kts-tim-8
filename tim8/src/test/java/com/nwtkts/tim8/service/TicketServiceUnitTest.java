package com.nwtkts.tim8.service;

import com.google.zxing.WriterException;
import com.itextpdf.text.DocumentException;
import com.nwtkts.tim8.common.TicketGenerator;
import com.nwtkts.tim8.dto.CreatePaymentDTO;
import com.nwtkts.tim8.dto.TicketDTO;
import com.nwtkts.tim8.model.*;
import com.nwtkts.tim8.repository.SeatRepository;
import com.nwtkts.tim8.repository.TicketRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketServiceUnitTest {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private TicketService ticketService;

	@MockBean
	private SeatService seatServiceMocked;

	@MockBean
	private SeatRepository seatRepositoryMocked;

	@MockBean
	private TicketRepository ticketRepository;

	@MockBean
	private PayPalService payPalService;
	
	@MockBean
	private TicketGenerator ticketGenerator;
	
	@MockBean
	private EmailService emailService;

	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	@Test(expected = InvalidDataException.class)
	public void buyTicketsTest_invalidIds_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		ids.add(2);

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(new ArrayList<>());

		ticketService.buyTickets(ids, new Date());
	}

	@Test(expected = InvalidDataException.class)
	public void buyTicketsTest_invalidManifState_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		ids.add(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 2, 0, 0, 0);

		Manifestation manifestation = new Manifestation();
		manifestation.setState(ManifestationState.CANCELED);
		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));

		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setManifestation(manifestation);

		Seat seat = new Seat();
		seat.setSupportedSeatCategory(ssc);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat);
		seats.add(seat);

		List<Ticket> tickets = new ArrayList<>();
		tickets.add(new Ticket());

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);
		Mockito.when(ticketRepository.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate())).thenReturn(tickets);

		ticketService.buyTickets(ids, new Date());
	}

	@Test(expected = InvalidDataException.class)
	public void buyTicketsTest_takenSeat_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		ids.add(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 2, 0, 0, 0);

		Manifestation manifestation = new Manifestation();
		manifestation.setState(ManifestationState.OPEN);
		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));

		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setManifestation(manifestation);

		Seat seat = new Seat();
		seat.setSupportedSeatCategory(ssc);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat);
		seats.add(seat);

		Ticket ticket = new Ticket();
		ticket.setActive(true);
		ticket.setId(1);

		List<Ticket> tickets = new ArrayList<>();
		tickets.add(ticket);

		Date date = new Date();

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);
		Mockito.when(ticketRepository.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate())).thenReturn(new ArrayList<>());
		Mockito.when(seatServiceMocked.checkIfSeatIsReserved(seat, date)).thenReturn(true);

		ticketService.buyTickets(ids, date);
	}

	@Test
	public void buyTicketsTest_ok() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		ids.add(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 2, 0, 0, 0);

		Manifestation manifestation = new Manifestation();
		manifestation.setState(ManifestationState.OPEN);
		manifestation.setDaysBeforeExpires(2);
		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));

		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setManifestation(manifestation);
		ssc.setTicketPrice(10d);

		Seat seat = new Seat();
		seat.setSupportedSeatCategory(ssc);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat);
		seats.add(seat);

		Ticket ticket = new Ticket();
		ticket.setActive(true);

		List<Ticket> tickets = new ArrayList<>();
		tickets.add(ticket);

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);
		Mockito.when(ticketRepository.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate())).thenReturn(new ArrayList<>());
		Mockito.when(ticketRepository.findOneBySeatAndDateAndActive(seat, new Timestamp(cal.getTimeInMillis()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepository.findFestivalTicket(seat)).thenReturn(Optional.empty());
		Mockito.when(ticketRepository.save(any(Ticket.class))).thenReturn(new Ticket());
		Mockito.when(payPalService.createPayment(any(String.class), any(String.class), any(String.class))).thenReturn(null);

		ticketService.buyTickets(ids, new Date());
	}

	@Test
	public void buyTicketsTest_festivalTickets() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		ids.add(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 2, 0, 0, 0);

		Manifestation manifestation = new Manifestation();
		manifestation.setState(ManifestationState.OPEN);
		manifestation.setDaysBeforeExpires(2);
		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));
		
		Set<SeatCategory> categories = new HashSet<SeatCategory>();
		List<Manifestation> manifestations = new ArrayList<Manifestation>();
		manifestations.add(manifestation);
		Location location = new Location("Petrovaradin", "Djava", 15D, 15D, categories, manifestations, true);
		
		manifestation.setLocation(location);

		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setManifestation(manifestation);
		ssc.setTicketPrice(10d);

		Seat seat1 = new Seat();
		seat1.setSupportedSeatCategory(ssc);

		Seat seat2 = new Seat();
		seat2.setSupportedSeatCategory(ssc);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat1);
		seats.add(seat2);

		Ticket ticket = new Ticket();
		ticket.setActive(true);

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);
		Mockito.when(seatServiceMocked.checkSeatForFestivalTicket(seat1, manifestation)).thenReturn(true);
		Mockito.when(seatServiceMocked.checkSeatForFestivalTicket(seat2, manifestation)).thenReturn(true);
		Mockito.when(ticketRepository.save(any(Ticket.class))).thenReturn(new Ticket());
		Mockito.when(payPalService.createPayment(any(String.class), any(String.class), any(String.class))).thenReturn(null);

		ticketService.buyTickets(ids, null);
	}

	@Test
	public void getAllBoughtTicketsByUserTest() throws InvalidDataException {
		RegisteredUser user = new RegisteredUser();

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 5, 5);

		SeatCategory sc = new SeatCategory();
		sc.setName("North");

		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setTicketPrice(150D);
		ssc.setSeatCategory(sc);

		Seat seat = new Seat(3, 3, ssc);
		seat.setId(1);
		seat.setSupportedSeatCategory(ssc);

		Manifestation manifestation = new Manifestation("Exit 2.0", "Desc", new Timestamp(cal.getTimeInMillis()), new Timestamp(cal.getTimeInMillis()), 20, new Location("Petrovaradin", "Djava", 15D, 15D, null, null, true), ManifestationState.OPEN, ManifestationCategory.ENTERTAINMENT, null, null);
		manifestation.setId(1);

		List<Ticket> tickets = new ArrayList<>();

		Ticket ticket = new Ticket(true, new Timestamp(cal.getTimeInMillis()), null, seat, user, manifestation);
		ticket.setActive(true);
		tickets.add(ticket);

		Mockito.when(ticketRepository.findAllByUserAndPaidAndActive(user, true, true)).thenReturn(tickets);

		ArrayList<TicketDTO> result = (ArrayList) ticketService.getAllBoughtTicketsByUser(user);

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(tickets.get(0).getSeat().getRow(), result.get(0).getRow());
		assertEquals(tickets.get(0).getSeat().getColumn(), result.get(0).getColumn());
		assertEquals(tickets.get(0).getSeat().getSupportedSeatCategory().getTicketPrice(), result.get(0).getTicketPrice());
		assertEquals(tickets.get(0).getManifestation().getId(), result.get(0).getManifestationId());
	}

	@Test
	public void getAllReservedTicketsByUserTest() throws InvalidDataException {
		RegisteredUser user = new RegisteredUser();

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 5, 5);

		SeatCategory sc = new SeatCategory();
		sc.setName("North");

		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setTicketPrice(150D);
		ssc.setSeatCategory(sc);

		Seat seat = new Seat(3, 3, ssc);
		seat.setId(1);
		seat.setSupportedSeatCategory(ssc);

		Manifestation manifestation = new Manifestation("Exit 2.0", "Desc", new Timestamp(cal.getTimeInMillis()), new Timestamp(cal.getTimeInMillis()), 20, new Location("Petrovaradin", "Djava", 15D, 15D, null, null, true), ManifestationState.OPEN, ManifestationCategory.ENTERTAINMENT, null, null);
		manifestation.setId(1);

		List<Ticket> tickets = new ArrayList<>();

		Ticket ticket = new Ticket(true, new Timestamp(cal.getTimeInMillis()), null, seat, user, manifestation);
		ticket.setActive(true);
		tickets.add(ticket);

		Mockito.when(ticketRepository.findAllByUserAndPaidAndActive(user, false, true)).thenReturn(tickets);

		ArrayList<TicketDTO> result = (ArrayList) ticketService.getAllReservedTicketsByUser(user);

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(tickets.get(0).getSeat().getRow(), result.get(0).getRow());
		assertEquals(tickets.get(0).getSeat().getColumn(), result.get(0).getColumn());
		assertEquals(tickets.get(0).getSeat().getSupportedSeatCategory().getTicketPrice(), result.get(0).getTicketPrice());
		assertEquals(tickets.get(0).getManifestation().getId(), result.get(0).getManifestationId());
	}
	
	@Test
	public void payTicketTest_ok() throws InvalidDataException {
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Integer id = 3;
		SeatCategory seatCat = new SeatCategory(1, "nameOfSeatCat", true, 10, 10, 10.00, 10.00, 10.00, 10.00, 10.00, false);
		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setTicketPrice(10.00);
		ssc.setSeatCategory(seatCat);
		Manifestation manifestation = new Manifestation();
		ssc.setManifestation(manifestation);
		Seat seat = new Seat(1, 2, ssc);
		Ticket t = new Ticket(false, null, null, seat, user, manifestation);
		t.setId(id);
		List<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t);
		user.setTickets(tickets);
		CreatePaymentDTO cpDTO = new CreatePaymentDTO();
		cpDTO.setStatus("success");
		Mockito.when(ticketRepository.findById(id)).thenReturn(Optional.of(t));

		ticketService.payTicket(id);
		Mockito.verify(this.payPalService, Mockito.times(1)).createPayment(t.getSeat().getSupportedSeatCategory().getTicketPrice().toString(), id.toString(), "reservations");
		

	}
	
	@Test(expected = InvalidDataException.class)
	public void payTicketTest_wrongTicket() throws InvalidDataException {
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Integer id = 3;
		SeatCategory seatCat = new SeatCategory(1, "nameOfSeatCat", true, 10, 10, 10.00, 10.00, 10.00, 10.00, 10.00, false);
		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setTicketPrice(10.00);
		ssc.setSeatCategory(seatCat);
		Manifestation manifestation = new Manifestation();
		ssc.setManifestation(manifestation);
		Seat seat = new Seat(1, 2, ssc);
		Ticket t = new Ticket(false, null, null, seat, user, manifestation);
		t.setId(id);
		List<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t);
		CreatePaymentDTO cpDTO = new CreatePaymentDTO();
		cpDTO.setStatus("success");
		
		ticketService.payTicket(id);
		
	}
	
	@Test
	public void markAsPaid_ok() throws InvalidDataException, DocumentException, WriterException, IOException, MessagingException {
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Integer id = 2;
		Ticket t = new Ticket();
		t.setPaid(false);
		t.setId(id);
		List<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t);
		user.setTickets(tickets);
		Mockito.when(ticketRepository.findById(id)).thenReturn(Optional.of(t));
		ticketService.markAsPaid("2");
		Mockito.verify(ticketGenerator, Mockito.times(1)).generatePdf(t);
		Mockito.verify(emailService, Mockito.times(1)).sendEmailWithTicket(t);

	}
	
	@Test(expected = InvalidDataException.class)
	public void markAsPaid_wrongTicket() throws InvalidDataException, DocumentException, WriterException, IOException, MessagingException {
		ticketService.markAsPaid("22");
	}

	@Test
	public void markAsPaid_multipleTickets_ok() throws InvalidDataException, DocumentException, WriterException, IOException, MessagingException {
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Integer id1 = 2;
		Integer id2 = 3;
		Ticket t = new Ticket();
		t.setPaid(false);
		t.setId(id1);
		Ticket t2 = new Ticket();
		t2.setPaid(false);
		t2.setId(id2);
		List<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t);
		tickets.add(t2);
		user.setTickets(tickets);
		Mockito.when(ticketRepository.findById(id1)).thenReturn(Optional.of(t));
		Mockito.when(ticketRepository.findById(id2)).thenReturn(Optional.of(t2));
		ticketService.markAsPaid("2-3");
		Mockito.verify(ticketGenerator, Mockito.times(1)).generatePdf(t);
		Mockito.verify(emailService, Mockito.times(1)).sendEmailWithTicket(t);
		Mockito.verify(ticketGenerator, Mockito.times(1)).generatePdf(t2);
		Mockito.verify(emailService, Mockito.times(1)).sendEmailWithTicket(t2);

	}
	
	@Test
	public void deleteTickets_ok() throws InvalidDataException {
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Integer id1 = 2;
		Integer id2 = 3;
		Ticket t = new Ticket();
		t.setPaid(false);
		t.setId(id1);
		Ticket t2 = new Ticket();
		t2.setPaid(false);
		t2.setId(id2);
		List<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t);
		tickets.add(t2);
		user.setTickets(tickets);
		ticketService.deleteTickets("2-3");
		Mockito.verify(ticketRepository, Mockito.times(1)).deleteById(id1);
		Mockito.verify(ticketRepository, Mockito.times(1)).deleteById(id2);
	}
	
	@Test(expected = InvalidDataException.class)
	public void deleteTickets_wrongTicket() throws InvalidDataException {
		ticketService.deleteTickets("22-31");
	}

}
