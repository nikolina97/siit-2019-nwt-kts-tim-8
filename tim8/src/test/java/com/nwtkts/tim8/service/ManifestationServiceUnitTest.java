package com.nwtkts.tim8.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Copy;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.nwtkts.tim8.dto.MainManifestationInfoDTO;
import com.nwtkts.tim8.dto.ManifestationDTO;
import com.nwtkts.tim8.dto.ManifestationInfoDTO;
import com.nwtkts.tim8.dto.SearchManifestationDTO;
import com.nwtkts.tim8.dto.SupportedSeatCategoryDTO;
import com.nwtkts.tim8.dto.UpdateManifestationDTO;
import com.nwtkts.tim8.model.Location;
import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.ManifestationCategory;
import com.nwtkts.tim8.model.ManifestationState;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.Seat;
import com.nwtkts.tim8.model.SeatCategory;
import com.nwtkts.tim8.model.SupportedSeatCategory;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.repository.LocationRepository;
import com.nwtkts.tim8.repository.ManifestationRepository;
import com.nwtkts.tim8.repository.SeatCategoryRepository;
import com.nwtkts.tim8.repository.SeatRepository;
import com.nwtkts.tim8.repository.SupportedSeatCategoryRepository;
import com.nwtkts.tim8.repository.TicketRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ManifestationServiceUnitTest {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private ManifestationService manifestationService;

	@MockBean
	private ManifestationRepository manifestationRepository;

	@MockBean
	private SeatRepository seatRepository;

	@MockBean
	private SeatCategoryRepository seatCategoryRepository;

	@MockBean
	private SupportedSeatCategoryRepository supportedSeatCategoryRepository;

	@MockBean
	private LocationRepository locationRepository;

	@MockBean
	private TicketRepository ticketRepository;
	
	private String[] manifestationNames;
	private String[] manifestationDescriptions;
	private Timestamp[] manifestationStartDates;
	private Timestamp[] manifestationEndDates;
	private String[] manifestationImages;
	private String[] manifestationCategories;
	private String[] manifestationStates;
	private HashMap<Integer, Location> locationFindMockData;
	private ArrayList<Manifestation> manifestationFindMockData;
	private ArrayList<Manifestation> manifestationCancelMockData;
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		manifestationNames = new String[] {
				"Exit", "Cola", "Zvezda-Bajern", 
				"RHCP", "ACDC", "Exit 2.0", 
				"Superbovl", "Logan vs KSI", "LCS", 
				"VidCon", "Sajam Tambure", "Megdan"
		};
		
		manifestationDescriptions = new String[] {
				"Exit is awesome", "Cola is awesome", "Fudbal is awesome", 
				"RHCP is awesome", "ACDC is awesome", "Exit is awesome", 
				"Superbovl is awesome", "Boks is awesome", "Esports is awesome", 
				"Youtube is awesome", "Tambura is awesome", "Megdan is awesome"
		};
		
		manifestationStartDates = new Timestamp[] {
				Timestamp.valueOf("2020-07-02 00:00:00"), Timestamp.valueOf("2019-12-02 00:00:00"), Timestamp.valueOf("2019-12-26 00:00:00"), 
				Timestamp.valueOf("2020-12-26 00:00:00"), Timestamp.valueOf("2021-09-06 00:00:00"), Timestamp.valueOf("2020-09-06 00:00:00"), 
				Timestamp.valueOf("2020-09-15 00:00:00"), Timestamp.valueOf("2020-10-15 00:00:00"), Timestamp.valueOf("2020-11-15 00:00:00"), 
				Timestamp.valueOf("2020-11-17 00:00:00"), Timestamp.valueOf("2020-06-09 00:00:00"), Timestamp.valueOf("2020-08-08 00:00:00")
		};
		
		manifestationEndDates = new Timestamp[] {
				Timestamp.valueOf("2020-07-05 00:00:00"), Timestamp.valueOf("2019-12-03 00:00:00"), Timestamp.valueOf("2019-12-26 00:00:00"), 
				Timestamp.valueOf("2020-12-26 00:00:00"), Timestamp.valueOf("2020-09-06 00:00:00"), Timestamp.valueOf("2020-12-06 00:00:00"), 
				Timestamp.valueOf("2020-09-15 00:00:00"), Timestamp.valueOf("2020-10-15 00:00:00"), Timestamp.valueOf("2020-11-17 00:00:00"), 
				Timestamp.valueOf("2020-11-19 00:00:00"), Timestamp.valueOf("2020-06-13 00:00:00"), Timestamp.valueOf("2020-08-10 00:00:00")
		};
		
		manifestationImages = new String[] {
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg"
		};
		
		manifestationCategories = new String[] {
				"ENTERTAINMENT", "ENTERTAINMENT", "SPORT", 
				"CULTURE", "CULTURE", "ENTERTAINMENT", 
				"SPORT", "SPORT", "SPORT", 
				"ENTERTAINMENT", "CULTURE", "CULTURE"
		};
		
		manifestationStates = new String[] {
				"OPEN", "PAST", "PAST", 
				"CANCELED", "SOLD_OUT", "OPEN", 
				"OPEN", "OPEN", "OPEN", 
				"OPEN", "OPEN", "OPEN"
		};
		
		Location djava = new Location();
		djava.setAddress("Petrovaradin");
		djava.setLatitude(Double.valueOf(12));
		djava.setLongitude(Double.valueOf(12));
		Location marakana = new Location();
		marakana.setAddress("Beograd");
		marakana.setLatitude(Double.valueOf(25));
		marakana.setLongitude(Double.valueOf(25));
		Location cair = new Location();
		cair.setAddress("Nis");
		cair.setLatitude(Double.valueOf(25.5));
		cair.setLongitude(Double.valueOf(25.5));
		
		locationFindMockData = new HashMap<Integer, Location>();
		
		locationFindMockData.put(1, djava);
		locationFindMockData.put(5, marakana);
		locationFindMockData.put(6, djava);
		locationFindMockData.put(7, cair);
		locationFindMockData.put(8, marakana);
		locationFindMockData.put(9, marakana);
		locationFindMockData.put(10, cair);
		locationFindMockData.put(11, djava);
		locationFindMockData.put(12, cair);
		
		manifestationFindMockData = new ArrayList<Manifestation>();
		for (Integer i : new Integer[] {1, 5, 6, 7, 8, 9, 10, 11, 12}) {
			Manifestation man = new Manifestation();
			man.setId(i);
			man.setName(manifestationNames[i-1]);
			man.setDescription(manifestationDescriptions[i-1]);
			man.setStartDate(manifestationStartDates[i-1]);
			man.setEndDate(manifestationEndDates[i-1]);
			man.setImage(manifestationImages[i-1]);
			man.setCategory(ManifestationCategory.valueOf(manifestationCategories[i-1]));
			man.setState(ManifestationState.valueOf(manifestationStates[i-1]));
			man.setLocation(locationFindMockData.get(i));
			manifestationFindMockData.add(man);
		}
		
		manifestationCancelMockData = new ArrayList<Manifestation>();
		for (Integer i : new Integer[] {2, 4}) {
			Manifestation man = new Manifestation();
			man.setId(i);
			man.setName(manifestationNames[i-1]);
			man.setDescription(manifestationDescriptions[i-1]);
			man.setStartDate(manifestationStartDates[i-1]);
			man.setEndDate(manifestationEndDates[i-1]);
			man.setImage(manifestationImages[i-1]);
			man.setCategory(ManifestationCategory.valueOf(manifestationCategories[i-1]));
			man.setState(ManifestationState.valueOf(manifestationStates[i-1]));
			man.setLocation(locationFindMockData.get(i));
			manifestationCancelMockData.add(man);
		}
	}

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	
	@Test
	public void findById_ok() throws InvalidDataException {
		Integer id = 11;
		int myDay = 02;
		int myMonth = 01;
		int myYear = 2019;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
                myYear, myMonth, myDay));
		myDay = 04;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
                myYear, myMonth, myDay));
		
		Manifestation m = new Manifestation();
		m.setActive(true);
		m.setCategory(ManifestationCategory.SPORT);
		m.setDaysBeforeExpires(4);
		m.setDescription("Desc test");
		m.setId(id);
		m.setEndDate(end);
		m.setImage("imgurl");
		Location l = new Location();
		l.setActive(true);
		l.setAddress("test addr");
		l.setId(4);
		l.setLatitude(14.4);
		l.setLongitude(-14.3);
		l.setName("name");
		ArrayList<Manifestation> manifs = new ArrayList<Manifestation>();
		manifs.add(m);
		l.setManifestations(manifs);
		m.setLocation(l);
		m.setName("Test Name");
		m.setStartDate(start);
		m.setState(ManifestationState.PAST);
		
		Mockito.when(manifestationRepository.findById(id)).thenReturn(Optional.of(m));
		ManifestationInfoDTO manifestation = manifestationService.findById(id);
		
		assertEquals(3,manifestation.getDates().size());
		assertEquals(m.getStartDate(), manifestation.getDates().get(0));
		assertEquals(m.getEndDate(), manifestation.getDates().get(manifestation.getDates().size()-1));
		assertEquals(m.getDaysBeforeExpires(), manifestation.getDaysBeforeExpires());
		assertEquals(m.getDescription(), manifestation.getDescription());
		assertEquals(m.getImage(), manifestation.getImage());
		assertEquals(m.getLocation().getId(), manifestation.getLocation().getLocationId());
		assertEquals(Integer.valueOf(0), manifestation.getLocation().getManifestationCount());
		assertEquals(m.getCategory(), manifestation.getManifestationCategory());
		assertEquals(m.getId(), manifestation.getManifestationId());
		assertEquals(m.getState(), manifestation.getManifestationState());
		assertEquals(m.getName(), manifestation.getName());
	}
	

	@Test
	public void findById_notFound() throws InvalidDataException {
		Integer id = 7;
		Mockito.when(manifestationRepository.findById(id)).thenReturn(Optional.empty());
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Manifestation not found");

	    manifestationService.findById(id);
	}
	
	@Test
	public void saveManifestation_ok() throws InvalidDataException {
		Manifestation manifestation = new Manifestation();
		manifestation.setId(1);
		int myYear = 2021;
		int myMonth = 02;
		int myDay = 02;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setStartDate(start);
		myYear = 2021;
		myMonth = 02;
		myDay = 12;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setEndDate(end);
		manifestation.setState(ManifestationState.OPEN);
		manifestation.setCategory(ManifestationCategory.CULTURE);
		manifestation.setName("Exit");
		manifestation.setDaysBeforeExpires(7);
		manifestation.setDescription("Zanimljiva");

		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location l = new Location("Petrovaradin", "Tvrdjava", 55.5, 55.9, categories, manifestations, true);
		l.setId(1);
		manifestation.setLocation(l);

		SeatCategory sc = new SeatCategory();
		sc.setId(1);
		sc.setColumns(3);
		sc.setEnumerable(true);
		sc.setName("Tribina 1");
		sc.setRows(3);
		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setSeatCategory(sc);
		ssc.setId(5);
		ssc.setManifestation(manifestation);
		ssc.setNumberOfSeats(null);
		ssc.setTicketPrice(500.2);
		Seat s = new Seat();
		List<Seat> seats = new ArrayList<Seat>();
		seats.add(s);
		ssc.setSeats(seats);
		Set<SupportedSeatCategory> sscSet = new HashSet<SupportedSeatCategory>();
		sscSet.add(ssc);
		manifestation.setSupportedSeatCategories(sscSet);

		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);

		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(7, locationID, "Exit", "Zanimljiva", null, null, start, end,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);

		Manifestation manifestation2 = new Manifestation();
		manifestation2.setId(6);
		myYear = 2020;
		myMonth = 10;
		myDay = 10;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation2.setStartDate(ts);

		myYear = 2020;
		myMonth = 10;
		myDay = 13;
		ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation2.setEndDate(ts);
		manifestation2.setState(ManifestationState.OPEN);
		manifestation2.setCategory(ManifestationCategory.CULTURE);
		manifestation2.setName("SSS");
		manifestation2.setDaysBeforeExpires(7);
		manifestation2.setLocation(l);
		manifestation2.setSupportedSeatCategories(sscSet);

		List<Manifestation> manifs = new ArrayList<Manifestation>();
		// manifs.add(manifestation);
		manifs.add(manifestation2);

		Mockito.when(seatCategoryRepository.findById(seatCategoryID))
				.thenReturn(Optional.of(new SeatCategory(1, "SC1", false, -1, -1, 1.1, 2.2, 3.3, 4.4, 5.5, true)));
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(l));
		Mockito.when(manifestationRepository.findByLocation_Id(l.getId())).thenReturn(manifs);
		Mockito.when(manifestationRepository.save(any(Manifestation.class))).thenReturn(manifestation);
		Mockito.when(seatCategoryRepository.findById(sc.getId())).thenReturn(Optional.of(sc));
		Mockito.when(supportedSeatCategoryRepository.save(ssc)).thenReturn(ssc);
		Mockito.when(seatRepository.save(any(Seat.class))).thenReturn(new Seat());
		ManifestationInfoDTO manifInfo = manifestationService.save(manifDTO);

		assertEquals(manifDTO.getDescription(), manifInfo.getDescription());
		assertEquals(manifDTO.getLocationId(), manifInfo.getLocation().getLocationId());
		assertEquals(manifestation.getId(), manifInfo.getManifestationId());
		assertEquals(manifDTO.getCategory(), manifInfo.getManifestationCategory());
		assertEquals(manifDTO.getState(), manifInfo.getManifestationState());
		assertEquals(manifDTO.getName(), manifInfo.getName());
		assertEquals(manifDTO.getStartDate(), manifInfo.getDates().get(0));
		assertEquals(manifDTO.getEndDate(), manifInfo.getDates().get(manifInfo.getDates().size() - 1));
	}

	@Test
	public void saveManifestation_wrongDate() throws InvalidDataException {
		Manifestation manifestation = new Manifestation();
		manifestation.setId(5);
		int myYear = 2021;
		int myMonth = 02;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setStartDate(ts);
		myYear = 2021;
		myMonth = 02;
		myDay = 12;
		ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setEndDate(ts);
		manifestation.setState(ManifestationState.OPEN);
		manifestation.setCategory(ManifestationCategory.CULTURE);
		manifestation.setName("SSS");
		manifestation.setDaysBeforeExpires(7);

		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location l = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		l.setId(1);
		manifestation.setLocation(l);

		SeatCategory sc = new SeatCategory();
		sc.setId(1);
		sc.setColumns(3);
		sc.setEnumerable(true);
		sc.setName("Tribina 1");
		sc.setRows(3);
		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setSeatCategory(sc);
		ssc.setId(5);
		ssc.setManifestation(manifestation);
		ssc.setNumberOfSeats(null);
		ssc.setTicketPrice(500.2);
		Seat s = new Seat();
		List<Seat> seats = new ArrayList<Seat>();
		seats.add(s);
		ssc.setSeats(seats);
		Set<SupportedSeatCategory> sscSet = new HashSet<SupportedSeatCategory>();
		sscSet.add(ssc);
		manifestation.setSupportedSeatCategories(sscSet);

		Manifestation manifestation2 = new Manifestation();
		manifestation2.setId(6);
		myYear = 2020;
		myMonth = 06;
		myDay = 10;
		ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation2.setStartDate(ts);

		myYear = 2020;
		myMonth = 06;
		myDay = 13;
		ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation2.setEndDate(ts);
		manifestation2.setState(ManifestationState.OPEN);
		manifestation2.setCategory(ManifestationCategory.CULTURE);
		manifestation2.setName("SSS");
		manifestation2.setDaysBeforeExpires(7);
		manifestation2.setLocation(l);
		manifestation2.setSupportedSeatCategories(sscSet);

		List<Manifestation> manifs = new ArrayList<Manifestation>();
		manifs.add(manifestation);
		manifs.add(manifestation2);

		myYear = 2020;
		myMonth = 06;
		myDay = 02;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		myYear = 2020;
		myMonth = 06;
		myDay = 12;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);

		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(7, locationID, "Exit", "Zanimljiva", null, null, start, end,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);

		Mockito.when(seatCategoryRepository.findById(seatCategoryID))
				.thenReturn(Optional.of(new SeatCategory(1, "SC1", false, -1, -1, 1.1, 2.2, 3.5, 8.3, 0.6, false)));
		Mockito.when(seatCategoryRepository.findById(seatCategoryID2))
				.thenReturn(Optional.of(new SeatCategory(2, "SC2", true, 10, 31, 2.2, 0.1, 3.6, 8.2, 9.1, true)));
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(l)); //
		Mockito.when(manifestationRepository.findByLocation_Id(l.getId())).thenReturn(manifs);
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Some other event will be held at this time");
		manifestationService.save(manifDTO);
	}

	@Test
	public void saveManifestation_missingFields() throws InvalidDataException {
		ManifestationDTO manifestationDTO = new ManifestationDTO();

		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Some data is missing");

		manifestationService.save(manifestationDTO);
	}

	@Test
	public void saveManifestation_locationNotFound() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(1, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(7, locationID, "Name", "Zanimljiva", null, null, ts, ts,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.empty());
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Location not found");
		manifestationService.save(manifDTO);

	}

	@Test
	public void saveManifestation_noSeatCategory() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(2, 30, 50.55);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(7, locationID, "Name", "Zanimljiva", null, null, ts, ts,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);
		Mockito.when(seatCategoryRepository.findById(seatCategoryID)).thenReturn(Optional.empty());
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(new Location()));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Seat category with id " + seatCategoryID + " not found");
		manifestationService.save(manifDTO);

	}

	@Test
	public void saveManifestation_emptyName() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);

		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(7, locationID, "", "Zanimljiva", null, null, ts, ts,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);
		Mockito.when(seatCategoryRepository.findById(seatCategoryID)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(seatCategoryRepository.findById(seatCategoryID2)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(new Location()));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Name can't be empty");
		manifestationService.save(manifDTO);
	}

	@Test
	public void saveManifestation_emptyDescription() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);

		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(7, locationID, "Exit", "", null, null, ts, ts,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);
		Mockito.when(seatCategoryRepository.findById(seatCategoryID)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(seatCategoryRepository.findById(seatCategoryID2)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(new Location()));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Description can't be empty");
		manifestationService.save(manifDTO);
	}

	@Test
	public void saveManifestation_negativeExpirationDays() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);

		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(-7, locationID, "Exit", "Zanimljiva", null, null, ts, ts,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);
		Mockito.when(seatCategoryRepository.findById(seatCategoryID)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(seatCategoryRepository.findById(seatCategoryID2)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(new Location()));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage(
				"Number of days after which the possibility of reservation expires can't be less than -1");
		manifestationService.save(manifDTO);
	}

	@Test
	public void saveManifestation_allowedReservationsWrongDate() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 02;
		int myDay = 15;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		myYear = 2019;
		myMonth = 02;
		myDay = 16;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);

		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(15, locationID, "Exit", "Zanimljiva", null, null, start, end,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);
		Mockito.when(seatCategoryRepository.findById(seatCategoryID)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(seatCategoryRepository.findById(seatCategoryID2)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(new Location()));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("The date when the reservations are no longer allowed cannot be earlier than today");
		manifestationService.save(manifDTO);
	}

	@Test
	@Ignore
	public void saveManifestation_startDateInThePast() throws InvalidDataException {
		int myYear = 2019;
		int myMonth = 12;
		int myDay = 02;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		myYear = 2019;
		myMonth = 12;
		myDay = 15;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);

		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(0, locationID, "Exit", "Zanimljiva", null, null, start, end,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);
		Mockito.when(seatCategoryRepository.findById(seatCategoryID)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(seatCategoryRepository.findById(seatCategoryID2)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(new Location()));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Start date must be in the future");
		manifestationService.save(manifDTO);
	}

	@Test
	public void saveManifestation_endDateBeforeStart() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		myYear = 2019;
		myMonth = 02;
		myDay = 01;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);

		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO = new ManifestationDTO(7, locationID, "Exit", "Zanimljiva", null, null, start, end,
				ManifestationState.OPEN, ManifestationCategory.CULTURE, sscList);
		Mockito.when(seatCategoryRepository.findById(seatCategoryID)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(seatCategoryRepository.findById(seatCategoryID2)).thenReturn(Optional.of(new SeatCategory()));
		Mockito.when(locationRepository.findById(locationID)).thenReturn(Optional.of(new Location()));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("End date can't be before start date");
		manifestationService.save(manifDTO);
	}

	@Test
	public void updateManifestation_ok() throws InvalidDataException {
		Integer manifID = 1;
		Manifestation manifestation = new Manifestation();
		manifestation.setId(1);
		int myYear = 2021;
		int myMonth = 02;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setStartDate(ts);
		myYear = 2021;
		myMonth = 02;
		myDay = 12;
		ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setEndDate(ts);
		manifestation.setState(ManifestationState.OPEN);
		manifestation.setCategory(ManifestationCategory.CULTURE);
		manifestation.setName("Name1");
		manifestation.setDescription("Description1");
		manifestation.setDaysBeforeExpires(7);

		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location l = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		l.setId(1);
		manifestation.setLocation(l);

		SeatCategory sc = new SeatCategory();
		sc.setId(1);
		sc.setColumns(3);
		sc.setEnumerable(true);
		sc.setName("Tribina 1");
		sc.setRows(3);
		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setSeatCategory(sc);
		ssc.setId(5);
		ssc.setManifestation(manifestation);
		ssc.setNumberOfSeats(null);
		ssc.setTicketPrice(500.2);
		Seat s = new Seat();
		List<Seat> seats = new ArrayList<Seat>();
		seats.add(s);
		ssc.setSeats(seats);
		Set<SupportedSeatCategory> sscSet = new HashSet<SupportedSeatCategory>();
		sscSet.add(ssc);
		manifestation.setSupportedSeatCategories(sscSet);

		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("Exit", "Zanimljiva");

		Mockito.when(manifestationRepository.findById(manifID)).thenReturn(Optional.of(manifestation));
		Mockito.when(manifestationRepository.save(manifestation)).thenReturn(manifestation);

		ManifestationInfoDTO manifInfo = manifestationService.update(manifID, manifDTO);

		assertEquals(manifDTO.getName(), manifInfo.getName());
		assertEquals(manifDTO.getDescription(), manifInfo.getDescription());
		assertEquals(manifestation.getLocation().getId(), manifInfo.getLocation().getLocationId());
		assertEquals(manifestation.getId(), manifInfo.getManifestationId());
		assertEquals(manifestation.getCategory(), manifInfo.getManifestationCategory());
		assertEquals(manifestation.getState(), manifInfo.getManifestationState());
		assertEquals(manifestation.getStartDate(), manifInfo.getDates().get(0));
		assertEquals(manifestation.getEndDate(), manifInfo.getDates().get(manifInfo.getDates().size() - 1));
	}

	@Test
	public void updateManifestation_manifestationNotFound() throws InvalidDataException {
		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("Name", "Description");
		Integer manifID = 1;

		Mockito.when(manifestationRepository.findById(manifID)).thenReturn(Optional.empty());
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Manifestation not found");
		manifestationService.update(manifID, manifDTO);
	}

	@Test
	public void updateManifestation_canceledManifestation() throws InvalidDataException {
		Manifestation manifestation = new Manifestation();
		manifestation.setId(5);
		int myYear = 2021;
		int myMonth = 02;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setStartDate(ts);
		myYear = 2021;
		myMonth = 02;
		myDay = 12;
		ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setEndDate(ts);
		manifestation.setState(ManifestationState.CANCELED);
		manifestation.setCategory(ManifestationCategory.CULTURE);
		manifestation.setName("SSS");
		manifestation.setDaysBeforeExpires(7);

		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location l = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		l.setId(1);
		manifestation.setLocation(l);

		SeatCategory sc = new SeatCategory();
		sc.setId(1);
		sc.setColumns(3);
		sc.setEnumerable(true);
		sc.setName("Tribina 1");
		sc.setRows(3);
		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setSeatCategory(sc);
		ssc.setId(5);
		ssc.setManifestation(manifestation);
		ssc.setNumberOfSeats(null);
		ssc.setTicketPrice(500.2);
		Seat s = new Seat();
		List<Seat> seats = new ArrayList<Seat>();
		seats.add(s);
		ssc.setSeats(seats);
		Set<SupportedSeatCategory> sscSet = new HashSet<SupportedSeatCategory>();
		sscSet.add(ssc);
		manifestation.setSupportedSeatCategories(sscSet);

		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("Novo ime", "novi opis");
		Integer manifID = 1;

		Mockito.when(manifestationRepository.findById(manifID)).thenReturn(Optional.of(manifestation));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("You can't edit canceled manifestation");
		manifestationService.update(manifID, manifDTO);
	}

	@Test
	public void updateManifestation_pastManifestation() throws InvalidDataException {
		Manifestation manifestation = new Manifestation();
		manifestation.setId(5);
		int myYear = 2021;
		int myMonth = 02;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setStartDate(ts);
		myYear = 2021;
		myMonth = 02;
		myDay = 12;
		ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", myYear, myMonth, myDay));
		manifestation.setEndDate(ts);
		manifestation.setState(ManifestationState.PAST);
		manifestation.setCategory(ManifestationCategory.CULTURE);
		manifestation.setName("SSS");
		manifestation.setDaysBeforeExpires(7);

		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location l = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		l.setId(1);
		manifestation.setLocation(l);

		SeatCategory sc = new SeatCategory();
		sc.setId(1);
		sc.setColumns(3);
		sc.setEnumerable(true);
		sc.setName("Tribina 1");
		sc.setRows(3);
		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setSeatCategory(sc);
		ssc.setId(5);
		ssc.setManifestation(manifestation);
		ssc.setNumberOfSeats(null);
		ssc.setTicketPrice(500.2);
		Seat s = new Seat();
		List<Seat> seats = new ArrayList<Seat>();
		seats.add(s);
		ssc.setSeats(seats);
		Set<SupportedSeatCategory> sscSet = new HashSet<SupportedSeatCategory>();
		sscSet.add(ssc);
		manifestation.setSupportedSeatCategories(sscSet);
		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("Novo ime", "novi opis");

		Integer manifID = 1;

		Mockito.when(manifestationRepository.findById(manifID)).thenReturn(Optional.of(manifestation));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("You can't edit manifestation that has passed");
		manifestationService.update(manifID, manifDTO);
	}

	@Test
	public void updateManifestation_missingFields() throws InvalidDataException {
		UpdateManifestationDTO manifestationDTO = new UpdateManifestationDTO();

		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Some data is missing");

		manifestationService.update(1, manifestationDTO);
	}

	@Test
	public void updateManifestation_emptyName() throws InvalidDataException {
		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("", "Desc");

		Integer manifID = 81;
		Manifestation manifestation = new Manifestation();
		manifestation.setState(ManifestationState.SOLD_OUT);
		Mockito.when(manifestationRepository.findById(manifID)).thenReturn(Optional.of(manifestation));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Name can't be empty");
		manifestationService.update(manifID, manifDTO);
	}

	@Test
	public void updateManifestation_emptyDescription() throws InvalidDataException {
		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("Name", "");

		Integer manifID = 81;
		Manifestation manifestation = new Manifestation();
		manifestation.setState(ManifestationState.SOLD_OUT);
		Mockito.when(manifestationRepository.findById(manifID)).thenReturn(Optional.of(manifestation));
		expectedEx.expect(InvalidDataException.class);
		expectedEx.expectMessage("Description can't be empty");
		manifestationService.update(manifID, manifDTO);
	}

	@Test
	public void findTest_emptyParams() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_nullParams() {
		String name = null;
		List<String> categories = null;
		List<String> locations = null;
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_simpleName() {
		String name = "i";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 6, 8, 10
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_nonexistantName() {
		String name = "prodigy";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		
		assertEquals(manifestations.size(), 0);
	}
	
	@Test
	public void findTest_singleLocation() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Petrovaradin"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 3);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 6, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_multiLocation() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Petrovaradin", "Beograd"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 6);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 8, 9, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_singleCategory() {
		String name = "";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport"}));
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 3);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				7, 8, 9
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_multiCategory() {
		String name = "";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport", "Culture"}));
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 6);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				5, 7, 8, 9, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_allParams() {
		String name = "vs";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport"}));
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Beograd"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData)
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 1);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_simplePageable() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(1, 4, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(manifestationFindMockData.subList(4, 8))
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8, 9, 10, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findTest_tooFarPageable() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(3, 4, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
						Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
				).thenReturn(
						new PageImpl<Manifestation>(new ArrayList<Manifestation>())
				);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		
		assertEquals(manifestations.size(), 0);
	}
	
	@Test
	public void findAllTest_successful() {
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
				Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
		).thenReturn(
				new PageImpl<Manifestation>(new ArrayList<Manifestation>(manifestationFindMockData))
		);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.findAll(pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findAllTest_simplePageable() {
		
		PageRequest pageable = PageRequest.of(1, 4, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
				Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
		).thenReturn(
				new PageImpl<Manifestation>(manifestationFindMockData.subList(4, 8))
		);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.findAll(pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8, 9, 10, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	public void findAllTest_tooFarPageable() {
		
		PageRequest pageable = PageRequest.of(3, 4, Sort.by(Sort.Direction.ASC, "id"));
		
		Mockito.when(manifestationRepository.findAllByStateNotIn(
				Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable)
		).thenReturn(
				new PageImpl<Manifestation>(new ArrayList<Manifestation>())
		);
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.findAll(pageable));
		
		assertEquals(manifestations.size(), 0);
	}

	@Test(expected = InvalidDataException.class)
	public void getManifestationInfoTest_nonExisting_throwsInvalidDataException() throws InvalidDataException {
		Integer id = 1;

		Mockito.when(manifestationRepository.findById(id)).thenReturn(Optional.empty());

		manifestationService.getManifestationInfo(id);
	}

	@Test
	public void getManifestationInfoTest_ok() throws InvalidDataException {
		Integer id = 1;

		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 11, 3, 10, 10, 10);

		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		cal2.set(2020, 11, 5, 10, 10, 10);

		List<Manifestation> manifestations = new ArrayList<>();
		manifestations.add(new Manifestation("Exit", "Exit is awesome", new Timestamp(cal1.getTimeInMillis()),
				new Timestamp(cal2.getTimeInMillis()), 10,
				new Location("Petrovaradin", "Djava", 15D, 15D, null, null, true), ManifestationState.OPEN,
				ManifestationCategory.ENTERTAINMENT, null, null));
		Manifestation manif = new Manifestation("Exit", "Exit is awesome", new Timestamp(cal1.getTimeInMillis()),
				new Timestamp(cal2.getTimeInMillis()), 10,
				new Location("Petrovaradin", "Djava", 15D, 15D, null, manifestations, true), ManifestationState.OPEN,
				ManifestationCategory.ENTERTAINMENT, null, null);

		Mockito.when(manifestationRepository.findById(id)).thenReturn(Optional.of(manif));

		ManifestationInfoDTO dto = manifestationService.getManifestationInfo(id);

		assertNotNull(dto);
		assertEquals(manif.getName(), dto.getName());
		assertEquals(manif.getDescription(), dto.getDescription());
		assertEquals(manif.getStartDate(), dto.getDates().get(0));
		assertEquals(manif.getEndDate(), dto.getDates().get(2));
		assertEquals(manif.getLocation().getLatitude(), dto.getLocation().getLatitude());
		assertEquals(manif.getLocation().getLongitude(), dto.getLocation().getLongitude());
		assertEquals(manif.getState(), dto.getManifestationState());
		assertEquals(manif.getCategory(), dto.getManifestationCategory());
	}

	@Test
	public void cancelTest_successful() throws InvalidDataException {
		Integer manifestationId = 1;
		
		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 6, 2, 0, 0, 0);

		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		cal2.set(2020, 6, 5, 0, 0, 0);
		
		Manifestation manifestationChanged = new Manifestation();
		manifestationChanged.setId(manifestationId);
		manifestationChanged.setName(manifestationNames[manifestationId-1]);
		manifestationChanged.setDescription(manifestationDescriptions[manifestationId-1]);
		manifestationChanged.setStartDate(manifestationStartDates[manifestationId-1]);
		manifestationChanged.setEndDate(manifestationEndDates[manifestationId-1]);
		manifestationChanged.setImage(manifestationImages[manifestationId-1]);
		manifestationChanged.setCategory(ManifestationCategory.valueOf(manifestationCategories[manifestationId-1]));
		manifestationChanged.setState(ManifestationState.CANCELED);
		manifestationChanged.setLocation(locationFindMockData.get(manifestationId));
		
		Mockito.when(manifestationRepository.findById(manifestationId))
				.thenReturn(Optional.of(manifestationFindMockData.get(0)));
		
		Mockito.when(manifestationRepository.save(manifestationChanged))
				.thenReturn(manifestationChanged);
		
		Boolean ret = manifestationService.cancel(manifestationId);
		
		Mockito.when(manifestationRepository.findById(manifestationId))
				.thenReturn(Optional.of(manifestationChanged));
		
		Manifestation manifestation = manifestationRepository.findById(manifestationId).get();
		
		assertEquals(true, ret);
		assertEquals("Exit", manifestation.getName());
		assertEquals("Exit is awesome", manifestation.getDescription());
		assertEquals(0, manifestation.getStartDate().compareTo(new Timestamp(cal1.getTimeInMillis())));
		assertEquals(0, manifestation.getEndDate().compareTo(new Timestamp(cal2.getTimeInMillis())));
		assertEquals(Double.valueOf(12), manifestation.getLocation().getLatitude());
		assertEquals(Double.valueOf(12), manifestation.getLocation().getLongitude());
		assertEquals("http://localhost:8080/mob_EXIT2-6.jpg", manifestation.getImage());
		assertEquals(ManifestationState.CANCELED, manifestation.getState());
		assertEquals(ManifestationCategory.ENTERTAINMENT, manifestation.getCategory());
	}
	
	@Test(expected = InvalidDataException.class)
	public void cancelTest_manifestationNotFound() throws InvalidDataException {
		Integer manifestationId = 100;
		
		Mockito.when(manifestationRepository.findById(manifestationId))
				.thenReturn(Optional.empty());
		
		try {
			manifestationService.cancel(manifestationId);
		} catch (InvalidDataException e) {
			String message = "Manifestation not found";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	public void cancelTest_manifestationPassed() throws InvalidDataException {
		Integer manifestationId = 2;
		
		Mockito.when(manifestationRepository.findById(manifestationId))
				.thenReturn(Optional.of(manifestationCancelMockData.get(0)));
		
		try {
			manifestationService.cancel(manifestationId);
		} catch (InvalidDataException e) {
			String message = "Manifestation has passed";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	public void cancelTest_manifestationAlreadyCanceled() throws InvalidDataException {
		Integer manifestationId = 4;
		
		Manifestation mocked = manifestationCancelMockData.get(1);
		
		Mockito.when(manifestationRepository.findById(manifestationId))
				.thenReturn(Optional.of(mocked));
		
		try {
			manifestationService.cancel(manifestationId);
		} catch (InvalidDataException e) {
			String message = "Manifestation has already been canceled";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getDailyTest_ok() throws ParseException, InvalidDataException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");

		Date startDate = sdf.parse("02-12-2019 21:00");
		Date endDate = sdf.parse("02-12-2019 23:00");

		String buyingDate1 = "10-10-2019 00:00";
		String buyingDate2 = "10-10-2019 00:00";
		String buyingDate3 = "12-11-2019 00:00";
		String buyingDate4 = "14-11-2019 00:00";
		String buyingDate5 = "10-12-2019 00:00";

		Date bdate1 = sdf.parse(buyingDate1);
		Date bdate2 = sdf.parse(buyingDate2);
		Date bdate3 = sdf.parse(buyingDate3);
		Date bdate4 = sdf.parse(buyingDate4);
		Date bdate5 = sdf.parse(buyingDate5);

		String date1String = "02-09-2019 00:00";
		String date2String = "20-12-2019 00:00";
		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());

		Manifestation manifestation = new Manifestation("Zvezda-Bajern", "champions league",
				new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), 3, new Location(),
				ManifestationState.PAST, ManifestationCategory.SPORT, null, null);

		Ticket t1 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate1.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t2 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate2.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t3 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate3.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t4 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate4.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t5 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate5.getTime()), new Seat(),
				new RegisteredUser(), manifestation);

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t1);
		tickets.add(t2);
		tickets.add(t3);
		tickets.add(t4);
		tickets.add(t5);

		Mockito.when(manifestationRepository.findById(1)).thenReturn(Optional.of(manifestation));
		Mockito.when(ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, timestamp1, timestamp2))
				.thenReturn(tickets);

		Map<String, Long> map = new HashMap<String, Long>();
		map.put("10-10-2019", 2L);
		map.put("12-11-2019", 1L);
		map.put("14-11-2019", 1L);
		map.put("10-12-2019", 1L);

		Map<String, Long> mapRes = manifestationService.getDaily(1, date1String, date2String);

		assertEquals(4, mapRes.size());
		assertEquals(Long.valueOf(2), mapRes.get("10-10-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("12-11-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("14-11-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("10-12-2019"));
		assertEquals(map, mapRes);

	}

	@Test(expected = InvalidDataException.class)
	public void getDailyTest_manifestationNotFound() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.empty());
		String date1String = "02-01-2019 00:00";
		String date2String = "08-01-2019 00:00";

		manifestationService.getDaily(manifestationId, date1String, date2String);
	}

	@Test(expected = InvalidDataException.class)
	public void getDailyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		Manifestation manifestation = new Manifestation();
		manifestation.setId(manifestationId);
		manifestation.setState(ManifestationState.PAST);
		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.of(manifestation));
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2019 00:00";

		manifestationService.getDaily(manifestationId, date1String, date2String);
	}

	@Test
	public void getDailyTest_emtpyList() throws ParseException, InvalidDataException {

		Integer manifestationId = 2;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");

		Date startDate = sdf.parse("02-12-2019 21:00");
		Date endDate = sdf.parse("02-12-2019 23:00");

		Manifestation manifestation = new Manifestation("Zvezda-Bajern", "champions league",
				new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), 3, new Location(),
				ManifestationState.PAST, ManifestationCategory.SPORT, null, null);
		String date1String = "02-06-2018 00:00";
		String date2String = "08-01-2019 00:00";

		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();

		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.of(manifestation));
		Mockito.when(ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, timestamp1, timestamp2))
				.thenReturn(tickets);

		Map<String, Long> mapRes = manifestationService.getDaily(manifestationId, date1String, date2String);

		assertEquals(0, mapRes.size());
	}

	@Test
	public void getWeeklyTest_ok() throws ParseException, InvalidDataException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");

		Date startDate = sdf.parse("02-12-2019 21:00");
		Date endDate = sdf.parse("02-12-2019 23:00");

		String buyingDate1 = "10-10-2019 00:00";
		String buyingDate2 = "10-10-2019 00:00";
		String buyingDate3 = "12-11-2019 00:00";
		String buyingDate4 = "14-11-2019 00:00";
		String buyingDate5 = "10-12-2019 00:00";

		Date bdate1 = sdf.parse(buyingDate1);
		Date bdate2 = sdf.parse(buyingDate2);
		Date bdate3 = sdf.parse(buyingDate3);
		Date bdate4 = sdf.parse(buyingDate4);
		Date bdate5 = sdf.parse(buyingDate5);

		String date1String = "02-09-2019 00:00";
		String date2String = "20-12-2019 00:00";
		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());

		Manifestation manifestation = new Manifestation("Zvezda-Bajern", "champions league",
				new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), 3, new Location(),
				ManifestationState.PAST, ManifestationCategory.SPORT, null, null);

		Ticket t1 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate1.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t2 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate2.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t3 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate3.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t4 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate4.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t5 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate5.getTime()), new Seat(),
				new RegisteredUser(), manifestation);

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t1);
		tickets.add(t2);
		tickets.add(t3);
		tickets.add(t4);
		tickets.add(t5);

		Mockito.when(manifestationRepository.findById(1)).thenReturn(Optional.of(manifestation));
		Mockito.when(ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, timestamp1, timestamp2))
				.thenReturn(tickets);

		Map<String, Long> map = new HashMap<String, Long>();
		map.put("Oct 2019 week: 2", 2L);
		map.put("Nov 2019 week: 3", 2L);
		map.put("Dec 2019 week: 2", 1L);

		Map<String, Long> mapRes = manifestationService.getWeekly(1, date1String, date2String);

		assertEquals(3, mapRes.size());
		assertEquals(Long.valueOf(2), mapRes.get("Oct 2019 week: 2"));
		assertEquals(Long.valueOf(2), mapRes.get("Nov 2019 week: 3"));
		assertEquals(Long.valueOf(1), mapRes.get("Dec 2019 week: 2"));
		assertEquals(map, mapRes);

	}

	@Test(expected = InvalidDataException.class)
	public void getWeeklyTest_manifestationNotFound() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.empty());
		String date1String = "02-01-2019 00:00";
		String date2String = "08-01-2019 00:00";

		manifestationService.getWeekly(manifestationId, date1String, date2String);
	}

	@Test(expected = InvalidDataException.class)
	public void getWeeklyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		Manifestation manifestation = new Manifestation();
		manifestation.setId(manifestationId);
		manifestation.setState(ManifestationState.PAST);
		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.of(manifestation));
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2019 00:00";

		manifestationService.getWeekly(manifestationId, date1String, date2String);
	}

	@Test
	public void getWeeklyTest_emtpyList() throws ParseException, InvalidDataException {

		Integer manifestationId = 2;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");

		Date startDate = sdf.parse("02-12-2019 21:00");
		Date endDate = sdf.parse("02-12-2019 23:00");

		Manifestation manifestation = new Manifestation("Zvezda-Bajern", "champions league",
				new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), 3, new Location(),
				ManifestationState.PAST, ManifestationCategory.SPORT, null, null);
		String date1String = "02-06-2018 00:00";
		String date2String = "08-01-2019 00:00";

		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();

		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.of(manifestation));
		Mockito.when(ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, timestamp1, timestamp2))
				.thenReturn(tickets);

		Map<String, Long> mapRes = manifestationService.getWeekly(manifestationId, date1String, date2String);

		assertEquals(0, mapRes.size());
	}

	@Test
	public void getMonthlyTest_ok() throws ParseException, InvalidDataException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");

		Date startDate = sdf.parse("02-12-2019 21:00");
		Date endDate = sdf.parse("02-12-2019 23:00");

		String buyingDate1 = "10-10-2019 00:00";
		String buyingDate2 = "10-10-2019 00:00";
		String buyingDate3 = "12-11-2019 00:00";
		String buyingDate4 = "14-11-2019 00:00";
		String buyingDate5 = "10-12-2019 00:00";

		Date bdate1 = sdf.parse(buyingDate1);
		Date bdate2 = sdf.parse(buyingDate2);
		Date bdate3 = sdf.parse(buyingDate3);
		Date bdate4 = sdf.parse(buyingDate4);
		Date bdate5 = sdf.parse(buyingDate5);

		String date1String = "02-09-2019 00:00";
		String date2String = "20-12-2019 00:00";
		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());

		Manifestation manifestation = new Manifestation("Zvezda-Bajern", "champions league",
				new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), 3, new Location(),
				ManifestationState.PAST, ManifestationCategory.SPORT, null, null);

		Ticket t1 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate1.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t2 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate2.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t3 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate3.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t4 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate4.getTime()), new Seat(),
				new RegisteredUser(), manifestation);
		Ticket t5 = new Ticket(true, new Timestamp(startDate.getTime()), new Timestamp(bdate5.getTime()), new Seat(),
				new RegisteredUser(), manifestation);

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t1);
		tickets.add(t2);
		tickets.add(t3);
		tickets.add(t4);
		tickets.add(t5);

		Mockito.when(manifestationRepository.findById(1)).thenReturn(Optional.of(manifestation));
		Mockito.when(ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, timestamp1, timestamp2))
				.thenReturn(tickets);

		Map<String, Long> map = new HashMap<String, Long>();
		map.put("Oct 2019", 2L);
		map.put("Nov 2019", 2L);
		map.put("Dec 2019", 1L);

		Map<String, Long> mapRes = manifestationService.getMonthly(1, date1String, date2String);

		assertEquals(3, mapRes.size());
		assertEquals(Long.valueOf(2), mapRes.get("Oct 2019"));
		assertEquals(Long.valueOf(2), mapRes.get("Nov 2019"));
		assertEquals(Long.valueOf(1), mapRes.get("Dec 2019"));
		assertEquals(map, mapRes);

	}

	@Test(expected = InvalidDataException.class)
	public void getMonthlyTest_manifestationNotFound() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.empty());
		String date1String = "02-01-2019 00:00";
		String date2String = "08-01-2019 00:00";

		manifestationService.getMonthly(manifestationId, date1String, date2String);
	}

	@Test(expected = InvalidDataException.class)
	public void getMonthlyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		Manifestation manifestation = new Manifestation();
		manifestation.setId(manifestationId);
		manifestation.setState(ManifestationState.PAST);
		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.of(manifestation));
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2019 00:00";

		manifestationService.getMonthly(manifestationId, date1String, date2String);
	}

	@Test
	public void getMonthlyTest_emtpyList() throws ParseException, InvalidDataException {

		Integer manifestationId = 2;
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");

		Date startDate = sdf.parse("02-12-2019 21:00");
		Date endDate = sdf.parse("02-12-2019 23:00");

		Manifestation manifestation = new Manifestation("Zvezda-Bajern", "champions league",
				new Timestamp(startDate.getTime()), new Timestamp(endDate.getTime()), 3, new Location(),
				ManifestationState.PAST, ManifestationCategory.SPORT, null, null);
		String date1String = "02-06-2018 00:00";
		String date2String = "08-01-2019 00:00";

		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();

		Mockito.when(manifestationRepository.findById(manifestationId)).thenReturn(Optional.of(manifestation));
		Mockito.when(ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, timestamp1, timestamp2))
				.thenReturn(tickets);

		Map<String, Long> mapRes = manifestationService.getMonthly(manifestationId, date1String, date2String);

		assertEquals(0, mapRes.size());
	}
}
