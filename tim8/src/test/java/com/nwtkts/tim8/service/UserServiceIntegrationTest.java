package com.nwtkts.tim8.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import com.nwtkts.tim8.dto.UserDTO;
import com.nwtkts.tim8.dto.UserEditProfileDTO;
import com.nwtkts.tim8.model.RegisteredUser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceIntegrationTest {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	@Test
	@Transactional
	public void editUserTest_changeFirstName_ok() throws InvalidDataException{
		
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String firstName = user.getFirstName();
		UserEditProfileDTO userDTO = new UserEditProfileDTO("Marko", "Peric");
		
		UserDTO userDTO_ = new UserDTO("user1", "123", "Marko", "Peric", "user1@gmail.com");
		
		UserDTO changedUser = userService.editUser(userDTO);
		
		assertEquals(userDTO_.getFirstName(), changedUser.getFirstName());
		assertEquals(userDTO_.getLastName(), changedUser.getLastName());
		assertEquals(userDTO_.getUsername(), changedUser.getUsername());
		assertEquals(userDTO_.getEmail(), changedUser.getEmail());
		assertFalse(firstName.equals(changedUser.getFirstName()));
				
	}
	
	@Test
	@Transactional
	public void editUserTest_changeLastName_ok() throws InvalidDataException{
		
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String lastName = user.getLastName();
		UserEditProfileDTO userDTO = new UserEditProfileDTO("Pera", "Markovic");
		
		UserDTO userDTO_ = new UserDTO("user1", "123", "Pera", "Markovic", "user1@gmail.com");
		
		UserDTO changedUser = userService.editUser(userDTO);
		
		assertEquals(userDTO_.getFirstName(), changedUser.getFirstName());
		assertEquals(userDTO_.getLastName(), changedUser.getLastName());
		assertEquals(userDTO_.getUsername(), changedUser.getUsername());
		assertEquals(userDTO_.getEmail(), changedUser.getEmail());
		assertFalse(lastName.equals(changedUser.getLastName()));
				
	}
	
	@Test
	@Transactional
	public void editUserTest_changeFirstAndLastName_ok() throws InvalidDataException{
		
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String lastName = user.getLastName();
		String firstName = user.getFirstName();
		
		UserEditProfileDTO userDTO = new UserEditProfileDTO("Marko", "Markovic");
		
		UserDTO userDTO_ = new UserDTO("user1", "123", "Marko", "Markovic", "user1@gmail.com");
		
		UserDTO changedUser = userService.editUser(userDTO);
		
		assertEquals(userDTO_.getFirstName(), changedUser.getFirstName());
		assertEquals(userDTO_.getLastName(), changedUser.getLastName());
		assertEquals(userDTO_.getUsername(), changedUser.getUsername());
		assertEquals(userDTO_.getEmail(), changedUser.getEmail());
		assertFalse(lastName.equals(changedUser.getFirstName()));
		assertFalse(firstName.equals(changedUser.getLastName()));
				
	}
	
	@Test(expected = InvalidDataException.class)
	public void editUserTest_emptyFields() throws InvalidDataException{
		
		UserEditProfileDTO userDTO = new UserEditProfileDTO("", "Peric");
		
		try {
			userService.editUser(userDTO);
		}
		catch (InvalidDataException e) {
			String message = "You can't leave empty fields";
			assertEquals(message, e.getMessage());
			throw e;
		}				
	}	

}
