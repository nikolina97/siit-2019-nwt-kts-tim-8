package com.nwtkts.tim8.service;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.UnsupportedEncodingException;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.MailException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.nwtkts.tim8.dto.UserChangePasswordDTO;
import com.nwtkts.tim8.model.Admin;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.User;
import com.nwtkts.tim8.repository.IUserRepository;
import com.nwtkts.tim8.repository.UserRepositoryIntegrationTest;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomUserDetailsServiceIntegrationTest {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private IUserRepository userRepository;
	
	@Test
	public void loadUserByUsername_userFound() {
		UserDetails u = customUserDetailsService.loadUserByUsername("user1");
		assertEquals("user1", u.getUsername());
		assertTrue(passwordEncoder.matches("123", u.getPassword())); //provjeravamo da li je lozinka encodovana
	}
	
	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsername_userNotFound() {
		
		customUserDetailsService.loadUserByUsername("user11");
	}
	
	@Test
	@Transactional
	public void registerRUser_successfully() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
		
		RegisteredUser user = new RegisteredUser("user12", "1234", "Marko", "Markovic", "user12@gmail.com", null);
		
		user = customUserDetailsService.saveRegisteredUser(user);
		
		assertEquals("user12", user.getUsername());
		assertEquals("user12@gmail.com", user.getEmail());
		assertTrue(passwordEncoder.matches("1234", user.getPassword()));  //provjeravamo da li je lozinka encodovana
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void registerRUser_usernameTaken() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
	
		RegisteredUser user = new RegisteredUser("user1", "111123", "Pera", "Peric", "abc@gmail.com", null);
			
		try {
			customUserDetailsService.saveRegisteredUser(user);		
		}
		catch (InvalidDataException e) {
			String message = "Username already taken!";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void registerRUser_emailTaken() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
	
		RegisteredUser user = new RegisteredUser("user111", "111123", "Pera", "Peric", "user1@gmail.com", null);
			
		try {
			customUserDetailsService.saveRegisteredUser(user);		
		}
		catch (InvalidDataException e) {
			String message = "Email already taken!";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void registerAdmin_successfully() throws InvalidDataException {
		
		User user = new User("admin2", "a1234", "Marko", "Markovic", "admin2@gmail.com", null);
		
		Admin a = new Admin(user);
		a = customUserDetailsService.saveAdmin(a);
		
		assertEquals("admin2", a.getUsername());
		assertEquals("admin2@gmail.com", a.getEmail());
		assertNotEquals("a1234", a.getPassword()); //provjeravamo da li je lozinka encodovana
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void registerAdmin_usernameTaken() throws InvalidDataException {
	
		User user = new User("user1", "111123", "Pera", "Peric", "abc@gmail.com", null);
		Admin a = new Admin(user);
		
		try {
			customUserDetailsService.saveAdmin(a);		
		}
		catch (InvalidDataException e) {
			String message = "Username already taken!";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void registerAdmin_emailTaken() throws InvalidDataException {
	
		User user = new User("admin123", "111123", "Pera", "Peric", "admin1@gmail.com", null);
		Admin a = new Admin(user);
		
		try {
			customUserDetailsService.saveAdmin(a);		
		}
		catch (InvalidDataException e) {
			String message = "Email already taken!";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test
	@Transactional
	public void changePassword_ok() throws InvalidDataException {
		UserChangePasswordDTO userDTO = new UserChangePasswordDTO("123", "new_pass", "new_pass");
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		customUserDetailsService.changePassword(userDTO);
		User u = userRepository.findByUsername("user1");
		
		assertTrue(passwordEncoder.matches(userDTO.getNewPassword(), u.getPassword()));
		
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void changePassword_invalidNewPassword() throws InvalidDataException {
		UserChangePasswordDTO userDTO = new UserChangePasswordDTO("123", "abc", "qwe");
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		try {
			customUserDetailsService.changePassword(userDTO);
		}
		catch (InvalidDataException e) {
			String message = "You heaven't repeted new password correctly!";
			assertEquals(message, e.getMessage());
			throw e;
		}	
	}
	
	@Test(expected = BadCredentialsException.class)
	@Transactional
	public void changePassword_invalidOldPassword() throws InvalidDataException {
		UserChangePasswordDTO userDTO = new UserChangePasswordDTO("1223", "abc", "abc");
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		customUserDetailsService.changePassword(userDTO);	
		
	}
}
