package com.nwtkts.tim8.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import com.nwtkts.tim8.dto.LocationDTO;
import com.nwtkts.tim8.dto.MainLocationInfoDTO;
import com.nwtkts.tim8.model.Location;
import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.ManifestationCategory;
import com.nwtkts.tim8.model.ManifestationState;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.Seat;
import com.nwtkts.tim8.model.SeatCategory;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.repository.LocationRepository;
import com.nwtkts.tim8.repository.TicketRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
public class LocationServiceUnitTest {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private LocationService locationService;
	
	@MockBean
	private LocationRepository locationRepository;
	
	@MockBean
	private TicketRepository ticketRepository;
	
	private String[] locationNames;
	private String[] locationAddresses;
	private Double[] locationLatitudes;
	private Double[] locationLongitudes;
	private Integer[] locationManifestationCounts;
	private String[] manifestationStates;
	private HashMap<Integer, ArrayList<Manifestation>> manifestationsForLocation;
	private ArrayList<Location> locationFindMockData;
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		locationNames = new String[] {
				"Djava", "Marakana", "Cair", "JNA"
		};

		locationAddresses = new String[] {
				"Petrovaradin", "Beograd", "Nis", "Beograd"
		};
		
		locationLatitudes = new Double[] {
				12.0, 25.0, 25.5, 41.1
		};
		
		locationLongitudes = new Double[] {
				12.0, 25.0, 25.5, 20.0
		};
		
		locationManifestationCounts = new Integer[] {
				3, 3, 3, 0
		};
		
		manifestationStates = new String[] {
				"OPEN", "PAST", "PAST", 
				"CANCELED", "SOLD_OUT", "OPEN", 
				"OPEN", "OPEN", "OPEN", 
				"OPEN", "OPEN", "OPEN"
		};
		
		manifestationsForLocation = new HashMap<>();
		manifestationsForLocation.put(1, new ArrayList<>());
		manifestationsForLocation.put(2, new ArrayList<>());
		manifestationsForLocation.put(3, new ArrayList<>());
		manifestationsForLocation.put(4, new ArrayList<>());
		
		for (Integer i : new Integer[] {1, 5, 6, 7, 8, 9, 10, 11, 12}) {
			Manifestation man = new Manifestation();
			man.setId(i);
			man.setState(ManifestationState.valueOf(manifestationStates[i-1]));
			if (i == 1 || i == 6 || i == 11) {
				manifestationsForLocation.get(1).add(man);
			}
			else if (i == 5 || i == 8 || i == 9) {
				manifestationsForLocation.get(2).add(man);
			}
			else if (i == 7 || i == 10 || i == 12) {
				manifestationsForLocation.get(3).add(man);
			}
		}
		
		Location djava = new Location("Petrovaradin", "Djava", 12.0, 12.0, null, manifestationsForLocation.get(1), true);
		djava.setId(1);
		Location marakana = new Location("Beograd", "Marakana", 25.0, 25.0, null, manifestationsForLocation.get(2), true);
		marakana.setId(2);
		Location cair = new Location("Nis", "Cair", 25.5, 25.5, null, manifestationsForLocation.get(3), true);
		cair.setId(3);
		Location jna = new Location("Beograd", "JNA", 41.1, 20.0, null, manifestationsForLocation.get(4), true);
		jna.setId(4);
		
		locationFindMockData = new ArrayList<>();
		locationFindMockData.add(djava);
		locationFindMockData.add(marakana);
		locationFindMockData.add(cair);
		locationFindMockData.add(jna);
	}
	
	@Rule
	public ExpectedException expectedEx = ExpectedException .none();
	
	
	@Test
	public void findById_ok() throws InvalidDataException {
		Integer id = 11;
		Location location = new Location();
		location.setAddress("Test Adresa");
		location.setLongitude(44.20);
		location.setLatitude(55.20);
		location.setName("Test Ime");
		location.setId(id);
		location.setManifestations(new ArrayList<Manifestation>());
		Mockito.when(locationRepository.findById(id)).thenReturn(Optional.of(location));
		MainLocationInfoDTO result = locationService.findById(id);
		assertEquals(location.getAddress(), result.getAddress());
		assertEquals(location.getLatitude(), result.getLatitude());
		assertEquals(location.getLongitude(), result.getLongitude());
		assertEquals(location.getId(), result.getLocationId());
		assertEquals(location.getName(), result.getName());
		assertEquals(Integer.valueOf(0), result.getManifestationCount());
	}
	
	@Test
	public void findById_notFound() throws InvalidDataException {
		Integer id = 42;
		Mockito.when(locationRepository.findById(id)).thenReturn(Optional.empty());
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location not found");

	    locationService.findById(id);
	}

	
	@Test
	public void saveLocation_missingData() throws InvalidDataException {
		LocationDTO location = new LocationDTO();
		location.setName("Ime1");
		location.setAddress("");
		location.setLatitude(20.3);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some data is missing");

	    locationService.save(location);
	}

	@Test
	public void saveLocation_emptyName() throws InvalidDataException {
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		LocationDTO location = new LocationDTO("","Petrovaradinnn", 55.5, 55.9, categories);

		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Name can't be empty");

	    locationService.save(location);
	}

	@Test
	public void saveLocation_emptyAddress() throws InvalidDataException {
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		LocationDTO location = new LocationDTO("Spens", "", 55.5, 55.9, categories);

		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Address can't be empty");

	    locationService.save(location);
	}

	@Test
	public void saveLocation_addressTaken() throws InvalidDataException {
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		LocationDTO location = new LocationDTO("Petrovaradin", "Spens", 55.5, 55.9, categories);

		Mockito.when(locationRepository.findOneByAddressAndLatitudeAndLongitude(location.getAddress(), location.getLatitude(), location.getLongitude())).thenReturn(new Location());


		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location on the same address already exists");

	    locationService.save(location);
	}

	@Test
	public void saveLocation_ok() throws InvalidDataException {
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Integer id = 1;
		MainLocationInfoDTO location = new MainLocationInfoDTO(id, "Djava", "Petrovaradin",55.5, 55.9, 0);

		Location locationSave = new Location("Petrovaradin", "Djava", 55.5, 55.9, categories, manifestations, true);
		locationSave.setId(id);

		LocationDTO locationDTO = new LocationDTO("Spens", "Petrovaradin", 55.6, 55.9, categories);
		
		//Mockito.when(locationRepository.findOneByAddressAndLatitudeAndLongitude(locationDTO.getAddress(),locationDTO.getLatitude(), locationDTO.getLongitude())).thenReturn(Optional.empty());
		Mockito.when(locationRepository.save(any(Location.class))).thenReturn(locationSave);

		MainLocationInfoDTO savedLocation = locationService.save(locationDTO);

		assertEquals(location.getLocationId(), savedLocation.getLocationId());
		assertEquals(location.getAddress(), savedLocation.getAddress());
		assertEquals(location.getLatitude(), savedLocation.getLatitude());
		assertEquals(location.getLongitude(), savedLocation.getLongitude());
		assertEquals(location.getName(), savedLocation.getName());
		assertEquals(Integer.valueOf(0), savedLocation.getManifestationCount());

	}

	@Test
	public void updateLocation_missingFields() throws InvalidDataException {
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO();
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some data is missing");

	    Integer id = 1;
	    locationService.update(id, locationDTO);
	}

	@Test
	public void updateLocation_emptyName() throws InvalidDataException {
		Integer id = 1;
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(id, "", "Novi Sad", 45.6, 42.3, null);

	    HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location location = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		location.setId(id);
		Mockito.when(locationRepository.findById(id)).thenReturn(Optional.of(location));

		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Name can't be empty");
	    locationService.update(id, locationDTO);
	}

	@Test
	public void updateLocation_emptyAddress() throws InvalidDataException {
		Integer id = 1;
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(id, "Spens", "", 45.6, 42.3, 6);

	    HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location location = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		location.setId(id);
		Mockito.when(locationRepository.findById(id)).thenReturn(Optional.of(location));

		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Address can't be empty");
	    locationService.update(id, locationDTO);
	}

	@Test
	public void updateLocation_locationNotFound() throws InvalidDataException {
		Integer id = 1;
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(id, "Spens", "Petrovaradin", 45.6, 42.3, 11);

		Mockito.when(locationRepository.findById(id)).thenReturn(Optional.empty());

		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location not found");
	    locationService.update(id, locationDTO);
	}

	@Test
	public void updateLocation_addressTaken() throws InvalidDataException {
		Integer id = 1;
		Integer id2 = 2;
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(id, "Spens", "Novi Sad", 45.6, 42.3, 14);

	    HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location location = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		location.setId(id);
		Location location2 = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		location2.setId(id2);
		Mockito.when(locationRepository.findById(id)).thenReturn(Optional.of(location));
	    Mockito.when(locationRepository.findOneByAddressAndLatitudeAndLongitude(locationDTO.getAddress(), locationDTO.getLatitude(), locationDTO.getLongitude())).thenReturn(location2);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location on the same address already exists");
	    locationService.update(id, locationDTO);
	}

	@Test
	public void updateLocation_ok() throws InvalidDataException {
		Integer id = 1;
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(id, "Djava", "Petrovaradin", 55.5, 55.6, 7);
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location location = new Location("Petrovaradin", "Tvrdjava", 55.5, 55.9, categories, manifestations, true);
		location.setId(id);
		Mockito.when(locationRepository.findById(id)).thenReturn(Optional.of(location));
	    Mockito.when(locationRepository.findOneByAddressAndLatitudeAndLongitude(locationDTO.getAddress(), locationDTO.getLatitude(), locationDTO.getLongitude())).thenReturn(null);
		Mockito.when(locationRepository.save(location)).thenReturn(location);
		MainLocationInfoDTO updatedLocation = locationService.update(id, locationDTO);
		assertEquals(locationDTO.getLocationId(), updatedLocation.getLocationId());
		assertEquals(locationDTO.getAddress(), updatedLocation.getAddress());
		assertEquals(locationDTO.getLatitude(), updatedLocation.getLatitude());
		assertEquals(locationDTO.getLongitude(), updatedLocation.getLongitude());
		assertEquals(locationDTO.getName(), updatedLocation.getName());
	}
	
	@Test
	public void deleteLocation_ok() throws Exception{
		Location location = new Location();
		location.setId(1);
		location.setAddress("Petrovaradinn");
		location.setLatitude(55.5);
		location.setLongitude(55.5);
		location.setName("Djava");
		
		Manifestation m = new Manifestation();
		m.setState(ManifestationState.PAST);
		
		List<Manifestation> manifestations = new ArrayList<>();
		manifestations.add(m);
		location.setManifestations(manifestations);
		
		Mockito.when(locationRepository.findById(location.getId())).thenReturn(Optional.of(location));
		Mockito.when(locationRepository.save(any(Location.class))).thenReturn(location);
		
		assertTrue(locationService.delete(1));
		assertFalse(location.getActive());
	}
	
	@Test
	public void deleteLocation_locationNotFound() throws Exception{
		Integer id = 1;
		Mockito.when(locationRepository.findById(id)).thenReturn(Optional.empty());
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location not found");
		locationService.delete(id);
	}
	
	public void deleteLocation_withActiveManifestation() throws Exception{
		Location location = new Location();
		location.setId(1);
		location.setAddress("Petrovaradinn");
		location.setLatitude(55.5);
		location.setLongitude(55.5);
		location.setName("Djava");
		
		Manifestation m = new Manifestation();
		m.setState(ManifestationState.OPEN);
		
		List<Manifestation>manifestations = new ArrayList<>();
		manifestations.add(m);
		location.setManifestations(manifestations);
		
		Mockito.when(locationRepository.findById(location.getId())).thenReturn(Optional.of(location));
		Mockito.when(locationRepository.save(location)).thenReturn(location);
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some manifestations will be held on this location");
		locationService.delete(location.getId());
	}
	
	@Test
	public void findAll_successful() {
		PageRequest pageRequest = PageRequest.of(0, 10);
		
		Mockito.when(locationRepository.findAll(pageRequest))
			.thenReturn(new PageImpl<Location>(locationFindMockData));
		
		List<MainLocationInfoDTO> locations = locationService.getAllPageable(pageRequest);
		ArrayList<Integer> returnedIds = new ArrayList<>(
				locations.stream().map(m -> m.getLocationId()).collect(Collectors.toList()));
		
		assertEquals(locations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 2, 3, 4
		}))));
		
		for (MainLocationInfoDTO location : locations) {
			assertEquals(location.getName(), locationNames[location.getLocationId()-1]);
			assertEquals(location.getAddress(), locationAddresses[location.getLocationId()-1]);
			assertEquals(location.getLatitude(), locationLatitudes[location.getLocationId()-1]);
			assertEquals(location.getLongitude(), locationLongitudes[location.getLocationId()-1]);
			assertEquals(location.getManifestationCount(), locationManifestationCounts[location.getLocationId()-1]);
		}
	}
	
	@Test
	public void findAll_simplePageable() {
		PageRequest pageRequest = PageRequest.of(1, 2);
		
		Mockito.when(locationRepository.findAll(pageRequest))
			.thenReturn(new PageImpl<Location>(locationFindMockData.subList(2, 4)));
		
		List<MainLocationInfoDTO> locations = locationService.getAllPageable(pageRequest);
		ArrayList<Integer> returnedIds = new ArrayList<>(
				locations.stream().map(m -> m.getLocationId()).collect(Collectors.toList()));
		
		assertEquals(locations.size(), 2);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				3, 4
		}))));
		
		for (MainLocationInfoDTO location : locations) {
			assertEquals(location.getName(), locationNames[location.getLocationId()-1]);
			assertEquals(location.getAddress(), locationAddresses[location.getLocationId()-1]);
			assertEquals(location.getLatitude(), locationLatitudes[location.getLocationId()-1]);
			assertEquals(location.getLongitude(), locationLongitudes[location.getLocationId()-1]);
			assertEquals(location.getManifestationCount(), locationManifestationCounts[location.getLocationId()-1]);
		}
	}
	
	@Test
	public void findAll_tooFarPageable() {
		PageRequest pageRequest = PageRequest.of(2, 2);
		
		Mockito.when(locationRepository.findAll(pageRequest))
			.thenReturn(new PageImpl<Location>(new ArrayList<Location>()));
		
		List<MainLocationInfoDTO> locations = locationService.getAllPageable(pageRequest);
		
		assertEquals(locations.size(), 0);
	}
	
	@Test
	public void getDailyTest_successfull() throws ParseException, InvalidDataException {

		Integer locationId = 2;	
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		String date1String = "02-09-2019 00:00";
		String date2String = "20-12-2019 00:00";
		
		String buyingDate1 = "10-10-2019 00:00";
		String buyingDate2 = "10-10-2019 00:00";
		String buyingDate3 = "12-11-2019 00:00";
		String buyingDate4 = "14-11-2019 00:00";
		String buyingDate5 = "10-12-2019 00:00";

		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		Date date1 = sdf.parse("03-01-2020 21:00");
		Date bdate1 = sdf.parse(buyingDate1);
		Date bdate2 = sdf.parse(buyingDate2);
		Date bdate3 = sdf.parse(buyingDate3);
		Date bdate4 = sdf.parse(buyingDate4);
		Date bdate5 = sdf.parse(buyingDate5);


		
		Ticket t1 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate1.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t2 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate2.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t3 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate3.getTime()),new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t4 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate4.getTime()),new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t5 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate5.getTime()),new Seat(), new RegisteredUser(), new Manifestation());

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t1);
		tickets.add(t2);
		tickets.add(t3);
		tickets.add(t4);
		tickets.add(t5);
		
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		Mockito.when(ticketRepository.findByLocation(locationId, timestamp1, timestamp2)).thenReturn(tickets);
		
		Map<String, Long> map = new HashMap<String, Long>();
		map.put("10-10-2019", 2L);
		map.put("12-11-2019", 1L);
		map.put("14-11-2019", 1L);
		map.put("10-12-2019", 1L);
		
		Map<String, Long> mapRes = locationService.getDaily(locationId,date1String , date2String);
		
		assertEquals(4, mapRes.size());
		assertEquals(Long.valueOf(2), mapRes.get("10-10-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("12-11-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("14-11-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("10-12-2019"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getDailyTest_locationNotFound() throws InvalidDataException, ParseException {
		Integer locationId = 2;
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.empty());
		String date1String = "02-01-2019 00:00";
		String date2String = "08-01-2019 00:00";
		
		locationService.getDaily(locationId,date1String , date2String);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getDailyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer locationId = 2;
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);	
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2019 00:00";
		
		locationService.getDaily(locationId,date1String , date2String);
	}
	
	@Test
	public void getDailyTest_emtpyList() throws ParseException, InvalidDataException {

		Integer locationId = 2;	
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		String date1String = "02-06-2010 00:00";
		String date2String = "08-01-2020 00:00";

		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		
		
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		Mockito.when(ticketRepository.findByLocation(locationId, timestamp1, timestamp2)).thenReturn(tickets);

		
		Map<String, Long> mapRes = locationService.getDaily(locationId,date1String , date2String);
		
		assertEquals(0, mapRes.size());
	}
	
	@Test
	public void getWeeklyTest_successfull() throws ParseException, InvalidDataException {

		Integer locationId = 2;	
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		String date1String = "02-08-2019 00:00";
		String date2String = "10-01-2020 00:00";

		String buyingDate1 = "02-09-2019 00:00";
		String buyingDate2 = "06-09-2019 00:00";
		String buyingDate3 = "16-10-2019 00:00";
		String buyingDate4 = "21-10-2019 00:00";
		String buyingDate5 = "22-10-2019 00:00";
		
		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		Date date1 = sdf.parse("03-01-2020 21:00");
		Date bdate1 = sdf.parse(buyingDate1);
		Date bdate2 = sdf.parse(buyingDate2);
		Date bdate3 = sdf.parse(buyingDate3);
		Date bdate4 = sdf.parse(buyingDate4);
		Date bdate5 = sdf.parse(buyingDate5);

		
		Ticket t1 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate1.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t2 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate2.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t3 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate3.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t4 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate4.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t5 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate5.getTime()), new Seat(), new RegisteredUser(), new Manifestation());

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t1);
		tickets.add(t2);
		tickets.add(t3);
		tickets.add(t4);
		tickets.add(t5);

		
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		Mockito.when(ticketRepository.findByLocation(locationId, timestamp1, timestamp2)).thenReturn(tickets);
		
		Map<String, Long> map = new HashMap<String, Long>();
		map.put("Sep 2019 week: 1", 2L);
		map.put("Oct 2019 week: 3", 1L);
		map.put("Oct 2019 week: 4", 2L);
		
		Map<String, Long> mapRes = locationService.getWeekly(locationId,date1String , date2String);
		
		assertEquals(3, mapRes.size());
		assertEquals(Long.valueOf(2), mapRes.get("Sep 2019 week: 1"));
		assertEquals(Long.valueOf(1), mapRes.get("Oct 2019 week: 3"));
		assertEquals(Long.valueOf(2), mapRes.get("Oct 2019 week: 4"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getWeeklyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer locationId = 2;
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);	
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2020 00:00";
		
		locationService.getWeekly(locationId,date1String , date2String);
	}
	
	@Test
	public void getWeeklyTest_emtpyList() throws ParseException, InvalidDataException {

		Integer locationId = 2;	
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		String date1String = "02-01-2020 00:00";
		String date2String = "08-01-2020 00:00";

		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		
		
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		Mockito.when(ticketRepository.findByLocation(locationId, timestamp1, timestamp2)).thenReturn(tickets);

		
		Map<String, Long> mapRes = locationService.getWeekly(locationId,date1String , date2String);
		
		assertEquals(0, mapRes.size());
	}
	
	@Test
	public void getMonthlyTest_successfull() throws ParseException, InvalidDataException {

		Integer locationId = 2;	
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		String date1String = "02-08-2019 00:00";
		String date2String = "10-03-2020 00:00";
		
		String buyingDate1 = "02-09-2019 00:00";
		String buyingDate2 = "06-09-2019 00:00";
		String buyingDate3 = "16-10-2019 00:00";
		String buyingDate4 = "21-10-2019 00:00";
		String buyingDate5 = "22-10-2019 00:00";
		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		Date date1 = sdf.parse("03-01-2020 21:00");
		Date bdate1 = sdf.parse(buyingDate1);
		Date bdate2 = sdf.parse(buyingDate2);
		Date bdate3 = sdf.parse(buyingDate3);
		Date bdate4 = sdf.parse(buyingDate4);
		Date bdate5 = sdf.parse(buyingDate5);
		
		Ticket t1 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate1.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t2 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate2.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t3 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate3.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t4 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate4.getTime()), new Seat(), new RegisteredUser(), new Manifestation());
		Ticket t5 = new Ticket(true, new Timestamp(date1.getTime()), new Timestamp(bdate5.getTime()), new Seat(), new RegisteredUser(), new Manifestation());

		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		tickets.add(t1);
		tickets.add(t2);
		tickets.add(t3);
		tickets.add(t4);
		tickets.add(t5);

		
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		Mockito.when(ticketRepository.findByLocation(locationId, timestamp1, timestamp2)).thenReturn(tickets);
		
		Map<String, Long> map = new HashMap<String, Long>();
		map.put("Sep 2019", 2L);
		map.put("Oct 2019", 3L);
		
		Map<String, Long> mapRes = locationService.getMonthly(locationId,date1String , date2String);
		
		assertEquals(2, mapRes.size());
		assertEquals(Long.valueOf(2), mapRes.get("Sep 2019"));
		assertEquals(Long.valueOf(3), mapRes.get("Oct 2019"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getMonthlyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer locationId = 2;
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);	
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2020 00:00";
		
		locationService.getMonthly(locationId,date1String , date2String);
	}
	
	@Test
	public void getMonthlyTest_emtpyList() throws ParseException, InvalidDataException {

		Integer locationId = 2;	
		Location location = new Location("Petrovaradin", "Djava", 12.00, 12.00, null, null, true);		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
		String date1String = "02-01-2020 00:00";
		String date2String = "08-01-2020 00:00";

		Date parseDate1 = sdf.parse(date1String);
		Date parseDate2 = sdf.parse(date2String);
		Timestamp timestamp1 = new Timestamp(parseDate1.getTime());
		Timestamp timestamp2 = new Timestamp(parseDate2.getTime());
		
		ArrayList<Ticket> tickets = new ArrayList<Ticket>();
		
		
		Mockito.when(locationRepository.findById(locationId)).thenReturn(Optional.of(location));
		Mockito.when(ticketRepository.findByLocation(locationId, timestamp1, timestamp2)).thenReturn(tickets);

		
		Map<String, Long> mapRes = locationService.getMonthly(locationId,date1String , date2String);
		
		assertEquals(0, mapRes.size());
	}
}
