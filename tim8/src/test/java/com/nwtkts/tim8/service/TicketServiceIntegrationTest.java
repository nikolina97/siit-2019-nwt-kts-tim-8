package com.nwtkts.tim8.service;

import com.nwtkts.tim8.dto.CreatePaymentDTO;
import com.nwtkts.tim8.dto.TicketDTO;
import com.nwtkts.tim8.model.RegisteredUser;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketServiceIntegrationTest {

	@Autowired
	private TicketService ticketService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void buyTicketsTest_nonExistingSeat_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(150);

		ticketService.buyTickets(ids, new Date());
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void buyTicketsTest_noLongerPossibleToReserve_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(10);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2019, 11, 2, 0, 0, 0);

		ticketService.buyTickets(ids, cal.getTime());
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void buyTicketsTest_manifestationStateIsNotOpen_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(20);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2019, Calendar.DECEMBER, 26, 0, 0, 0);

		ticketService.buyTickets(ids, cal.getTime());
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void buyTicketsTest_someoneElsesReservedSeat_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 6, 4, 0, 0, 0);

		ticketService.buyTickets(ids, cal.getTime());
	}

	@Test
	@Transactional
	@Ignore
	public void buyTicketsTest_ok() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(28);
		ids.add(29);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 8, 6, 0, 0, 0);

		ticketService.buyTickets(ids, cal.getTime());
	}

	@Test
	@Transactional
	@Ignore
	public void buyTicketsTest_festivalTickets() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(28);
		ids.add(29);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 8, 6, 0, 0, 0);

		ticketService.buyTickets(ids, null);
	}

	@Test
	@Transactional
	public void getAllBoughtTicketsByUserTest() throws InvalidDataException {
		RegisteredUser user = (RegisteredUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ArrayList<TicketDTO> result = (ArrayList) ticketService.getAllBoughtTicketsByUser(user);

		assertNotNull(result);
		assertEquals(3, result.size());
		assertEquals(Integer.valueOf(1), result.get(0).getRow());
		assertEquals(Integer.valueOf(2), result.get(0).getColumn());
		assertEquals(Double.valueOf(150), result.get(0).getTicketPrice());
		assertEquals(Integer.valueOf(1), result.get(0).getManifestationId());
		assertEquals("Exit", result.get(0).getManifestationName());
		assertEquals("Tribina 1", result.get(0).getSeatCategoryName());
		assertEquals(Integer.valueOf(2), result.get(1).getRow());
		assertEquals(Integer.valueOf(2), result.get(1).getColumn());
		assertEquals(Double.valueOf(150), result.get(1).getTicketPrice());
		assertEquals(Integer.valueOf(1), result.get(1).getManifestationId());
		assertEquals("Exit", result.get(1).getManifestationName());
		assertEquals("Tribina 1", result.get(1).getSeatCategoryName());
		assertEquals(Integer.valueOf(1), result.get(2).getRow());
		assertEquals(Integer.valueOf(2), result.get(2).getColumn());
		assertEquals(Double.valueOf(200), result.get(2).getTicketPrice());
		assertEquals(Integer.valueOf(2), result.get(2).getManifestationId());
		assertEquals("Cola", result.get(2).getManifestationName());
		assertEquals("Tribina 1", result.get(2).getSeatCategoryName());
	}

	@Test
	@Transactional
	public void getAllReservedTicketsByUserTest() throws InvalidDataException {
		RegisteredUser user = (RegisteredUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ArrayList<TicketDTO> result = (ArrayList) ticketService.getAllReservedTicketsByUser(user);

		assertNotNull(result);
		assertEquals(4, result.size());
		assertEquals(Integer.valueOf(1), result.get(0).getRow());
		assertEquals(Integer.valueOf(1), result.get(0).getColumn());
		assertEquals(Double.valueOf(150), result.get(0).getTicketPrice());
		assertEquals(Integer.valueOf(1), result.get(0).getManifestationId());
		assertEquals("Exit", result.get(0).getManifestationName());
		assertEquals("Tribina 1", result.get(0).getSeatCategoryName());
		assertEquals(Integer.valueOf(1), result.get(1).getRow());
		assertEquals(Integer.valueOf(1), result.get(1).getColumn());
		assertEquals(Double.valueOf(150), result.get(1).getTicketPrice());
		assertEquals(Integer.valueOf(1), result.get(1).getManifestationId());
		assertEquals("Exit", result.get(1).getManifestationName());
		assertEquals("Tribina 1", result.get(1).getSeatCategoryName());
		assertEquals(Integer.valueOf(1), result.get(2).getRow());
		assertEquals(Integer.valueOf(1), result.get(2).getColumn());
		assertEquals(Double.valueOf(150), result.get(2).getTicketPrice());
		assertEquals(Integer.valueOf(1), result.get(2).getManifestationId());
		assertEquals("Exit", result.get(2).getManifestationName());
		assertEquals("Tribina 1", result.get(2).getSeatCategoryName());
		assertEquals(Integer.valueOf(1), result.get(3).getRow());
		assertEquals(Integer.valueOf(1), result.get(3).getColumn());
		assertEquals(Double.valueOf(150), result.get(3).getTicketPrice());
		assertEquals(Integer.valueOf(1), result.get(3).getManifestationId());
		assertEquals("Exit", result.get(3).getManifestationName());
		assertEquals("Tribina 1", result.get(3).getSeatCategoryName());
	}
	
	@Test
	@Transactional
	public void payTicketTest_ok() throws InvalidDataException {
		Integer id = 1;
		CreatePaymentDTO cpDTO = ticketService.payTicket(id);
		assertNotNull(cpDTO);
		assertEquals("success", cpDTO.getStatus());
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void payTicketTest_wrongTickets() throws InvalidDataException {
		Integer id = 20;
		try {
			ticketService.payTicket(id);	
		}catch (InvalidDataException e) {
			String message = "This ticket does not exists or ticket is not yours";
			assertEquals(message, e.getMessage());
			throw e;
		}
		
	}
	
	@Test
	@Transactional
	public void markAsPaidTest_ok() throws InvalidDataException {
		String ticketS = "1";
		ticketService.markAsPaid(ticketS);
	}
	
	@Test(expected = InvalidDataException.class)
	public void markAsPaidTest_wrongTickets() throws InvalidDataException {
		String ticketS = "12-13";
		try {
			ticketService.markAsPaid(ticketS);	
		}catch (InvalidDataException e) {
			String message = "This ticket does not exists or ticket is not yours";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}

	@Test
	@Transactional
	public void deleteTickets_ok() throws InvalidDataException {
		String ticketS = "1-5";
		ticketService.deleteTickets(ticketS);
	}
	
	@Test(expected = InvalidDataException.class)
	public void deleteTickets_wrongTickets() throws InvalidDataException {
		String ticketS = "12-13";
		try {
			ticketService.deleteTickets(ticketS);	
		}catch (InvalidDataException e) {
			String message = "This ticket does not exists or ticket is not yours";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
}
