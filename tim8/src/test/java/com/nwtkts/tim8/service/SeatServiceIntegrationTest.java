package com.nwtkts.tim8.service;

import com.nwtkts.tim8.dto.SeatsBySupportedSeatCategoryDTO;
import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.Seat;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.repository.SeatRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SeatServiceIntegrationTest {

	@Autowired
	private SeatService seatService;

	@Autowired
	private SeatRepository seatRepository;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void reserveSeatsTest_nonExistingSeats_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(150);

		seatService.reserveSeats(ids, new Date());
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void reserveSeatsTest_reservationExpired_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(14);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2019, 11, 2, 0, 0, 0);

		seatService.reserveSeats(ids, cal.getTime());
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void reserveSeatsTest_notAllowedManifState_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(40);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 11, 26, 0, 0, 0);

		seatService.reserveSeats(ids, cal.getTime());
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void reserveSeatsTest_reservedSeatWithFestivalTicket_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 6, 4, 0, 0, 0);

		seatService.reserveSeats(ids, cal.getTime());
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void reserveSeatsTest_reservedSeat_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(3);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 6, 4, 0, 0, 0);

		seatService.reserveSeats(ids, cal.getTime());
	}

	@Test
	@Transactional
	public void reserveSeatsTest_singleSeatOk() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(7);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 6, 4, 0, 0, 0);

		List<Ticket> result = seatService.reserveSeats(ids, cal.getTime());

		assertEquals(1, result.size());
		assertFalse(result.get(0).getPaid());
		assertEquals(cal.getTimeInMillis(), result.get(0).getDate().getTime());
		assertEquals(7, result.get(0).getSeat().getId().intValue());
		assertEquals(3, result.get(0).getSeat().getRow().intValue());
		assertEquals(1, result.get(0).getSeat().getColumn().intValue());
		assertEquals(1, result.get(0).getManifestation().getId().intValue());
		assertEquals("user1", result.get(0).getUser().getUsername());
	}

	@Test
	@Transactional
	public void reserveSeatsTest_multipleSeatsOk() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(7);
		ids.add(8);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 6, 4, 0, 0, 0);

		List<Ticket> result = seatService.reserveSeats(ids, cal.getTime());

		assertEquals(2, result.size());
		assertFalse(result.get(0).getPaid());
		assertEquals(cal.getTimeInMillis(), result.get(0).getDate().getTime());
		assertEquals(7, result.get(0).getSeat().getId().intValue());
		assertEquals(3, result.get(0).getSeat().getRow().intValue());
		assertEquals(1, result.get(0).getSeat().getColumn().intValue());
		assertEquals(1, result.get(0).getManifestation().getId().intValue());
		assertEquals("user1", result.get(0).getUser().getUsername());
		assertFalse(result.get(1).getPaid());
		assertEquals(cal.getTimeInMillis(), result.get(1).getDate().getTime());
		assertEquals(8, result.get(1).getSeat().getId().intValue());
		assertEquals(3, result.get(1).getSeat().getRow().intValue());
		assertEquals(2, result.get(1).getSeat().getColumn().intValue());
		assertEquals(1, result.get(1).getManifestation().getId().intValue());
		assertEquals("user1", result.get(1).getUser().getUsername());
	}

	@Test
	@Transactional
	public void checkIfReservationExpiredTest_forbidden() {
		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(-1);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 0, 1);

		assertEquals("Reservation is forbidden", seatService.checkIfReservationExpired(manifestation, cal.getTime()));
	}

	@Test
	@Transactional
	public void checkIfReservationExpiredTest_passed() {
		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(14);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2019, 1, 12);

		assertEquals("Manifestation has passed", seatService.checkIfReservationExpired(manifestation, cal.getTime()));
	}

	@Test
	@Transactional
	public void checkIfReservationExpiredTest_expired() {
		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(14);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 12);

		assertEquals("Reservation expired", seatService.checkIfReservationExpired(manifestation, cal.getTime()));
	}

	@Test
	@Transactional
	public void checkIfReservationExpiredTest_ok() {
		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(5);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 1);

		assertNull(seatService.checkIfReservationExpired(manifestation, cal.getTime()));
	}

	@Test
	@Transactional
	public void checkIfSeatIsReservedTest_reservedSeat() {
		Seat seat = seatRepository.findById(1).get();

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 6, 3);

		assertTrue(seatService.checkIfSeatIsReserved(seat, cal.getTime()));
	}

	@Test
	@Transactional
	public void checkIfSeatIsReservedTest_ok() {
		Seat seat = seatRepository.findById(1).get();

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 6, 4);

		assertTrue(seatService.checkIfSeatIsReserved(seat, cal.getTime()));
	}

	@Test(expected = InvalidDataException.class)
	public void getAllSeatsByManifestationTest_nonExistingManif_throwsInvalidDataException() throws InvalidDataException {
		Integer id = 150;
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 2, 0, 0, 0);

		seatService.getAllSeatsByManifestation(id, cal.getTime());
	}

	@Test(expected = InvalidDataException.class)
	public void getAllSeatsByManifestationTest_notAllowedManifState_throwsInvalidDataException() throws InvalidDataException {
		Integer id = 3;
		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 11, 3, 10, 10, 10);

		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		cal2.set(2020, 11, 5, 10, 10, 10);

		seatService.getAllSeatsByManifestation(id, cal1.getTime());
	}

	@Test
	public void getAllSeatsByManifestationTest_ok() throws InvalidDataException {
		Integer id = 1;
		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 6, 3, 0, 0, 0);

		ArrayList<SeatsBySupportedSeatCategoryDTO> result = (ArrayList<SeatsBySupportedSeatCategoryDTO>) seatService.getAllSeatsByManifestation(id, cal1.getTime());

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(9, result.get(0).getSeats().size());
	}

}
