package com.nwtkts.tim8.service;

import com.nwtkts.tim8.dto.SeatsBySupportedSeatCategoryDTO;
import com.nwtkts.tim8.model.*;
import com.nwtkts.tim8.repository.ManifestationRepository;
import com.nwtkts.tim8.repository.SeatRepository;
import com.nwtkts.tim8.repository.SupportedSeatCategoryRepository;
import com.nwtkts.tim8.repository.TicketRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SeatServiceUnitTest {

	@Autowired
	private SeatService seatService;

	@Autowired
	private AuthenticationManager authenticationManager;

	@MockBean
	private SeatRepository seatRepositoryMocked;

	@MockBean
	private TicketRepository ticketRepositoryMocked;

	@MockBean
	private ManifestationRepository manifestationRepositoryMocked;

	@MockBean
	private SupportedSeatCategoryRepository supportedSeatCategoryRepositoryMocked;

	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	@Test
	public void checkIfReservationExpiredTest_forbidden() {
		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(-1);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 1, 0, 0, 0);

		assertEquals("Reservation is forbidden", seatService.checkIfReservationExpired(manifestation, cal.getTime()));
	}

	@Test
	public void checkIfReservationExpiredTest_passed() {
		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 0, 1, 0, 0, 0);

		assertEquals("Manifestation has passed", seatService.checkIfReservationExpired(manifestation, cal.getTime()));
	}

	@Test
	public void checkIfReservationExpiredTest_expired() {
		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(14);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 11, 0, 0, 0);

		assertEquals("Reservation expired", seatService.checkIfReservationExpired(manifestation, cal.getTime()));
	}

	@Test
	public void checkIfReservationExpiredTest_ok() {
		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 1, 0, 0, 0);

		assertNull(seatService.checkIfReservationExpired(manifestation, cal.getTime()));
	}

	@Test
	public void checkManifestationStateTest() {
		Manifestation manifestation = new Manifestation();

		manifestation.setState(ManifestationState.OPEN);
		assertNull(seatService.checkManifestationState(manifestation));
		manifestation.setState(ManifestationState.SOLD_OUT);
		assertEquals("All tickets are sold out", seatService.checkManifestationState(manifestation));
		manifestation.setState(ManifestationState.PAST);
		assertEquals("Manifestation has passed", seatService.checkManifestationState(manifestation));
		manifestation.setState(ManifestationState.CANCELED);
		assertEquals("Manifestation is canceled", seatService.checkManifestationState(manifestation));
	}

	@Test
	public void checkIfSeatIsReservedTest_reservedSeat() {
		Seat seat = new Seat();

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 0, 1, 0, 0, 0);

		Ticket ticket = new Ticket();
		ticket.setActive(true);

		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat, new Timestamp(cal.getTimeInMillis()), true)).thenReturn(Optional.of(ticket));
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat)).thenReturn(Optional.empty());

		assertTrue(seatService.checkIfSeatIsReserved(seat, cal.getTime()));
	}

	@Test
	public void checkIfSeatIsReservedTest_reservedSeatWithFestivalTicket() {
		Seat seat = new Seat();

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 0, 1, 0, 0, 0);

		Ticket ticket = new Ticket();
		ticket.setActive(true);

		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat, new Timestamp(cal.getTimeInMillis()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat)).thenReturn(Optional.of(ticket));

		assertTrue(seatService.checkIfSeatIsReserved(seat, cal.getTime()));
	}

	@Test
	public void checkIfSeatIsReservedTest_nonReservedSeat() {
		Seat seat = new Seat();

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 0, 1, 0, 0, 0);

		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat, new Timestamp(cal.getTimeInMillis()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat)).thenReturn(Optional.empty());

		assertFalse(seatService.checkIfSeatIsReserved(seat, cal.getTime()));
	}

	@Test
	public void checkSeatForFestivalTicketTest_seatHasTaken() {
		Seat seat = new Seat();

		Manifestation manifestation = new Manifestation();
		Calendar cal = Calendar.getInstance();
		cal.clear();

		cal.set(2020, 2, 2, 0, 0, 0);
		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));

		List<Ticket> tickets = new ArrayList<>();
		tickets.add(new Ticket());

		Mockito.when(ticketRepositoryMocked.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate())).thenReturn(tickets);

		assertFalse(seatService.checkSeatForFestivalTicket(seat, manifestation));
	}

	@Test
	public void checkSeatForFestivalTicketTest_seatHasNotTaken() {
		Seat seat = new Seat();

		Manifestation manifestation = new Manifestation();
		Calendar cal = Calendar.getInstance();
		cal.clear();

		cal.set(2020, 2, 2, 0, 0, 0);
		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));

		Mockito.when(ticketRepositoryMocked.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate())).thenReturn(new ArrayList<>());

		assertTrue(seatService.checkSeatForFestivalTicket(seat, manifestation));
	}

	@Test
	public void isFestivalTicketTest_true() {
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(any(Seat.class))).thenReturn(Optional.of(new Ticket()));

		assertTrue(seatService.isFestivalTicket(new Seat()));
	}

	@Test
	public void isFestivalTicketTest_false() {
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(any(Seat.class))).thenReturn(Optional.empty());

		assertFalse(seatService.isFestivalTicket(new Seat()));
	}

	@Test(expected = InvalidDataException.class)
	public void getAllSeatsByManifestationTest_nonExistingManif_throwsInvalidDataException() throws InvalidDataException {
		Integer id = 1;
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 2, 2, 0, 0, 0);

		Mockito.when(manifestationRepositoryMocked.findById(id)).thenReturn(Optional.empty());

		seatService.getAllSeatsByManifestation(id, cal.getTime());
	}

	@Test(expected = InvalidDataException.class)
	public void getAllSeatsByManifestationTest_notAllowedManifState_throwsInvalidDataException() throws InvalidDataException {
		Integer id = 1;
		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 11, 3, 10, 10, 10);

		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		cal2.set(2020, 11, 5, 10, 10, 10);

		Manifestation manif = new Manifestation("Exit", "Desc", new Timestamp(cal1.getTimeInMillis()), new Timestamp(cal2.getTimeInMillis()), 10, new Location("Petrovaradin", "Djava", 15D, 15D, null, null, true), ManifestationState.CANCELED, ManifestationCategory.ENTERTAINMENT, null, null);

		Mockito.when(manifestationRepositoryMocked.findById(id)).thenReturn(Optional.of(manif));

		seatService.getAllSeatsByManifestation(id, cal1.getTime());
	}

	@Test
	public void getAllSeatsByManifestationTest_ok() throws InvalidDataException {
		Integer id = 1;
		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 11, 3, 10, 10, 10);

		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		cal2.set(2020, 11, 5, 10, 10, 10);

		Seat seat1 = new Seat();
		seat1.setId(1);
		seat1.setRow(1);
		seat1.setColumn(1);

		Seat seat2 = new Seat();
		seat2.setId(2);
		seat2.setRow(2);
		seat2.setColumn(2);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat1);
		seats.add(seat2);

		SeatCategory sc = new SeatCategory();
		sc.setName("Istok");
		sc.setRows(3);
		sc.setColumns(3);
		sc.setEnumerable(true);

		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setSeats(seats);
		ssc.setId(1);
		ssc.setTicketPrice(150D);
		ssc.setSeatCategory(sc);
		ssc.setNumberOfSeats(1);

		Set<SupportedSeatCategory> sscs = new HashSet<>();
		sscs.add(ssc);

		Manifestation manif = new Manifestation("Exit", "Desc", new Timestamp(cal1.getTimeInMillis()), new Timestamp(cal2.getTimeInMillis()), 10, new Location("Petrovaradin", "Djava", 15D, 15D, new HashSet<>(), null, true), ManifestationState.OPEN, ManifestationCategory.ENTERTAINMENT, sscs, null);

		Mockito.when(manifestationRepositoryMocked.findById(id)).thenReturn(Optional.of(manif));
		Mockito.when(supportedSeatCategoryRepositoryMocked.findAllByManifestation(manif)).thenReturn(sscs);
		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat1, new Timestamp(cal1.getTimeInMillis()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat2, new Timestamp(cal1.getTimeInMillis()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat1)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat2)).thenReturn(Optional.empty());

		ArrayList<SeatsBySupportedSeatCategoryDTO> result = (ArrayList<SeatsBySupportedSeatCategoryDTO>) seatService.getAllSeatsByManifestation(id, cal1.getTime());

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(2, result.get(0).getSeats().size());
		assertTrue(result.get(0).getIsEnumerable());
		assertEquals(1, result.get(0).getId().intValue());
		assertEquals(3, result.get(0).getRows().intValue());
		assertEquals(3, result.get(0).getColumns().intValue());
		assertEquals(1, result.get(0).getNumberOfSeats().intValue());
		assertEquals("Istok", result.get(0).getName());
		assertEquals(1, result.get(0).getSeats().get(0).getSeatId().intValue());
		assertEquals(1, result.get(0).getSeats().get(0).getRow().intValue());
		assertEquals(1, result.get(0).getSeats().get(0).getColumn().intValue());
		assertFalse(result.get(0).getSeats().get(0).isReserved());
		assertEquals(2, result.get(0).getSeats().get(1).getSeatId().intValue());
		assertEquals(2, result.get(0).getSeats().get(1).getRow().intValue());
		assertEquals(2, result.get(0).getSeats().get(1).getColumn().intValue());
		assertFalse(result.get(0).getSeats().get(1).isReserved());
	}

	@Test
	public void getAllSeatsByManifestationTest_festivalTicket() throws InvalidDataException {
		Integer id = 1;
		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 11, 3, 10, 10, 10);

		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		cal2.set(2020, 11, 5, 10, 10, 10);

		List<Seat> seats = new ArrayList<>();

		Seat seat1 = new Seat();
		seat1.setId(1);
		seat1.setRow(1);
		seat1.setColumn(1);

		Seat seat2 = new Seat();
		seat2.setId(2);
		seat2.setRow(2);
		seat2.setColumn(2);

		seats.add(seat1);
		seats.add(seat2);

		SeatCategory sc = new SeatCategory();
		sc.setName("Zapad");
		sc.setRows(3);
		sc.setColumns(3);
		sc.setEnumerable(true);

		SupportedSeatCategory ssc = new SupportedSeatCategory();
		ssc.setSeats(seats);
		ssc.setId(1);
		ssc.setTicketPrice(150D);
		ssc.setSeatCategory(sc);
		ssc.setNumberOfSeats(1);

		Set<SupportedSeatCategory> sscs = new HashSet<>();
		sscs.add(ssc);

		Manifestation manif = new Manifestation("Exit", "Desc", new Timestamp(cal1.getTimeInMillis()), new Timestamp(cal2.getTimeInMillis()), 10, new Location("Petrovaradin", "Djava", 15D, 15D, new HashSet<>(), null, true), ManifestationState.OPEN, ManifestationCategory.ENTERTAINMENT, sscs, null);

		List<Ticket> tickets = new ArrayList<>();
		tickets.add(new Ticket());

		Mockito.when(manifestationRepositoryMocked.findById(id)).thenReturn(Optional.of(manif));
		Mockito.when(supportedSeatCategoryRepositoryMocked.findAllByManifestation(manif)).thenReturn(sscs);
		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat1, new Timestamp(cal1.getTimeInMillis()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat2, new Timestamp(cal1.getTimeInMillis()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat1)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat2)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findBetweenTwoDates(seat1, manif.getStartDate(), manif.getEndDate())).thenReturn(new ArrayList<>());
		Mockito.when(ticketRepositoryMocked.findBetweenTwoDates(seat2, manif.getStartDate(), manif.getEndDate())).thenReturn(tickets);

		ArrayList<SeatsBySupportedSeatCategoryDTO> result = (ArrayList<SeatsBySupportedSeatCategoryDTO>) seatService.getAllSeatsByManifestation(id, null);

		assertNotNull(result);
		assertEquals(1, result.size());
		assertEquals(2, result.get(0).getSeats().size());
		assertTrue(result.get(0).getIsEnumerable());
		assertEquals(1, result.get(0).getId().intValue());
		assertEquals(3, result.get(0).getRows().intValue());
		assertEquals(3, result.get(0).getColumns().intValue());
		assertEquals(1, result.get(0).getNumberOfSeats().intValue());
		assertEquals("Zapad", result.get(0).getName());
		assertEquals(1, result.get(0).getSeats().get(0).getSeatId().intValue());
		assertEquals(1, result.get(0).getSeats().get(0).getRow().intValue());
		assertEquals(1, result.get(0).getSeats().get(0).getColumn().intValue());
		assertFalse(result.get(0).getSeats().get(0).isReserved());
		assertEquals(2, result.get(0).getSeats().get(1).getSeatId().intValue());
		assertEquals(2, result.get(0).getSeats().get(1).getRow().intValue());
		assertEquals(2, result.get(0).getSeats().get(1).getColumn().intValue());
		assertTrue(result.get(0).getSeats().get(1).isReserved());
	}

	@Test(expected = InvalidDataException.class)
	public void reserveSeatsTest_nonExistingSeats_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(new ArrayList<>());

		seatService.reserveSeats(ids, new Date());
	}

	@Test(expected = InvalidDataException.class)
	public void reserveSeatsTest_reservationExpired_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);

		Seat seat = new Seat();
		seat.setId(1);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat);

		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(14);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 11, 0, 0, 0);

		SupportedSeatCategory supportedSeatCategory = new SupportedSeatCategory();

		supportedSeatCategory.setManifestation(manifestation);
		seat.setSupportedSeatCategory(supportedSeatCategory);

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);

		seatService.reserveSeats(ids, new Date(cal.getTimeInMillis()));
	}

	@Test(expected = InvalidDataException.class)
	public void reserveSeatsTest_notAllowedManifState_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);

		Seat seat = new Seat();
		seat.setId(1);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat);

		Manifestation manifestation = new Manifestation();
		manifestation.setState(ManifestationState.CANCELED);
		manifestation.setDaysBeforeExpires(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 14, 0, 0, 0);

		SupportedSeatCategory supportedSeatCategory = new SupportedSeatCategory();

		supportedSeatCategory.setManifestation(manifestation);
		seat.setSupportedSeatCategory(supportedSeatCategory);

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);

		seatService.reserveSeats(ids, new Date(cal.getTimeInMillis()));
	}

	@Test(expected = InvalidDataException.class)
	public void reserveSeatsTest_reservedSeatWithFestivalTicket_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);

		Seat seat = new Seat();
		seat.setId(1);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat);

		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 14, 0, 0, 0);

		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));

		SupportedSeatCategory supportedSeatCategory = new SupportedSeatCategory();

		supportedSeatCategory.setManifestation(manifestation);
		seat.setSupportedSeatCategory(supportedSeatCategory);

		List<Ticket> tickets = new ArrayList<>();
		tickets.add(new Ticket());

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);
		Mockito.when(ticketRepositoryMocked.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate())).thenReturn(tickets);

		seatService.reserveSeats(ids, null);
	}

	@Test(expected = InvalidDataException.class)
	public void reserveSeatsTest_reservedSeat_throwsInvalidDataException() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);

		Seat seat = new Seat();
		seat.setId(1);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat);

		Manifestation manifestation = new Manifestation();
		manifestation.setDaysBeforeExpires(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 14, 0, 0, 0);

		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));

		SupportedSeatCategory supportedSeatCategory = new SupportedSeatCategory();

		supportedSeatCategory.setManifestation(manifestation);
		seat.setSupportedSeatCategory(supportedSeatCategory);

		Ticket ticket = new Ticket();
		ticket.setActive(true);

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);
		Mockito.when(ticketRepositoryMocked.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate())).thenReturn(new ArrayList<>());
		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat, new Timestamp(cal.getTimeInMillis()), true)).thenReturn(Optional.of(ticket));

		seatService.reserveSeats(ids, new Date(cal.getTimeInMillis()));
	}

	@Test(expected = InvalidDataException.class)
	public void reserveSeatsTest_ok() throws InvalidDataException {
		List<Integer> ids = new ArrayList<>();
		ids.add(1);
		ids.add(2);

		Manifestation manifestation = new Manifestation();
		manifestation.setId(1);
		manifestation.setDaysBeforeExpires(2);

		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 14, 0, 0, 0);

		manifestation.setStartDate(new Timestamp(cal.getTimeInMillis()));
		manifestation.setEndDate(new Timestamp(cal.getTimeInMillis()));

		SupportedSeatCategory supportedSeatCategory = new SupportedSeatCategory();

		supportedSeatCategory.setManifestation(manifestation);

		Seat seat = new Seat();
		seat.setSupportedSeatCategory(supportedSeatCategory);
		seat.setId(1);
		seat.setRow(1);
		seat.setColumn(1);

		List<Seat> seats = new ArrayList<>();
		seats.add(seat);

		seat = new Seat();
		seat.setSupportedSeatCategory(supportedSeatCategory);
		seat.setId(2);
		seat.setRow(2);
		seat.setColumn(2);

		seats.add(seat);

		Ticket ticket = new Ticket();
		ticket.setActive(true);

		Mockito.when(seatRepositoryMocked.findAllById(ids)).thenReturn(seats);
		Mockito.when(ticketRepositoryMocked.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate())).thenReturn(new ArrayList<>());
		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat, new Timestamp(cal.getTimeInMillis()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.save(any(Ticket.class))).thenReturn(new Ticket());

		List<Ticket> result = seatService.reserveSeats(ids, new Date(cal.getTimeInMillis()));

		assertEquals(2, result.size());

		assertEquals(cal.getTimeInMillis(), result.get(0).getDate().getTime());
		assertEquals(1, result.get(0).getSeat().getId().intValue());
		assertEquals(1, result.get(0).getSeat().getRow().intValue());
		assertEquals(1, result.get(0).getSeat().getColumn().intValue());
		assertEquals(((RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername(), result.get(0).getUser().getUsername());
		assertEquals(manifestation.getId(), result.get(0).getManifestation().getId());
		assertFalse(result.get(0).getPaid());

		assertEquals(cal.getTimeInMillis(), result.get(1).getDate().getTime());
		assertEquals(1, result.get(1).getSeat().getId().intValue());
		assertEquals(1, result.get(1).getSeat().getRow().intValue());
		assertEquals(1, result.get(1).getSeat().getColumn().intValue());
		assertEquals(((RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername(), result.get(1).getUser().getUsername());
		assertEquals(manifestation.getId(), result.get(1).getManifestation().getId());
		assertFalse(result.get(1).getPaid());
	}

	@Test
	public void checkSeatForBuying_reservedSeat() {
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 14, 0, 0, 0);

		Seat seat = new Seat();
		Date date = new Date(cal.getTimeInMillis());
		RegisteredUser user = new RegisteredUser();

		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat, new Timestamp(date.getTime()), true)).thenReturn(Optional.of(new Ticket()));

		assertTrue(seatService.checkSeatForBuying(seat, date, user));
	}

	@Test
	public void checkSeatForBuying_reservedSeatWithFestivalTicket() {
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 14, 0, 0, 0);

		Seat seat = new Seat();
		Date date = new Date(cal.getTimeInMillis());
		RegisteredUser user = new RegisteredUser();

		Ticket ticket = new Ticket();
		ticket.setUser(user);

		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat, new Timestamp(date.getTime()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat)).thenReturn(Optional.of(ticket));

		assertFalse(seatService.checkSeatForBuying(seat, date, user));
	}

	@Test
	public void checkSeatForBuying_ok() {
		Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(2020, 1, 14, 0, 0, 0);

		Seat seat = new Seat();
		Date date = new Date(cal.getTimeInMillis());
		RegisteredUser user = new RegisteredUser();

		Mockito.when(ticketRepositoryMocked.findOneBySeatAndDateAndActive(seat, new Timestamp(date.getTime()), true)).thenReturn(Optional.empty());
		Mockito.when(ticketRepositoryMocked.findFestivalTicket(seat)).thenReturn(Optional.empty());

		assertFalse(seatService.checkSeatForBuying(seat, date, user));
	}

}
