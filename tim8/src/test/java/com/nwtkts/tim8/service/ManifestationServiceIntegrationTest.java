
package com.nwtkts.tim8.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.nwtkts.tim8.dto.MainManifestationInfoDTO;
import com.nwtkts.tim8.dto.ManifestationDTO;
import com.nwtkts.tim8.dto.ManifestationInfoDTO;
import com.nwtkts.tim8.dto.SearchManifestationDTO;
import com.nwtkts.tim8.dto.SupportedSeatCategoryDTO;
import com.nwtkts.tim8.dto.UpdateManifestationDTO;
import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.ManifestationCategory;
import com.nwtkts.tim8.model.ManifestationState;
import com.nwtkts.tim8.repository.ManifestationRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ManifestationServiceIntegrationTest {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private ManifestationService manifestationService;
	
	@Autowired
	private ManifestationRepository manifestationRepository;
	
	private String[] manifestationNames;
	private String[] manifestationDescriptions;
	private Timestamp[] manifestationStartDates;
	private Timestamp[] manifestationEndDates;
	private String[] manifestationImages;
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		manifestationNames = new String[] {
				"Exit", "Cola", "Zvezda-Bajern", 
				"RHCP", "ACDC", "Exit 2.0", 
				"Superbovl", "Logan vs KSI", "LCS", 
				"VidCon", "Sajam Tambure", "Megdan"
		};
		
		manifestationDescriptions = new String[] {
				"Exit is awesome", "Cola is awesome", "Fudbal is awesome", 
				"RHCP is awesome", "ACDC is awesome", "Exit is awesome", 
				"Superbovl is awesome", "Boks is awesome", "Esports is awesome", 
				"Youtube is awesome", "Tambura is awesome", "Megdan is awesome"
		};
		
		manifestationStartDates = new Timestamp[] {
				Timestamp.valueOf("2020-07-02 00:00:00"), Timestamp.valueOf("2019-12-02 00:00:00"), Timestamp.valueOf("2019-12-26 00:00:00"), 
				Timestamp.valueOf("2020-12-26 00:00:00"), Timestamp.valueOf("2021-09-06 00:00:00"), Timestamp.valueOf("2020-09-06 00:00:00"), 
				Timestamp.valueOf("2020-09-15 00:00:00"), Timestamp.valueOf("2020-10-15 00:00:00"), Timestamp.valueOf("2020-11-15 00:00:00"), 
				Timestamp.valueOf("2020-11-17 00:00:00"), Timestamp.valueOf("2020-06-09 00:00:00"), Timestamp.valueOf("2020-08-08 00:00:00")
		};
		
		manifestationEndDates = new Timestamp[] {
				Timestamp.valueOf("2020-07-05 00:00:00"), Timestamp.valueOf("2019-12-03 00:00:00"), Timestamp.valueOf("2019-12-26 00:00:00"), 
				Timestamp.valueOf("2020-12-26 00:00:00"), Timestamp.valueOf("2020-09-06 00:00:00"), Timestamp.valueOf("2020-12-06 00:00:00"), 
				Timestamp.valueOf("2020-09-15 00:00:00"), Timestamp.valueOf("2020-10-15 00:00:00"), Timestamp.valueOf("2020-11-17 00:00:00"), 
				Timestamp.valueOf("2020-11-19 00:00:00"), Timestamp.valueOf("2020-06-13 00:00:00"), Timestamp.valueOf("2020-08-10 00:00:00")
		};
		
		manifestationImages = new String[] {
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", 
				"http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg", "http://localhost:8080/mob_EXIT2-6.jpg"
		};
	}
	
	@Rule
	public ExpectedException expectedEx = ExpectedException .none();
	
	@Test
	public void findById_ok() throws InvalidDataException {
		Integer id = 4;
		int myDay = 26;
		int myMonth = 12;
		int myYear = 2020;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
                myYear, myMonth, myDay));
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
                myYear, myMonth, myDay));
		ManifestationInfoDTO manifestation = manifestationService.findById(id);
		assertEquals(1,manifestation.getDates().size());
		assertEquals(start, manifestation.getDates().get(0));
		assertEquals(end, manifestation.getDates().get(manifestation.getDates().size()-1));
		assertEquals(Integer.valueOf(7), manifestation.getDaysBeforeExpires());
		assertEquals("RHCP is awesome", manifestation.getDescription());
		assertEquals("http://localhost:8080/mob_EXIT2-6.jpg", manifestation.getImage());
		assertEquals(Integer.valueOf(2), manifestation.getLocation().getLocationId());
		assertEquals(ManifestationCategory.CULTURE, manifestation.getManifestationCategory());
		assertEquals(id, manifestation.getManifestationId());
		assertEquals(ManifestationState.CANCELED, manifestation.getManifestationState());
		assertEquals("RHCP", manifestation.getName());
	}
	
	@Test
	public void findById_notFound() throws InvalidDataException {
		Integer id = 148;
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Manifestation not found");

	    manifestationService.findById(id);
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_ok() throws InvalidDataException {
		int myYear = 2021;
		int myMonth = 10;
		int myDay = 10;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		
		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,locationID, "Exit", "Zanimljiva", null, "mob_EXIT2-6.jpg", ts, ts, ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);

		Integer id = manifestationRepository.findAll().size()+1;
		ManifestationInfoDTO manifInfo =  manifestationService.save(manifDTO);

		assertEquals(manifDTO.getDescription(), manifInfo.getDescription());
		assertEquals(manifDTO.getLocationId(), manifInfo.getLocation().getLocationId());
		assertEquals(manifDTO.getCategory(), manifInfo.getManifestationCategory());
		assertEquals(manifDTO.getState(), manifInfo.getManifestationState());
		assertEquals(manifDTO.getName(), manifInfo.getName());
		assertEquals(manifDTO.getStartDate(), manifInfo.getDates().get(0));
		assertEquals(manifDTO.getEndDate(), manifInfo.getDates().get(manifInfo.getDates().size()-1));
		assertEquals(id, manifInfo.getManifestationId());
	    
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_wrongDate() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 07;
		int myDay = 03;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		myYear = 2020;
		myMonth = 07;
		myDay = 07;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
                myYear, myMonth, myDay));
		
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		
		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,locationID, "Exit", "Zanimljiva", null, null, start, end, ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);

		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some other event will be held at this time");
		
	    manifestationService.save(manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_missingFields() throws InvalidDataException {
		ManifestationDTO manifestationDTO = new ManifestationDTO();
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some data is missing");
		
	    manifestationService.save(manifestationDTO);
	}
	
	@Test
	@Rollback
	@Transactional
	public void saveManifestation_noLocation() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(1, 20, 50.0);
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		Integer locationID = 55;
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,locationID,"Name", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location not found");
	    manifestationService.save(manifDTO);
		
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_noSeatCategory() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		Integer seatCategoryID = 77;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,locationID,"Name", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Seat category with id " + seatCategoryID + " not found");
	    manifestationService.save(manifDTO);
		
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_emptyName() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		
		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,locationID, "", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Name can't be empty");
	    manifestationService.save(manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_emptyDescription() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		
		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,locationID, "Exit", "", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Description can't be empty");
	    manifestationService.save(manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_allowedReservationsWrongDate() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 02;
		int myDay = 15;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		
		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(15,locationID, "Exit", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("The date when the reservations are no longer allowed cannot be earlier than today");
	    manifestationService.save(manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_negativeExpirationDays() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp ts = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		
		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(-7,locationID, "Exit", "Zanimljiva", null, null, ts, ts,ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Number of days after which the possibility of reservation expires can't be less than -1");
	    manifestationService.save(manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	@Ignore
	public void saveManifestation_startDateInThePast() throws InvalidDataException {
		int myYear = 2019;
		int myMonth = 12;
		int myDay = 02;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		myYear = 2019;
		myMonth = 12;
		myDay = 15;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		
		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(-1,locationID, "Exit", "Zanimljiva", null, null, start, end, ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Start date must be in the future");
	    manifestationService.save(manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void saveManifestation_endDateBeforeStart() throws InvalidDataException {
		int myYear = 2020;
		int myMonth = 12;
		int myDay = 02;
		Timestamp start = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		myYear = 2019;
		myMonth = 02;
		myDay = 01;
		Timestamp end = Timestamp.valueOf(String.format("%04d-%02d-%02d 00:00:00", 
		                                                myYear, myMonth, myDay));
		Integer seatCategoryID = 1;
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(seatCategoryID, 20, 50.0);
		
		Integer seatCategoryID2 = 2;
		SupportedSeatCategoryDTO sscDTO2 = new SupportedSeatCategoryDTO(seatCategoryID2, 30, 50.55); 
		List<SupportedSeatCategoryDTO> sscList = new ArrayList<SupportedSeatCategoryDTO>();
		sscList.add(sscDTO);
		sscList.add(sscDTO2);
		Integer locationID = 1;
		ManifestationDTO manifDTO =
				new ManifestationDTO(7,locationID, "Exit", "Zanimljiva", null, null, start, end, ManifestationState.OPEN,ManifestationCategory.CULTURE, sscList);
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("End date can't be before start date");
	    manifestationService.save(manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void updateManifestation_ok() throws InvalidDataException {
		UpdateManifestationDTO manifDTO =
				new UpdateManifestationDTO("Ime", "Opis");

		Integer manifID = 7;
		Manifestation manifestation = manifestationRepository.findOneById(manifID).get();
		ManifestationInfoDTO manifInfo =  manifestationService.update(manifID, manifDTO);
		
		assertEquals(manifDTO.getDescription(), manifInfo.getDescription());
		assertEquals(manifestation.getLocation().getId(), manifInfo.getLocation().getLocationId());
		assertEquals(manifestation.getCategory(), manifInfo.getManifestationCategory());
		assertEquals(manifestation.getState(), manifInfo.getManifestationState());
		assertEquals(manifDTO.getName(), manifInfo.getName());
		assertEquals(manifestation.getStartDate(), manifInfo.getDates().get(0));
		assertEquals(manifestation.getEndDate(), manifInfo.getDates().get(manifInfo.getDates().size()-1));
		assertEquals(manifID, manifInfo.getManifestationId());
	}
	
	@Test
	@Transactional
	@Rollback
	public void updateManifestation_noManifestation() throws InvalidDataException {
		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("ImeTest", "OpisTest");
		Integer manifID = 32;
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Manifestation not found");
	    manifestationService.update(manifID, manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void updateManifestation_canceledManifestation() throws InvalidDataException {
		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("ImeTest", "OpisTest");
		Integer manifID = 4;
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("You can't edit canceled manifestation");
	    manifestationService.update(manifID, manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void updateManifestation_pastManifestation() throws InvalidDataException {
		UpdateManifestationDTO manifDTO = new UpdateManifestationDTO("ImeTest", "OpisTest");
		Integer manifID = 3;
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("You can't edit manifestation that has passed");
	    manifestationService.update(manifID, manifDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void updateManifestation_missingFields() throws InvalidDataException {
		UpdateManifestationDTO manifestationDTO = new UpdateManifestationDTO("Ime", null);
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some data is missing");
		
	    manifestationService.update(1, manifestationDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void updateManifestation_emptyName() throws InvalidDataException {
		UpdateManifestationDTO manifestationDTO = new UpdateManifestationDTO("", "Opis");
		Integer manifID = 1;
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Name can't be empty");
	    manifestationService.update(manifID, manifestationDTO);
	}
	
	@Test
	@Transactional
	@Rollback
	public void updateManifestation_emptyDescription() throws InvalidDataException {
		UpdateManifestationDTO manifestationDTO = new UpdateManifestationDTO("Ime", "");
		Integer manifID = 5;
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Description can't be empty");
	    manifestationService.update(manifID, manifestationDTO);
	
	}
	
	@Test
	@Transactional
	public void findTest_emptyParams() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_nullParams() {
		String name = null;
		List<String> categories = null;
		List<String> locations = null;
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_simpleName() {
		String name = "i";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 6, 8, 10
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_nonexistantName() {
		String name = "prodigy";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		
		assertEquals(manifestations.size(), 0);
	}
	
	@Test
	@Transactional
	public void findTest_singleLocation() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Petrovaradin"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 3);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 6, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_multiLocation() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Petrovaradin", "Beograd"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 6);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 8, 9, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_singleCategory() {
		String name = "";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport"}));
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 3);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				7, 8, 9
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_multiCategory() {
		String name = "";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport", "Culture"}));
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 6);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				5, 7, 8, 9, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_allParams() {
		String name = "vs";
		List<String> categories = new ArrayList<String>(Arrays.asList(new String[]{"Sport"}));
		List<String> locations = new ArrayList<String>(Arrays.asList(new String[]{"Beograd"}));
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 1);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_simplePageable() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(1, 4, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8, 9, 10, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findTest_tooFarPageable() {
		String name = "";
		List<String> categories = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();
		
		SearchManifestationDTO dto = new SearchManifestationDTO();
		dto.setName(name);
		dto.setCategories(categories);
		dto.setLocations(locations);
		
		PageRequest pageable = PageRequest.of(3, 4, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.find(dto, pageable));
		
		assertEquals(manifestations.size(), 0);
	}
	
	@Test
	@Transactional
	public void findAllTest_successful() {
		
		PageRequest pageable = PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.findAll(pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 9);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 5, 6, 7, 8, 9, 10, 11, 12
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findAllTest_simplePageable() {
		
		PageRequest pageable = PageRequest.of(1, 4, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.findAll(pageable));
		ArrayList<Integer> returnedIds = new ArrayList<>(
				manifestations.stream().map(m -> m.getId()).collect(Collectors.toList()));
		
		assertEquals(manifestations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				8, 9, 10, 11
		}))));
		
		for (MainManifestationInfoDTO manifestation : manifestations) {
			assertEquals(manifestation.getName(), manifestationNames[manifestation.getId()-1]);
			assertEquals(manifestation.getDescription(), manifestationDescriptions[manifestation.getId()-1]);
			assertEquals(manifestation.getStartDate(), manifestationStartDates[manifestation.getId()-1]);
			assertEquals(manifestation.getEndDate(), manifestationEndDates[manifestation.getId()-1]);
			assertEquals(manifestation.getImage(), manifestationImages[manifestation.getId()-1]);
		}
	}
	
	@Test
	@Transactional
	public void findAllTest_tooFarPageable() {
		
		PageRequest pageable = PageRequest.of(3, 4, Sort.by(Sort.Direction.ASC, "id"));
		
		ArrayList<MainManifestationInfoDTO> manifestations = new ArrayList<>(manifestationService.findAll(pageable));
		
		assertEquals(manifestations.size(), 0);
	}

	@Test(expected = InvalidDataException.class)
	@Transactional
	public void getManifestationInfoTest_nonExisting_throwsInvalidDataException() throws InvalidDataException {
		Integer id = 150;

		ManifestationInfoDTO dto = manifestationService.getManifestationInfo(id);
	}

	@Test
	@Transactional
	public void getManifestationInfoTest_ok() throws InvalidDataException {
		Integer id = 1;

		ManifestationInfoDTO dto = manifestationService.getManifestationInfo(id);

		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 6, 2, 0, 0, 0);

		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		cal2.set(2020, 6, 5, 0, 0, 0);

		assertNotNull(dto);
		assertEquals("Exit", dto.getName());
		assertEquals("Exit is awesome", dto.getDescription());
		assertEquals(0, dto.getDates().get(0).compareTo(new Timestamp(cal1.getTimeInMillis())));
		assertEquals(0, dto.getDates().get(3).compareTo(new Timestamp(cal2.getTimeInMillis())));
		assertEquals(Double.valueOf(12), dto.getLocation().getLatitude());
		assertEquals(Double.valueOf(12), dto.getLocation().getLongitude());
		assertEquals(ManifestationState.OPEN, dto.getManifestationState());
		assertEquals(ManifestationCategory.ENTERTAINMENT, dto.getManifestationCategory());
	}
	
	@Test
	@Transactional
	@Rollback
	public void cancelTest_successful() throws InvalidDataException {
		Integer manifestationId = 1;
		
		Calendar cal1 = Calendar.getInstance();
		cal1.clear();
		cal1.set(2020, 6, 2, 0, 0, 0);

		Calendar cal2 = Calendar.getInstance();
		cal2.clear();
		cal2.set(2020, 6, 5, 0, 0, 0);
		
		Boolean ret = manifestationService.cancel(manifestationId);
		Manifestation manifestation = manifestationRepository.findById(manifestationId).get();
		
		assertEquals(true, ret);
		assertEquals("Exit", manifestation.getName());
		assertEquals("Exit is awesome", manifestation.getDescription());
		assertEquals(0, manifestation.getStartDate().compareTo(new Timestamp(cal1.getTimeInMillis())));
		assertEquals(0, manifestation.getEndDate().compareTo(new Timestamp(cal2.getTimeInMillis())));
		assertEquals(Double.valueOf(12), manifestation.getLocation().getLatitude());
		assertEquals(Double.valueOf(12), manifestation.getLocation().getLongitude());
		assertEquals("http://localhost:8080/mob_EXIT2-6.jpg", manifestation.getImage());
		assertEquals(ManifestationState.CANCELED, manifestation.getState());
		assertEquals(ManifestationCategory.ENTERTAINMENT, manifestation.getCategory());
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void cancelTest_manifestationNotFound() throws InvalidDataException {
		Integer manifestationId = 100;
		
		try {
			manifestationService.cancel(manifestationId);
		} catch (InvalidDataException e) {
			String message = "Manifestation not found";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void cancelTest_manifestationPassed() throws InvalidDataException {
		Integer manifestationId = 2;
		
		try {
			manifestationService.cancel(manifestationId);
		} catch (InvalidDataException e) {
			String message = "Manifestation has passed";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	@Transactional
	public void cancelTest_manifestationAlreadyCanceled() throws InvalidDataException {
		Integer manifestationId = 4;
		
		try {
			manifestationService.cancel(manifestationId);
		} catch (InvalidDataException e) {
			String message = "Manifestation has already been canceled";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void getDailyTest_successfull() throws ParseException, InvalidDataException {

		Integer manifestationId = 2;	
		
		String date1String = "02-01-2019 00:00";
		String date2String = "08-08-2020 00:00";	

		Map<String, Long> map = new HashMap<String, Long>();
		map.put("17-10-2019", 2L);
		map.put("02-11-2019", 1L);
		
		Map<String, Long> mapRes = manifestationService.getDaily(manifestationId,date1String , date2String);
		
		assertEquals(2, mapRes.size());
		assertEquals(Long.valueOf(2), mapRes.get("17-10-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("02-11-2019"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getDailyTest_manifestationNotFound() throws InvalidDataException, ParseException {
		Integer manifestationId = 20;
		String date1String = "02-01-2019 00:00";
		String date2String = "08-01-2020 00:00";
		
		manifestationService.getDaily(manifestationId,date1String , date2String);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getDailyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2019 00:00";
		
		manifestationService.getDaily(manifestationId,date1String , date2String);
	}
	
	@Test
	public void getWeeklyTest_successfull() throws ParseException, InvalidDataException {

		Integer manifestationId = 1;	
		
		String date1String = "02-01-2019 00:00";
		String date2String = "08-01-2020 00:00";	

		Map<String, Long> map = new HashMap<String, Long>();
		map.put("Dec 2019 week: 2", 3L);
		
		Map<String, Long> mapRes = manifestationService.getWeekly(manifestationId,date1String , date2String);
		
		assertEquals(1, mapRes.size());
		assertEquals(Long.valueOf(3), mapRes.get("Dec 2019 week: 2"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getWeeklyTest_manifestationNotFound() throws InvalidDataException, ParseException {
		Integer manifestationId = 20;
		String date1String = "02-01-2020 00:00";
		String date2String = "08-01-2020 00:00";
		
		manifestationService.getWeekly(manifestationId,date1String , date2String);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getWeeklyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2019 00:00";
		
		manifestationService.getDaily(manifestationId, date1String, date2String);
	}
	
	@Test
	public void getMonthlyTest_successfull() throws ParseException, InvalidDataException {

		Integer manifestationId = 2;	
		
		String date1String = "02-01-2019 00:00";
		String date2String = "08-03-2020 00:00";	
				
		Map<String, Long> map = new HashMap<String, Long>();
		map.put("Nov 2019", 1L);
		map.put("Oct 2019", 2L);
		
		Map<String, Long> mapRes = manifestationService.getMonthly(manifestationId,date1String , date2String);
		
		assertEquals(2, mapRes.size());
		assertEquals(Long.valueOf(1), mapRes.get("Nov 2019"));
		assertEquals(Long.valueOf(2), mapRes.get("Oct 2019"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getMonthlyTest_manifestationNotFound() throws InvalidDataException, ParseException {
		Integer manifestationId = 20;
		String date1String = "02-01-2020 00:00";
		String date2String = "08-01-2020 00:00";
		
		manifestationService.getMonthly(manifestationId,date1String , date2String);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getMonthlyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer manifestationId = 2;
		String date1String = "08-08-2020 00:00";
		String date2String = "12-01-2020 00:00";
		
		manifestationService.getMonthly(manifestationId,date1String , date2String);
	}
}
