package com.nwtkts.tim8.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mail.MailException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.view.RedirectView;

import com.nwtkts.tim8.dto.UserChangePasswordDTO;
import com.nwtkts.tim8.model.Admin;
import com.nwtkts.tim8.model.Authority;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.User;
import com.nwtkts.tim8.model.UserType;
import com.nwtkts.tim8.repository.IAuthorityRepository;
import com.nwtkts.tim8.repository.IUserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomUserDetailsServiceUnitTest {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@MockBean
	private IUserRepository userRepository;
	
	@MockBean
	private EmailService emailService;
	
	@MockBean
	private PasswordEncoder passwordEncoder;
	
	@MockBean
	private IAuthorityRepository authorityRepository;
	
	@Test
	public void loadUserByUsername_userFound() {
		//kreiramo korisnika koga cemo mockovati

		List<Authority> authorities = new ArrayList<Authority>();
		Authority authority = new Authority();
		authority.setUserType(UserType.ROLE_REGISTEREDUSER);
		authorities.add(authority);
		User user = new User("user1", "123", "Pera", "Peric", "user1@gmail.com", authorities);
		
		Mockito.when(userRepository.findByUsername(user.getUsername())).thenReturn(user);
		UserDetails u = customUserDetailsService.loadUserByUsername("user1");

		assertEquals("user1", u.getUsername());
		assertEquals("123", u.getPassword());
	}
	 
	@Test(expected = UsernameNotFoundException.class)
	public void loadUserByUsername_userNotFound() {
		
		customUserDetailsService.loadUserByUsername("user11");
	}
		
	@Test
	public void registerRUser_successfully() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
	
		RegisteredUser user = new RegisteredUser("user1", "123", "Pera", "Peric", "user1@gmail.com", null);
		Authority authority = new Authority();
		authority.setUserType(UserType.ROLE_REGISTEREDUSER);
		
		Mockito.when(passwordEncoder.encode(user.getPassword())).thenReturn("$12345");
		Mockito.when(authorityRepository.findById(1)).thenReturn(Optional.of(authority));
		
		RegisteredUser u = customUserDetailsService.saveRegisteredUser(user);
		
		assertEquals("user1", u.getUsername());
		assertEquals("$12345", u.getPassword());
		assertEquals("user1@gmail.com", u.getEmail());
		assertEquals("Pera", u.getFirstName());
		Mockito.verify(emailService, Mockito.times(1)).sendNotificaitionAsyncRegistration(user);
		
	}
	
	@Test(expected = InvalidDataException.class)
	public void registerRUser_usernameTaken() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
	
		RegisteredUser user = new RegisteredUser("user11", "123", "Pera", "Peric", "user11@gmail.com", null);
		
		Mockito.when(userRepository.findByUsername("user11")).thenReturn(user);
		
		try {
			customUserDetailsService.saveRegisteredUser(user);		
		}
		catch (InvalidDataException e) {
			String message = "Username already taken!";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	public void registerRUser_emailTaken() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
	
		RegisteredUser user = new RegisteredUser("user11", "123", "Pera", "Peric", "user11@gmail.com", null);
		
		Mockito.when(userRepository.findByEmail("user11@gmail.com")).thenReturn(user);
		
		try {
			customUserDetailsService.saveRegisteredUser(user);		
		}
		catch (InvalidDataException e) {
			String message = "Email already taken!";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	public void registerRUser_incompleteData_Username() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
	
		RegisteredUser user1 = new RegisteredUser("", "123", "Pera", "Peric", "user11@gmail.com", null);

		try {
			customUserDetailsService.saveRegisteredUser(user1);
		}
		catch (InvalidDataException e) {
			String message = "User information is incomplete!";
			assertEquals(message, e.getMessage());
			throw e;
		}
		
	}
	
	@Test(expected = InvalidDataException.class)
	public void registerRUser_incompleteData_Password() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
	
		RegisteredUser user1 = new RegisteredUser("user11", "", "Pera", "Peric", "user11@gmail.com", null);

		try {
			customUserDetailsService.saveRegisteredUser(user1);
		}
		catch (InvalidDataException e) {
			String message = "User information is incomplete!";
			assertEquals(message, e.getMessage());
			throw e;
		}
		
	}
	
	@Test(expected = InvalidDataException.class)
	public void registerRUser_incompleteData_EmailAndFirstName() throws InvalidDataException, MailException, InterruptedException, UnsupportedEncodingException {
	
		RegisteredUser user1 = new RegisteredUser("user11", "123", "", "Peric", "", null);

		try {
			customUserDetailsService.saveRegisteredUser(user1);
		}
		catch (InvalidDataException e) {
			String message = "User information is incomplete!";
			assertEquals(message, e.getMessage());
			throw e;
		}
		
	}
	
	@Test
	public void registerAdmin_successfully() throws InvalidDataException {
	
		User user = new User("admin1", "10223", "Mika", "Mikic", "admin1@gmail.com", null);
		Authority authority = new Authority();
		authority.setUserType(UserType.ROLE_ADMIN);
		
		Mockito.when(passwordEncoder.encode(user.getPassword())).thenReturn("$12345");
		Mockito.when(authorityRepository.findById(2)).thenReturn(Optional.of(authority));
		
		Admin a = new Admin(user);
		
		a = customUserDetailsService.saveAdmin(a);
		
		assertEquals("admin1", a.getUsername());
		assertEquals("$12345", a.getPassword());
		assertEquals("admin1@gmail.com", a.getEmail());
		assertEquals("Mika", a.getFirstName());
		
	}
	
	@Test(expected = InvalidDataException.class)
	public void registerAdmin_usernameTaken() throws InvalidDataException {
	
		User user = new User("user11", "123", "Pera", "Peric", "user11@gmail.com", null);
		
		Mockito.when(userRepository.findByUsername("user11")).thenReturn(user);
		
		try {
			Admin a = new Admin(user);
			customUserDetailsService.saveAdmin(a);		
		}
		catch (InvalidDataException e) {
			String message = "Username already taken!";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	public void registerAdmin_emailTaken() throws InvalidDataException {
	
		User user = new User("user11", "123", "Pera", "Peric", "user11@gmail.com", null);
		
		Mockito.when(userRepository.findByEmail("user11@gmail.com")).thenReturn(user);
		
		try {
			Admin a = new Admin(user);
			customUserDetailsService.saveAdmin(a);		
		}
		catch (InvalidDataException e) {
			String message = "Email already taken!";
			assertEquals(message, e.getMessage());
			throw e;
		}
	}
	
	@Test(expected = InvalidDataException.class)
	public void registerRUser_incompleteData_UsernameAndEmail() throws InvalidDataException {
	
		RegisteredUser user1 = new RegisteredUser("", "123", "Pera", "Peric", "", null);

		try {
			Admin a = new Admin(user1);
			customUserDetailsService.saveAdmin(a);
		}
		catch (InvalidDataException e) {
			String message = "User information is incomplete!";
			assertEquals(message, e.getMessage());
			throw e;
		}
		
	}
	
	@Test
	public void confirmRegistration_validToken() throws InvalidDataException {
		RegisteredUser user1 = new RegisteredUser("user1", "123", "Pera", "Peric", "user1@gmail.com", null);
		
		Mockito.when(userRepository.findByToken("01abc2345")).thenReturn(user1);
		
		Boolean token = customUserDetailsService.confirmRegistration("01abc2345");
		
		assertEquals(true, token);
		
	}
	
	@Test(expected = InvalidDataException.class)
	public void confirmRegistration_invalidToken() throws InvalidDataException {
		RegisteredUser user1 = new RegisteredUser("user1", "123", "Pera", "Peric", "user1@gmail.com", null);
		
		Mockito.when(userRepository.findByToken("01abc2345")).thenReturn(user1);
		
		customUserDetailsService.confirmRegistration("7878878");
			
	}

}
