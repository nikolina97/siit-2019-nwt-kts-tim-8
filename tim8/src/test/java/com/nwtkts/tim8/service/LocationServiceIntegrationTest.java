package com.nwtkts.tim8.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.nwtkts.tim8.dto.LocationDTO;
import com.nwtkts.tim8.dto.MainLocationInfoDTO;
import com.nwtkts.tim8.dto.MainManifestationInfoDTO;
import com.nwtkts.tim8.model.Location;
import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.SeatCategory;
import com.nwtkts.tim8.repository.LocationRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LocationServiceIntegrationTest {
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private LocationService locationService;
	
	@Autowired
	private LocationRepository locationRepository;
	
	private String[] locationNames;
	private String[] locationAddresses;
	private Double[] locationLatitudes;
	private Double[] locationLongitudes;
	private Integer[] locationManifestationCounts;
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		locationNames = new String[] {
				"Djava", "Marakana", "Cair", "JNA"
		};

		locationAddresses = new String[] {
				"Petrovaradin", "Beograd", "Nis", "Beograd"
		};
		
		locationLatitudes = new Double[] {
				12.0, 25.0, 25.5, 41.1
		};
		
		locationLongitudes = new Double[] {
				12.0, 25.0, 25.5, 20.0
		};
		
		locationManifestationCounts = new Integer[] {
				3, 3, 3, 0
		};
	}
	
	@Rule
	public ExpectedException expectedEx = ExpectedException .none();
	
	
	@Test
	public void findById_ok() throws InvalidDataException {
		Integer id = 2;
		MainLocationInfoDTO result = locationService.findById(id);
		assertEquals("Beograd", result.getAddress());
		assertEquals(Double.valueOf(25), result.getLatitude());
		assertEquals(Double.valueOf(25), result.getLongitude());
		assertEquals(id, result.getLocationId());
		assertEquals("Marakana", result.getName());
		assertEquals(Integer.valueOf(3), result.getManifestationCount());
	}
	
	@Test
	public void findById_notFound() throws InvalidDataException {
		Integer id = 42;
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location not found");

	    locationService.findById(id);
	}
	
	@Test
	@Transactional
	public void saveLocation_ok() throws InvalidDataException {
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		LocationDTO location = new LocationDTO("Pionir", "Beograd", 42.5, 42.6, categories);
		Integer id = locationRepository.findAll().size()+1;
		MainLocationInfoDTO savedLocation = locationService.save(location);
	    
	    assertEquals(id, savedLocation.getLocationId());
		assertEquals(location.getAddress(), savedLocation.getAddress());
		assertEquals(location.getLatitude(), savedLocation.getLatitude());
		assertEquals(location.getLongitude(), savedLocation.getLongitude());
		assertEquals(location.getName(), savedLocation.getName());
		assertEquals(Integer.valueOf(0), savedLocation.getManifestationCount());
	    }
	
	@Test
	public void saveLocation_missingFields() throws InvalidDataException {
		LocationDTO location = new LocationDTO();
		location.setLatitude(55.3);
		location.setAddress("Adresa1");
		location.setName("Ime1");
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some data is missing");
		
	    locationService.save(location);
	}
	
	@Test
	public void saveLocation_emptyName() throws InvalidDataException {
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		LocationDTO location = new LocationDTO("", "Narodnog fronta 5", 55.5, 55.9, categories);
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Name can't be empty");
		
	    locationService.save(location);
	}
	
	@Test
	public void saveLocation_emptyAddress() throws InvalidDataException {
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		LocationDTO location = new LocationDTO("Zeleznik", "", 55.5, 55.9, categories);
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Address can't be empty");
		
	    locationService.save(location);
	}
	

	@Test
	@Transactional
	public void saveLocation_addressTaken() throws InvalidDataException {
		Location existantLocation = locationRepository.findById(1).get();
		HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		LocationDTO location = new LocationDTO("Spens",existantLocation.getAddress(), existantLocation.getLatitude(), existantLocation.getLongitude(), categories);

		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location on the same address already exists");
		
	    locationService.save(location);
	}
	
	@Test
	@Transactional
	public void updateLocation_ok() throws InvalidDataException {
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(2, "Cair", "Nis", 41.6, 43.3, null);

		
		MainLocationInfoDTO updatedLocation = locationService.update(2, locationDTO);
		assertEquals(locationDTO.getLocationId(), updatedLocation.getLocationId());
		assertEquals(locationDTO.getAddress(), updatedLocation.getAddress());
		assertEquals(locationDTO.getLatitude(), updatedLocation.getLatitude());
		assertEquals(locationDTO.getLongitude(), updatedLocation.getLongitude());
		assertEquals(locationDTO.getName(), updatedLocation.getName());
	}
	
	@Test
	@Transactional
	public void updateLocation_addressTaken() throws InvalidDataException {
		Location existantLocation = locationRepository.findById(1).get();
		MainLocationInfoDTO location = new MainLocationInfoDTO(2, "Cair", existantLocation.getAddress(), existantLocation.getLatitude(), existantLocation.getLongitude(), null);

		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location on the same address already exists");
		
	    locationService.update(2, location);
	}
	
	@Test
	public void updateLocation_missingFields() throws InvalidDataException {
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO();
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some data is missing");
		
	    Integer id = 1;
	    locationService.update(id, locationDTO);
	}
	
	@Test
	public void updateLocation_emptyName() throws InvalidDataException {
		Integer id = 1;
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(id, "", "Novi Sad", 45.6, 42.3, null);
		
	    HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
	    
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Name can't be empty");
	    locationService.update(id, locationDTO);
	}
	
	@Test
	public void updateLocation_emptyAddress() throws InvalidDataException {
		Integer id = 1;
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(id, "Spens", "", 45.6, 42.3, null);
		
	    HashSet<SeatCategory> categories = new HashSet<SeatCategory>();
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		Location location = new Location("Petrovaradin", "Spens", 55.5, 55.9, categories, manifestations, true);
		location.setId(id);
	    
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Address can't be empty");
	    locationService.update(id, locationDTO);
	}
	
	@Test
	public void updateLocation_locationNotFound() throws InvalidDataException {
		Integer id = 50;
		MainLocationInfoDTO locationDTO = new MainLocationInfoDTO(id, "Spens", "Petrovaradin", 45.6, 42.3, null);
		
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location not found");
	    locationService.update(id, locationDTO);
	}
	
	@Test
	@Transactional
	public void deleteLocation_ok() throws Exception {
		Location locationActive = locationRepository.findById(4).get();
		assertTrue(locationActive.getActive());
		assertTrue(locationService.delete(4));
		Location locationDeleted = locationRepository.findById(4).get();
		assertFalse(locationDeleted.getActive());
	}
	
	@Test
	@Transactional
	public void deleteLocation_locationNotFound() throws Exception{
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Location not found");
		locationService.delete(5);
	}
	
	@Transactional
	public void deleteLocation_withActiveManifestation() throws Exception{
		expectedEx.expect(InvalidDataException.class);
	    expectedEx.expectMessage("Some manifestations will be held on this location");
		locationService.delete(1);
	}
	
	@Test
	public void findAll_successful() {
		PageRequest pageRequest = PageRequest.of(0, 10);
		List<MainLocationInfoDTO> locations = locationService.getAllPageable(pageRequest);
		ArrayList<Integer> returnedIds = new ArrayList<>(
				locations.stream().map(m -> m.getLocationId()).collect(Collectors.toList()));
		
		assertEquals(locations.size(), 4);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				1, 2, 3, 4
		}))));
		
		for (MainLocationInfoDTO location : locations) {
			assertEquals(location.getName(), locationNames[location.getLocationId()-1]);
			assertEquals(location.getAddress(), locationAddresses[location.getLocationId()-1]);
			assertEquals(location.getLatitude(), locationLatitudes[location.getLocationId()-1]);
			assertEquals(location.getLongitude(), locationLongitudes[location.getLocationId()-1]);
			assertEquals(location.getManifestationCount(), locationManifestationCounts[location.getLocationId()-1]);
		}
	}
	
	@Test
	public void findAll_simplePageable() {
		PageRequest pageRequest = PageRequest.of(1, 2);
		List<MainLocationInfoDTO> locations = locationService.getAllPageable(pageRequest);
		ArrayList<Integer> returnedIds = new ArrayList<>(
				locations.stream().map(m -> m.getLocationId()).collect(Collectors.toList()));
		
		assertEquals(locations.size(), 2);
		
		assertTrue(returnedIds.containsAll(new ArrayList<>(Arrays.asList(new Integer[] {
				3, 4
		}))));
		
		for (MainLocationInfoDTO location : locations) {
			assertEquals(location.getName(), locationNames[location.getLocationId()-1]);
			assertEquals(location.getAddress(), locationAddresses[location.getLocationId()-1]);
			assertEquals(location.getLatitude(), locationLatitudes[location.getLocationId()-1]);
			assertEquals(location.getLongitude(), locationLongitudes[location.getLocationId()-1]);
			assertEquals(location.getManifestationCount(), locationManifestationCounts[location.getLocationId()-1]);
		}
	}
	
	@Test
	public void findAll_tooFarPageable() {
		PageRequest pageRequest = PageRequest.of(2, 2);
		List<MainLocationInfoDTO> locations = locationService.getAllPageable(pageRequest);
		
		assertEquals(locations.size(), 0);
	}
	
	@Test
	public void getDailyTest_successfull() throws ParseException, InvalidDataException {

		Integer locationId = 1;	
		
		String date1String = "02-01-2019 00:00";
		String date2String = "08-08-2020 00:00";	

		Map<String, Long> map = new HashMap<String, Long>();
		map.put("13-12-2019", 1L);
		map.put("11-12-2019", 1L);
		map.put("10-12-2019", 1L);
		map.put("17-10-2019", 2L);
		map.put("02-11-2019", 1L);
		
		Map<String, Long> mapRes = locationService.getDaily(locationId,date1String , date2String);
		
		assertEquals(5, mapRes.size());
		assertEquals(Long.valueOf(1), mapRes.get("13-12-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("11-12-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("10-12-2019"));
		assertEquals(Long.valueOf(2), mapRes.get("17-10-2019"));
		assertEquals(Long.valueOf(1), mapRes.get("02-11-2019"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getDailyTest_locationNotFound() throws InvalidDataException, ParseException {
		Integer locationId = 10;
		String date1String = "02-01-2019 00:00";
		String date2String = "08-01-2020 00:00";
		
		locationService.getDaily(locationId,date1String , date2String);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getDailyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer locationId = 2;
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2019 00:00";
		
		locationService.getDaily(locationId,date1String , date2String);
	}
	
	@Test
	public void getWeeklyTest_successfull() throws ParseException, InvalidDataException {

		Integer locationId = 1;	
		
		String date1String = "02-01-2019 00:00";
		String date2String = "08-01-2020 00:00";	

		Map<String, Long> map = new HashMap<String, Long>();
		map.put("Nov 2019 week: 1", 1L);
		map.put("Oct 2019 week: 3", 2L);
		map.put("Dec 2019 week: 2", 3L);
		
		Map<String, Long> mapRes = locationService.getWeekly(locationId,date1String , date2String);
		
		assertEquals(3, mapRes.size());
		assertEquals(Long.valueOf(1), mapRes.get("Nov 2019 week: 1"));
		assertEquals(Long.valueOf(2), mapRes.get("Oct 2019 week: 3"));
		assertEquals(Long.valueOf(3), mapRes.get("Dec 2019 week: 2"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getWeeklyTest_locationNotFound() throws InvalidDataException, ParseException {
		Integer locationId = 10;
		String date1String = "02-01-2020 00:00";
		String date2String = "08-01-2020 00:00";
		
		locationService.getWeekly(locationId,date1String , date2String);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getWeeklyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer locationId = 2;
		String date1String = "08-01-2020 00:00";
		String date2String = "02-01-2019 00:00";
		
		locationService.getDaily(locationId,date1String , date2String);
	}
	
	@Test
	public void getMonthlyTest_successfull() throws ParseException, InvalidDataException {

		Integer locationId = 1;	
		
		String date1String = "02-01-2019 00:00";
		String date2String = "08-03-2020 00:00";	
				
		Map<String, Long> map = new HashMap<String, Long>();
		map.put("Dec 2019", 3L);
		map.put("Nov 2019", 1L);
		map.put("Oct 2019", 2L);
		
		Map<String, Long> mapRes = locationService.getMonthly(locationId,date1String , date2String);
		
		assertEquals(3, mapRes.size());
		assertEquals(Long.valueOf(3), mapRes.get("Dec 2019"));
		assertEquals(Long.valueOf(1), mapRes.get("Nov 2019"));
		assertEquals(Long.valueOf(2), mapRes.get("Oct 2019"));
		assertEquals(map, mapRes);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getMonthlyTest_locationNotFound() throws InvalidDataException, ParseException {
		Integer locationId = 10;
		String date1String = "02-01-2020 00:00";
		String date2String = "08-01-2020 00:00";
		
		locationService.getMonthly(locationId,date1String , date2String);
	}
	
	@Test(expected = InvalidDataException.class)
	public void getMonthlyTest_invalidDates() throws InvalidDataException, ParseException {
		Integer locationId = 2;
		String date1String = "08-08-2020 00:00";
		String date2String = "12-01-2020 00:00";
		
		locationService.getMonthly(locationId,date1String , date2String);
	}
}
