package com.nwtkts.tim8.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.nwtkts.tim8.dto.CompletePaymentDTO;
import com.nwtkts.tim8.dto.CreatePaymentDTO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PayPalServiceIntegrationTest {
	@Autowired
	private PayPalService payPalService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Before
	public void setUp() {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken("user1", "123"));

		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	@Test
	@Transactional
	public void createPaymentTest_ok() {
		String ticketPrice = "150.0";
		String ids = "1";
		CreatePaymentDTO cpDTO = payPalService.createPayment(ticketPrice, ids, "reservations");
		assertNotNull(cpDTO);
		assertEquals("success", cpDTO.getStatus());
	}
}
