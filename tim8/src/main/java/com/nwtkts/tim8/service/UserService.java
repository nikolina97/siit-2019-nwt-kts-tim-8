package com.nwtkts.tim8.service;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.nwtkts.tim8.dto.UserDTO;
import com.nwtkts.tim8.dto.UserEditProfileDTO;
import com.nwtkts.tim8.mapper.UserMapper;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.model.User;
import com.nwtkts.tim8.repository.IUserRepository;
import com.nwtkts.tim8.repository.TicketRepository;

@Service
public class UserService {

	@Autowired
	private TicketRepository ticketRepository;

	@Autowired
	private IUserRepository userRepository;

	@Autowired
	private CustomUserDetailsService customService;

	public List<Ticket> userTickets(Pageable pageable) {
		return ((RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getTickets()
				.subList(pageable.getPageNumber() * pageable.getPageSize(),
						pageable.getPageNumber() * pageable.getPageSize() + pageable.getPageSize());
	}

	public boolean cancel(Integer id) throws InvalidDataException {
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (user.getTickets().stream().noneMatch(t -> t.getId().equals(id)))
			throw new InvalidDataException("This ticket does not exists or ticket is not yours");

		Ticket ticket = ticketRepository.findById(id).get();

		/*Date now = new Date();
		Date expirationDate = new Date(new Timestamp(ticket.getDate().getTime()
				- ticket.getSeat().getSupportedSeatCategory().getManifestation().getDaysBeforeExpires() * 24 * 60 * 60
						* 1000).getTime());

		if (expirationDate.before(now))
			throw new NotAcceptableException(
					"You can't cancel reservation after " + new SimpleDateFormat("dd.MM.yyyy").format(expirationDate));*/
		if (ticket.getPaid())
			throw new InvalidDataException("You can't cancel an already paid reservation");

		ticket.setActive(false);
		ticketRepository.save(ticket);

		return true;
	}

	
	@Transactional
	public UserDTO editUser(UserEditProfileDTO userDTO) throws InvalidDataException {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User managedUser = userRepository.getOne(user.getId());

		try {
			if (userDTO.getFirstName().contentEquals("") || userDTO.getLastName().equals("")) {
				throw new InvalidDataException("You can't leave empty fields");
			}

			User checkUser = null;

			managedUser.setFirstName(userDTO.getFirstName());
			managedUser.setLastName(userDTO.getLastName());

			return UserMapper.toDTO(managedUser);
		} catch (NullPointerException e) {
			throw new InvalidDataException("Must enter every field!");
		}
	}
	
	@Transactional
	public UserDTO uploadImage(MultipartFile file) throws InvalidDataException {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User managedUser = userRepository.getOne(user.getId());
		String currentDir = System.getProperty("user.dir");
		String imgPath = currentDir+"\\src\\main\\resources\\static\\"+ file.getOriginalFilename();
		System.out.println(imgPath);
		File f = new File(imgPath);
		try {
			file.transferTo(f);
			managedUser.setImage("http://localhost:8080/"+file.getOriginalFilename());
			return UserMapper.toDTO(managedUser);
		} catch (IllegalStateException | IOException e) {
			throw new InvalidDataException("Error with uploading image");
		}
    }
}
