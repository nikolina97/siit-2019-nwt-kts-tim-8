package com.nwtkts.tim8.service;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.model.VerificationToken;

@Service
public class EmailService {

	@Autowired
	private JavaMailSender javaMailSender;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private VerificationTokenService verificationService;

	@Async
	public void sendEmailWithTicket(Ticket ticket) throws MessagingException {
		MimeMessage mail = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mail, true);

		String manifestationName = ticket.getSeat().getSupportedSeatCategory().getManifestation().getName();
		helper.setTo(ticket.getUser().getEmail());
		helper.setSubject(manifestationName + " - Ticket");

		String text = String.format("Dear " + ticket.getUser().getFirstName()
				+ ",\n\nWe are sending you a ticket you bought for the event " + manifestationName + ".\n\n"
				+ manifestationName + " Team");
		helper.setText(text);

		File pdf = new File("./src/main/resources/ticket_" + ticket.getSeat().getId() + ".pdf");
		FileSystemResource resource  = new FileSystemResource(pdf);
		helper.addAttachment(ticket.getManifestation().getName() + " - ticket.pdf", resource);

		javaMailSender.send(mail);
		
		pdf.delete();
	}
	
	@Async
	public void sendEmailWithReminderAboutReservation(Ticket ticket) {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(ticket.getUser().getEmail());
		mail.setFrom(env.getProperty("spring.mail.username"));
		
		String manifestationName = ticket.getSeat().getSupportedSeatCategory().getManifestation().getName();
		
		mail.setSubject(manifestationName + " - Reminder");
		
		String expirationDate = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(ticket.getDate() == null ? ticket.getManifestation().getStartDate() : new Date(ticket.getDate().getTime() - ticket.getSeat().getSupportedSeatCategory().getManifestation().getDaysBeforeExpires() * 24 * 60 * 60 * 1000));
		String text = String.format("Dear " + ticket.getUser().getFirstName()
				+ ",\n\nWe remind you that you have booked you ticket for the event " + manifestationName
				+ ". We also remind you that your reservation will expire on " + expirationDate
				+ ". Don't forget to buy your ticket on time!\n\n" + manifestationName + " Team");
		
		mail.setText(text);
		
		javaMailSender.send(mail);
	}
	
	@Async
	public void sendEmailWithReminderAboutEvent(Ticket ticket) {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(ticket.getUser().getEmail());
		mail.setFrom(env.getProperty("spring.mail.username"));
		
		String manifestationName = ticket.getSeat().getSupportedSeatCategory().getManifestation().getName();
		
		mail.setSubject(manifestationName + " - Reminder");
		
		String manifestationDate = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(ticket.getDate() == null ? ticket.getManifestation().getStartDate() : new Date(ticket.getDate().getTime()));
		String text = String.format("Dear " + ticket.getUser().getFirstName() + ",\n\nWe remind you that event "
				+ manifestationName + " will be held on " + manifestationDate
				+ ". Don't forget to come to our event!\n\n" + manifestationName + " Team");
		
		mail.setText(text);
		
		javaMailSender.send(mail);
	}

	@Async
	public void sendNotificaitionAsyncRegistration(RegisteredUser user) throws MailException, InterruptedException, UnsupportedEncodingException {

		String token = UUID.randomUUID().toString();
		VerificationToken verToken = new VerificationToken();
		verToken.setId(null);
		verToken.setToken(token);
		verToken.setUser(user);
		verificationService.saveToken(verToken);

		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo(user.getEmail());
		mail.setFrom(env.getProperty("spring.mail.username"));
		mail.setSubject("Confirmation of registration");
		String tekst = null;
		tekst = String.format("Confirm your registration on this link: \nhttp://localhost:4200/registration/confirmation/%s",URLEncoder.encode(token, "UTF-8"));

		mail.setText(tekst);
		javaMailSender.send(mail);

		System.out.println("Email sent.");
	}


}
