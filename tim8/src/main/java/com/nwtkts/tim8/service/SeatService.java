package com.nwtkts.tim8.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import com.nwtkts.tim8.dto.SeatsBySupportedSeatCategoryDTO;
import com.nwtkts.tim8.mapper.SeatsBySupportedSeatCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.nwtkts.tim8.dto.SeatDTO;
import com.nwtkts.tim8.mapper.SeatMapper;
import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.ManifestationState;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.Seat;
import com.nwtkts.tim8.model.SeatCategory;
import com.nwtkts.tim8.model.SupportedSeatCategory;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.repository.ManifestationRepository;
import com.nwtkts.tim8.repository.SeatRepository;
import com.nwtkts.tim8.repository.SupportedSeatCategoryRepository;
import com.nwtkts.tim8.repository.TicketRepository;

@Service
public class SeatService {

	@Autowired
	private SeatRepository seatRepository;

	@Autowired
	private ManifestationRepository manifestationRepository;

	@Autowired
	private SupportedSeatCategoryRepository supportedSeatCategoryRepository;

	@Autowired
	private TicketRepository ticketRepository;

	public Collection<SeatsBySupportedSeatCategoryDTO> getAllSeatsByManifestation(Integer manifestationId, Date date_)
			throws InvalidDataException {

		Optional<Manifestation> manifestationSearch = manifestationRepository.findById(manifestationId);

		if (!manifestationSearch.isPresent()) {
			throw new InvalidDataException("Invalid manifestation id");
		}

		Manifestation manifestation = manifestationSearch.get();

		String manifestationState = checkManifestationState(manifestation);
		if (manifestationState != null) {
			throw new InvalidDataException(manifestationState);
		}

		ArrayList<SeatsBySupportedSeatCategoryDTO> seatCategories = new ArrayList<>();

		Collection<SupportedSeatCategory> categories = supportedSeatCategoryRepository
				.findAllByManifestation(manifestation);

		if (categories.isEmpty()) {
			return seatCategories;
		}

		for (SupportedSeatCategory ssc : categories) {

			SeatsBySupportedSeatCategoryDTO sscDTO = SeatsBySupportedSeatCategoryMapper.toDTO(ssc);

			Collection<Seat> seats_ = ssc.getSeats(); //seatRepository.findAllBySupportedSeatCategory(ssc);
			boolean reservedSeat = false;
			if (!seats_.isEmpty()) {
				for (Seat s : seats_) {
					if (date_ != null) {
						reservedSeat = checkIfSeatIsReserved(s, date_);	
					}
					else {
						reservedSeat = !checkSeatForFestivalTicket(s, manifestation);
					}
					if (!reservedSeat) {
						reservedSeat = isFestivalTicket(s);
					}
					SeatDTO seatDTO = SeatMapper.toDTO(s, reservedSeat);
					sscDTO.getSeats().add(seatDTO);
				}
				seatCategories.add(sscDTO);
			}
		}
		for (SeatCategory sCat : manifestation.getLocation().getCategories()) {
			if (sCat.getStage()) {
				SupportedSeatCategory suppSc = new SupportedSeatCategory();
				suppSc.setSeatCategory(sCat);
				seatCategories.add(SeatsBySupportedSeatCategoryMapper.toDTO(suppSc));
			}
		}
		
		return seatCategories;
	}

	public boolean checkIfSeatIsReserved(Seat seat, Date date) {

		Timestamp ts = new Timestamp(date.getTime());

		Optional<Ticket> ticketSearch = ticketRepository.findOneBySeatAndDateAndActive(seat, ts, true);

		if (ticketSearch.isPresent()) {
			if (ticketSearch.get().getActive()) {
				return true;

			}
		}
		
		ticketSearch = ticketRepository.findFestivalTicket(seat);

		if (ticketSearch.isPresent()) {
			if (ticketSearch.get().getActive()) {
				return true;

			}
		}

		return false;
	}

	public String checkIfReservationExpired(Manifestation manifestation, Date date) {
		if (manifestation.getDaysBeforeExpires() <= 0) {
			return ("Reservation is forbidden");
		}
		
		if (date == null) {
			date = manifestation.getStartDate();
		}
		Date today = new Date();
		long difference = (today.getTime() - date.getTime()) / 86400000;
		difference = Math.abs(difference);

		if (today.after(date)) {
			return ("Manifestation has passed");
		}

		if (difference < manifestation.getDaysBeforeExpires()) {
			return ("Reservation expired");
		}

		return null;
	}

	public String checkManifestationState(Manifestation manifestation) {
		if (manifestation.getState() == ManifestationState.CANCELED) {
			return "Manifestation is canceled";
		} else if (manifestation.getState() == ManifestationState.PAST) {
			return "Manifestation has passed";
		} else if (manifestation.getState() == ManifestationState.SOLD_OUT) {
			return "All tickets are sold out";
		}
		return null;
	}
	
	public Boolean checkSeatForFestivalTicket(Seat seat, Manifestation manifestation) {
		
		List<Ticket> ticketSearch = (List<Ticket>) ticketRepository.findBetweenTwoDates(seat, manifestation.getStartDate(), manifestation.getEndDate());
		
		if (ticketSearch.size() > 0) {
			return false;
		}
		return true;
	}
	
	public boolean isFestivalTicket(Seat seat) {
		Optional<Ticket> ticketSearch = ticketRepository.findFestivalTicket(seat);
		
		if (ticketSearch.isPresent()) {
			return true;
		}
		return false;
	}
	
	public boolean checkSeatForBuying(Seat seat, Date date, RegisteredUser ru) {
		Timestamp ts = new Timestamp(date.getTime());

		Optional<Ticket> ticketSearch = ticketRepository.findOneBySeatAndDateAndActive(seat, ts, true);

		if (ticketSearch.isPresent()) {
			return true;
		}
		
		ticketSearch = ticketRepository.findFestivalTicket(seat);
		
		if (ticketSearch.isPresent()) {
			
			Ticket t = ticketSearch.get();
			if (t.getUser() == ru) {
				return false;
			}
			return true;
		}
		
		return false;
	}

	@Transactional(readOnly = false, rollbackFor = InvalidDataException.class, propagation = Propagation.REQUIRED)
	public List<Ticket> reserveSeats(List<Integer> seatIds, Date date_) throws InvalidDataException{
		List<Seat> seats = seatRepository.findAllById(seatIds);
		if (seats.isEmpty()) {
			throw new InvalidDataException("Invalid seats");
		}
		Manifestation manif = seats.get(0).getSupportedSeatCategory().getManifestation();
		String checkIfResExpired = checkIfReservationExpired(manif, date_);
		if (checkIfResExpired != null) {
			throw new InvalidDataException(checkIfResExpired);
		}
		if (manif.getState() != ManifestationState.OPEN) {
		throw new InvalidDataException("Reservation is disabled");
		}
		for (Seat seat : seats) {
			if (date_ == null) {
				if (!(checkSeatForFestivalTicket(seat,manif))){
					throw new InvalidDataException("Seat is already reserved or bought.");
				}
			}
			else {
				if (checkIfSeatIsReserved(seat, date_)) {
					throw new InvalidDataException("Seat is already reserved or bought.");
				}
			}			
		}
		List<Ticket> tickets = new ArrayList<Ticket>();
		for (Seat seat : seats) {
			Timestamp ts = null;
			if (date_ != null) {
				ts = new Timestamp(date_.getTime());
			}
			RegisteredUser ru = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Ticket t = new Ticket(false, ts, new Timestamp(new Date().getTime()), seat, ru, manif);
			tickets.add(t);
			ticketRepository.save(t);
		}
		return tickets;
	}
}
