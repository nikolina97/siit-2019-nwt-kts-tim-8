package com.nwtkts.tim8.service;

import com.nwtkts.tim8.dto.CompletePaymentDTO;
import com.nwtkts.tim8.dto.CreatePaymentDTO;
import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PayPalService {

	private String clientId = "ASUJTWroJmZthSS8S9zAzue1kAl4B-Y9XKyl3jwVa38s1LHGVfVyTRt6hhWinUuuf_DdlpSjertSzPZj";
	private String clientSecret = "EK-NqYkYvqul8wsbXioi16B4KbZmvPvHINlgapjEbidbnhKVwyP-LQG40fz_-IFWhhdMfocNhF08xO5F";

	public CreatePaymentDTO createPayment(String price, String ids, String cancelUrl) {
		Amount amount = new Amount();
		amount.setCurrency("USD");
		amount.setTotal(price);
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		List<Transaction> transactions = new ArrayList<>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		Payment payment = new Payment();
		payment.setIntent("sale");
		payment.setPayer(payer);
		payment.setTransactions(transactions);

		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setCancelUrl("http://localhost:4200/" + cancelUrl);
		redirectUrls.setReturnUrl("http://localhost:4200/payment-confirmation?ids=" + ids);
		payment.setRedirectUrls(redirectUrls);

		Payment createdPayment;
		CreatePaymentDTO result = new CreatePaymentDTO();

		try {
			String redirectUrl = "";
			APIContext context = new APIContext(clientId, clientSecret, "sandbox");
			createdPayment = payment.create(context); //This object contains usefull data about the payment, like amount, total, tex, shipping, currency, and so on

			if (createdPayment != null) {
				List<Links> links = createdPayment.getLinks();

				for (Links link : links) {
					if (link.getRel().equals("approval_url")) {
						redirectUrl = link.getHref(); //URL to the PayPal service
						break;
					}
				}

				result.setStatus("success");
				result.setRedirectUrl(redirectUrl);
			}
		} catch (PayPalRESTException e) {
			System.err.println("Error happened during payment creation!");
			e.printStackTrace();
		}

		return result;
	}

	public CompletePaymentDTO completePayment(String paymentId, String payerId) {
		Payment payment = new Payment();
		payment.setId(paymentId);

		PaymentExecution paymentExecution = new PaymentExecution();
		paymentExecution.setPayerId(payerId);

		CompletePaymentDTO result = new CompletePaymentDTO();

		try {
			APIContext context = new APIContext(clientId, clientSecret, "sandbox");
			Payment createdPayment = payment.execute(context, paymentExecution);

			if (createdPayment != null) {
				result.setStatus("success");
				result.setPayment(createdPayment);
			}
		} catch (PayPalRESTException e) {
			System.err.println(e.getDetails());
		}

		return result;
	}

}
