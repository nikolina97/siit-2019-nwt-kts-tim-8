package com.nwtkts.tim8.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.nwtkts.tim8.dto.*;
import com.nwtkts.tim8.mapper.MainManifestationInfoMapper;
import com.nwtkts.tim8.model.*;
import org.omg.CORBA.DynAnyPackage.Invalid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.nwtkts.tim8.mapper.ManifestationInfoMapper;
import com.nwtkts.tim8.repository.LocationRepository;
import com.nwtkts.tim8.repository.ManifestationRepository;
import com.nwtkts.tim8.repository.SeatCategoryRepository;
import com.nwtkts.tim8.repository.SeatRepository;
import com.nwtkts.tim8.repository.SupportedSeatCategoryRepository;
import com.nwtkts.tim8.repository.TicketRepository;

@Service
public class ManifestationService {

	@Autowired
	private ManifestationRepository manifestationRepository;
	
	@Autowired
	private SeatRepository seatRepository;
	
	@Autowired
	private SeatCategoryRepository seatCategoryRepository;
	
	@Autowired
	private SupportedSeatCategoryRepository supportedSeatCategoryRepository;
	
	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private TicketRepository ticketRepository;

	public ManifestationInfoDTO findById(Integer id) throws InvalidDataException {
		Manifestation manifestation = manifestationRepository.findById(id).orElse(null);

		if(manifestation == null)
			throw new InvalidDataException("Manifestation not found");

		return ManifestationInfoMapper.toDTO(manifestation);
	}

	public ManifestationInfoDTO save(ManifestationDTO dto) throws InvalidDataException {
		if(Stream.of(dto.getCategory(), dto.getDaysBeforeExpires(), dto.getDescription(), dto.getEndDate(), dto.getLocationId(), dto.getName(), dto.getStartDate(), dto.getState(), dto.getSscDTOs()).anyMatch(Objects::isNull))
			throw new InvalidDataException("Some data is missing");

		Location location = locationRepository.findById(dto.getLocationId()).orElse(null);
		if(location == null)
			throw new InvalidDataException("Location not found");

		for(SupportedSeatCategoryDTO sscDTO : dto.getSscDTOs()) {
			if(seatCategoryRepository.findById(sscDTO.getSeatCategoryId()).orElse(null) == null)
				throw new InvalidDataException("Seat category with id " + sscDTO.getSeatCategoryId() + " not found");
		}

		if(dto.getName().isEmpty())
			throw new InvalidDataException("Name can't be empty");
		else if(dto.getDescription().isEmpty())
			throw new InvalidDataException("Description can't be empty");
		else if(dto.getDaysBeforeExpires() < -1)
			throw new InvalidDataException("Number of days after which the possibility of reservation expires can't be less than -1");
		else if(dto.getStartDate().getTime() - 1000 * 60 * 60 * 24 * dto.getDaysBeforeExpires() < (new Date()).getTime())
			throw new InvalidDataException("The date when the reservations are no longer allowed cannot be earlier than today");
		else if((new Date()).after(dto.getStartDate()))
			throw new InvalidDataException("Start date must be in the future");
		else if(dto.getEndDate().getTime() < dto.getStartDate().getTime())
			throw new InvalidDataException("End date can't be before start date");

		List<Manifestation> manifestations = manifestationRepository.findByLocation_Id(location.getId());

		for(Manifestation manifestationInTheSameLocation : manifestations) {
			if (((dto.getStartDate().compareTo(manifestationInTheSameLocation.getStartDate()) == 0
					|| dto.getStartDate().compareTo(manifestationInTheSameLocation.getEndDate()) == 0
					|| dto.getEndDate().compareTo(manifestationInTheSameLocation.getStartDate()) == 0
					|| dto.getEndDate().compareTo(manifestationInTheSameLocation.getEndDate()) == 0)
					|| (dto.getStartDate().after(manifestationInTheSameLocation.getStartDate())
					&& dto.getStartDate().before(manifestationInTheSameLocation.getEndDate()))
					|| (dto.getEndDate().after(manifestationInTheSameLocation.getStartDate())
					&& dto.getEndDate().before(manifestationInTheSameLocation.getEndDate()))
					|| (dto.getStartDate().before(manifestationInTheSameLocation.getStartDate())
					&& dto.getEndDate().after(manifestationInTheSameLocation.getEndDate())))
					&& manifestationInTheSameLocation.getState() != ManifestationState.CANCELED)
				throw new InvalidDataException("Some other event will be held at this time");
		}

		File file = new File(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\" + dto.getImageName());
		try {
			if(file.createNewFile()) {
				String base64 = dto.getImage().substring(dto.getImage().indexOf(",") + 1);

				FileOutputStream stream = new FileOutputStream(file, false);
				stream.write(Base64.getDecoder().decode(base64));
				stream.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		Manifestation manifestation = new Manifestation();
		manifestation.setName(dto.getName());
		manifestation.setDescription(dto.getDescription());
		manifestation.setStartDate(dto.getStartDate());
		manifestation.setEndDate(dto.getEndDate());
		manifestation.setDaysBeforeExpires(dto.getDaysBeforeExpires());
		manifestation.setLocation(location);
		manifestation.setState(dto.getState());
		manifestation.setCategory(dto.getCategory());
		manifestation.setImage("http://localhost:8080/" + dto.getImageName());

		manifestation = manifestationRepository.save(manifestation);

		for(SupportedSeatCategoryDTO sscDTO : dto.getSscDTOs()) {
			SupportedSeatCategory supportedSeatCategory = new SupportedSeatCategory();

			SeatCategory seatCategory = seatCategoryRepository.findById(sscDTO.getSeatCategoryId()).get();

			supportedSeatCategory.setTicketPrice(sscDTO.getTicketPrice());
			supportedSeatCategory.setSeatCategory(seatCategory);
			supportedSeatCategory.setManifestation(manifestation);
			supportedSeatCategory.setNumberOfSeats(seatCategory.getEnumerable() ? seatCategory.getRows() * seatCategory.getColumns() : sscDTO.getNumberOfSeats());

			SupportedSeatCategory ssc = supportedSeatCategoryRepository.save(supportedSeatCategory);

			if(seatCategory.getEnumerable()) {
				for(int i = 1; i <= seatCategory.getRows(); i++) {
					for(int j = 1; j <= seatCategory.getColumns(); j++) {
						Seat seat = new Seat();

						seat.setSupportedSeatCategory(ssc);
						seat.setRow(i);
						seat.setColumn(j);

						seatRepository.save(seat);
					}
				}
			} else {
				for(int i = 0; i < supportedSeatCategory.getNumberOfSeats(); i++) {
					Seat seat = new Seat();

					seat.setSupportedSeatCategory(ssc);
					seat.setRow(-1);
					seat.setColumn(-1);

					seatRepository.save(seat);
				}
			}

			supportedSeatCategoryRepository.save(supportedSeatCategory);
		}

		return ManifestationInfoMapper.toDTO(manifestation);
	}

	public ManifestationInfoDTO update(Integer id, UpdateManifestationDTO dto) throws InvalidDataException {
		if(Stream.of(dto.getName(), dto.getDescription()).anyMatch(Objects::isNull))
			throw new InvalidDataException("Some data is missing");

		Manifestation manifestation = manifestationRepository.findById(id).orElse(null);
		if(manifestation == null)
			throw new InvalidDataException("Manifestation not found");

		if(manifestation.getState() == ManifestationState.CANCELED )
			throw new InvalidDataException("You can't edit canceled manifestation");
		else if(manifestation.getState() == ManifestationState.PAST)
			throw new InvalidDataException("You can't edit manifestation that has passed");

		if(dto.getName().isEmpty())
			throw new InvalidDataException("Name can't be empty");
		else if(dto.getDescription().isEmpty())
			throw new InvalidDataException("Description can't be empty");

		manifestation.setName(dto.getName());
		manifestation.setDescription(dto.getDescription());

		manifestation = manifestationRepository.save(manifestation);

		return ManifestationInfoMapper.toDTO(manifestation);
	}

	public List<MainManifestationInfoDTO> find(SearchManifestationDTO dto, Pageable pageable) {
		List<Manifestation> manifestations = manifestationRepository.findAllByStateNotIn(Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable).getContent();

		if(dto.getName() != null && !dto.getName().isEmpty())
			manifestations = manifestations.stream().filter(m -> m.getName().toLowerCase().contains(dto.getName().toLowerCase())).collect(Collectors.toList());
		if(dto.getCategories() != null && !dto.getCategories().isEmpty())
			manifestations = manifestations.stream().filter(m -> dto.getCategories().stream().anyMatch(c -> c.equalsIgnoreCase(m.getCategory().toString()))).collect(Collectors.toList());
		if(dto.getLocations() != null && !dto.getLocations().isEmpty())
			manifestations = manifestations.stream().filter(m -> dto.getLocations().stream().anyMatch(l -> l.equalsIgnoreCase(m.getLocation().getAddress()))).collect(Collectors.toList());

		List<MainManifestationInfoDTO> result = new ArrayList<>();

		for (Manifestation manifestation : manifestations)
			result.add(MainManifestationInfoMapper.toDTO(manifestation));
		
		return result;
	}

	public List<MainManifestationInfoDTO> findAll(Pageable pageable) {
		List<Manifestation> manifestations = manifestationRepository.findAllByStateNotIn(Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST), pageable).getContent();
		List<MainManifestationInfoDTO> result = new ArrayList<>();
		for(Manifestation manifestation : manifestations)
			result.add(MainManifestationInfoMapper.toDTO(manifestation));

		return result;
	}

	public UpdateManifestationDTO getInfoForUpdate(Integer id) throws InvalidDataException {
		Manifestation manifestation = manifestationRepository.findById(id).orElse(null);

		if(manifestation == null)
			throw new InvalidDataException("Manifestation not found");

		return new UpdateManifestationDTO(manifestation.getName(), manifestation.getDescription());
	}

	public Long count() {
		return manifestationRepository.countByStateNotIn(Arrays.asList(ManifestationState.CANCELED, ManifestationState.PAST));
	}

	public ManifestationInfoDTO getManifestationInfo(Integer manifestationId) throws InvalidDataException {
		Optional<Manifestation> manifestationSearch = manifestationRepository.findById(manifestationId);
		
		if (!manifestationSearch.isPresent()) {
			throw new InvalidDataException("Invalid manifestation Id");
		}
		
		Manifestation manifestation = manifestationSearch.get();		
		ManifestationInfoDTO manifestationDTO = ManifestationInfoMapper.toDTO(manifestation);
		
		return manifestationDTO;
	}

	public Boolean cancel(Integer id) throws InvalidDataException {
		Manifestation manifestation = manifestationRepository.findById(id).orElse(null);

		if(manifestation == null)
			throw new InvalidDataException("Manifestation not found");

		if(manifestation.getState() == ManifestationState.PAST)
			throw new InvalidDataException("Manifestation has passed");
		else if(manifestation.getState() == ManifestationState.CANCELED)
			throw new InvalidDataException("Manifestation has already been canceled");

		manifestation.setState(ManifestationState.CANCELED);
		manifestationRepository.save(manifestation);

		Collection<Ticket> tickets = ticketRepository.findAllByManifestation(manifestation);
		if (!tickets.isEmpty()) {
			for (Ticket ticket : tickets) {
				ticket.setActive(false);
				ticketRepository.save(ticket);
			}
		}
		

		return true;
	}
	
	public Map<String, Long> getDaily(Integer id, String start, String end) throws InvalidDataException, ParseException {
		Optional<Manifestation> manifSearch = manifestationRepository.findById(id);
		
		if (!manifSearch.isPresent()) {
			throw new InvalidDataException("Manifestation not found.");
		}
		
		Manifestation manifestation = manifSearch.get();
		
		if(manifestation.getState().equals(ManifestationState.CANCELED)) {
			throw new InvalidDataException("This manifestation is canceled.");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Timestamp startDate = new Timestamp(sdf.parse(start).getTime());
		Timestamp endDate = new Timestamp(sdf.parse(end).getTime());
		
		if(endDate.before(startDate) || endDate.equals(startDate)) {
			throw new InvalidDataException("Invalid date!");
		}
		
		ArrayList<Ticket> boughtTickets = (ArrayList<Ticket>) ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, startDate, endDate);

		Map<String, Long> reportData = boughtTickets.stream().collect(Collectors
				.groupingBy(t -> sdf.format(t.getBuyingDate()), Collectors.counting()));
		Map<String, Long> reportDataHash = new HashMap<String, Long>(reportData);

		return reportDataHash;
	}
	
	public Map<String, Long> getWeekly(Integer id, String start, String end) throws InvalidDataException, ParseException {
		Optional<Manifestation> manifSearch = manifestationRepository.findById(id);
		
		if (!manifSearch.isPresent()) {
			throw new InvalidDataException("Manifestation not found.");
		}
		
		Manifestation manifestation = manifSearch.get();
		
		if(manifestation.getState().equals(ManifestationState.CANCELED)) {
			throw new InvalidDataException("This manifestation is canceled.");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Timestamp startDate = new Timestamp(sdf.parse(start).getTime());
		Timestamp endDate = new Timestamp(sdf.parse(end).getTime());
		
		if(endDate.before(startDate) || endDate.equals(startDate)) {
			throw new InvalidDataException("Invalid date!");
		}
		
		ArrayList<Ticket> boughtTickets = (ArrayList<Ticket>) ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, startDate, endDate);

		SimpleDateFormat sdf2 = new SimpleDateFormat("MMM yyyy 'week: 'W", Locale.ENGLISH);
		
		Map<String, Long> reportData = boughtTickets.stream().collect(Collectors
				.groupingBy(t -> sdf2.format(t.getBuyingDate()), Collectors.counting()));
		Map<String, Long> reportDataHash = new HashMap<String, Long>(reportData);

		return reportDataHash;
	}
	
	public Map<String, Long> getMonthly(Integer id, String start, String end) throws InvalidDataException, ParseException {
		Optional<Manifestation> manifSearch = manifestationRepository.findById(id);
		
		if (!manifSearch.isPresent()) {
			throw new InvalidDataException("Manifestation not found.");
		}
		
		Manifestation manifestation = manifSearch.get();
		
		if(manifestation.getState().equals(ManifestationState.CANCELED)) {
			throw new InvalidDataException("This manifestation is canceled.");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Timestamp startDate = new Timestamp(sdf.parse(start).getTime());
		Timestamp endDate = new Timestamp(sdf.parse(end).getTime());
		
		if(endDate.before(startDate) || endDate.equals(startDate)) {
			throw new InvalidDataException("Invalid date!");
		}
		
		ArrayList<Ticket> boughtTickets = (ArrayList<Ticket>) ticketRepository.findAllByManifestationBetweenTwoDates(manifestation, startDate, endDate);

		SimpleDateFormat sdf2 = new SimpleDateFormat("MMM yyyy", Locale.ENGLISH);
		
		Map<String, Long> reportData = boughtTickets.stream().collect(Collectors
				.groupingBy(t -> sdf2.format(t.getBuyingDate()), Collectors.counting()));
		Map<String, Long> reportDataHash = new HashMap<String, Long>(reportData);

		return reportDataHash;
	}

}
