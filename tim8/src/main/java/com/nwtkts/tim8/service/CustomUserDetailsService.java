package com.nwtkts.tim8.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.RedirectView;

import com.nwtkts.tim8.dto.UserChangePasswordDTO;
import com.nwtkts.tim8.model.Admin;
import com.nwtkts.tim8.model.Authority;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.model.User;
import com.nwtkts.tim8.model.UserType;
import com.nwtkts.tim8.repository.IAuthorityRepository;
import com.nwtkts.tim8.repository.IUserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	protected final Log LOGGER = LogFactory.getLog(getClass());

	@Autowired
	private IUserRepository userRepository;

	@Autowired
	IAuthorityRepository authorityRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private EmailService emailService;

	// Funkcija koja na osnovu username-a iz baze vraca objekat User-a
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetails u =  userRepository.findByUsername(username);
		if(u!= null)
			return u;
		else
			throw new UsernameNotFoundException(String.format("User with username '%s' not found", username));
	}
	
	
	public void encodePassword(User u) {
		String pass =  this.passwordEncoder.encode(u.getPassword());
		u.setPassword(pass);
	}
	
	public String encodePassword(String password) {
		return this.passwordEncoder.encode(password);		
	}
	
public RegisteredUser saveRegisteredUser(RegisteredUser ru) throws InvalidDataException, MailException, UnsupportedEncodingException, InterruptedException {
		
		User u = findByUsername(ru.getUsername());
		if(u != null) {
			throw new InvalidDataException("Username already taken!"); 
		}
		u = findByEmail(ru.getEmail());
		if(u != null) {
			throw new InvalidDataException("Email already taken!"); 
		}
		if (ru.getUsername().isEmpty() || ru.getFirstName().isEmpty() || ru.getLastName().isEmpty() || ru.getEmail().isEmpty()
				|| ru.getPassword().isEmpty()) {
			throw new InvalidDataException("User information is incomplete!");
		}
		encodePassword(ru);
		List<Authority> authorities = new ArrayList<Authority>();
		Authority a = findAuthority(1);
		authorities.add(a);
		ru.setAuthorities(authorities);
		ru.setTickets(new ArrayList<Ticket>());
		ru.setVerified(false);
		this.userRepository.save(ru);

		emailService.sendNotificaitionAsyncRegistration(ru);
		return ru;
	}

	
	// Funkcija pomocu koje korisnik menja svoju lozinku
	public void changePassword(UserChangePasswordDTO userDTO) throws InvalidDataException {

		if(!userDTO.getNewPassword().equals(userDTO.getRepeatedPassword())) {
			throw new InvalidDataException("You heaven't repeted new password correctly!");
		}
		Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
		String username = currentUser.getName();

		if (authenticationManager != null) {
			LOGGER.debug("Re-authenticating user '" + username + "' for password change request.");

			System.out.println(authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, userDTO.getOldPassword())));
		} else {
			LOGGER.debug("No authentication manager set. can't change Password!");
			return;
		}

		LOGGER.debug("Changing password for user '" + username + "'");

		User user = (User) loadUserByUsername(username);

		// pre nego sto u bazu upisemo novu lozinku, potrebno ju je hesirati
		// ne zelimo da u bazi cuvamo lozinke u plain text formatu
		user.setPassword(passwordEncoder.encode(userDTO.getNewPassword()));
		userRepository.save(user);

	}
	
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public User findUserByToken(String token) {
		return userRepository.findByToken(token);
	}
	
	public Authority findAuthority(Integer id) {
		return authorityRepository.findById(id).get();
	}


	public Admin saveAdmin(Admin user) throws InvalidDataException {
		
		User u = findByUsername(user.getUsername());
		if(u != null) {
			throw new InvalidDataException("Username already taken!"); 
		}
		u = findByEmail(user.getEmail());
		if(u != null) {
			throw new InvalidDataException("Email already taken!"); 
		}
		if (user.getUsername().isEmpty() || user.getFirstName().isEmpty() || user.getLastName().isEmpty() || user.getEmail().isEmpty()
				|| user.getPassword().isEmpty()) {
			throw new InvalidDataException("User information is incomplete!");
		}
		encodePassword(user);
		List<Authority> authorities = new ArrayList<Authority>();
		Authority a = findAuthority(2);
		a.setUserType(UserType.ROLE_ADMIN);
		authorities.add(a);
		user.setAuthorities(authorities);
		this.userRepository.save(user);
		
		return user;
	}


	public boolean confirmRegistration(String token) throws InvalidDataException {
		User user = findUserByToken(token);
		if (user != null) {
			RegisteredUser u = ((RegisteredUser) user);
			u.setVerified(true);
			this.userRepository.save(u);
			return true;
		}
		throw new InvalidDataException("Invalid token!");
	}
}
