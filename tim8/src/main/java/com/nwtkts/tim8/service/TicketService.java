package com.nwtkts.tim8.service;

import com.google.zxing.WriterException;
import com.itextpdf.text.DocumentException;
import com.nwtkts.tim8.common.TicketGenerator;
import com.nwtkts.tim8.dto.CreatePaymentDTO;
import com.nwtkts.tim8.dto.ManifestationInfoDTO;
import com.nwtkts.tim8.dto.TicketDTO;
import com.nwtkts.tim8.mapper.ManifestationInfoMapper;
import com.nwtkts.tim8.mapper.TicketMapper;
import com.nwtkts.tim8.model.*;
import com.nwtkts.tim8.repository.SeatRepository;
import com.nwtkts.tim8.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TicketService {
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private SeatRepository seatRepository;
	
	@Autowired
	private SeatService seatService;

	@Autowired
	private PayPalService payPalService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private TicketGenerator ticketGenerator;
	
	public List<TicketDTO> getAllBoughtTicketsByUser(RegisteredUser ru) throws InvalidDataException {

		Collection<Ticket> tickets = ticketRepository.findAllByUserAndPaidAndActive(ru, true, true);
		List<TicketDTO> dtoTickets = new ArrayList<TicketDTO>();
		
		if (tickets.isEmpty()) {
			return dtoTickets;
		}
		
		for (Ticket ticket : tickets) {
			
			TicketDTO ticketDTO = TicketMapper.toDTO(ticket);
			dtoTickets.add(ticketDTO);
		}
		
		return dtoTickets;	}

	public List<TicketDTO> getAllReservedTicketsByUser(RegisteredUser ru) throws InvalidDataException{
		
		Collection<Ticket> tickets = ticketRepository.findAllByUserAndPaidAndActive(ru, false, true);
		List<TicketDTO> dtoTickets = new ArrayList<TicketDTO>();
		
		if (tickets.isEmpty()) {
			return dtoTickets;
		}
		
		for (Ticket ticket : tickets) {
			
			TicketDTO ticketDTO = TicketMapper.toDTO(ticket);
			dtoTickets.add(ticketDTO);
		}
		
		return dtoTickets;
	}

	@Transactional(readOnly = false, rollbackFor = InvalidDataException.class, propagation = Propagation.REQUIRED)
	public CreatePaymentDTO buyTickets(List<Integer> seatIds, Date date_) throws InvalidDataException{
		List<Seat> seats = seatRepository.findAllById(seatIds);
		if (seats.isEmpty()) {
			throw new InvalidDataException("Invalid seats");
		}
		Manifestation manif = seats.get(0).getSupportedSeatCategory().getManifestation();
	
		if (manif.getState() != ManifestationState.OPEN) {
		throw new InvalidDataException("Manifestation is not available for buying tickets.");
		}
		for (Seat seat : seats) {
			if (date_ == null) {
				if (!(seatService.checkSeatForFestivalTicket(seat,manif))){
					throw new InvalidDataException("Seat is already reserved or bought.");
				}
			}
			else {
				if (seatService.checkIfSeatIsReserved(seat, date_)) {
					throw new InvalidDataException("Seat is already reserved or bought.");
				}
			}			
		}

		StringBuilder ids = new StringBuilder();
		double days = 1.00;
		for (int i = 0; i < seats.size(); i++) {
			Timestamp ts = null;
			if (date_ != null) {
				ts = new Timestamp(date_.getTime());
			}
			RegisteredUser ru = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (date_ == null) {
				ManifestationInfoDTO manifestationDTO = ManifestationInfoMapper.toDTO(manif);
			    days = (double) manifestationDTO.getDates().size();
			}
			Ticket t = new Ticket(false, ts, new Timestamp(new Date().getTime()), seats.get(i), ru, manif);
			t = ticketRepository.save(t);

			ids.append(t.getId());
			if(i != seats.size() - 1)
				ids.append("-");
		}
		return payPalService.createPayment(Double.toString(seats.stream().mapToDouble(s -> s.getSupportedSeatCategory().getTicketPrice()).sum()*days), ids.toString(), "payment-confirmation?cancel=true&ids=" + ids);
		
	}

	public CreatePaymentDTO payTicket(Integer id) throws InvalidDataException {
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if(user.getTickets().stream().noneMatch(t -> t.getId().equals(id)))
			throw new InvalidDataException("This ticket does not exists or ticket is not yours");

		Ticket ticket = ticketRepository.findById(id).orElse(null);

		return payPalService.createPayment(ticket.getSeat().getSupportedSeatCategory().getTicketPrice().toString(), id.toString(), "reservations");
	}

	public void markAsPaid(String ids) throws InvalidDataException {
		String[] id = ids.split("-");
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		for(String i : id) {
			if (user.getTickets().stream().noneMatch(ticket -> ticket.getId().equals(Integer.valueOf(i))))
				throw new InvalidDataException("This ticket does not exists or ticket is not yours");

			Ticket t = ticketRepository.findById(Integer.valueOf(i)).orElse(null);

			if(t != null) {
				t.setPaid(true);
				ticketRepository.save(t);
			}

			// Placanje i slanje karte na mail
			try {
				// Ovim redosledom moraju ici metode, sada kada nema transakcija
				ticketGenerator.generatePdf(t);
				emailService.sendEmailWithTicket(t);
			} catch (DocumentException e) {
				System.err.println("An error occurred while creating PDF file");
			} catch (WriterException e) {
				System.err.println("An error occurred while creating QR code");
			} catch (IOException e) {
				System.err.println("An error occurred while writing to PDF file");
			} catch (MessagingException e) {
				System.err.println("An error occurred while sending email");
			}
		}
	}

	public void deleteTickets(String ids) throws InvalidDataException {
		String[] id = ids.split("-");
		RegisteredUser user = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		for(String i : id) {
			if (user.getTickets().stream().noneMatch(ticket -> ticket.getId().equals(Integer.valueOf(i))))
				throw new InvalidDataException("This ticket does not exists or ticket is not yours");

			ticketRepository.deleteById(Integer.valueOf(i));
		}
	}
}
