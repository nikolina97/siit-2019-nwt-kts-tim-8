package com.nwtkts.tim8.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nwtkts.tim8.model.VerificationToken;
import com.nwtkts.tim8.repository.IVerificationTokenRepository;

@Service
public class VerificationTokenService {
	@Autowired
	private IVerificationTokenRepository verificationTokenRepository;

	public void saveToken(VerificationToken token) {
		verificationTokenRepository.save(token);
	}

}
