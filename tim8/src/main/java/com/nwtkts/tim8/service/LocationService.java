package com.nwtkts.tim8.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.nwtkts.tim8.dto.LocationDTO;
import com.nwtkts.tim8.dto.MainLocationInfoDTO;
import com.nwtkts.tim8.dto.MainManifestationInfoDTO;
import com.nwtkts.tim8.dto.ManifestationInfoDTO;
import com.nwtkts.tim8.mapper.LocationMapper;
import com.nwtkts.tim8.mapper.MainLocationInfoMapper;
import com.nwtkts.tim8.mapper.MainManifestationInfoMapper;
import com.nwtkts.tim8.mapper.ManifestationInfoMapper;
import com.nwtkts.tim8.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.nwtkts.tim8.repository.LocationRepository;
import com.nwtkts.tim8.repository.ManifestationRepository;
import com.nwtkts.tim8.repository.TicketRepository;

@Service
public class LocationService {

	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private TicketRepository ticketRepository;

	public List<MainLocationInfoDTO> getAllPageable(Pageable pageable) {
		List<Location> locations = locationRepository.findAll(pageable).getContent();
		List<MainLocationInfoDTO> result = new ArrayList<>();
		for (Location location : locations)
			result.add(MainLocationInfoMapper.toDTO(location));

		return result;
	}

	public List<MainLocationInfoDTO> getAll() {
		List<Location> locations = locationRepository.findAll();
		List<MainLocationInfoDTO> result = new ArrayList<>();
		for (Location location : locations)
			result.add(MainLocationInfoMapper.toDTO(location));

		return result;
	}

	public MainLocationInfoDTO findById(Integer id) throws InvalidDataException {
		Location location = locationRepository.findById(id).orElse(null);

		if(location == null)
			throw new InvalidDataException("Location not found");

		return MainLocationInfoMapper.toDTO(location);
	}

	public MainLocationInfoDTO save(LocationDTO dto) throws InvalidDataException {
		if(Stream.of(dto.getAddress(), dto.getName(), dto.getLatitude(), dto.getLongitude(), dto.getCategories()).anyMatch(Objects::isNull))
			throw new InvalidDataException("Some data is missing");
		else if(dto.getName().isEmpty())
			throw new InvalidDataException("Name can't be empty");
		else if(dto.getAddress().isEmpty())
			throw new InvalidDataException("Address can't be empty");

		Location sameLocation = locationRepository.findOneByAddressAndLatitudeAndLongitude(dto.getAddress(), dto.getLatitude(), dto.getLongitude());

		if(sameLocation != null)
			throw new InvalidDataException("Location on the same address already exists");

		Location location = new Location(dto.getAddress(), dto.getName(), dto.getLatitude(), dto.getLongitude(), dto.getCategories(), new ArrayList<>(), true);

		Location saved = locationRepository.save(location);

		return MainLocationInfoMapper.toDTO(saved);
	}

	public MainLocationInfoDTO update(Integer id, MainLocationInfoDTO locationDTO) throws InvalidDataException {
		if(Stream.of(locationDTO.getAddress(), locationDTO.getName(), locationDTO.getLatitude(), locationDTO.getLongitude()).anyMatch(Objects::isNull))
			throw new InvalidDataException("Some data is missing");

		Location location = locationRepository.findById(id).orElse(null);
		if(location == null)
			throw new InvalidDataException("Location not found");
		else if(locationDTO.getName().isEmpty())
			throw new InvalidDataException("Name can't be empty");
		else if(locationDTO.getAddress().isEmpty())
			throw new InvalidDataException("Address can't be empty");

		Location sameLocation = locationRepository.findOneByAddressAndLatitudeAndLongitude(locationDTO.getAddress(), locationDTO.getLatitude(), locationDTO.getLongitude());

		if(sameLocation != null && sameLocation.getId() != id)
			throw new InvalidDataException("Location on the same address already exists");

		location.setAddress(locationDTO.getAddress());
		location.setName(locationDTO.getName());
		location.setLatitude(locationDTO.getLatitude());
		location.setLongitude(locationDTO.getLongitude());

		location = locationRepository.save(location);

		return MainLocationInfoMapper.toDTO(location);
	}

	public Boolean delete(Integer id) throws InvalidDataException {
		Location locationForDelete = locationRepository.findById(id).orElse(null);

		if(locationForDelete == null)
			throw new InvalidDataException("Location not found");
		else if(locationForDelete.getManifestations().stream().anyMatch(m -> m.getState() != ManifestationState.PAST && m.getState() != ManifestationState.CANCELED))
			throw new InvalidDataException("Some manifestations will be held on this location");

		locationForDelete.setActive(false);
		for(Manifestation manifestation : locationForDelete.getManifestations())
			manifestation.setActive(false);

		locationRepository.save(locationForDelete);

		return true;
	}

	public List<MainManifestationInfoDTO> getManifestations(Integer id, Pageable pageable) throws InvalidDataException {
		Location location = locationRepository.findById(id).orElse(null);

		if(location == null)
			throw new InvalidDataException("Location not found");

		if(pageable.getPageNumber() * pageable.getPageSize() + 1 > location.getManifestations().size())
			return new ArrayList<>();

		List<MainManifestationInfoDTO> result = new ArrayList<>();

		List<Manifestation> manifestations = location.getManifestations().stream().filter(m -> m.getState().equals(ManifestationState.OPEN) || m.getState().equals(ManifestationState.SOLD_OUT)).collect(Collectors.toList());

		for(Manifestation manifestation : manifestations.subList(pageable.getPageNumber() * pageable.getPageSize(), Math.min((pageable.getPageNumber() * pageable.getPageSize() + pageable.getPageSize()), manifestations.size())))
			result.add(MainManifestationInfoMapper.toDTO(manifestation));

		return result;
	}

	public Integer getManifestationsCount(Integer id) throws InvalidDataException {
		Location location = locationRepository.findById(id).orElse(null);

		if(location == null)
			throw new InvalidDataException("Location not found");

		return location.getManifestations().stream().filter(m -> m.getState().equals(ManifestationState.OPEN) || m.getState().equals(ManifestationState.SOLD_OUT)).collect(Collectors.toList()).size();
	}

	public Set<SeatCategory> getCategories(Integer id) throws InvalidDataException {
		Location location = locationRepository.findById(id).orElse(null);

		if(location == null)
			throw new InvalidDataException("Location not found");
		
		return location.getCategories().stream().filter(element -> !element.getStage()).collect(Collectors.toSet());
	}
	
	public Map<String, Long> getDaily(Integer id, String start, String end) throws InvalidDataException, ParseException {
		Optional<Location> locationSearch = locationRepository.findById(id);
		
		if (!locationSearch.isPresent()) {
			throw new InvalidDataException("Location not found");
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Timestamp startDate = new Timestamp(sdf.parse(start).getTime());
		Timestamp endDate = new Timestamp(sdf.parse(end).getTime());
		
		if(endDate.before(startDate) || endDate.equals(startDate)) {
			throw new InvalidDataException("Invalid date!");
		}
		
		ArrayList<Ticket> boughtTickets = (ArrayList<Ticket>) ticketRepository.findByLocation(id, startDate, endDate);

		Map<String, Long> reportData = boughtTickets.stream().collect(Collectors
				.groupingBy(t -> sdf.format(t.getBuyingDate()), Collectors.counting()));
		Map<String, Long> reportDataHash = new HashMap<String, Long>(reportData);

		return reportDataHash;
	}
	
	public Map<String, Long> getWeekly(Integer id, String start, String end) throws InvalidDataException, ParseException {
		Optional<Location> locationSearch = locationRepository.findById(id);
		
		if (!locationSearch.isPresent()) {
			throw new InvalidDataException("Location not found");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Timestamp startDate = new Timestamp(sdf.parse(start).getTime());
		Timestamp endDate = new Timestamp(sdf.parse(end).getTime());
		
		if(endDate.before(startDate) || endDate.equals(startDate)) {
			throw new InvalidDataException("Invalid date!");
		}
		
		ArrayList<Ticket> boughtTickets = (ArrayList<Ticket>) ticketRepository.findByLocation(id, startDate, endDate);
		SimpleDateFormat sdf2 = new SimpleDateFormat("MMM yyyy 'week: 'W", Locale.ENGLISH);
		
		Map<String, Long> reportData = boughtTickets.stream().collect(Collectors
				.groupingBy(t -> sdf2.format(t.getBuyingDate()), Collectors.counting()));
		Map<String, Long> reportDataHash = new HashMap<String, Long>(reportData);
		return reportDataHash;
	}
	
	public Map<String, Long> getMonthly(Integer id, String start, String end) throws InvalidDataException, ParseException {
		Optional<Location> locationSearch = locationRepository.findById(id);
		
		if (!locationSearch.isPresent()) {
			throw new InvalidDataException("Location not found");
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Timestamp startDate = new Timestamp(sdf.parse(start).getTime());
		Timestamp endDate = new Timestamp(sdf.parse(end).getTime());
		
		if(endDate.before(startDate) || endDate.equals(startDate)) {
			throw new InvalidDataException("Invalid date!");
		}
		
		ArrayList<Ticket> boughtTickets = (ArrayList<Ticket>) ticketRepository.findByLocation(id, startDate, endDate);
		SimpleDateFormat sdf2 = new SimpleDateFormat("MMM yyyy", Locale.ENGLISH);
		
		Map<String, Long> reportData = boughtTickets.stream().collect(Collectors
				.groupingBy(t -> sdf2.format(t.getBuyingDate()), Collectors.counting()));
		Map<String, Long> reportDataHash = new HashMap<String, Long>(reportData);

		return reportDataHash;
	}
}
