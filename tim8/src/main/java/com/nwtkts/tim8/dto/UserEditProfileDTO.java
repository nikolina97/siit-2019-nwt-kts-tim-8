package com.nwtkts.tim8.dto;

public class UserEditProfileDTO {
	private String firstName;
	private String lastName;
	
	public UserEditProfileDTO() {
		super();
	}

	public UserEditProfileDTO(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
