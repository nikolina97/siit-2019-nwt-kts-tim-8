package com.nwtkts.tim8.dto;

import java.util.List;

public class SeatsBySupportedSeatCategoryDTO {
	
	protected Integer id;
	protected String name;
	protected Double ticketPrice;
	protected Integer numberOfSeats;
	protected Integer rows;
	protected Integer columns;
	protected Double xCoordinate;
	protected Double yCoordinate;
	protected Double width;
	protected Double height;
	protected Double distance;
	protected Boolean stage;
	protected Boolean isEnumerable;
	protected List<SeatDTO> seats;

	public SeatsBySupportedSeatCategoryDTO() {
	}

	public SeatsBySupportedSeatCategoryDTO(Integer id, String name, Double ticketPrice, Integer numberOfSeats,
			Integer rows, Integer columns, Double xCoordinate, Double yCoordinate, Double width, Double height,
			Double distance, Boolean stage, Boolean isEnumerable, List<SeatDTO> seats) {
		super();
		this.id = id;
		this.name = name;
		this.ticketPrice = ticketPrice;
		this.numberOfSeats = numberOfSeats;
		this.rows = rows;
		this.columns = columns;
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.width = width;
		this.height = height;
		this.distance = distance;
		this.stage = stage;
		this.isEnumerable = isEnumerable;
		this.seats = seats;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public Integer getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(Integer numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public List<SeatDTO> getSeats() {
		return seats;
	}

	public void setSeats(List<SeatDTO> seats) {
		this.seats = seats;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getColumns() {
		return columns;
	}

	public void setColumns(Integer columns) {
		this.columns = columns;
	}

	public Double getxCoordinate() {
		return xCoordinate;
	}

	public void setxCoordinate(Double xCoordinate) {
		this.xCoordinate = xCoordinate;
	}

	public Double getyCoordinate() {
		return yCoordinate;
	}

	public void setyCoordinate(Double yCoordinate) {
		this.yCoordinate = yCoordinate;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Boolean getStage() {
		return stage;
	}

	public void setStage(Boolean stage) {
		this.stage = stage;
	}

	public Boolean getIsEnumerable() {
		return isEnumerable;
	}

	public void setIsEnumerable(Boolean isEnumerable) {
		this.isEnumerable = isEnumerable;
	}
	
}
