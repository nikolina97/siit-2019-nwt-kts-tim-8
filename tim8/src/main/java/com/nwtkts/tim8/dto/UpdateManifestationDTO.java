package com.nwtkts.tim8.dto;

public class UpdateManifestationDTO {

	private String name, description;

	public UpdateManifestationDTO(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public UpdateManifestationDTO() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
