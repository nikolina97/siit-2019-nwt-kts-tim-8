package com.nwtkts.tim8.dto;

import com.nwtkts.tim8.model.SeatCategory;

import java.util.Set;

public class LocationDTO {

	private String name, address;
	private Double latitude, longitude;
	private Set<SeatCategory> categories;

	public LocationDTO() {
		super();
	}

	public LocationDTO(String name, String address, Double latitude, Double longitude, Set<SeatCategory> categories) {
		this.name = name;
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
		this.categories = categories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Set<SeatCategory> getCategories() {
		return categories;
	}

	public void setCategories(Set<SeatCategory> categories) {
		this.categories = categories;
	}

}
