package com.nwtkts.tim8.dto;

import java.sql.Timestamp;

public class MainManifestationInfoDTO {

	private Integer id;
	private String name, description, image;
	private Timestamp startDate, endDate;

	public MainManifestationInfoDTO(Integer id, String name, String description, String image, Timestamp startDate, Timestamp endDate) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.image = image;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

}
