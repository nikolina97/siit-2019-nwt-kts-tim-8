package com.nwtkts.tim8.dto;

public class CompletePaymentParamsDTO {

	private String paymentId, payerId;

	public CompletePaymentParamsDTO(String paymentId, String payerId) {
		this.paymentId = paymentId;
		this.payerId = payerId;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPayerId() {
		return payerId;
	}

	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}

}
