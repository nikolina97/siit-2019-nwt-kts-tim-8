package com.nwtkts.tim8.dto;

import com.nwtkts.tim8.model.ManifestationCategory;
import com.nwtkts.tim8.model.ManifestationState;

import java.sql.Timestamp;
import java.util.List;

public class ManifestationDTO {

	private Integer daysBeforeExpires, locationId;
	private String name, description, image, imageName;
	private Timestamp startDate, endDate;
	private ManifestationState state;
	private ManifestationCategory category;
	private List<SupportedSeatCategoryDTO> sscDTOs;

	public ManifestationDTO() {
	}

	public ManifestationDTO(Integer daysBeforeExpires, Integer locationId, String name, String description, String image, String imageName, Timestamp startDate, Timestamp endDate, ManifestationState state, ManifestationCategory category, List<SupportedSeatCategoryDTO> sscDTOs) {
		this.daysBeforeExpires = daysBeforeExpires;
		this.locationId = locationId;
		this.name = name;
		this.imageName = imageName;
		this.description = description;
		this.image = image;
		this.startDate = startDate;
		this.endDate = endDate;
		this.state = state;
		this.category = category;
		this.sscDTOs = sscDTOs;
	}

	public Integer getDaysBeforeExpires() {
		return daysBeforeExpires;
	}

	public void setDaysBeforeExpires(Integer daysBeforeExpires) {
		this.daysBeforeExpires = daysBeforeExpires;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public ManifestationState getState() {
		return state;
	}

	public void setState(ManifestationState state) {
		this.state = state;
	}

	public ManifestationCategory getCategory() {
		return category;
	}

	public void setCategory(ManifestationCategory category) {
		this.category = category;
	}

	public List<SupportedSeatCategoryDTO> getSscDTOs() {
		return sscDTOs;
	}

	public void setSscDTOs(List<SupportedSeatCategoryDTO> sscDTOs) {
		this.sscDTOs = sscDTOs;
	}

}
