package com.nwtkts.tim8.dto;

import java.util.List;

public class SeatDateDTO {
	
	private List<Integer> seatIds;
	
	private String date;
	
	public SeatDateDTO() {
		super();
	}

	public SeatDateDTO(List<Integer> seatIds, String date) {
		super();
		this.seatIds = seatIds;
		this.date = date;
	}

	public List<Integer> getSeatIds() {
		return seatIds;
	}

	public void setSeatIds(List<Integer> seatIds) {
		this.seatIds = seatIds;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
	
	
}
