package com.nwtkts.tim8.dto;

import java.sql.Timestamp;

public class TicketDTO {
	
	protected Integer ticketId;
		
	protected Integer row;
	
	protected Integer column;
	
	protected Timestamp startDate;
	
	protected Timestamp endDate;
	
	protected Double ticketPrice;
	
	protected String manifestationName;
	
	protected Integer manifestationId;
	
	protected String seatCategoryName;

	public TicketDTO() {
		super();
	}

	public TicketDTO(Integer ticketId, Integer row, Integer column, Timestamp startDate, Timestamp endDate,
			Double ticketPrice, String manifestationName, Integer manifestationId, String seatCategoryName) {
		super();
		this.ticketId = ticketId;
		this.row = row;
		this.column = column;
		this.startDate = startDate;
		this.endDate = endDate;
		this.ticketPrice = ticketPrice;
		this.manifestationName = manifestationName;
		this.manifestationId = manifestationId;
		this.seatCategoryName = seatCategoryName;
	}


	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getColumn() {
		return column;
	}

	public void setColumn(Integer column) {
		this.column = column;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}

	public String getManifestationName() {
		return manifestationName;
	}

	public void setManifestationName(String manifestationName) {
		this.manifestationName = manifestationName;
	}

	public Integer getManifestationId() {
		return manifestationId;
	}

	public void setManifestationId(Integer manifestationId) {
		this.manifestationId = manifestationId;
	}

	public String getSeatCategoryName() {
		return seatCategoryName;
	}

	public void setSeatCategoryName(String seatCategoryName) {
		this.seatCategoryName = seatCategoryName;
	}

}
