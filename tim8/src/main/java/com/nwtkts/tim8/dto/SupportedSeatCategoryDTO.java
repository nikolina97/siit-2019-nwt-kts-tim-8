package com.nwtkts.tim8.dto;

public class SupportedSeatCategoryDTO {

	private Integer seatCategoryId, numberOfSeats;
	private Double ticketPrice;

	public SupportedSeatCategoryDTO() {
		super();
	}

	public SupportedSeatCategoryDTO(Integer seatCategoryId, Integer numberOfSeats, Double ticketPrice) {
		this.seatCategoryId = seatCategoryId;
		this.numberOfSeats = numberOfSeats;
		this.ticketPrice = ticketPrice;
	}

	public Integer getSeatCategoryId() {
		return seatCategoryId;
	}

	public void setSeatCategoryId(Integer seatCategoryId) {
		this.seatCategoryId = seatCategoryId;
	}

	public Integer getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(Integer numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public Double getTicketPrice() {
		return ticketPrice;
	}

	public void setTicketPrice(Double ticketPrice) {
		this.ticketPrice = ticketPrice;
	}
}
