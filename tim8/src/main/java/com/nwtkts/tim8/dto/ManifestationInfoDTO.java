package com.nwtkts.tim8.dto;

import java.sql.Timestamp;
import java.util.List;

import com.nwtkts.tim8.model.ManifestationCategory;
import com.nwtkts.tim8.model.ManifestationState;

public class ManifestationInfoDTO {

	private Integer manifestationId;

	private String name;

	private String description;

	private String image;

	private List<Timestamp> dates;

	private MainLocationInfoDTO location;

	private ManifestationState manifestationState;

	private ManifestationCategory manifestationCategory;

	private Integer daysBeforeExpires;

	public ManifestationInfoDTO() {
		super();
	}

	public ManifestationInfoDTO(Integer manifestationId, String name, String description, String image, List<Timestamp> dates,
								MainLocationInfoDTO location, ManifestationState manifestationState, ManifestationCategory manifestationCategory, Integer daysBeforeExpires) {
		super();
		this.manifestationId = manifestationId;
		this.name = name;
		this.description = description;
		this.image = image;
		this.dates = dates;
		this.location = location;
		this.manifestationState = manifestationState;
		this.manifestationCategory = manifestationCategory;
		this.daysBeforeExpires = daysBeforeExpires;
	}

	public Integer getManifestationId() {
		return manifestationId;
	}

	public void setManifestationId(Integer manifestationId) {
		this.manifestationId = manifestationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<Timestamp> getDates() {
		return dates;
	}

	public void setDates(List<Timestamp> dates) {
		this.dates = dates;
	}

	public MainLocationInfoDTO getLocation() {
		return location;
	}

	public void setLocation(MainLocationInfoDTO location) {
		this.location = location;
	}

	public ManifestationState getManifestationState() {
		return manifestationState;
	}

	public void setManifestationState(ManifestationState manifestationState) {
		this.manifestationState = manifestationState;
	}

	public ManifestationCategory getManifestationCategory() {
		return manifestationCategory;
	}

	public void setManifestationCategory(ManifestationCategory manifestationCategory) {
		this.manifestationCategory = manifestationCategory;
	}

	public Integer getDaysBeforeExpires() {
		return daysBeforeExpires;
	}

	public void setDaysBeforeExpires(Integer daysBeforeExpires) {
		this.daysBeforeExpires = daysBeforeExpires;
	}
}
