package com.nwtkts.tim8.dto;

public class MainLocationInfoDTO {

	private Integer locationId;

	private String name;

	private String address;

	private Double latitude;

	private Double longitude;

	private Integer manifestationCount;

	public MainLocationInfoDTO() {
		super();
	}

	public MainLocationInfoDTO(Integer locationId, String name, String address, Double latitude, Double longitude, Integer manifestationCount) {
		this.locationId = locationId;
		this.name = name;
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
		this.manifestationCount = manifestationCount;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Integer getManifestationCount() {
		return manifestationCount;
	}

	public void setManifestationCount(Integer manifestationCount) {
		this.manifestationCount = manifestationCount;
	}
}
