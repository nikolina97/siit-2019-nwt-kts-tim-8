package com.nwtkts.tim8.dto;

public class SeatDTO {
	
	protected Integer seatId;
	
	protected Integer row;
	
	protected Integer column;
	
	protected boolean isReserved;

	public SeatDTO(Integer id, Integer row, Integer column, boolean isReserved) {
		super();
		this.seatId = id;
		this.row = row;
		this.column = column;
		this.isReserved = isReserved;
	}

	public SeatDTO() {
		super();
	}

	
	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getColumn() {
		return column;
	}

	public void setColumn(Integer column) {
		this.column = column;
	}

	public boolean isReserved() {
		return isReserved;
	}

	public void setReserved(boolean isReserved) {
		this.isReserved = isReserved;
	}
	
	
}


