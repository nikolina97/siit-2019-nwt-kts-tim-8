package com.nwtkts.tim8.dto;

import java.util.List;

public class SearchManifestationDTO {
	
	private String name;
	private List<String> categories;
	private List<String> locations;
	
	public SearchManifestationDTO() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public List<String> getLocations() {
		return locations;
	}

	public void setLocations(List<String> locations) {
		this.locations = locations;
	}

}
