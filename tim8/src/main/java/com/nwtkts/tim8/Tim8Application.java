package com.nwtkts.tim8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Tim8Application {

	public static void main(String[] args) {
		SpringApplication.run(Tim8Application.class, args);
	}

}
