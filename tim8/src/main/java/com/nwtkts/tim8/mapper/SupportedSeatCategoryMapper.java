package com.nwtkts.tim8.mapper;

import com.nwtkts.tim8.dto.SupportedSeatCategoryDTO;
import com.nwtkts.tim8.model.SupportedSeatCategory;

public class SupportedSeatCategoryMapper {

	public static SupportedSeatCategoryDTO toDTO(SupportedSeatCategory ssc) {
		SupportedSeatCategoryDTO sscDTO = new SupportedSeatCategoryDTO(ssc.getSeatCategory().getId(), ssc.getNumberOfSeats(), ssc.getTicketPrice());
		
		return sscDTO;
	}
}
