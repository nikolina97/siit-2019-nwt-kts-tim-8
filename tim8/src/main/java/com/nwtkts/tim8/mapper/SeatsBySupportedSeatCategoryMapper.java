package com.nwtkts.tim8.mapper;

import com.nwtkts.tim8.dto.SeatDTO;
import com.nwtkts.tim8.dto.SeatsBySupportedSeatCategoryDTO;
import com.nwtkts.tim8.dto.SupportedSeatCategoryDTO;
import com.nwtkts.tim8.model.SeatCategory;
import com.nwtkts.tim8.model.SupportedSeatCategory;

import java.util.ArrayList;
import java.util.List;

public class SeatsBySupportedSeatCategoryMapper {
	public static SeatsBySupportedSeatCategoryDTO toDTO(SupportedSeatCategory ssc) {
		List<SeatDTO> seats = new ArrayList<SeatDTO>();
		SeatCategory seatCategory = ssc.getSeatCategory();
		SeatsBySupportedSeatCategoryDTO sscDTO = new SeatsBySupportedSeatCategoryDTO(ssc.getId(), seatCategory.getName(), ssc.getTicketPrice(),
				ssc.getNumberOfSeats(), seatCategory.getRows(), seatCategory.getColumns(), seatCategory.getxCoordinate(), seatCategory.getyCoordinate(), 
				seatCategory.getWidth(), seatCategory.getHeight(), seatCategory.getDistance(), seatCategory.getStage(), seatCategory.getEnumerable(),seats);
		return sscDTO;
	}
}
