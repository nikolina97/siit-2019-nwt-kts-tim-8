package com.nwtkts.tim8.mapper;

import com.nwtkts.tim8.dto.UserDTO;
import com.nwtkts.tim8.model.User;

public class UserMapper {
	
	public static User toUser(UserDTO userDTO) {
		User user = new User(userDTO.getUsername(), userDTO.getPassword(), userDTO.getFirstName(), userDTO.getLastName(), 
				userDTO.getEmail(), null);
		return user;
	}
	
	public static UserDTO toDTO(User user) {
		UserDTO userDTO = new UserDTO(user.getUsername(), user.getPassword(), user.getFirstName(), user.getLastName(), 
				user.getEmail(), user.getImage());
		return userDTO;
	}
}
