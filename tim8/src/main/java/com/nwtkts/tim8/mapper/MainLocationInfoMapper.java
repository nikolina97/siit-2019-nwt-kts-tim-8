package com.nwtkts.tim8.mapper;

import com.nwtkts.tim8.dto.MainLocationInfoDTO;
import com.nwtkts.tim8.model.Location;
import com.nwtkts.tim8.model.ManifestationState;

import java.util.stream.Collectors;

public class MainLocationInfoMapper {

	public static MainLocationInfoDTO toDTO(Location location) {
		return new MainLocationInfoDTO(location.getId(), location.getName(), location.getAddress(), location.getLatitude(), location.getLongitude(), location.getManifestations().stream().filter(m -> m.getState().equals(ManifestationState.OPEN) || m.getState().equals(ManifestationState.SOLD_OUT)).collect(Collectors.toList()).size());
	}

}
