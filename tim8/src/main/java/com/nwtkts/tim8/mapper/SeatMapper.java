package com.nwtkts.tim8.mapper;

import com.nwtkts.tim8.dto.SeatDTO;
import com.nwtkts.tim8.model.Seat;

public class SeatMapper {
	
	public static SeatDTO toDTO(Seat seat, boolean isReserved) {
		SeatDTO seatDTO = new SeatDTO(seat.getId(), seat.getRow(), seat.getColumn(), isReserved);
		return seatDTO;
	}

}
