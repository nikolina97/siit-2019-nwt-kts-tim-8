package com.nwtkts.tim8.mapper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.nwtkts.tim8.dto.LocationDTO;
import com.nwtkts.tim8.dto.MainLocationInfoDTO;
import com.nwtkts.tim8.dto.ManifestationInfoDTO;
import com.nwtkts.tim8.model.Manifestation;

public class ManifestationInfoMapper {
	
	public static ManifestationInfoDTO toDTO(Manifestation manifestation) {
		MainLocationInfoDTO locationDTO = MainLocationInfoMapper.toDTO(manifestation.getLocation());
		List<Timestamp> dates = getListOfDates(manifestation.getStartDate(), manifestation.getEndDate());
		return new ManifestationInfoDTO(manifestation.getId(), manifestation.getName(), manifestation.getDescription(), manifestation.getImage(), dates,
				locationDTO, manifestation.getState(), manifestation.getCategory(), manifestation.getDaysBeforeExpires());
	}
	
	public static List<Timestamp> getListOfDates(Timestamp startDate, Timestamp endDate) {
		ArrayList<Timestamp> dates = new ArrayList<Timestamp>();
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(startDate);
		cal2.setTime(endDate);
		boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
		                  cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR); 
		if (sameDay) {
			dates.add(new Timestamp(startDate.getTime()));
			return dates;
		}
		else {			
			while(!sameDay) {
				Date result = cal1.getTime();
				dates.add(new Timestamp(result.getTime()));
				cal1.add(Calendar.DATE, 1);
				sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
		                  cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR); 
			}
			dates.add(endDate);
		}
		return dates;
	}
}
