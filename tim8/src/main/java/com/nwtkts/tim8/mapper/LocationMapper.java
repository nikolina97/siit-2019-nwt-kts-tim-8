package com.nwtkts.tim8.mapper;

import com.nwtkts.tim8.dto.LocationDTO;
import com.nwtkts.tim8.model.Location;

public class LocationMapper {
	
	public static LocationDTO toDTO(Location location) {
		return new LocationDTO(location.getName(), location.getAddress(), location.getLatitude(), location.getLongitude(), location.getCategories());
	}
}
