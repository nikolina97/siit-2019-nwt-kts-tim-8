package com.nwtkts.tim8.mapper;

import com.nwtkts.tim8.dto.MainManifestationInfoDTO;
import com.nwtkts.tim8.model.Manifestation;

public class MainManifestationInfoMapper {

	public static MainManifestationInfoDTO toDTO(Manifestation manifestation) {
		return new MainManifestationInfoDTO(manifestation.getId(), manifestation.getName(), manifestation.getDescription(), manifestation.getImage(), manifestation.getStartDate(), manifestation.getEndDate());
	}

}
