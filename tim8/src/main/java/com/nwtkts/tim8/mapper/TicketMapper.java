package com.nwtkts.tim8.mapper;

import java.sql.Timestamp;

import com.nwtkts.tim8.dto.TicketDTO;
import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.Ticket;

public class TicketMapper {
	
	public static TicketDTO toDTO(Ticket ticket) {
		Timestamp startDate = ticket.getDate();
		Timestamp endDate = startDate;

		Manifestation manifestation = ticket.getManifestation();
		if (startDate == null) {
			manifestation = ticket.getManifestation();
			startDate = manifestation.getStartDate();
			endDate = manifestation.getEndDate();
			
		}
		TicketDTO ticketDTO = new TicketDTO(ticket.getId(), ticket.getSeat().getRow(), ticket.getSeat().getColumn(), 
				startDate, endDate, ticket.getSeat().getSupportedSeatCategory().getTicketPrice(), manifestation.getName(),
				manifestation.getId(), ticket.getSeat().getSupportedSeatCategory().getSeatCategory().getName());
		return ticketDTO;

	}

}
