package com.nwtkts.tim8.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
private WebDriver webDriver;
	
	@FindBy(css = "ul > li:nth-child(2) > button.btn.btn-round")
	private WebElement logInButton;
	
	@FindBy(css = "ul > li:nth-child(3) > button.btn.btn-round")
	private WebElement signUpButton;
	
	@FindBy(css = "ul.navbar-nav > li:nth-child(2) > a > i")
	private WebElement prifileIcon;

	@FindBy(css = "div.sidebar-wrapper > ul.nav > li.nav-item > a[href='/reservations']")
	private WebElement reservationsLink;
	
	@FindBy(css = "button[title=\"Reports\"]")
	private List<WebElement> manifReportsButtons;
	
	@FindBy(css = "div.sidebar-wrapper > ul > li:nth-child(2)")
	private WebElement locations;
	
	public HomePage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public WebElement getLogInButton() {
		return logInButton;
	}
	
	public WebElement getSignUpButton() {
		return signUpButton;
	}
	
	public WebElement getPrifileIcon() {
		return prifileIcon;
	}
		
	public WebElement getReservationsLink() {
		return reservationsLink;
	}
	
	public List<WebElement> getmanifReportsButton() {
		return manifReportsButtons;
	}
	
	
	public WebElement getLocations() {
		return locations;
	}

	public void ensureLoginButtonIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(this.logInButton));	
	}
	
	public void ensureSignUpButtonIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(this.signUpButton));	
	}
	
	public void ensureProfileIconIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(this.prifileIcon));	
	}
	
	public void ensureReportButtonsAreDisplayed() {
		(new WebDriverWait(webDriver, 10)).until(ExpectedConditions.visibilityOfAllElements(this.manifReportsButtons));
	}
	
	public void ensureLocationsIsDisplayed() {
		(new WebDriverWait(webDriver, 10)).until(ExpectedConditions.visibilityOf(this.locations));
	}
}
