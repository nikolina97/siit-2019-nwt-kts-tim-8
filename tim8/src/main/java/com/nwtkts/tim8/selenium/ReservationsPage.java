package com.nwtkts.tim8.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReservationsPage {
	
	private WebDriver webDriver;
	
	@FindBy(css = "div.row > div.col-lg-3.col-md-6:first-child > div.card > div.card-body > a.btn.btn-danger")
	private WebElement firstReservationCancelLink;
	
	@FindBy(css = "div.row > div.col-lg-3.col-md-6:nth-child(2) > div.card > div.card-body > a.btn.btn-danger")
	private WebElement secondReservationCancelLink;
	
	@FindBy(xpath = "//div[@class='card' and ./div/h4/a = ' Vojvodina-Zvezda ']/div/a[@class='btn btn-success']")
	private List<WebElement> confirmPaymentTekmaButtons;

	public ReservationsPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public WebElement getFirstReservationCancelLink() {
		return this.firstReservationCancelLink;
	}
	
	public WebElement getSecondReservationCancelLink() {
		return this.secondReservationCancelLink;
	}
	
	public List<WebElement> getConfirmPaymentTekmaButtons() {
		return this.confirmPaymentTekmaButtons;
	}
	
	public void ensureReservationsAreLoaded() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div.row > div.col-lg-3.col-md-6")));
	}
	
	public void ensureAlertIsPresent() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.alertIsPresent());
	}

}
