package com.nwtkts.tim8.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewLocationPage {

	private WebDriver driver;

	@FindBy(css = "input[formControlName=\"name\"]")
	private WebElement nameInput;

	@FindBy(css = "input[formControlName=\"address\"]")
	private WebElement addressInput;

	@FindBy(css = "input[formControlName=\"latitude\"]")
	private WebElement latitudeInput;

	@FindBy(css = "input[formControlName=\"longitude\"]")
	private WebElement longitudeInput;

	@FindBy(css = "input[type=\"file\"]")
	private WebElement layoutInput;

	@FindBy(className = "btn-success")
	private WebElement addButton;

	@FindBy(className = "alert")
	private WebElement errorMessage;

	public NewLocationPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getAddButton() {
		return addButton;
	}

	public WebElement getErrorMessage() {
		return errorMessage;
	}

	public void setNameInput(String value) {
		this.nameInput.clear();
		this.nameInput.sendKeys(value);
	}

	public void setAddressInput(String value) {
		this.addressInput.clear();
		this.addressInput.sendKeys(value);
	}

	public void setLatitudeInput(String value) {
		this.latitudeInput.clear();
		this.latitudeInput.sendKeys(value);
	}

	public void setLongitudeInput(String value) {
		this.longitudeInput.clear();
		this.longitudeInput.sendKeys(value);
	}

	public void setLayoutInput(String value) {
		this.layoutInput.sendKeys(value);
	}

	public void ensureEntireFormIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("input[formControlName=\"name\"]")));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(addressInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(latitudeInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(longitudeInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input[type=\"file\"]")));
	}

	public void ensureErrorMessageIsDisplayer() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(errorMessage));
	}

}
