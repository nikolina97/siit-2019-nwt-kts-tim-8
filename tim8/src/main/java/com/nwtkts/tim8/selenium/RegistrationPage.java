package com.nwtkts.tim8.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage {

	private WebDriver webDriver;
	
	@FindBy(css = "#username")
	private WebElement username;
	
	@FindBy(css = "#password")
	private WebElement password;
	
	@FindBy(css = "#repeatedPassword")
	private WebElement repeatedPassword;
	
	@FindBy(css = "#firstname")
	private WebElement firstName;
	
	@FindBy(css = "#lastname")
	private WebElement lastName;
	
	@FindBy(css = "#email")
	private WebElement email;
	
	@FindBy(css = "div.row > div.col-lg-9 > button.btn.btn-round")
	private WebElement registrationButton;
	
	@FindBy(css = "h3")
	private WebElement emailVerification;
	
	@FindBy(css ="form > fieldset > div:first-of-type > div > small")
	private WebElement usernameError;
	
	@FindBy(css ="form > fieldset > div:nth-of-type(2) > div > small")
	private WebElement passwordError;

	@FindBy(css ="form > fieldset > div:nth-of-type(3) > div > small")
	private WebElement repeatedPasswordError;
	
	@FindBy(css ="form > fieldset > div:nth-of-type(4) > div > small")
	private WebElement firstNameError;
	
	@FindBy(css ="form > fieldset > div:nth-of-type(5) > div > small")
	private WebElement lastNameError;
	
	@FindBy(css ="form > fieldset > div:nth-of-type(6) > div > small")
	private WebElement emailError;
	
	@FindBy(css = "#toast-container > div:last-of-type")
	private WebElement toastr;
	
	public RegistrationPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	
	public WebElement getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username.clear();
		this.username.sendKeys(username);
	}


	public WebElement getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}


	public WebElement getRepeatedPassword() {
		return repeatedPassword;
	}


	public void setRepeatedPassword(String repeatedPassword) {
		this.repeatedPassword.clear();
		this.repeatedPassword.sendKeys(repeatedPassword);
	}


	public WebElement getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName.clear();
		this.firstName.sendKeys(firstName);
	}


	public WebElement getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName.clear();
		this.lastName.sendKeys(lastName);
	}


	public WebElement getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email.clear();
		this.email.sendKeys(email);
	}


	public WebElement getRegistrationButton() {
		return registrationButton;
	}


	public void setRegistrationButton(WebElement registrationButton) {
		this.registrationButton = registrationButton;
	}


	public WebElement getEmailVerification() {
		return emailVerification;
	}


	public void setEmailVerification(WebElement emailVerification) {
		this.emailVerification = emailVerification;
	}


	public WebElement getUsernameError() {
		return usernameError;
	}


	public WebElement getPasswordError() {
		return passwordError;
	}


	public WebElement getRepeatedPasswordError() {
		return repeatedPasswordError;
	}


	public WebElement getFirstNameError() {
		return firstNameError;
	}


	public WebElement getLastNameError() {
		return lastNameError;
	}


	public WebElement getEmailError() {
		return emailError;
	}

	public WebElement getToastr() {
		return toastr;
	}


	public void setToastr(WebElement toastr) {
		this.toastr = toastr;
	}


	public void ensureToastrIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#toast-container > div:last-of-type")));	
	}
	
	public void ensureVerificationMessageIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("h3")));	
	}
}
