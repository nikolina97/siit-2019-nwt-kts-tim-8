package com.nwtkts.tim8.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PayPalPage {
	
	private WebDriver webDriver;
	
	@FindBy(css = "div.fieldWrapper > input#email")
	private WebElement emailInput;

	@FindBy(css = "div.actions > button#btnNext")
	private WebElement nextButton;
	
	@FindBy(css = "div.fieldWrapper > input#password")
	private WebElement passwordInput;
	
	@FindBy(css = "div.actions > button#btnLogin")
	private WebElement logInButton;
	
	@FindBy(css = "button#payment-submit-btn")
	private WebElement continueButton;
	
	@FindBy(css = "a[data-testid = 'cancel-link'")
	private WebElement cancelBuying;
	
	public PayPalPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public WebElement getEmailInput() {
		return emailInput;
	}

	public void setEmailInput(String text) {
		this.emailInput.clear();
		this.emailInput.sendKeys(text);
	}
	
	public WebElement getNextButton() {
		return nextButton;
	}
	
	public WebElement getPasswordInput() {
		return passwordInput;
	}

	public void setPasswordInput(String text) {
		this.passwordInput.clear();
		this.passwordInput.sendKeys(text);
	}
	
	public WebElement getLogInButton() {
		return logInButton;
	}
	
	public WebElement getCancelBuying() {
		return cancelBuying;
	}

	public WebElement getContinueButton() {
		return continueButton;
	}

	public void ensureEmailInputIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(emailInput));	
	}
	
	public void ensureNextButtonIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(nextButton));
	}
	
	public void ensurePasswordInputIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(passwordInput));	
	}
	
	public void ensureLoginButtonIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(logInButton));
	}
	
	public void ensureContitueButtonIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(continueButton));
	}

	public void ensureCancelLinkIsDiplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(cancelBuying));
	}
	
}
