package com.nwtkts.tim8.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LocationInfoPage {

	private WebDriver driver;

	@FindBy(css = "h3.card-title")
	private WebElement nameLabel;

	@FindBy(css = "div.card-body > p")
	private WebElement addressLabel;

	@FindBy(css = "div.card div.card")
	private List<WebElement> events;

	public LocationInfoPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getNameLabel() {
		return nameLabel;
	}

	public WebElement getAddressLabel() {
		return addressLabel;
	}

	public List<WebElement> getEvents() {
		return events;
	}

	public void ensureLocationIsLoaded() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(nameLabel));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(addressLabel));
	}

}
