package com.nwtkts.tim8.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePage {

	private WebDriver webDriver;
	
	@FindBy(css = "#oldPassword")
	private WebElement password;
	
	@FindBy(css = "#newPassword")
	private WebElement newPassword;
	
	@FindBy(css = "#repeatedPassword")
	private WebElement repeatedPassword;
	
	@FindBy(css = "#firstName")
	private WebElement firstName;
	
	@FindBy(css = "#lastName")
	private WebElement lastName;
	
	@FindBy(css ="form > fieldset > div:first-of-type > div > small")
	private WebElement passwordError;
	
	@FindBy(css ="form > fieldset > div:nth-of-type(2) > div > small")
	private WebElement newPasswordError;

	@FindBy(css ="form > fieldset > div:nth-of-type(3) > div > small")
	private WebElement repeatedPasswordError;
	
	@FindBy(css ="form > fieldset > div:first-of-type > div > small")
	private WebElement firstNameError;
	
	@FindBy(css ="form > fieldset > div:nth-of-type(2) > div > small")
	private WebElement lastNameError;
	
	@FindBy(id = "image" )
	private WebElement imageUpload;
	
	@FindBy(css = "img.card-img-top.img-responsive")
	private WebElement image;
	
	@FindBy(name = "edit-profile")
	private WebElement editButton;
	
	@FindBy(name = "change-password")
	private WebElement changePasswordButton;
	
	@FindBy(name = "save")
	private WebElement saveButton;
	
	@FindBy(css = "#toast-container > div:last-of-type")
	private WebElement toastr;
	
	
	public ProfilePage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	
	public WebElement getPassword() {
		return password;
	}

	
	public void setPassword(String newPassword) {
		this.password.clear();
		this.password.sendKeys(newPassword);
	}

	public WebElement getNewPassword() {
		return newPassword;
	}


	public void setNewPassword(String newPassword) {
		this.newPassword.clear();
		this.newPassword.sendKeys(newPassword);
	}
	
	public WebElement getRepeatedPassword() {
		return repeatedPassword;
	}


	public void setRepeatedPassword(String repeatedPassword) {
		this.repeatedPassword.clear();
		this.repeatedPassword.sendKeys(repeatedPassword);
	}


	public WebElement getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName.clear();
		this.firstName.sendKeys(firstName);
	}


	public WebElement getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName.clear();
		this.lastName.sendKeys(lastName);
	}

	
	public WebElement getPasswordError() {
		return passwordError;
	}

	
	public WebElement getNewPasswordError() {
		return newPasswordError;
	}

	public WebElement getRepeatedPasswordError() {
		return repeatedPasswordError;
	}


	public WebElement getFirstNameError() {
		return firstNameError;
	}


	public WebElement getLastNameError() {
		return lastNameError;
	}

	
	public WebElement getToastr() {
		return toastr;
	}

	
	public WebElement getImageUpload() {
		return imageUpload;
	}
	
	public void setImageUpload(String filePath) {
		this.imageUpload.clear();
		this.imageUpload.sendKeys(filePath);
	}

	public WebElement getImage() {
		return image;
	}


	public void setImage(WebElement image) {
		this.image = image;
	}


	public WebElement getEditButton() {
		return editButton;
	}


	public WebElement getChangePasswordButton() {
		return changePasswordButton;
	}


	public WebElement getSaveButton() {
		return saveButton;
	}


	public void ensureToastrIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#toast-container > div:last-of-type")));	
	}
	
	public void ensureFirstNameIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.visibilityOf(firstName));	
	}
	
	public void ensurePasswordIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.visibilityOf(password));	
	}
	
	public void ensureImageChanged() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.attributeToBe(image, "src", "http://localhost:8080/marc.jpg"));	
	}
}
