package com.nwtkts.tim8.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LocationsPage {

	private WebDriver driver;

	@FindBy(css = "button[title=\"New\"]")
	private WebElement addButton;

	@FindBy(className = "card")
	private List<WebElement> locations;

	@FindBy(css = "button[title=\"Edit\"]")
	private List<WebElement> editButtons;

	@FindBy(css = "button[title=\"Delete\"]")
	private List<WebElement> deleteButtons;
	
	@FindBy(css = "button[title=\"Reports\"]")
	private List<WebElement> reportsButtons;

	public LocationsPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getAddButton() {
		return addButton;
	}

	public List<WebElement> getLocations() {
		return locations;
	}

	public List<WebElement> getEditButtons() {
		return editButtons;
	}

	public List<WebElement> getDeleteButtons() {
		return deleteButtons;
	}
	
	public List<WebElement> getReportsButtons() {
		return reportsButtons;
	}

	public void ensureAddButtonIsClickable() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(addButton));
	}

	public void ensureLocationsAreVisible() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElements(locations));
	}

	public void ensureEditButtonIsClickable() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElements(editButtons));
	}

	public void ensureDeleteButtonIsClickable() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElements(deleteButtons));
	}
	
	public void ensureReportButtonIsClickable() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElements(this.reportsButtons));
	}
}
