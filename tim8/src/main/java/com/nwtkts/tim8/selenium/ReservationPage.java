package com.nwtkts.tim8.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReservationPage {
		
	private WebDriver webDriver;
	
	@FindBy(css = "ul[role='tablist'] > li:last-child > a > span")
	private WebElement ticketsButton;
	
	@FindBy(css = "table > tbody > tr")
	private WebElement rows;
	
	@FindBy(css = "circle.false")
	private WebElement firstFreeSeat;

	@FindBy(css = "circle.true")
	private WebElement firstReservedSeat;
	
	@FindBy(css = "button#reservation")
	private WebElement reserveButton;
	
	@FindBy(css = "button#buying")
	private WebElement buyingButton;
	
	@FindBy(css = "#toast-container > div:last-of-type")
	private WebElement taoast;
	
	@FindBy(css = "div.container > div.row > div:last-child > form > h5:last-child > input")
	private WebElement festivalTicketRadio;
	
	@FindBy(css = "rect#false")
	private WebElement parterre;
	
	@FindBy(css = "div.mat-dialog-actions > button:last-child")
	private WebElement okButton;
	
	@FindBy(css = "button[aria-label = 'Add']")
	private WebElement plusButton;
	
	@FindBy(css = "input.mat-input-element")
	private WebElement matInput;
	
	public ReservationPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public WebElement getTicketsButton() {
		return ticketsButton;
	}

	public WebElement getTable() {
		return rows;
	}
	
	public WebElement getFirstFreeSeat() {
		return firstFreeSeat;
	}
	
	public WebElement getReserveButton() {
		return reserveButton;
	}
	
	public WebElement getTaoast() {
		return taoast;
	}

	public WebElement getParterre() {
		return parterre;
	}

	public WebElement getFestivalTicketRadio() {
		return festivalTicketRadio;
	}
	
	public WebElement getOkButton() {
		return okButton;
	}
	
	public WebElement getPlusButton() {
		return plusButton;
	}
	
	public WebElement getBuyingButton() {
		return buyingButton;
	}

	public void setMatInput(String number) {
		this.matInput.clear();
		this.matInput.sendKeys(number);
	}

	public void ensureTicketsButtonIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(ticketsButton));	
	}
	
	public void ensureTableIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(rows));	
	}
	
	public void ensureReservationButtonIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(reserveButton));	
	}
	
	public void ensureSeatsAreDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(firstFreeSeat));	
	}
	
	public void ensureToastIsDisplayed() {
		(new WebDriverWait(webDriver, 30))
		.until(ExpectedConditions.elementToBeClickable(taoast));	
	}
	
	public void ensureFestivalTicketRadioIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(festivalTicketRadio));			
	}
	
	public void ensureTextInsideMatInput(String text) {  
		(new WebDriverWait(webDriver, 10))  
		.until(ExpectedConditions.textToBePresentInElementValue(matInput, text));  
	}
	
	public void ensureParterreIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(parterre));
	}
	
	public void ensureMatInputIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(matInput));
	}
	
	public void ensureBuyingButtonIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(buyingButton));
	}

}
