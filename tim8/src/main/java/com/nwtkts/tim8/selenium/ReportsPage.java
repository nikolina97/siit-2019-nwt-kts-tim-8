package com.nwtkts.tim8.selenium;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReportsPage {
	
	private WebDriver webDriver;
	
	@FindBy(name = "dps")
	private WebElement startDateInput;
	
	@FindBy(name= "dpe")
	private WebElement endDateInput;
	
	@FindBy(css = "div.btn-light")
	private List<WebElement> dayButtons;
	
	@FindBy(css = "select[title=\"Select month\"]")
	private WebElement monthSelect;
	
	@FindBy(css = "select[title=\"Select year\"]")
	private WebElement yearSelect;

	@FindBy(name = "start-date-button")
	private WebElement startDateButton;
	
	@FindBy(name = "end-date-button")
	private WebElement endDateButton;
	
	@FindBy(name = "get-report")
	private WebElement reportButton;
	
	@FindBy(css = "button.btn.btn-primary")
	private List<WebElement> dailyMonthlyWeeklyButtons;

	@FindBy(css = "app-chart")
	private WebElement chart;
	
	public ReportsPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
	
	public WebElement getStartDateInput() {
		return startDateInput;
	}

	public WebElement getEndDateInput() {
		return endDateInput;
	}

	public List<WebElement> getDayButtons() {
		return dayButtons;
	}

	public WebElement getMonthSelect() {
		return monthSelect;
	}

	public WebElement getYearSelect() {
		return yearSelect;
	}

	public WebElement getStartDateButton() {
		return startDateButton;
	}

	public WebElement getEndDateButton() {
		return endDateButton;
	}

	public WebElement getReportButton() {
		return reportButton;
	}
	
	public List<WebElement> getDailyMonthlyWeeklyButtons() {
		return dailyMonthlyWeeklyButtons;
	}

	public WebElement getChart() {
		return chart;
	}

	public void ensureStartIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.visibilityOf(startDateInput));	
	}
	
	public void ensureEndIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.visibilityOf(endDateInput));	
	}
	
	public void ensureMonthIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(this.monthSelect));	
	}
	
	public void ensureYearIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(this.yearSelect));	
	}
	
	public void ensureStartDateButtonDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.visibilityOf(this.startDateButton));	
	}
	
	public void ensureEndDateButtonDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.visibilityOf(this.endDateButton));	
	}
	
	public void ensureDailyMonthlyWeekliAreDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.visibilityOfAllElements(this.dailyMonthlyWeeklyButtons));	
	}
	
	public void ensureChartIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.visibilityOf(this.chart));	
	}
	
	public void ensureStartDateInputNotEmpty() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.attributeToBeNotEmpty(this.startDateInput, "value"));
	}
	
	public void ensureEndDateInputNotEmpty() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.attributeToBeNotEmpty(this.endDateInput, "value"));
	}
	
	public void ensureChartLabelsNotEmpty() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.attributeToBeNotEmpty(this.chart, "ng-reflect-labels"));
	}
	
	public void ensureChartValuesNotEmpty() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.attributeToBeNotEmpty(this.chart, "ng-reflect-data"));
	}
}
