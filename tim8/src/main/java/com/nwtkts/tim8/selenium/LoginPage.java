package com.nwtkts.tim8.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {

	private WebDriver webDriver;
	
	@FindBy(css = "#username")
	private WebElement username;
	
	@FindBy(css = "#password")
	private WebElement password;
	
	@FindBy(css = "div.row > div.col-lg-9 > button.btn.btn-round")
	private WebElement logInButton;
	
	@FindBy(id = "username-empty")
	private WebElement usernameError;
	
	@FindBy(id = "password-empty")
	private WebElement passwordError;
	
	@FindBy(css = "#toast-container > div:last-of-type")
	private WebElement toastr;

	public LoginPage(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public WebElement getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username.clear();
		this.username.sendKeys(username);
	}

	public WebElement getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}

	public WebElement getLogInButton() {
		return logInButton;
	}
	
	public void ensureIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(username));	
	}

	public WebElement getUsernameError() {
		return usernameError;
	}

	public WebElement getPasswordError() {
		return passwordError;
	}

	public WebElement getToastr() {
		return toastr;
	}

	public void ensureToastrIsDisplayed() {
		(new WebDriverWait(webDriver, 10))
		.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#toast-container > div:last-of-type")));
	}
}
