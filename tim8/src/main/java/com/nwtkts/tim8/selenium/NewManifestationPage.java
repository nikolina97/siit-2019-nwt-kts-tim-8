package com.nwtkts.tim8.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class NewManifestationPage {

	private WebDriver driver;

	@FindBy(css = "button[type=\"submit\"]")
	private WebElement addButton;

	@FindBy(name = "image")
	private WebElement imageInput;

	@FindBy(css = "input[formControlName=\"name\"]")
	private WebElement nameInput;

	@FindBy(css = "input[formControlName=\"description\"]")
	private WebElement descriptionInput;

	@FindBy(css = "mat-select[formControlName=\"location\"]")
	private WebElement locationSelect;

	@FindBy(css = "input[formControlName=\"startDate\"]")
	private WebElement startDateInput;

	@FindBy(css = "input[formControlName=\"endDate\"]")
	private WebElement endDateInput;

	@FindBy(css = "mat-checkbox[formControlName=\"reservationAllowed\"]")
	private WebElement reservationAllowedBox;

	@FindBy(css = "input[formControlName=\"daysBeforeExpires\"]")
	private WebElement daysBeforeExpiresInput;

	@FindBy(css = "mat-select[formControlName=\"category\"]")
	private WebElement categorySelect;

	@FindBy(css = "mat-checkbox[formControlName=\"checked\"]")
	private List<WebElement> checkedBoxes;

	@FindBy(css = "input[formControlName=\"ticketPrice\"]")
	private List<WebElement> ticketPriceInputs;

	@FindBy(css = "input[formControlName=\"numberOfSeats\"]")
	private List<WebElement> numberOfSeatsInputs;

	@FindBy(className = "alert")
	private WebElement errorMessage;

	public NewManifestationPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getAddButton() {
		return addButton;
	}

	public void setImageInput(String path) {
		this.imageInput.sendKeys(path);
	}

	public void setLocationSelect(String value) {
		this.locationSelect.click();
		driver.findElement(By.cssSelector("mat-option[ng-reflect-value=\"" + value + "\"]")).click();
	}

	public WebElement getReservationAllowedBox() {
		return reservationAllowedBox;
	}

	public void setCategorySelect(String value) {
		this.categorySelect.click();
		driver.findElement(By.cssSelector("mat-option[ng-reflect-value=\"" + value + "\"]")).click();
	}

	public List<WebElement> getCheckedBoxes() {
		return checkedBoxes;
	}

	public WebElement getErrorMessage() {
		return errorMessage;
	}

	public void setNameInput(String value) {
		this.nameInput.clear();
		this.nameInput.sendKeys(value);
	}

	public void setDescriptionInput(String value) {
		this.descriptionInput.clear();
		this.descriptionInput.sendKeys(value);
	}

	public void setStartDateInput(String date, String time) {
		this.startDateInput.clear();
		this.startDateInput.sendKeys(date);
		this.startDateInput.sendKeys(Keys.TAB);
		this.startDateInput.sendKeys(time);
	}

	public void setEndDateInput(String date, String time) {
		this.endDateInput.clear();
		this.endDateInput.sendKeys(date);
		this.endDateInput.sendKeys(Keys.TAB);
		this.endDateInput.sendKeys(time);
	}

	public void setDaysBeforeExpiresInput(String value) {
		this.daysBeforeExpiresInput.clear();
		this.daysBeforeExpiresInput.sendKeys(value);
	}

	public void setTicketPriceInputs(String value) {
		for(WebElement input : ticketPriceInputs) {
			input.clear();
			input.sendKeys(value);
		}
	}

	public void setNumberOfSeatsInputs(String value) {
		for(WebElement input : numberOfSeatsInputs) {
			input.clear();
			input.sendKeys(value);
		}
	}

	public void ensureEntireFormIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(nameInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(descriptionInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(startDateInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(endDateInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(reservationAllowedBox));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(daysBeforeExpiresInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(addButton));
	}

	public void ensureErrorMessageIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(errorMessage));
	}

}
