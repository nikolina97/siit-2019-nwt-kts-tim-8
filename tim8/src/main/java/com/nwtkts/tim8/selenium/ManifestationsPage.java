package com.nwtkts.tim8.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ManifestationsPage {

	private WebDriver driver;

	@FindBy(css = "button[title=\"New\"]")
	private WebElement addButton;

	@FindBy(className = "card")
	private List<WebElement> events;

	@FindBy(css = "button[title=\"Edit\"]")
	private List<WebElement> editButtons;

	@FindBy(css = "button[title=\"Cancel\"]")
	private List<WebElement> cancelButtons;

	@FindBy(css = "button[title='Book a ticket'][ng-reflect-router-link = '/manifestations,1']")
	private WebElement bookingButton;

	@FindBy(css = "div.card > a[ng-reflect-router-link = '/manifestations,1'] > img")
	private WebElement firstManifestationImage;

	@FindBy(css = "button[title='Book a ticket'][ng-reflect-router-link = '/manifestations,4']")
	private WebElement bookingButtonResExpManifestation;

	@FindBy(css = "button[title='Book a ticket'][ng-reflect-router-link = '/manifestations,3']")
	private WebElement bookingButtonResForbManifestation;
	
	@FindBy(css = "button#show-filters")
	private WebElement showFiltersButton;
	
	@FindBy(css = "mat-drawer")
	private WebElement drawer;

	@FindBy(css = "mat-select#mat-select-0")
	private WebElement locationsFilterSelect;

	@FindBy(css = "mat-select#mat-select-1")
	private WebElement categoriesFilterSelect;

	@FindBy(css = "button#apply-filters")
	private WebElement applyFiltersButton;
	
	@FindBy(css = "input[name='search']")
	private WebElement searchInput;
	
	@FindBy(css = "button#search-submit")
	private WebElement searchButton;

	public ManifestationsPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getAddButton() {
		return addButton;
	}

	public List<WebElement> getEvents() {
		return events;
	}

	public List<WebElement> getEditButtons() {
		return editButtons;
	}

	public List<WebElement> getCancelButtons() {
		return cancelButtons;
	}

	public void ensureAddButtonIsClickable(int time) {
		(new WebDriverWait(driver, time)).until(ExpectedConditions.elementToBeClickable(addButton));
	}

	public void ensureEventsAreVisible() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElements(events));
	}

	public void ensureEditButtonIsClickable() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElements(editButtons));
	}

	public void ensureCancelButtonIsClickable() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElements(cancelButtons));
	}

	public WebElement getBookingButton() {
		return bookingButton;
	}

	public WebElement getBookingButtonResExpManifestation() {
		return bookingButtonResExpManifestation;
	}

	public WebElement getFirstManifestationImage() {
		return firstManifestationImage;
	}

	public WebElement getBookingButtonResForbManifestation() {
		return bookingButtonResForbManifestation;
	}
	
	public WebElement getShowFiltersButton() {
		return showFiltersButton;
	}
	
	public WebElement getDrawer() {
		return drawer;
	}
	
	public WebElement getLocationsFilterSelect() {
		return locationsFilterSelect;
	}
	
	public WebElement getCategoriesFilterSelect() {
		return categoriesFilterSelect;
	}
	
	public WebElement getApplyFiltersButton() {
		return applyFiltersButton;
	}
	
	public WebElement getSearchInput() {
		return searchInput;
	}
	
	public WebElement getSearchButton() {
		return searchButton;
	}

	public void setBookingButtonResForbManifestation(WebElement bookingButtonResForbManifestation) {
		this.bookingButtonResForbManifestation = bookingButtonResForbManifestation;
	}

	public void ensureBookingButtonIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(bookingButton));
	}

	public void ensureManifestationImageIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(firstManifestationImage));
	}

	public void ensureBookingButtonResExpManifestationisDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(bookingButtonResExpManifestation));
	}

	public void ensureBookingButtonResForbManifestationisDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(bookingButtonResForbManifestation));
	}

	public void ensureDrawerIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.visibilityOf(drawer));
	}

	public void ensureShowFiltersButtonIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(showFiltersButton));
	}

	public void ensureApplyFiltersButtonIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(applyFiltersButton));
	}

	public void ensureSearchButtonIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(searchButton));
	}

	public void ensureNoManifTextIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(By.cssSelector("h4.empty-list")));
	}

	public void ensureLocationsFilterIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(locationsFilterSelect));
	}

	public void ensureCategoriesFilterIsDisplayed() {
		(new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(categoriesFilterSelect));
	}
	
	public void selectLocations(String[] ids) {
		int attempts = 0;
	    while(attempts < 4) {
	        try {
	        	locationsFilterSelect.click();
	            break;
	        } catch(Exception e) {
	        }
	        attempts++;
	    }
		
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("mat-option")));
		for (String id : ids) {
			driver.findElement(By.id(id)).click();
		}
		driver.findElement(By.cssSelector(
				"div.cdk-overlay-container > div.cdk-overlay-backdrop.cdk-overlay-backdrop-showing"))
		.click();
	}
	
	public void selectCategories(String[] ids) {
		int attempts = 0;
	    while(attempts < 4) {
	        try {
	        	categoriesFilterSelect.click();
	            break;
	        } catch(Exception e) {
	        }
	        attempts++;
	    }
		
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(
				By.cssSelector("mat-option")));
		for (String id : ids) {
			driver.findElement(By.id(id)).click();
		}
		driver.findElement(By.cssSelector(
				"div.cdk-overlay-container > div.cdk-overlay-backdrop.cdk-overlay-backdrop-showing"))
		.click();
	}
	
	public void applyFilters() {
		ensureApplyFiltersButtonIsDisplayed();
		applyFiltersButton.click();
		driver.findElement(By.cssSelector("div.mat-drawer-backdrop.ng-star-inserted.mat-drawer-shown")).click();
	}

}
