package com.nwtkts.tim8.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditManifestationPage {

	private WebDriver driver;

	@FindBy(css = "button[type=\"submit\"]")
	private WebElement saveButton;

	@FindBy(css = "input[formControlName=\"name\"]")
	private WebElement nameInput;

	@FindBy(css = "input[formControlName=\"description\"]")
	private WebElement descriptionInput;

	public EditManifestationPage(WebDriver driver) {
		this.driver = driver;
	}

	public WebElement getSaveButton() {
		return saveButton;
	}

	public void setNameInput(String value) {
		this.nameInput.clear();
		this.nameInput.sendKeys(value);
	}

	public void setDescriptionInput(String value) {
		this.descriptionInput.clear();
		this.descriptionInput.sendKeys(value);
	}

	public void ensureEntireFormIsDisplayed() {
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(nameInput));
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(descriptionInput));
	}

}
