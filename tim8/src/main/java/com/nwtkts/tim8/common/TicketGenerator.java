package com.nwtkts.tim8.common;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.nwtkts.tim8.model.Ticket;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TicketGenerator {

	public void generatePdf(Ticket ticket) throws DocumentException, WriterException, IOException {
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream("src/main/resources/ticket_" + ticket.getSeat().getId() + ".pdf"));
		document.open();

		PdfPTable table = new PdfPTable(6);

		List<String> header = new ArrayList<String>();
		header.add("Manifestation");
		header.add("Date");
		header.add("Seat");
		header.add("User");
		header.add("Price");
		header.add("QR code");

		header.stream().forEach(columnTitle -> {
			PdfPCell headerCell = new PdfPCell();
			headerCell.setBorderWidth(1);
			headerCell.setPhrase(new Phrase(columnTitle, FontFactory.getFont(FontFactory.HELVETICA, 11)));
			headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headerCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(headerCell);
		});

		String manifestationName = ticket.getSeat().getSupportedSeatCategory().getManifestation().getName();
		String manifestationDate = new SimpleDateFormat("dd.MM.yyyy HH:mm").format(ticket.getDate() == null ? ticket.getManifestation().getStartDate() : new Date(ticket.getDate().getTime()));
		String seat = ticket.getSeat().getSupportedSeatCategory().getSeatCategory().getName() + " " + ticket.getSeat().getRow() + "-" + ticket.getSeat().getColumn();
		String userName = ticket.getUser().getFirstName() + " " + ticket.getUser().getLastName();
		String ticketPrice = String.format("%.2f$", ticket.getSeat().getSupportedSeatCategory().getTicketPrice());
		String ticketId = ticket.getId().toString();

		List<String> cells = new ArrayList<String>();
		cells.add(manifestationName);
		cells.add(manifestationDate);
		cells.add(seat);
		cells.add(userName);
		cells.add(ticketPrice);

		cells.stream().forEach(content -> {
			PdfPCell cell = new PdfPCell();
			cell.setBorderWidth(1);
			cell.setPhrase(new Phrase(content, FontFactory.getFont(FontFactory.HELVETICA, 11)));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			table.addCell(cell);
		});

		//----------- QR code -----------
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix bitMatrix = qrCodeWriter.encode(ticketId, BarcodeFormat.QR_CODE, 350, 350);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);

		Image img = Image.getInstance(byteArrayOutputStream.toByteArray());
		img.scalePercent(19);

		PdfPCell qrCodeCell = new PdfPCell(img);
		qrCodeCell.setBorderWidth(1);
		qrCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qrCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qrCodeCell.setPadding(2);
		table.addCell(qrCodeCell);
		//-------------------------------

		document.add(table);
		document.close();
	}

}
