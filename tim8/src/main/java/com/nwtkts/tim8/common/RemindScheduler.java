package com.nwtkts.tim8.common;

import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.ManifestationState;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.repository.ManifestationRepository;
import com.nwtkts.tim8.repository.TicketRepository;
import com.nwtkts.tim8.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RemindScheduler {
	
	@Autowired
	private TicketRepository ticketRepository;
	
	@Autowired
	private ManifestationRepository manifestationRepository;
	
	@Autowired
	private EmailService emailService;

	@Scheduled(fixedDelay = 20 * 60 * 1000)
	public void remindUsers() {
		List<Ticket> tickets = ticketRepository.findAll().stream().filter(t -> (new Date()).before(t.getDate() == null ? t.getManifestation().getStartDate() : t.getDate()) && t.getActive()).collect(Collectors.toList());
		
		//tickets.stream().filter(t -> !t.getPaid()).forEach(t -> emailService.sendEmailWithReminderAboutReservation(t));
		//tickets.stream().filter(Ticket::getPaid).forEach(t -> emailService.sendEmailWithReminderAboutEvent(t));
	}
	
	@Scheduled(fixedDelay = 2* 60 * 1000)
	public void checkExpirationDates() {
		List<Ticket> tickets = ticketRepository.findAll();
		
		tickets.stream().filter(t -> !t.getPaid()).forEach(t -> {
			Date expirationDate = new Date(new Timestamp((t.getDate() == null ? t.getManifestation().getStartDate().getTime() : t.getDate().getTime()) - t.getSeat().getSupportedSeatCategory().getManifestation().getDaysBeforeExpires() * 24 * 60 * 60 * 1000).getTime());

			if(expirationDate.before(new Date())) {
				t.setActive(false);
				ticketRepository.save(t);
			}
		});
	}
	
	@Scheduled(fixedDelay = 2 * 60 * 1000)
	public void changeManifestationState() {
		List<Manifestation> manifestations = manifestationRepository.findAll();
		
		manifestations.stream().filter(m -> m.getEndDate().before(new Date())).forEach(m -> {
			m.setState(ManifestationState.PAST);
			manifestationRepository.save(m);
		});
	}
	
}
