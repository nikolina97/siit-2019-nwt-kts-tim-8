package com.nwtkts.tim8.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import com.nwtkts.tim8.model.Seat;
import com.nwtkts.tim8.model.SupportedSeatCategory;

public interface SeatRepository extends JpaRepository<Seat, Integer> {
	
	Collection<Seat> findAllBySupportedSeatCategory(SupportedSeatCategory supportedSeatCategory);
	
	Optional<Seat> findById(Integer id);
	
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@Query(value = "select s from Seat s where s.id in :ids")
	List<Seat> findAllById(List<Integer> ids);
}
