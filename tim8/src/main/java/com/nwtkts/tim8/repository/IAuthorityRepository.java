package com.nwtkts.tim8.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nwtkts.tim8.model.Authority;

public interface IAuthorityRepository extends JpaRepository<Authority, Integer>{
}
