package com.nwtkts.tim8.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nwtkts.tim8.model.Location;

public interface LocationRepository extends JpaRepository<Location, Integer> {
	
	Location findOneByAddressAndLatitudeAndLongitude(String address, Double latitude, Double longitude);

}
