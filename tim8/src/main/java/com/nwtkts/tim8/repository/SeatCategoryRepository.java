package com.nwtkts.tim8.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nwtkts.tim8.model.SeatCategory;

public interface SeatCategoryRepository extends JpaRepository<SeatCategory, Integer> {

}
