package com.nwtkts.tim8.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.SupportedSeatCategory;

public interface SupportedSeatCategoryRepository extends JpaRepository<SupportedSeatCategory, Integer> {
	
	Collection<SupportedSeatCategory> findAllByManifestation(Manifestation manifestaiton);
}
