package com.nwtkts.tim8.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nwtkts.tim8.model.VerificationToken;

public interface IVerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
	public VerificationToken save(VerificationToken token);
}
