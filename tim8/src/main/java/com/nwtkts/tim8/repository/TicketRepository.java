package com.nwtkts.tim8.repository;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nwtkts.tim8.model.Manifestation;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.model.Seat;
import com.nwtkts.tim8.model.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {
	
	Optional<Ticket> findOneBySeatAndDateAndActive(Seat seat, Timestamp ts, Boolean active);
	
	@Query("SELECT t FROM Ticket t WHERE t.seat = :seat AND (t.date BETWEEN :startDate AND :endDate)")
	Collection<Ticket> findBetweenTwoDates(Seat seat, Timestamp startDate, Timestamp endDate);
	
	@Query("SELECT t from Ticket t WHERE t.seat = :seat AND t.date is null")
	Optional<Ticket> findFestivalTicket(Seat seat);
	
	Collection<Ticket> findAllByUserAndPaidAndActive(RegisteredUser user, Boolean paid, Boolean active);
	
	/*@Query("SELECT * from Ticket t WHERE t.date BETWEEN :startDate and :endDate AND t.manifestation.id = :id")
	Collection<Ticket> findAllForManifestationBetweenTwoDates(Manifestation id, Timestamp startDate, Timestamp endDate);*/
	
	@Query("SELECT t from Ticket t WHERE t.manifestation = :manif AND t.paid = true AND (t.buyingDate BETWEEN :startDate and :endDate)")
	Collection<Ticket> findAllByManifestationBetweenTwoDates(Manifestation manif, Timestamp startDate, Timestamp endDate);
	
	@Query(value = "select * from ticket t where t.manifestation_id in(select m.id from manifestation m where m.location_id = ?1) and (t.buying_date between ?2 and ?3) and t.paid = true", nativeQuery = true)
	Collection<Ticket> findByLocation(Integer locationID, Timestamp startDate, Timestamp endDate);
	
	Collection<Ticket> findAllByManifestation(Manifestation manifestation);
}
