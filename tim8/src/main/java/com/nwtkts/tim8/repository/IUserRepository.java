package com.nwtkts.tim8.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nwtkts.tim8.model.User;

public interface IUserRepository extends JpaRepository<User, Integer> {
	List<User> findAll();
	User findByUsername(String username);
	User findByEmail(String email);
	User findByUsernameAndPassword(String username, String password);
	
	@Query(value = "select * from user u where u.id =(select t.user from verification_tokens t where t.token = ?1)", nativeQuery = true)
	User findByToken(String token);
	
}
