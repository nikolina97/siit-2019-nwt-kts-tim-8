package com.nwtkts.tim8.repository;

import java.util.List;
import java.util.Optional;

import com.nwtkts.tim8.model.ManifestationState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.nwtkts.tim8.model.Manifestation;

public interface ManifestationRepository extends JpaRepository<Manifestation, Integer> {

	List<Manifestation> findByLocation_Id(Integer id);
	List<Manifestation> findByNameContainingIgnoreCase(String name);
	Optional<Manifestation> findOneById(Integer id);
	Page<Manifestation> findAllByStateNotIn(List<ManifestationState> states, Pageable pageable);
	Long countByStateNotIn(List<ManifestationState> states);
	
}
