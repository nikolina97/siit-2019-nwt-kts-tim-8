package com.nwtkts.tim8.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nwtkts.tim8.dto.LocationDTO;
import com.nwtkts.tim8.dto.MainLocationInfoDTO;
import com.nwtkts.tim8.service.InvalidDataException;
import com.nwtkts.tim8.service.LocationService;

@RestController
@RequestMapping("/api")
public class LocationController {
	
	@Autowired
	private LocationService locationService;

	@GetMapping("/locations")
	public ResponseEntity<List<MainLocationInfoDTO>> getAllPageable(Pageable pageable) {
		return new ResponseEntity<>(locationService.getAllPageable(pageable), HttpStatus.OK);
	}

	@GetMapping("/locations/all")
	public ResponseEntity<List<MainLocationInfoDTO>> getAll() {
		return new ResponseEntity<>(locationService.getAll(), HttpStatus.OK);
	}

	@GetMapping("/locations/{id}")
	public ResponseEntity<?> findById(@PathVariable Integer id) {
		try {
			return new ResponseEntity<>(locationService.findById(id), HttpStatus.OK);
		} catch (InvalidDataException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/locations")
	public ResponseEntity<?> save(@RequestBody LocationDTO location) {
		try {
			return new ResponseEntity<>(locationService.save(location), HttpStatus.OK);
		} catch (InvalidDataException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping("/locations/{id}")
	public ResponseEntity<?> update(@PathVariable Integer id, @RequestBody MainLocationInfoDTO locationDTO) {
		try {
			return new ResponseEntity<>(locationService.update(id, locationDTO), HttpStatus.OK);
		} catch (InvalidDataException e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), e.getMessage().equals("Location not found") ? HttpStatus.NOT_FOUND : HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/locations/{id}")
	public ResponseEntity<String> delete(@PathVariable Integer id) {
		try {
			locationService.delete(id);

			return new ResponseEntity<>("Location deleted", HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<>(e.getMessage(), e.getMessage().equals("Location not found") ? HttpStatus.NOT_FOUND : HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/locations/{id}/manifestations")
	public ResponseEntity<?> getManifestations(@PathVariable Integer id, Pageable pageable) {
		try {
			return new ResponseEntity<>(locationService.getManifestations(id, pageable), HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/locations/{id}/manifCount")
	public ResponseEntity<?> getManifestationsCount(@PathVariable Integer id) {
		try {
			return new ResponseEntity<>(locationService.getManifestationsCount(id), HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/locations/{id}/categories")
	public ResponseEntity<?> getCategories(@PathVariable Integer id) {
		try {
			return new ResponseEntity<>(locationService.getCategories(id), HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/reportDailyLocation/{locationId}/{startDate}/{endDate}")
	public ResponseEntity<?> dailyReport(@PathVariable("locationId") Integer manifestationId, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate){
		try {
			Map<String, Long> report = locationService.getDaily(manifestationId, startDate, endDate);
			return new ResponseEntity<Map<String, Long>>(report, HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (ParseException e) {
			return new ResponseEntity<String>("Wrong date format", HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/reportWeeklyLocation/{locationId}/{startDate}/{endDate}")
	public ResponseEntity<?> weeklyReport(@PathVariable("locationId") Integer manifestationId, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate){
		try {
			Map<String, Long> report = locationService.getWeekly(manifestationId, startDate, endDate);
			return new ResponseEntity<Map<String, Long>>(report, HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (ParseException e) {
			return new ResponseEntity<String>("Wrong date format", HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/reportMonthlyLocation/{locationId}/{startDate}/{endDate}")
	public ResponseEntity<?> monthlyReport(@PathVariable("locationId") Integer manifestationId, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate){
		try {
			Map<String, Long> report = locationService.getMonthly(manifestationId, startDate, endDate);
			return new ResponseEntity<Map<String, Long>>(report, HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (ParseException e) {
			return new ResponseEntity<String>("Wrong date format", HttpStatus.NOT_ACCEPTABLE);
		}
	}
}
