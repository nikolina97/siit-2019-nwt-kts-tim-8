package com.nwtkts.tim8.controller;

import com.nwtkts.tim8.dto.UserDTO;
import com.nwtkts.tim8.dto.UserEditProfileDTO;
import com.nwtkts.tim8.mapper.UserMapper;
import com.nwtkts.tim8.model.Ticket;
import com.nwtkts.tim8.model.User;
import com.nwtkts.tim8.service.InvalidDataException;
import com.nwtkts.tim8.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	private UserService userService;

	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@GetMapping("/user/tickets")
	public ResponseEntity<List<Ticket>> userTickets(Pageable pageable) {
		return new ResponseEntity<List<Ticket>>(userService.userTickets(pageable), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@PostMapping("/user/cancel/{id}")
	public ResponseEntity<String> cancel(@PathVariable Integer id) {
		try {
			userService.cancel(id);
			
			return new ResponseEntity<String>("Ticket reservation successfully canceled", HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), e.getMessage().equals("This ticket does not exists or ticket is not yours") ? HttpStatus.NOT_FOUND : HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_REGISTEREDUSER')")
	@GetMapping(value = "/getLoggedUser", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findUserByUsername(){
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserDTO userDTO = UserMapper.toDTO(user);
		return new ResponseEntity<UserDTO>(userDTO, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_REGISTEREDUSER')")
	@PutMapping(value = "/editProfile", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> editProfile(@RequestBody UserEditProfileDTO userDTO){
		try {
			UserDTO editedUser = userService.editUser(userDTO);
			return new ResponseEntity<UserDTO>(editedUser, HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_REGISTEREDUSER')")
	@PostMapping(value = "/uploadImage")
	public ResponseEntity<?> uploadImage(@RequestParam("file") MultipartFile file){
		System.out.println(file.getName());
		System.out.println(file.getOriginalFilename());
		try {
			return new ResponseEntity<UserDTO>(userService.uploadImage(file), HttpStatus.OK);
		} catch (InvalidDataException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		}
		
	}
}
