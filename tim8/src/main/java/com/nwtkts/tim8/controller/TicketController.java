package com.nwtkts.tim8.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import com.nwtkts.tim8.dto.SeatDateDTO;
import com.nwtkts.tim8.dto.TicketDTO;
import com.nwtkts.tim8.model.RegisteredUser;
import com.nwtkts.tim8.service.InvalidDataException;
import com.nwtkts.tim8.service.TicketService;

@RestController
@RequestMapping("/api/ticket")
public class TicketController {
	
	@Autowired
	private TicketService ticketService;
	
	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@GetMapping("/getAllBoughtTicketsByUser")
	public ResponseEntity<?> getAllBoughtTicketsByUser() {
		try {
			RegisteredUser ru = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return new ResponseEntity<List<TicketDTO>>(ticketService.getAllBoughtTicketsByUser(ru), HttpStatus.OK);
		}
		catch(InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@GetMapping("/getAllReservedTicketsByUser")
	public ResponseEntity<?> getAllReservedTicketsByUser() {
		try {
			RegisteredUser ru = (RegisteredUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			return new ResponseEntity<List<TicketDTO>>(ticketService.getAllReservedTicketsByUser(ru), HttpStatus.OK);
		}
		catch(InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@PostMapping("/buyTickets")
	public ResponseEntity<?> buyTickets(@RequestBody SeatDateDTO seatsDate) {
		try {
			Date date_ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(seatsDate.getDate());
			return new ResponseEntity<>(ticketService.buyTickets(seatsDate.getSeatIds(), date_), HttpStatus.OK);
		} catch (ParseException e) {
			try {
				return new ResponseEntity<>(ticketService.buyTickets(seatsDate.getSeatIds(), null), HttpStatus.OK);
			} catch (InvalidDataException e1) {
				e1.printStackTrace();
				return new ResponseEntity<String>(e1.getMessage(), HttpStatus.CONFLICT);
			}
			
		} catch (InvalidDataException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@PutMapping("/payTicket/{id}")
	public ResponseEntity<?> payTicket(@PathVariable Integer id) {
		try {
			return new ResponseEntity<>(ticketService.payTicket(id), HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@GetMapping("/markAsPaid/{ids}")
	public ResponseEntity<?> markAsPaid(@PathVariable String ids) {
		try {
			ticketService.markAsPaid(ids);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@GetMapping("/deleteTickets/{ids}")
	public ResponseEntity<?> deleteTickets(@PathVariable String ids) {
		try {
			ticketService.deleteTickets(ids);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
