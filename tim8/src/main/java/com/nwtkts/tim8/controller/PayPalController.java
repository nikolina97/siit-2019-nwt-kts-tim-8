package com.nwtkts.tim8.controller;

import com.nwtkts.tim8.dto.CompletePaymentDTO;
import com.nwtkts.tim8.dto.CompletePaymentParamsDTO;
import com.nwtkts.tim8.dto.CreatePaymentDTO;
import com.nwtkts.tim8.service.PayPalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/payment")
public class PayPalController {

	@Autowired
	private PayPalService payPalService;

	@PostMapping("/complete")
	public ResponseEntity<CompletePaymentDTO> completePayment(@RequestBody CompletePaymentParamsDTO dto){
		return new ResponseEntity<>(payPalService.completePayment(dto.getPaymentId(), dto.getPayerId()), HttpStatus.OK);
	}

}
