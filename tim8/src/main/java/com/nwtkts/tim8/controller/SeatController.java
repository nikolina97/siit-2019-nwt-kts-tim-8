package com.nwtkts.tim8.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import com.nwtkts.tim8.dto.SeatDateDTO;
import com.nwtkts.tim8.dto.SeatsBySupportedSeatCategoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.nwtkts.tim8.model.Ticket;

import com.nwtkts.tim8.service.InvalidDataException;
import com.nwtkts.tim8.service.SeatService;

@RestController
@RequestMapping("/api/seat")
public class SeatController {
	
	@Autowired
	private SeatService seatService;
	
	@PostMapping("/getAllSeats/{manifestationId}")
	public ResponseEntity<?> getAllSeatsByManifestation(@PathVariable("manifestationId") Integer manifestationId, @RequestBody String date) {
		try {
			Date date_ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
			return new ResponseEntity<Collection<SeatsBySupportedSeatCategoryDTO>>(seatService.getAllSeatsByManifestation(manifestationId, date_), HttpStatus.OK);

		} catch(ParseException e) {
			try {
				return new ResponseEntity<Collection<SeatsBySupportedSeatCategoryDTO>>(seatService.getAllSeatsByManifestation(manifestationId, null), HttpStatus.OK);
			} catch (InvalidDataException e1) {
				return new ResponseEntity<String>(e1.getMessage(), HttpStatus.BAD_REQUEST);
			}
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_REGISTEREDUSER')")
	@PostMapping("/reserveSeats")
	public ResponseEntity<?> reserveSeats(@RequestBody SeatDateDTO seatsDate) {
		try {
			Date date_ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(seatsDate.getDate());
			return new ResponseEntity<Collection<Ticket>>(seatService.reserveSeats(seatsDate.getSeatIds(), date_), HttpStatus.OK);
		} catch (ParseException e) {
			try {
				return new ResponseEntity<Collection<Ticket>>(seatService.reserveSeats(seatsDate.getSeatIds(), null), HttpStatus.OK);
			} catch (InvalidDataException e1) {
				e1.printStackTrace();
				return new ResponseEntity<String>(e1.getMessage(), HttpStatus.CONFLICT);
			}
			
		} catch (InvalidDataException e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
