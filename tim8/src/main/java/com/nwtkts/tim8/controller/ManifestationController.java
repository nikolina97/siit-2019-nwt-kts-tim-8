package com.nwtkts.tim8.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nwtkts.tim8.dto.MainManifestationInfoDTO;
import com.nwtkts.tim8.dto.ManifestationDTO;
import com.nwtkts.tim8.dto.ManifestationInfoDTO;
import com.nwtkts.tim8.dto.SearchManifestationDTO;
import com.nwtkts.tim8.dto.UpdateManifestationDTO;
import com.nwtkts.tim8.service.InvalidDataException;
import com.nwtkts.tim8.service.ManifestationService;

@RestController
@RequestMapping("/api")
public class ManifestationController {

	@Autowired
	private ManifestationService manifestationService;

	@GetMapping("/manifestations/{id}")
	public ResponseEntity<?> findById(@PathVariable Integer id) {
		try {
			return new ResponseEntity<>(manifestationService.findById(id), HttpStatus.OK);
		} catch (InvalidDataException e) {
			//e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PostMapping("/manifestations")
	public ResponseEntity<?> save(@RequestBody ManifestationDTO dto) {
		try {
			return new ResponseEntity<>(manifestationService.save(dto), HttpStatus.OK);
		} catch (InvalidDataException e) {
			//e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), e.getMessage().contains("not found") ? HttpStatus.NOT_FOUND : HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping("/manifestations/{id}")
	public ResponseEntity<?> update(@PathVariable Integer id, @RequestBody UpdateManifestationDTO dto) {
		try {
			return new ResponseEntity<>(manifestationService.update(id, dto), HttpStatus.OK);
		} catch (InvalidDataException e) {
			//e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), e.getMessage().contains("not found") ? HttpStatus.NOT_FOUND : HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@PutMapping("/manifestations/cancel/{id}")
	public ResponseEntity<?> cancel(@PathVariable Integer id) {
		try {
			return new ResponseEntity<>(manifestationService.cancel(id), HttpStatus.OK);
		} catch (InvalidDataException e) {
			//e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), e.getMessage().equals("Manifestation not found") ? HttpStatus.NOT_FOUND : HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/manifestations/search")
	public ResponseEntity<List<MainManifestationInfoDTO>> search(@RequestBody SearchManifestationDTO input, Pageable pageable) {
		return new ResponseEntity<>(manifestationService.find(input, pageable), HttpStatus.OK);
	}

	@GetMapping("/manifestations")
	public ResponseEntity<List<MainManifestationInfoDTO>> getAll(Pageable pageable) {
		return new ResponseEntity<>(manifestationService.findAll(pageable), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/manifestations/infoForUpdate/{id}")
	public ResponseEntity<?> getManifestationInfoForUpdate(@PathVariable Integer id) {
		try {
			return new ResponseEntity<>(manifestationService.getInfoForUpdate(id), HttpStatus.OK);
		} catch (InvalidDataException e) {
			//e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/manifestations/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<>(manifestationService.count(), HttpStatus.OK);
	}
	
	@GetMapping("manifestations/getManifestationInfo/{id}")
	public ResponseEntity<?> getManifestationInfo(@PathVariable("id") Integer manifestationId){
		try {
			return new ResponseEntity<ManifestationInfoDTO>(manifestationService.getManifestationInfo(manifestationId), HttpStatus.OK);
			
		}catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/reportDailyManifestation/{manifestationId}/{startDate}/{endDate}")
	public ResponseEntity<?> dailyReport(@PathVariable("manifestationId") Integer manifestationId, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate){
		try {
			Map<String, Long> report = manifestationService.getDaily(manifestationId, startDate, endDate);
			return new ResponseEntity<Map<String, Long>>(report, HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (ParseException e) {
			return new ResponseEntity<String>("Wrong date format", HttpStatus.NOT_ACCEPTABLE);
		}	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/reportWeeklyManifestation/{manifestationId}/{startDate}/{endDate}")
	public ResponseEntity<?> weeklyReport(@PathVariable("manifestationId") Integer manifestationId, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate){
		try {
			Map<String, Long> report = manifestationService.getWeekly(manifestationId, startDate, endDate);
			return new ResponseEntity<Map<String, Long>>(report, HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (ParseException e) {
			return new ResponseEntity<String>("Wrong date format", HttpStatus.NOT_ACCEPTABLE);
		}	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/reportMonthlyManifestation/{manifestationId}/{startDate}/{endDate}")
	public ResponseEntity<?> monthlyReport(@PathVariable("manifestationId") Integer manifestationId, @PathVariable("startDate") String startDate, @PathVariable("endDate") String endDate){
		try {
			Map<String, Long> report = manifestationService.getMonthly(manifestationId, startDate, endDate);
			return new ResponseEntity<Map<String, Long>>(report, HttpStatus.OK);
		} catch (InvalidDataException e) {
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (ParseException e) {
			return new ResponseEntity<String>("Wrong date format", HttpStatus.NOT_ACCEPTABLE);
		}
	}
}
