	package com.nwtkts.tim8.model;

public enum ManifestationState {
	OPEN, CANCELED, SOLD_OUT, PAST
}
