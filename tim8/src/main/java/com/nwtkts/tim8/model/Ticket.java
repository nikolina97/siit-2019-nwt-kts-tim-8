package com.nwtkts.tim8.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ticket")
public class Ticket {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "paid")
	private Boolean paid;
	
	@Column(name = "date")
	private Timestamp date;
	
	@Column(name = "buying_date")
	private Timestamp buyingDate;
	
	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "seat_id")
	private Seat seat;
	
	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private RegisteredUser user;
	
	@Column(name="is_active")
	private Boolean active;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="manifestation_id")
	private Manifestation manifestation;

	public Ticket() {
		super();
	}

	public Ticket(Boolean paid, Timestamp date, Timestamp buyingDate, Seat seat, RegisteredUser user,
			Manifestation manifestation) {
		super();
		this.paid = paid;
		this.date = date;
		this.buyingDate = buyingDate;
		this.seat = seat;
		this.user = user;
		this.active = true;
		this.manifestation = manifestation;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getPaid() {
		return paid;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public Seat getSeat() {
		return seat;
	}

	public void setSeat(Seat seat) {
		this.seat = seat;
	}

	public RegisteredUser getUser() {
		return user;
	}

	public void setUser(RegisteredUser user) {
		this.user = user;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Manifestation getManifestation() {
		return manifestation;
	}

	public void setManifestation(Manifestation manifestation) {
		this.manifestation = manifestation;
	}
	
	public Timestamp getBuyingDate() {
		return buyingDate;
	}

	public void setBuyingDate(Timestamp buyingDate) {
		this.buyingDate = buyingDate;
	}
}
