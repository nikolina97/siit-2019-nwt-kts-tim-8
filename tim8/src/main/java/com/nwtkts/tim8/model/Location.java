package com.nwtkts.tim8.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "location")
@Where(clause = "is_active = 1")
public class Location {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "latitude")
	private Double latitude;
	
	@Column(name = "longitude")
	private Double longitude;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id")
	private Set<SeatCategory> categories;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "location", fetch = FetchType.LAZY)
	@JsonIgnoreProperties("manifestation")
	@JsonIgnore
	private List<Manifestation> manifestations;
	
	@Column(name="is_active")
	private Boolean active;

	public Location() {
		super();
		active = true;
	}

	public Location(String address, String name, Double latitude, Double longitude,
			Set<SeatCategory> categories, List<Manifestation> manifestations, Boolean active) {
		super();
		this.address = address;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.categories = categories;
		this.manifestations = manifestations;
		this.active = active;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Set<SeatCategory> getCategories() {
		return categories;
	}

	public void setCategories(Set<SeatCategory> categories) {
		this.categories = categories;
	}

	public List<Manifestation> getManifestations() {
		return manifestations;
	}

	public void setManifestations(List<Manifestation> manifestations) {
		this.manifestations = manifestations;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
}
