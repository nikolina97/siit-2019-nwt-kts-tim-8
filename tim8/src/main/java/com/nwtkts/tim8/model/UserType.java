package com.nwtkts.tim8.model;

public enum UserType {
	ROLE_REGISTEREDUSER, ROLE_ADMIN
}
