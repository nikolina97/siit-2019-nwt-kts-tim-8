package com.nwtkts.tim8.model;

public enum ManifestationCategory {
	CULTURE, SPORT, ENTERTAINMENT
}
