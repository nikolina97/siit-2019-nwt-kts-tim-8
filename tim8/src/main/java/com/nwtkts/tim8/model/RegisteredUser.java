package com.nwtkts.tim8.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class RegisteredUser extends User {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1721610948689451571L;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.LAZY)
	@JsonIgnoreProperties("user")
	private List<Ticket> tickets;

	@Column(name = "verified")
	private boolean verified;
	
	public RegisteredUser() {
		super();
	}
	
	public RegisteredUser(List<Ticket> tickets, boolean verified) {
		super();
		this.tickets = tickets;
		this.verified = verified;
	}

	public RegisteredUser(String username, String password, String firstName, String lastName, String email,
			List<Authority> authorities) {
		super(username, password, firstName, lastName, email, authorities);
		this.tickets = new ArrayList<Ticket>();
		this.verified = false;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public List<Ticket> getTickets() {
		return tickets;
	}

	public void setTickets(List<Ticket> tickets) {
		this.tickets = tickets;
	}

}
