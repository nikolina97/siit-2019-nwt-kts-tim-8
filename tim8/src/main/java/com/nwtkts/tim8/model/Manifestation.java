package com.nwtkts.tim8.model;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "manifestation")
@Where(clause = "is_active = 1")
public class Manifestation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "start_date")
	private Timestamp startDate;
	
	@Column(name = "end_date")
	private Timestamp endDate;
	
	@Column(name = "days_before_reservation_expires")
	private Integer daysBeforeExpires;
	
	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id")
	private Location location;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "state")
	private ManifestationState state;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "category")
	private ManifestationCategory category;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "manifestation", fetch = FetchType.LAZY)
	@JsonIgnoreProperties("manifestation")
	private Set<SupportedSeatCategory> supportedSeatCategories;

	@Column(name = "image")
	private String image;

	@Column(name="is_active")
	private Boolean active;

	public Manifestation() {
		super();
		this.active = true;
	}

	public Manifestation(String name, String description, Timestamp startDate, Timestamp endDate,
			Integer daysBeforeExpires, Location location, ManifestationState state, ManifestationCategory category,
			Set<SupportedSeatCategory> supportedSeatCategories, String image) {
		super();
		this.name = name;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.daysBeforeExpires = daysBeforeExpires;
		this.location = location;
		this.state = state;
		this.category = category;
		this.supportedSeatCategories = supportedSeatCategories;
		this.image = image;
		this.active = true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Integer getDaysBeforeExpires() {
		return daysBeforeExpires;
	}

	public void setDaysBeforeExpires(Integer daysBeforeExpires) {
		this.daysBeforeExpires = daysBeforeExpires;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public ManifestationState getState() {
		return state;
	}

	public void setState(ManifestationState state) {
		this.state = state;
	}

	public ManifestationCategory getCategory() {
		return category;
	}

	public void setCategory(ManifestationCategory category) {
		this.category = category;
	}

	public Set<SupportedSeatCategory> getSupportedSeatCategories() {
		return supportedSeatCategories;
	}

	public void setSupportedSeatCategories(Set<SupportedSeatCategory> supportedSeatCategories) {
		this.supportedSeatCategories = supportedSeatCategories;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
}
