package com.nwtkts.tim8.model;

import javax.persistence.Entity;

@Entity
public class Admin extends User {

	public Admin() {
		super();
	}
	
	public Admin(User user) {
		super(user.getUsername(), user.getPassword(), user.getFirstName(), user.getLastName(), user.getEmail(), user.getAuthorities());
	}

}
