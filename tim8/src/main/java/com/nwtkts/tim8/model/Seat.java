package com.nwtkts.tim8.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "seat")
public class Seat {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "seat_row")
	private Integer row;
	
	@Column(name = "seat_column")
	private Integer column;
	
	@OneToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "supported_seat_category_id")
	private SupportedSeatCategory supportedSeatCategory;

	public Seat() {
		super();
	}

	public Seat(Integer row, Integer column, SupportedSeatCategory supportedSeatCategory) {
		super();
		this.row = row;
		this.column = column;
		this.supportedSeatCategory = supportedSeatCategory;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getColumn() {
		return column;
	}

	public void setColumn(Integer column) {
		this.column = column;
	}

	public SupportedSeatCategory getSupportedSeatCategory() {
		return supportedSeatCategory;
	}

	public void setSupportedSeatCategory(SupportedSeatCategory supportedSeatCategory) {
		this.supportedSeatCategory = supportedSeatCategory;
	}
	
}
