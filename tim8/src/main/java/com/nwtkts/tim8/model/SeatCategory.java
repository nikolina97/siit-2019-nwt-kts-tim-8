package com.nwtkts.tim8.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seat_category")
public class SeatCategory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "enumerable")
	private Boolean enumerable;
	
	@Column(name = "rows_number")
	private Integer rows;
	
	@Column(name = "columns_number")
	private Integer columns;
	
	@Column(name = "x_coordinate")
	private Double xCoordinate;
	
	@Column(name = "y_coordinate")
	private Double yCoordinate;

	@Column(name = "width")
	private Double width;
	
	@Column(name = "height")
	private Double height;
	
	@Column(name = "distance")
	private Double distance;
	
	@Column(name = "stage")
	private Boolean stage;
	
	public SeatCategory() {
		super();
	}

	public SeatCategory(Integer id, String name, Boolean enumerable, Integer rows, Integer columns, Double xCoordinate,
			Double yCoordinate, Double width, Double height, Double distance, boolean stage) {
		super();
		this.id = id;
		this.name = name;
		this.enumerable = enumerable;
		this.rows = rows;
		this.columns = columns;
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.width = width;
		this.height = height;
		this.distance = distance;
		this.stage = stage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnumerable() {
		return enumerable;
	}

	public void setEnumerable(Boolean enumerable) {
		this.enumerable = enumerable;
	}

	public Integer getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public Integer getColumns() {
		return columns;
	}

	public void setColumns(Integer columns) {
		this.columns = columns;
	}

	public Double getxCoordinate() {
		return xCoordinate;
	}

	public void setxCoordinate(Double xCoordinate) {
		this.xCoordinate = xCoordinate;
	}

	public Double getyCoordinate() {
		return yCoordinate;
	}

	public void setyCoordinate(Double yCoordinate) {
		this.yCoordinate = yCoordinate;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Boolean getStage() {
		return stage;
	}

	public void setStage(Boolean stage) {
		this.stage = stage;
	}
	
	
}
